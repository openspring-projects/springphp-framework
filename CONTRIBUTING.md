# How to contribute

SpringPHP loves to welcome your contributions. There are several ways to help out:

* Create an [issue](https://gitlab.com/openspring-projects/springphp-framework/issues) on Gitlab, if you have found a bug
* Write test cases for open bug issues
* Write patches for open bug/feature issues, preferably with test cases included
* Contribute to the [documentation](https://gitlab.com/openspring-projects/springphp-framework-doc)

There are a few guidelines that we need contributors to follow so that we have a
chance of keeping on top of things.

## Getting Started

* Make sure you have a [Gitlab account](https://github.com/signup/free).
* Submit an [issue](https://gitlab.com/openspring-projects/springphp-framework/issues), assuming one does not already exist.
  * Clearly describe the issue including steps to reproduce when it is a bug.
  * Make sure you fill in the earliest version that you know has the issue.
* Fork the repository on Gitlab.

## Making Changes

* Create a topic branch from where you want to base your work.
  * This is usually the master branch.
  * Only target release branches if you are certain your fix must be on that
    branch.
  * To quickly create a topic branch based on master; `git branch
    master/my_contribution master` then checkout the new branch with `git
    checkout master/my_contribution`. Better avoid working directly on the
    `master` branch, to avoid conflicts if you pull in updates from origin.
* Make commits of logical units.
* Check for unnecessary whitespace with `git diff --check` before committing.
* Use descriptive commit messages and reference the #issue number.

## Which branch to base the work

* Bugfix branches will be based on master.
* New features that are backwards compatible will be based on the appropriate 'next' branch. For example if you want to contribute to the next 3.x branch, you should base your changes on `3.next`.
* New features or other non backwards compatible changes will go in the next major release branch. Development on 4.0 has not started yet, so breaking changes are unlikely to be merged in.

## Submitting Changes

* Push your changes to a topic branch in your fork of the repository.
* Submit a pull request to the repository in the SpringPHP organization, with the
  correct target branch.

## Test cases and codesniffer

SpringPHP tests requires [PHPUnit](https://phpunit.de/manual/current/en/installation.html).
To install PHPUnit use composer:

    php composer.phar require "phpunit/phpunit:*"

To run the test cases locally use the following command:

    vendor/bin/phpunit

You can copy file `phpunit.xml.dist` to `phpunit.xml` and modify the database
driver settings as required to run tests for particular database.

## Reporting a Security Issue

If you've found a security related issue in SpringPHP, please don't open an issue in github. Instead contact us at openspring.community@gmail.com.