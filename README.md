---
title: PHP Framework
keywords: php, php7, api, web service, java
tags: [php, php7, api, web service, java]
summary: SpringPHP Faster and Stronger
---

[![pipeline status](https://gitlab.com/openspring-projects/springphp-framework/badges/develop/pipeline.svg)](https://gitlab.com/openspring-projects/springphp-framework/commits/develop)
[![coverage report](https://gitlab.com/openspring-projects/springphp-framework/badges/develop/coverage.svg)](https://gitlab.com/openspring-projects/springphp-framework/commits/develop)

## SpringPHP
SpringPHP makes building web api simpler, faster, while requiring less code. A modern PHP 7 Framework offering a flexible database access layer and a powerful scaffolding system that makes building both small and complex systems simpler, easier and, of course, tastier. Build fast, grow solid with SpringPHP.

## Installing SpringPHP via Composer
You can install SpringPHP into your project using Composer.

If you're starting a new project, we recommend using the app skeleton as a starting point.

First of all, in order to be able to:
- Create new SpringPHP projects
- Generate database access layer
- Generate request mapping
- Generate API documentation
you will need to install the SpringPHP-Initializer to into your system.
To do so, please go to [springphp-devtools](https://gitlab.com/openspring-projects/springphp-framework-devtools) project and follow the very simple instructions described on README.md file.

For existing applications you can run the following command:

```batch
$ composer require openspring/springphp-framework:^1.0.0
```

Or just the following command, to install the last version:
```batch
$ composer require openspring/springphp-framework
```

## Contributing
There are currently a number of outstanding issues that need to be addressed. Please, read [How to contribute](CONTRIBUTING.md)

## Quickstart boilerplate project
The community has developped a quickstart boilerplate project that offers a lot of default functionnalities and capabilities to help developpers get started as fast as possible. The project does exist here [springphp-sample](https://gitlab.com/openspring-projects/springphp-framework-sample). Follow its very simple instructions to set it up and start your project from there.

Enjoy :)