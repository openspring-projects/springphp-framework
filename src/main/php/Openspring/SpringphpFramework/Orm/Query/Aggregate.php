<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Orm\Query;

class Aggregate
{
    public static function MAX(String $fieldName = '1', String $alias = 'max'): String
    {
        return trim('MAX(' . $fieldName . ')' . ' ' . $alias);
    }

    public static function MIN(String $fieldName = '1', String $alias = 'min'): String
    {
        return trim('MIN(' . $fieldName . ')' . ' ' . $alias);
    }

    public static function COUNT(String $fieldName = '1', String $alias = 'count'): String
    {
        return trim('COUNT(' . $fieldName . ')' . ' ' . $alias);
    }
    
    public static function SUM(String $fieldName = '1', String $alias = 'sum'): String
    {
        return trim('SUM(' . $fieldName . ')' . ' ' . $alias);
    }
}