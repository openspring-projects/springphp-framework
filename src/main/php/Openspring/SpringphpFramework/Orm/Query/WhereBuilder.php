<?php
/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Orm\Query;

use Openspring\SpringphpFramework\Enumeration\SQLTokenType;
use Openspring\SpringphpFramework\Utils\DatabaseUtils;
use Openspring\SpringphpFramework\Utils\Objects;
use Openspring\SpringphpFramework\Utils\Strings;

/**
 * Builder of where sql clause
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
class WhereBuilder
{
    public $gwhere = array();

    public function andEqual($column, $_values, $property = NULL)
    {
        return $this->setCondition(SQLTokenType::AND, $column, $_values, $property);
    }

    public function orEqual($column, $_values, $property = NULL)
    {
        return $this->setCondition(SQLTokenType::OR, $column, $_values, $property);
    }

    public function andLike($column, $_values, $property = NULL)
    {
        return $this->setCondition(SQLTokenType::AND, $column, $_values, $property, true);
    }

    public function orLike($column, $_values, $property = NULL)
    {
        return $this->setCondition(SQLTokenType::OR, $column, $_values, $property, true);
    }

    public function openAndBracket()
    {
        $this->gwhere[] .= SQLTokenType::AND . " (";
        return $this;
    }

    public function openOrBracket()
    {
        $this->gwhere[] .= SQLTokenType::OR . " (";
        return $this;
    }

    public function closeBracket()
    {
        $this->gwhere[] .= ")";
        return $this;
    }

    public function getWhere($keepFirstOperator = true)
    {
        $wsql = implode(" ", $this->gwhere);
        
        $wsql = Strings::replaceAll($wsql, array(
            SQLTokenType::AND . ' ( )' => '',
            SQLTokenType::OR . ' ( )' => '',
            '( ' . SQLTokenType::AND . ' (' => '((',
            '( ' . SQLTokenType::OR . ' (' => '((',
            ') )' => '))',
            ') )' => '))'
        ));

        if (strlen($wsql) < 3) return $wsql;

        return $keepFirstOperator ? $wsql : substr($wsql, 3, strlen($wsql) - 3);
    }

    public function init()
    {
        $this->getWhere = array();
    }

    private function secureValues($_values)
    {
        if (is_numeric($_values)) return $_values;
        if (is_string($_values)) return DatabaseUtils::cleanString($_values);

        if (is_array($_values))
        {
            $securedValues = array();
            foreach ($_values as $value)
            {
                $securedValues[] = DatabaseUtils::cleanString($value);
            }

            return $securedValues;
        }

        return '';
    }

    private function getOperator($operator)
    {
        $lastItem = $this->gwhere[count($this->gwhere) - 1];
        if (Strings::endsWith($lastItem, '(')) return '';
        
        return $operator;
    }

    private function setCondition(String $operator, String $column, $_values, $property = NULL, $useLike = false)
    {
        $values = $this->getValue($_values, $property);

        if (is_string($values) || is_numeric($values))
        {
            if (! Strings::isEmpty($values))
            {
                if ($useLike)
                {
                    $this->gwhere[] .= $operator . " ({$column} " . SQLTokenType::LIKE . " '%{$values}%')";
                }
                else
                {
                    $val = is_numeric($values) ? $values : "'{$values}'";
                    $this->gwhere[] .= $operator . " ({$column}={$val})";
                }
            }

            return $this;
        }

        if (is_array($values))
        {
            switch (count($values))
            {
                case 0: // no items found
                    return $this;
                case 1: // one item found
                    if (! Strings::isEmpty($values[0]))
                    {
                        if ($useLike) $this->gwhere[] .= $this->getOperator($operator . ' ') . " ($column " . SQLTokenType::LIKE . " '%" . $values[0] . "%')";
                        else $this->gwhere[] .= $this->getOperator($operator . ' ') . " ($column='" . $values[0] . "')";
                    }
                    return $this;
                default: // many items found
                    $this->gwhere[] .= $this->getOperator($operator . ' ') . " ($column " . SQLTokenType::IN . " ('" . implode("','", $values) . "'))";
                    return $this;
            }
        }

        return $this;
    }

    private function getValue($_values, $property = NULL)
    {
        $fvalue = NULL;

        // if object property is set, then go get it !
        if ($property == NULL)
        {
            $fvalue = $this->secureValues($_values);
        }
        else
        {
            $fvalue = $this->secureValues(Objects::get($_values, $property, ''));
        }

        return $fvalue;
    }
}