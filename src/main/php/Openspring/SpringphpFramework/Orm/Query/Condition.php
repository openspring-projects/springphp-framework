<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Orm\Query;

use Openspring\SpringphpFramework\Utils\DatabaseUtils;
use Openspring\SpringphpFramework\Utils\Strings;
use Openspring\SpringphpFramework\Type\StringBuilder;

class Condition
{
    protected $field;
    protected $isParameter = false;

    public function __construct(String $field, String $alias = '', bool $isParameter = false)
    {
        $this->isParameter = $isParameter;
        $this->field = $this->getFieldName($field, $alias = '');
    }

    public static function field(String $field, String $alias = '')
    {
        return new Condition($field, $alias, false);
    }

    public static function joinField(String $field, String $alias = '')
    {
        return new Condition($field, $alias, true);
    }

    public function equals(?String $value, bool $isField = false): String
    {
        if (Strings::isEmpty($value)) return '';

        return $this->getCondition($this->field . ' = ' . $this->getValue($value, $isField));
    }

    public function likeStrict(?String $value, bool $isField = false): String
    {
        if (Strings::isEmpty($value)) return '';

        return $this->getCondition($this->field . ' LIKE ' . $this->getValue($value, $isField));
    }

    public function like(?String $value, bool $isField = false): String
    {
        if (Strings::isEmpty($value)) return '';
        $nvalue = $this->isParameter($value) ? $value : "%$value%";

        return $this->likeStrict($nvalue, $isField);
    }

    public function in(?String $value, bool $isField = false): String
    {
        if (Strings::isEmpty($value)) return '';
        $nvalue = $this->isParameter($value) ? $value : "%$value%";

        return $this->getCondition($this->field . ' IN (' . $this->getInValue($value, $isField)) . ')';
    }

    public function greaterThan(?String $value, bool $isField = false): String
    {
        if (Strings::isEmpty($value)) return '';

        return $this->getCondition($this->field . ' > ' . $this->getValue($value, $isField));
    }

    public function greaterOrEqualThan(?String $value, bool $isField = false): String
    {
        if (Strings::isEmpty($value)) return '';

        return $this->getCondition($this->field . ' >= ' . $this->getValue($value, $isField));
    }

    public function lessThan(?String $value, bool $isField = false): String
    {
        if (Strings::isEmpty($value)) return '';

        return $this->getCondition($this->field . ' < ' . $this->getValue($value, $isField));
    }

    public function lessOrEqualThan(?String $value, bool $isField = false): String
    {
        if (Strings::isEmpty($value)) return '';

        return $this->getCondition($this->field . ' <= ' . $this->getValue($value, $isField));
    }

    public function between(?String $value1, ?String $value2, bool $isField = false): String
    {
        if (Strings::isEmpty($value1) || Strings::isEmpty($value2)) return '';

        return $this->getCondition($this->field . ' BETWEEN ' . $this->getValue($value1, $isField) . ' AND ' . $this->getValue($value2, $isField));
    }

    public function isNull(): String
    {
        return $this->getCondition($this->field . ' IS NULL');
    }

    public function isEmpty(bool $trim = true): String
    {
        $field = $trim ? 'TRIM(' . $this->field . ')' : $this->field;
        return $this->getCondition('(' . $this->isNull() . ' OR ' . $field . " = ''" . ' OR ' . $this->field . " = 0)");
    }

    // Not operators
    public function notEquals(?String $value, bool $isField = false): String
    {
        if (Strings::isEmpty($value)) return '';

        return $this->getCondition($this->field . ' <> ' . $this->getValue($value, $isField));
    }

    public function notLikeStrict(?String $value, bool $isField = false): String
    {
        if (Strings::isEmpty($value)) return '';

        return $this->getCondition($this->field . ' NOT LIKE ' . $this->getValue($value, $isField));
    }

    public function notLike(?String $value, bool $isField = false): String
    {
        if (Strings::isEmpty($value)) return '';
        $nvalue = $this->isParameter($value) ? $value : "%$value%";

        return $this->notLikeStrict($nvalue, $isField);
    }

    public function notIn(?String $value, bool $isField = false): String
    {
        if (Strings::isEmpty($value)) return '';
        $nvalue = $this->isParameter($value) ? $value : "%$value%";

        return $this->getCondition($this->field . ' NOT IN (' . $this->getInValue($value, $isField)) . ')';
    }

    public function notBetween(?String $value1, ?String $value2, bool $isField = false): String
    {
        if (Strings::isEmpty($value1) || Strings::isEmpty($value2)) return '';

        return $this->getCondition($this->field . ' NOT BETWEEN ' . $this->getValue($value1, $isField) . ' AND ' . $this->getValue($value2, $isField));
    }

    public function isNotNull(): String
    {
        return $this->getCondition($this->field . ' IS NOT NULL');
    }

    public function isNotEmpty(bool $trim = true): String
    {
        $field = $trim ? 'TRIM(' . $this->field . ')' : $this->field;
        return $this->getCondition('(' . $this->isNotNull() . ' AND ' . $field . " != '')");
    }

    protected function getCondition(String $condition): String
    {
        return $condition;
    }

    protected function getValue(?String $value, bool $isField = false): String
    {
        if ($isField || is_numeric($value)) return $value;
        $value = DatabaseUtils::cleanString($value);

        return $this->isParameter($value, ':') ? $value : "'{$value}'";
    }

    protected function getInValue(?String $value, bool $isField = false): String
    {
        if ($isField || is_numeric($value)) return $value;
        $sb = new StringBuilder();
        $valueItems = explode(',', $value);
        foreach ($valueItems as $valueItem)
        {
            $sb->append($this->getValue($valueItem));
        }

        return $this->isParameter($value, ':') ? $value : $sb->toString(',');
    }

    protected function isParameter(?String $value): bool
    {
        return ($this->isParameter || Strings::startsWith($value, ':'));
    }

    protected function getFieldName(String $field, String $alias = '')
    {
        if (Strings::isEmpty($alias)) return $field;
        $items = explode('.', $field);

        if (count($items) == 1) return $alias . '.' . $field;
        unset($items[0]);

        return $alias . '.' . implode('', $items);
    }
}