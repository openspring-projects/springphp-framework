<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Orm\Query;

use Openspring\SpringphpFramework\Utils\Strings;

class Select
{
    const PIPE_KWORD = '___pipe';
    const JSON = 'Json';
    const HTML_NEW_LINE = 'HtmlNewLine';
    const URL_ENCODE = 'UrlEncode';
    const BOOLEAN = 'Boolean';
    const NUMBER = 'Number';
    const DATE_TIME = 'DateTime';
    const NOT_NULL = 'NotNull';
    const NVL = 'Nvl';

    public static function as(String $fieldName, String $alias): String
    {
        return $fieldName . ' ' . $alias;
    }

    public static function asJson(String $fieldName, String $alias = ''): String
    {
        return self::getFieldName($fieldName, $alias) . self::PIPE_KWORD . self::JSON;
    }

    public static function asHtmlNewLined(String $fieldName, String $alias = ''): String
    {
        return self::getFieldName($fieldName, $alias) . self::PIPE_KWORD . self::HTML_NEW_LINE;
    }

    public static function asUrlEncoded(String $fieldName, String $alias = ''): String
    {
        return self::getFieldName($fieldName, $alias) . self::PIPE_KWORD . self::URL_ENCODE;
    }

    public static function asBool(String $fieldName, String $alias = ''): String
    {
        return self::getFieldName($fieldName, $alias) . self::PIPE_KWORD . self::BOOLEAN;
    }

    public static function asNumber(String $fieldName, String $alias = ''): String
    {
        return self::getFieldName($fieldName, $alias) . self::PIPE_KWORD . self::NUMBER;
    }

    public static function asDateTime(String $fieldName, String $alias = ''): String
    {
        return self::getFieldName($fieldName, $alias) . self::PIPE_KWORD . self::DATE_TIME;
    }

    public static function asNotNull(String $fieldName, String $alias = ''): String
    {
        return self::getFieldName($fieldName, $alias) . self::PIPE_KWORD . self::NOT_NULL;
    }

    public static function asNvl(String $fieldName, String $alias = ''): String
    {
        return self::getFieldName($fieldName, $alias) . self::PIPE_KWORD . self::NVL;
    }

    public static function asCamlCased(String $fieldName): String
    {
        $alias = '';
        $fieldName = trim($fieldName);
        $fname = $fieldName;
        $parts = explode('.', $fieldName);
        if (count($parts) == 2)
        {
            $alias = $parts[0] . '.';
            $fname = $parts[1];
        }

        if (Strings::contain($fieldName, ' ') || ! Strings::contain($fieldName, '_'))
        {
            return $fieldName . ' ' . Strings::getStringPart($fname, '(', 0);
        }

        return $alias . $fname . ' ' . self::camelCaseField($fname);
    }

    protected static function getFieldName(String $fieldName, String $alias = ''): String
    {
        $fieldName = trim($fieldName);
        if (Strings::isEmpty($alias)) return $fieldName;

        $fn = (!Strings::startsWith($fieldName, '(')) ? trim(explode(' ', $fieldName)[0]) : $fieldName;
        
        return $fn . ' ' . $alias;
    }

    protected static function camelCaseField(String $fieldName)
    {
        $i = array(
                    "-",
                    "_");

        $fieldName = preg_replace('/([a-z])([A-Z])/', "\\1 \\2", $fieldName);
        $fieldName = preg_replace('@[^a-zA-Z0-9\-_ ]+@', '', $fieldName);
        $fieldName = str_replace($i, ' ', $fieldName);
        $fieldName = str_replace(' ', '', ucwords(strtolower($fieldName)));
        $fieldName = strtolower(substr($fieldName, 0, 1)) . substr($fieldName, 1);

        return $fieldName;
    }
}