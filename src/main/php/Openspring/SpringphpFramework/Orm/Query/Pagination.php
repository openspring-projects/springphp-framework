<?php

/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Orm\Query;

use Openspring\SpringphpFramework\Type\Any;

class Pagination
{
    /**
     * Return pagination object containing:
     * - startRow
     * - endRow
     * - totalPages
     *
     * @param int $page
     * @param int $pageSize
     * @param int $totalRows
     * @return \Openspring\SpringphpFramework\Type\Any
     */
    public static function getPager(int $page, int $pageSize, int $totalRows)
    {
        $out = new Any();
        $totalPages = ($pageSize > 0) ? ceil($totalRows / $pageSize) : 1;

        $out->startRow = ($page - 1) * $pageSize;
        $out->endRow = $out->startRow + $pageSize;
        $out->totalPages = $totalPages;

        if ($out->totalPages == 1)
        {
            $out->startRow = 0;
            $out->endRow = $pageSize;
        }

        return $out;
    }
}