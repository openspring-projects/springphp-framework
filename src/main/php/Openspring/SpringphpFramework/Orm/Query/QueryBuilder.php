<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Orm\Query;

use Openspring\SpringphpFramework\Orm\Type\DataType;
use Openspring\SpringphpFramework\Utils\Strings;
use Openspring\SpringphpFramework\Orm\Type\QueryParameter;
use Openspring\SpringphpFramework\Orm\Connection;
use DeepCopy\Filter\KeepFilter;
use Openspring\SpringphpFramework\Utils\Arrays;

class QueryBuilder extends QueryContext
{
    protected $queryParts = array();
    protected $parameters = array();
    protected $externalQuery = NULL;
    protected $camelCaseEnabled = false;
    protected $currentTabAlias = '';
    protected $tablePrefix = '';

    public function __construct(Connection $connection)
    {
        parent::__construct($connection);

        $this->clear();
    }

    public function clear(): QueryBuilder
    {
        $this->queryParts = array();

        $this->queryParts['SELECT'] = '';
        $this->queryParts['FROM'] = '';
        $this->queryParts['WHERE'] = '';
        $this->queryParts['GROUP BY'] = '';
        $this->queryParts['HAVING'] = '';
        $this->queryParts['ORDER BY'] = '';

        return $this;
    }

    public function camelcase(bool $value = true): QueryBuilder
    {
        $this->camelCaseEnabled = $value;
        return $this;
    }

    public function for(String $tableAlias = ''): QueryBuilder
    {
        $this->currentTabAlias = (Strings::isEmpty($tableAlias)) ? '' : $tableAlias . '.';
        return $this;
    }

    public function setTablePrefix(String $prefix): QueryBuilder
    {
        $this->tablePrefix = $prefix;
        return $this;
    }

    public function select(): QueryBuilder
    {
        $input = func_get_args();
        if (count($input) == 0)
        {
            $input = Arrays::of("*");
        }

        $lastParam = $input[count($input) - 1];
        if (is_bool($lastParam))
        {
            if ($lastParam == false)
            {
                return $this;
            }
            else
            {
                unset($input[count($input) - 1]);
            }
        }

        $fields = $this->getFieldsNames($input);
        $this->setSQL('SELECT', implode(', ', $fields));

        return $this;
    }

    public function from(): QueryBuilder
    {
        $this->currentTabAlias = '';
        $froms = func_get_args();
        foreach ($froms as &$from)
        {
            $from = $this->getTableName($from);
        }

        $this->setSQL('FROM', implode(', ', $froms));
        return $this;
    }

    public function innerJoin(String $table, String $on, bool $enable = true): QueryBuilder
    {
        if ($enable) $this->setSQL('FROM', 'INNER JOIN ' . $this->getTableName($table) . ' ON ' . $on, ' ');
        return $this;
    }

    public function leftJoin(String $table, String $on, bool $enable = true): QueryBuilder
    {
        if ($enable) $this->setSQL('FROM', 'LEFT JOIN ' . $this->getTableName($table) . ' ON ' . $on, ' ');
        return $this;
    }

    public function rightJoin(String $table, String $on, bool $enable = true): QueryBuilder
    {
        if ($enable) $this->setSQL('FROM', 'RIGHT JOIN ' . $this->getTableName($table) . ' ON ' . $on, ' ');
        return $this;
    }

    public function fullOuterJoin(String $table, String $on, bool $enable = true): QueryBuilder
    {
        if ($enable) $this->setSQL('FROM', 'FULL OUTER JOIN ' . $this->getTableName($table) . ' ON ' . $on, ' ');
        return $this;
    }

    public function leftSemiJoin(String $table, String $on, bool $enable = true): QueryBuilder
    {
        if ($enable) $this->setSQL('FROM', 'LEFT SEMI JOIN ' . $this->getTableName($table) . ' ON ' . $on, ' ');
        return $this;
    }

    public function leftAntiJoin(String $table, String $on, bool $enable = true): QueryBuilder
    {
        if ($enable) $this->setSQL('FROM', 'LEFT ANTI JOIN ' . $this->getTableName($table) . ' ON ' . $on, ' ');
        return $this;
    }

    public function crossJoin(String $table, String $on, bool $enable = true): QueryBuilder
    {
        if ($enable) $this->setSQL('FROM', 'CROSS JOIN ' . $this->getTableName($table) . ' ON ' . $on, ' ');
        return $this;
    }

    public function naturalJoin(String $table, String $on, bool $enable = true): QueryBuilder
    {
        if ($enable) $this->setSQL('FROM', 'NATURAL JOIN ' . $this->getTableName($table) . ' ON ' . $on, ' ');
        return $this;
    }

    public function naturalLeftJoin(String $table, String $on, bool $enable = true): QueryBuilder
    {
        if ($enable) $this->setSQL('FROM', 'NATURAL LEFT JOIN ' . $this->getTableName($table) . ' ON ' . $on, ' ');
        return $this;
    }

    public function naturalRightJoin(String $table, String $on, bool $enable = true): QueryBuilder
    {
        if ($enable) $this->setSQL('FROM', 'NATURAL RIGHT JOIN ' . $this->getTableName($table) . ' ON ' . $on, ' ');
        return $this;
    }

    public function where(String $where, bool $enable = true): QueryBuilder
    {
        if (! $enable) return $this;
        $this->currentTabAlias = '';
        if (! Strings::isEmpty($where)) $this->setSQL('WHERE', $where);
        return $this;
    }

    public function groupBy(): QueryBuilder
    {
        $input = func_get_args();
        $fields = $this->getFieldsNames($input, false);
        $this->setSQL('GROUP BY', implode(', ', $fields));

        return $this;
    }

    public function having(String $havingClause, bool $enable = true): QueryBuilder
    {
        if (! $enable) return $this;
        $this->setSQL('HAVING', $havingClause);
        return $this;
    }

    public function orderByAsc(String $field, bool $enable = true): QueryBuilder
    {
        if (! $enable) return $this;
        return $this->orderBy($this->removeAlias($field) . ' ' . OrderBy::ASC);
    }

    public function orderByDesc(String $field, bool $enable = true): QueryBuilder
    {
        if (! $enable) return $this;
        return $this->orderBy($this->removeAlias($field) . ' ' . OrderBy::DESC);
    }

    public function orderBy(): QueryBuilder
    {
        $input = func_get_args();
        $fields = $this->getFieldsNames($input, true);
        $this->setSQL('ORDER BY', implode(', ', $fields));

        return $this;
    }

    public function setParameter(String $name, $value, String $type = DataType::VARCHAR): QueryBuilder
    {
        $this->parameters[$name] = new QueryParameter($name, $value, $type);
        return $this;
    }

    public function setQueryParameter(QueryParameter $queryParameter): QueryBuilder
    {
        $this->parameters[$queryParameter->getName()] = $queryParameter;
        return $this;
    }

    public function setQueryParameters($parameters = array()): QueryBuilder
    {
        foreach ($parameters as $queryParameter)
        {
            $this->parameters[$queryParameter->getName()] = $queryParameter;
        }

        return $this;
    }

    private function setSQL(String $part, String $sql, String $separator = ', '): void
    {
        $this->queryParts[$part] = ($this->queryParts[$part] == '') ? $sql : ($this->queryParts[$part] . $separator . $sql);
    }

    public function showQuery(): QueryBuilder
    {
        dump($this->getQuery());
        return $this;
    }

    public function setQuery(String $q): QueryBuilder
    {
        $this->externalQuery = $q;
        return $this;
    }

    public function reset(): QueryBuilder
    {
        $this->queryParts = array();
        $this->parameters = array();
        $this->externalQuery = NULL;

        return $this->clear();
    }

    public function getQueryWhere(): String
    {
        return $this->valorizeQuery($this->queryParts['WHERE']);
    }

    public function getQuery(): String
    {
        if ($this->externalQuery != NULL) return $this->externalQuery;

        $query = '';
        foreach ($this->queryParts as $part => $sql)
        {
            if ($sql == '') continue;
            $query = ($query == '') ? $part . ' ' . $sql : ($query . ' ' . $part . ' ' . $sql);
        }

        return $this->valorizeQuery($query);
    }

    public function getSubQuery(): String
    {
        return '(' . $this->getQuery() . ')';
    }

    protected function getTableName(String $tabname): String
    {
        return $this->tablePrefix . $tabname;
    }

    protected function getFieldsNames(array $array, $keepAlias = true): array
    {
        if (Strings::isEmpty($this->currentTabAlias) && ! $this->camelCaseEnabled)
        {
            return $array;
        }

        $fields = [];
        $addAlias = false;
        foreach ($array as $field)
        {
            $addAlias = Strings::contain($field, ' ') ? false : true;
            $ffield = $field;
            if ($addAlias && $this->camelCaseEnabled && ! $this->isOneWordFieldPiped($field))
            {
                $ffield = Select::asCamlCased($field);
            }

            if (! $addAlias)
            {
                $fields[] = $ffield;
            }
            else
            {
                if ($this->isOneWordFieldPiped($ffield))
                {
                    $parts = explode(Select::PIPE_KWORD, $ffield);
                    $items = explode('.', $parts[0]);
                    $pureField = (count($items) == 1) ? $items[0] : $items[1];
                    if ($this->camelCaseEnabled)
                    {
                        $tabAlias = (count($items) == 1) ? $this->currentTabAlias : $items[0] . '.';
                        $fields[] = $tabAlias . Select::asCamlCased($pureField) . Select::PIPE_KWORD . $parts[1];
                    }
                    else
                    {
                        $fields[] = $this->currentTabAlias . $parts[0] . ' ' . $pureField . Select::PIPE_KWORD . $parts[1];
                    }
                }
                else
                {
                    $fields[] = $this->currentTabAlias . $ffield;
                }
            }
        }

        if (! $keepAlias)
        {
            return $this->removeAliases($fields);
        }

        return $fields;
    }

    protected function removeAliases(Array $array): array
    {
        foreach ($array as &$value)
        {
            $value = $this->removeAlias($value);
        }

        return $array;
    }

    protected function removeAlias(String $value): String
    {
        return explode(' ', $value)[0];
    }

    protected function valorizeQuery(String $query): String
    {
        $vquery = $query;

        foreach ($this->parameters as $queryParameter)
        {
            $securedValue = '';
            switch ($queryParameter->getType())
            {
                case DataType::VARCHAR:
                    $securedValue = "'" . Strings::getSecuredDbInput($queryParameter->getValue()) . "'";
                    break;
                default:
                    $securedValue = Strings::getSecuredDbInput($queryParameter->getValue());
                    break;
            }

            $vquery = Strings::replace($queryParameter->getQPName(), $securedValue, $vquery);
        }

        return $vquery;
    }

    protected function isAliased(String $exp): bool
    {
        $matchs = array();
        preg_match('/[\w]+[.]/', $exp, $matchs);

        return count($matchs) > 0;
    }

    protected function isOneWordFieldPiped(String $oneWordField): bool
    {
        if (Strings::contain($oneWordField, ' ')) return false;
        return Strings::contain($oneWordField, Select::PIPE_KWORD);
    }
}