<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Orm\Query;

use Openspring\SpringphpFramework\Core\Initializable;
use Openspring\SpringphpFramework\Utils\Strings;

class Pipe implements Initializable
{

    public static function initialize(): void
    {
        // Ignore
    }

    public static function json(?String $value)
    {
        if (Strings::isEmpty($value)) return null;
        return json_decode($value);
    }

    public static function htmlNewLine(?String $value): String
    {
        if (Strings::isEmpty($value)) return '';
        $html = nl2br($value);

        return $html;
    }

    public static function urlEncode(?String $value): String
    {
        if (Strings::isEmpty($value)) return '';
        $uencoded = urlencode($value);

        return $uencoded;
    }

    public static function boolean(?String $value): bool
    {
        if (Strings::isEmpty($value)) return false;
        return ($value == '0') ? false : true;
    }
    
    public static function number(?String $value): int
    {
        if (Strings::isEmpty($value)) return 0;
        return intval($value);
    }

    public static function dateTime(?String $value): String
    {
        if (Strings::isEmpty($value)) return '';
        return explode('.', $value)[0];
    }

    public static function notNull(?String $value): String
    {
        if (Strings::isEmpty($value)) return '';
        return ($value == null) ? '' : $value;
    }

    public static function array(?String $value): array
    {
        if (Strings::isEmpty($value)) return array();
        return implode(';', $value);
    }

    public static function nvl(?String $value, ?String $default = ''): String
    {
        return ($value == null || strlen(trim($value)) == 0) ? $default : $value;
    }
}