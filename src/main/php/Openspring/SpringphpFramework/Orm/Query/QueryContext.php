<?php
/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Orm\Query;

use Openspring\SpringphpFramework\Orm\Connection;
use Openspring\SpringphpFramework\Type\ResultList;
use Openspring\SpringphpFramework\Utils\Strings;

abstract class QueryContext
{
    protected $connection = NULL;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getConnection(): Connection
    {
        return $this->connection;
    }

    public function execute($page = 0, $pageSize = 0, $show_sql = false): ?ResultList 
    {
        $query = $this->getQuery();
        $tablePrefix = $this->getConnection()
                         	->getDataSourceConfig()
                         	->getTablePrefix();
        $query = Strings::replace('#__', $tablePrefix, $query);

        if ($show_sql) echo $query;
        
		return $this->getConnection()
					->executeNativeSQL($query, $page, $pageSize, 0);
    }
}