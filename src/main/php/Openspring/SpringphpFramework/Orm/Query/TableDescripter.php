<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Orm\Query;

use Openspring\SpringphpFramework\Utils\Strings;

abstract class TableDescripter
{
    public static function as(String $alias): void
    {
        static::$alias = Strings::isEmpty($alias) ? '' : $alias . '.';
    }

    public static function getField(String $field, String $alias = NULL): String
    {
        return (static::getAlias($alias) . $field);
    }

    public static function getTableName(String $tname, String $alias = NULL): String
    {
        return trim(($tname . ' ' . Strings::replace('.', '', static::getAlias($alias))));
    }

    protected static function getAlias(String $alias = NULL): String
    {
        return $alias ? ($alias . '.') : static::$alias;
    }
}