<?php
/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Orm\Driver;

/**
 * Definition of database driver
 * 
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
interface DatabaseDriver
{
    public function isAlive(): bool;
    public function connect(): void;
    public function getDefaultClientCharset(): String;
    public function setDefaultClientCharset(String $value): void;
    public function setAutocommit(bool $value): void;
    public function getAutocommit(): bool;
    public function commit(): bool;
    public function rollback(): bool;
    public function getQueryCount(String $query): int;
    public function execute_SQL(String $query, int $page = 0, int $pageSize = 0, int $totalRows = 0);
    public function execute_CRUD_SQL(String $query);
    public function getLastError();
    public function getConnectionIdentifier();
    public function getLastInsertedRowId();
    public function getManagedTypes(): array;
    public function getDatabaseForeignKeysQuery(): String;
    public function getDatabaseTablesQuery(): String;
    public function getTableFieldsQuery(String $tableName): String;
    public function getDatabasePrimaryKeysQuery(String $tableName): String;
    public function close(): void;
}