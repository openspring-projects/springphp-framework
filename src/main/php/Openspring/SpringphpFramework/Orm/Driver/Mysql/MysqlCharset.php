<?php
/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Orm\Driver\Mysql;

use Openspring\SpringphpFramework\Enumeration\EnumerationType;

/**
 * Mysql database supported charset
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
class MysqlCharset extends EnumerationType
{
    const ASCII = 'ascii';
    const UJIS = 'ujis';
    const SJIS = 'sjis';
    const HEBREW = 'hebrew';
    const TIS620 = 'tis620';
    const EUCKR = 'euckr';
    const KOI8U = 'koi8u';
    const GB2312 = 'gb2312';
    const GREEK = 'greek';
    const CP1250 = 'cp1250';
    const GBK = 'gbk';
    const LATIN5 = 'latin5';
    const ARMSCII8 = 'armscii8';
    const UTF8 = 'utf8';
    const UCS2 = 'ucs2';
    const CP866 = 'cp866';
    const KEYBCS2 = 'keybcs2';
    const MACCE = 'macce';
    const MACROMAN = 'macroman';
    const CP852 = 'cp852';
    const LATIN7 = 'latin7';
    const UTF8MB4 = 'utf8mb4';
    const CP1251 = 'cp1251';
    const UTF16 = 'utf16';
    const CP1256 = 'cp1256';
    const CP1257 = 'cp1257';
    const UTF32 = 'utf32';
    const BINARY = 'binary';
    const GEOSTD8 = 'geostd8';
    const CP932 = 'cp932';
    const EUCJPMS = 'eucjpms';
}