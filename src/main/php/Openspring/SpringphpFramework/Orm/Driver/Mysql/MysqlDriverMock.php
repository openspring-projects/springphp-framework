<?php
/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Orm\Driver\Mysql;

use Openspring\SpringphpFramework\Mock\DataMockito;
use Openspring\SpringphpFramework\Orm\DataSource\DataSourceConfig;
use Openspring\SpringphpFramework\Orm\Driver\DatabaseDriver;
use Openspring\SpringphpFramework\Type\Any;
use Openspring\SpringphpFramework\Type\ResultList;

/**
 * Mysql database driver mocker
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
class MysqlDriverMock implements DatabaseDriver
{
    public $dbLink = NULL;
    public $dsConfig = NULL;
    public $fieldQuoter = '`';
    protected $autocommit = true;
    protected $defaultClientCharset = 'utf8';

    public function __construct(DataSourceConfig $dsConfig)
    {
        $this->dsConfig = $dsConfig;
    }
    
    public function connect(): void
    {
        $this->dbLink = new Any();
        
        // set autocommit value
        $this->setAutocommit($this->autocommit);
    }

    public function getDataSourceConfig(): DataSourceConfig
    {
        return $this->dsConfig;
    }
    
    public function getDefaultClientCharset(): String
    {
        return $this->defaultClientCharset;
    }
    
    public function setDefaultClientCharset(String $value): void
    {
        MysqlCharset::validate($value);
        
        $this->defaultClientCharset = $value;
    }
    
    public function setAutocommit(bool $value): void
    {
        $this->autocommit = $value;
    }
    
    public function commit(): bool
    {
        return true;
    }
    
    public function rollback(): bool
    {
        return true;
    }
    
    public function getAutocommit(): bool
    {
        return $this->autocommit;
    }

    public function getQueryCount(String $query): int
    {
        return DataMockito::get($query);
    }

    public function execute_SQL(String $query, int $page = 1, int $pageSize = 25, int $totalRows = 0): ?ResultList
    {
        return $this->execute_QUERY($query, $page, $pageSize, $totalRows, 0);
    }

    private function execute_QUERY(String $query, int $page, int $pageSize, int $totalRows)
    {
        if (DataMockito::isDebugEnabled()) echo PHP_EOL . "[MOCKED-DRIVER] " . $query;
        return DataMockito::getResultList($query, $page, $pageSize);
    }

    public function execute_CRUD_SQL(String $query): bool
    {
        if (DataMockito::isDebugEnabled()) echo PHP_EOL . "[MOCKED-DRIVER] " . $query;
        $done = DataMockito::get($query);
        
        return ($done == null) ? false : $done;
    }

    public function getLastError()
    {
        return null;
    }

    public function cleanup($resourceResult): void
    {
        // Done
    }

    public function getConnectionIdentifier()
    {
        return $this->dbLink;
    }

    public function getLastInsertedRowId()
    {
        return 0;
    }

    public function isAlive(): bool
    {
        return ($this->dbLink != null);
    }

    public function close(): void
    {
        $this->dbLink = null;
    }

    public function getManagedTypes(): array
    {
        return array('INT','TIN','SMA','MED','BIG','DEC','FLO','DOU','REA','BIT','BOO','SER');
    }

    public function getDatabaseForeignKeysQuery(): String
    {
        return "SELECT i.TABLE_NAME tname, k.COLUMN_NAME cname, k.REFERENCED_TABLE_NAME rtname, k.REFERENCED_COLUMN_NAME rcname
    			FROM information_schema.TABLE_CONSTRAINTS i LEFT JOIN information_schema.KEY_COLUMN_USAGE k 
    			ON i.CONSTRAINT_NAME = k.CONSTRAINT_NAME WHERE i.CONSTRAINT_TYPE = 'FOREIGN KEY' AND 
    			i.TABLE_SCHEMA = '" . $this->getDataSourceConfig()->getDatabase() . "' ORDER BY i.TABLE_NAME, k.REFERENCED_TABLE_NAME";
    }

    public function getDatabaseTablesQuery(): String
    {
        return "SELECT TABLE_NAME tname FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='" . $this->getDataSourceConfig()->getDatabase() . "'";
    }

    public function getTableFieldsQuery(String $tableName): String
    {
        return "DESC $tableName";
    }

    public function getDatabasePrimaryKeysQuery(String $tableName): String
    {
        return "SELECT TABLE_NAME tname, COLUMN_NAME cname FROM key_column_usage k " + "WHERE  table_schema = '" . $this->getDataSourceConfig()->getDatabase() . "' AND constraint_name = 'PRIMARY' ORDER BY TABLE_NAME";
    }
}