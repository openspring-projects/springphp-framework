<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Orm\Driver\Mysql;

use Openspring\SpringphpFramework\Exception\Data\InvalidDatabaseOperationException;
use Openspring\SpringphpFramework\Log\LogFactory;
use Openspring\SpringphpFramework\Orm\DataSource\DataSourceConfig;
use Openspring\SpringphpFramework\Orm\Driver\DatabaseDriver;
use Openspring\SpringphpFramework\Orm\Query\Pagination;
use Openspring\SpringphpFramework\Orm\Query\Pipe;
use Openspring\SpringphpFramework\Type\DynamicObject;
use Openspring\SpringphpFramework\Type\ResultList;
use Openspring\SpringphpFramework\Utils\Convertors;
use Openspring\SpringphpFramework\Utils\Strings;
use Exception;
use Openspring\SpringphpFramework\Exception\Data\DatabaseErrorException;
use Openspring\SpringphpFramework\Context\Environment;

/**
 * Mysql database driver
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
class MysqlDriver implements DatabaseDriver
{
    protected $dbLink = NULL;
    protected $dsConfig = NULL;
    protected $fieldQuoter = '`';
    protected $autocommit = true;
    protected $defaultClientCharset = 'utf8';
    protected $logger = NULL;

    public function __construct(DataSourceConfig $dsConfig)
    {
        $this->dsConfig = $dsConfig;
        $this->logger = LogFactory::getLogger(self::class);
    }

    public function connect(): void
    {
        $conf = $this->getDataSourceConfig();

        $this->dbLink = mysqli_connect($conf->getHost(), $conf->getUser(), $conf->getPassword(), $conf->getDatabase(), $conf->getPort());

        if (mysqli_connect_errno())
        {
            throw new Exception(mysqli_connect_error());
        }

        // define client charset
        $this->setDefaultClientCharset(MysqlCharset::UTF8);

        // set autocommit value
        $this->setAutocommit($this->autocommit);
    }

    public function getDefaultClientCharset(): String
    {
        return $this->defaultClientCharset;
    }

    public function setDefaultClientCharset(String $value): void
    {
        MysqlCharset::validate($value);

        $this->defaultClientCharset = $value;
        mysqli_set_charset($this->dbLink, $this->defaultClientCharset);
    }

    public function getDataSourceConfig(): DataSourceConfig
    {
        return $this->dsConfig;
    }

    public function setAutocommit(bool $value): void
    {
        $this->autocommit = $value;
        mysqli_autocommit($this->dbLink, $value);

        if (! $this->autocommit)
        {
            $this->execute_CRUD_SQL('SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED');
        }
        else
        {
            $this->execute_CRUD_SQL('SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ');
        }
    }

    public function getAutocommit(): bool
    {
        return $this->autocommit;
    }

    public function commit(): bool
    {
        return mysqli_commit($this->dbLink);
    }

    public function rollback(): bool
    {
        return mysqli_rollback($this->dbLink);
    }

    public function getQueryCount(String $query): int
    {
        $cquery = "SELECT count(*) totalRows FROM (($query) q)";

        if ($this->dsConfig->getShowSQL()) $this->logger->debug($cquery);
        $tmp = $this->mysqlQuery($cquery);
        if ($tmp === FALSE) throw new Exception($this->getLastError());
        $rec = mysqli_fetch_assoc($tmp);

        return $rec['totalRows'];
    }

    public function execute_SQL(String $query, int $page = 0, int $pageSize = 0, int $totalRows = 0): ?ResultList
    {
        $queryResult = $this->getQueryResult($query, $page, $pageSize, $totalRows);

        if ($queryResult == NULL || $queryResult['totalRows'] == 0)
        {
            return new ResultList(array(), $page, $pageSize, 0, 0);
        }

        $newSource = array();
        $source = $queryResult['result'];

        while ($enr = $this->fetchAssoc($source))
        {
            foreach ($enr as $key => $value)
            {
                $pipe = Strings::substringToEnd($key, '___pipe');

                if ($pipe != '')
                {
                    unset($enr[$key]);

                    $newKey = Strings::replace($pipe, '', $key);
                    $pipeFunction = lcfirst(Strings::replace('___pipe', '', $pipe));
                    $params = explode('_', $pipeFunction);
                    $pipeFunctionName = $params[0];

                    if (count($params) == 1)
                    {
                        $enr[$newKey] = Pipe::$pipeFunctionName($value);
                    }
                    else
                    {
                        $param1 = Convertors::hexToString($params[1]);
                        $enr[$newKey] = Pipe::$pipeFunctionName($value, $param1);
                    }
                }
            }

            $newSource[] = new DynamicObject($enr);
        }

        $this->cleanup($source);

        return new ResultList($newSource, $page, $pageSize, $queryResult['totalRows'], $queryResult['totalPages']);
    }

    public function checkErrors(String $query): void
    {
        $errno = mysqli_errno($this->dbLink);
        $error = mysqli_error($this->dbLink) . ' [' . $query . ']';

        if ($errno != 0)
        {
            if (Environment::isActiveProfileProd())
            {
                $error = 'Database query error';
            }

            throw new DatabaseErrorException($error);
        }
    }

    public function execute_CRUD_SQL(String $query): bool
    {
        $pResult = $this->mysqlQuery($query);
        if ($pResult === false)
        {
            throw new InvalidDatabaseOperationException($this->getLastError($this->dbLink));
        }

        return true;
    }

    public function getLastError()
    {
        return mysqli_error($this->dbLink);
    }

    protected function getQueryResult(String $query, int $page = 0, int $pageSize = 0, int $totalRows = 0): ?array
    {
        $totalPages = 0;

        if ($page > 0)
        {
            if ($totalRows == 0)
            {
                $cquery = "SELECT count(*) totalRows FROM (($query) q)";
                if ($this->dsConfig->getShowSQL()) $this->logger->debug($cquery);
                $tmp = $this->mysqlQuery($cquery);
                if ($tmp === FALSE) throw new InvalidDatabaseOperationException($this->getLastError());
                $rec = mysqli_fetch_assoc($tmp);
                $totalRows = $rec['totalRows'];
            }

            $out = Pagination::getPager($page, $pageSize, $totalRows);
            $query .= " LIMIT " . $out->startRow . ", " . $pageSize;
            $totalPages = $out->totalPages;
        }

        if ($this->dsConfig->getShowSQL()) $this->logger->debug($query);
        $result = $this->mysqlQuery($query);

        if ($result === false) return NULL;

        if ($totalRows == 0) $totalRows = mysqli_num_rows($result);
        if ($this->dsConfig->getShowSQL()) $this->logger->debug($totalRows . " rows selected with params {page:$page, pageSize:$pageSize}");

        return array(
                        'result' => $result,
                        'totalRows' => $totalRows,
                        'totalPages' => $totalPages);
    }

    protected function mysqlQuery(String $query)
    {
        $data = mysqli_query($this->dbLink, $query);
        $this->checkErrors($query);
        return $data;
    }

    protected function cleanup($resourceResult): void
    {
        mysqli_free_result($resourceResult);
    }

    protected function fetchAssoc($resourceResult)
    {
        try
        {
            return mysqli_fetch_assoc($resourceResult);
        } catch (Exception $e)
        {
            throw new Exception($this->getLastError());
        }
    }

    protected function fetchArray($resourceResult)
    {
        $pResult = @mysqli_fetch_array($resourceResult);
        if ($pResult === FALSE) throw new Exception(self::getLastError());
        return $pResult;
    }

    public function getConnectionIdentifier()
    {
        return $this->dbLink;
    }

    public function getLastInsertedRowId()
    {
        $pResult = @mysqli_insert_id($this->dbLink);
        if ($pResult === FALSE) throw new Exception($this->getLastError($this->dbLink));
        return $pResult;
    }

    public function isAlive(): bool
    {
        return mysqli_ping($this->dbLink);
    }

    public function close(): void
    {
        mysqli_close($this->dbLink);
    }

    public function getManagedTypes(): array
    {
        return array(
                        'INT',
                        'TIN',
                        'SMA',
                        'MED',
                        'BIG',
                        'DEC',
                        'FLO',
                        'DOU',
                        'REA',
                        'BIT',
                        'BOO',
                        'SER');
    }

    public function getDatabaseForeignKeysQuery(): String
    {
        return "SELECT i.TABLE_NAME tname, k.COLUMN_NAME cname, k.REFERENCED_TABLE_NAME rtname, k.REFERENCED_COLUMN_NAME rcname
    			FROM information_schema.TABLE_CONSTRAINTS i LEFT JOIN information_schema.KEY_COLUMN_USAGE k 
    			ON i.CONSTRAINT_NAME = k.CONSTRAINT_NAME WHERE i.CONSTRAINT_TYPE = 'FOREIGN KEY' AND 
    			i.TABLE_SCHEMA = '" . $this->getDataSourceConfig()->getDatabase() . "' ORDER BY i.TABLE_NAME, k.REFERENCED_TABLE_NAME";
    }

    public function getDatabaseTablesQuery(): String
    {
        return "SELECT TABLE_NAME tname FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='" . $this->getDataSourceConfig()->getDatabase() . "'";
    }

    public function getTableFieldsQuery(String $tableName): String
    {
        return "SHOW FULL COLUMNS FROM $tableName";
    }

    public function getDatabasePrimaryKeysQuery(String $tableName): String
    {
        return "SELECT TABLE_NAME tname, COLUMN_NAME cname FROM key_column_usage k " + "WHERE  table_schema = '" . $this->getDataSourceConfig()->getDatabase() . "' AND constraint_name = 'PRIMARY' ORDER BY TABLE_NAME";
    }
}