<?php
/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Orm\Relation;

class PrimaryKeysManager
{
    private $primaryKeysList = array();

    public function __construct($pKeysArray = array())
    {
        $this->primaryKeysList = $pKeysArray;
    }

    public function setAutoInsertId($id): void
    {
        foreach ($this->primaryKeysList as $column)
        {
            $column->set($id);
        }
    }

    public function isAUTO(): bool
    {
        if (count($this->primaryKeysList) > 1) return false;

        foreach ($this->primaryKeysList as $column)
        {
            return ($column->isPrimaryKey() && $column->getPrimaryKey()->isAUTO()) ? true : false;
        }
    }

    public function isEditMode(): bool
    {
        foreach ($this->primaryKeysList as $pk)
        {
            if (! in_array($pk->get(), array(0,'',NULL))) return false;
        }

        return true;
    }

    public function getWhereCondition(String $operator = '=', String $alias = ''): String
    {
        $where = '';
        foreach ($this->primaryKeysList as $pk)
        {
            $where .= ' AND (' . (($pk->isNumeric()) ? $alias . $pk->getName() . $operator . $pk->get() : $alias . $pk->getName() . $operator . "'" . $pk->get() . "'") . ')';
        }

        return trim(substr($where, 4, strlen($where)));
    }

    public function getKeyValueArray($enr): array
    {
        $array = array();
        foreach ($this->primaryKeysList as $pk)
        {
            $array[$pk->getName()] = $enr[$pk->getName()];
        }

        return $array;
    }

    public function getOrderByString(): String
    {
        $orderBy = '';
        foreach ($this->primaryKeysList as $pk)
        {
            $orderBy .= ', ' . $pk->getName;
        }

        return substr($orderBy, 1, strlen($orderBy));
    }

    public function setPrimaryKeys($arrayPKeys): void
    {
        $this->primaryKeysList = $arrayPKeys;
    }

    public function getPrimaryKeys(): array
    {
        return $this->primaryKeysList;
    }
}