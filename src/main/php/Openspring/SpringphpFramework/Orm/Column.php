<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Orm;

use Openspring\SpringphpFramework\Exception\InvalidArgumentException;
use Openspring\SpringphpFramework\Manager\ErrorManager;
use Openspring\SpringphpFramework\Manager\RM;
use Openspring\SpringphpFramework\Orm\Type\DataType;
use Openspring\SpringphpFramework\Orm\Type\PrimaryKey;
use Openspring\SpringphpFramework\Type\BaseError;
use Openspring\SpringphpFramework\Utils\Dates;
use Openspring\SpringphpFramework\Utils\EncryptionUtils;
use Openspring\SpringphpFramework\Utils\Strings;
use DateTime;

class Column
{
    private $entity = NULL;
    private $name = NULL;
    private $value = NULL;
    private $columnType = NULL;
    private $columnLen = 0;
    private $primaryKey = false;
    private $changed = false;
    private $unique = false;
    private $required = false;

    function __construct(?DaoSupport $entity, String $name = '', int $length = 0, ?String $type = NULL, ?PrimaryKey $primaryKey = null, bool $unique = false, bool $required = false, bool $default = NULL)
    {
        $this->setEntity($entity);
        $this->setName($name);
        $this->setColumnLen($length);
        $this->setColumnType($type);
        $this->setPrimaryKey($primaryKey);
        $this->setUnique($unique);
        $this->setRequired($required);
        $this->value = $default;
    }

    // entity
    protected function setEntity(?DaoSupport $entity)
    {
        $this->entity = $entity;
    }

    public function getEntity(): ?DaoSupport
    {
        return $this->entity;
    }

    // name
    public function setName($value): void
    {
        $this->name = $value;
    }

    public function getName(): String
    {
        return $this->name;
    }

    // primaryKey
    public function setPrimaryKey(?PrimaryKey $primaryKey): void
    {
        $this->primaryKey = $primaryKey;
        if ($this->isPrimaryKey() && $this->primaryKey->isAUTO())
        {
            $this->value = 0;
        }
    }

    public function getPrimaryKey(): ?PrimaryKey
    {
        return $this->primaryKey;
    }

    // is pk ?
    public function isPrimaryKey(): bool
    {
        return ($this->primaryKey != null);
    }

    // changed
    public function setChanged(bool $value): void
    {
        $this->changed = $value;
    }

    public function isChanged(): bool
    {
        return $this->changed;
    }

    // columnLen
    public function setColumnLen(int $value): void
    {
        $this->columnLen = ($value == '') ? 0 : $value;
    }

    public function getColumnLen(): int
    {
        return $this->columnLen;
    }

    // unique
    public function setUnique(bool $value): void
    {
        /*
         * TODO: to improve in case of compound key
         * $this->unique = ($this->isPrimaryKey()) ? true : $value;
         */
        $this->unique = $value;
    }

    public function isUnique(): bool
    {
        return $this->unique;
    }

    // required
    public function setRequired(bool $value)
    {
        $this->required = $value;
    }

    public function isRequired(): bool
    {
        return $this->required;
    }

    // columnType
    public function setColumnType(String $value): void
    {
        if ($value == NULL) $value = DataType::VARCHAR;

        DataType::validate($value);

        $this->columnType = $value;
        if ($this->columnType == DataType::GUID)
        {
            $this->value = EncryptionUtils::newGuid($this->getColumnLen());
            $this->setChanged(TRUE);
        }
    }

    public function getColumnType(): String
    {
        return $this->columnType;
    }

    // value
    public function setValue($value): void
    {
        $this->value = $value;
    }

    public function set($value): void
    {
        if (is_object($value) || is_array($value))
        {
            $this->setValue($value);
            $this->setChanged(true);
            return;
        }

        if ($this->value == $value)
        {
            if (is_null($this->value) && is_null($value))
            {
                return;
            }
        }

        $colType = ($this->columnType == DataType::FLOAT) ? DataType::INT : $this->columnType;

        // check required
        if ($this->isRequired() && $value == '')
        {
            ErrorManager::addError(new BaseError(RM::translate('System.valid-value-required-for-column') . ' ' . $this->getName() . ' [' . $value . ']', 100));
            return;
        }

        // check unicity
        if ($this->getEntity() != null && $this->isUnique())
        {
            $dbvalue = (in_array($this->getColumnType(), array(
                                                                DataType::BOOLEAN,
                                                                DataType::INT,
                                                                DataType::FLOAT))) ? $value : "'$value'";
            $resultList = null;
            if (DatabaseContext::getConnection() != NULL)
            {
                if (! $this->getEntity()->isEditMode())
                {
                    $resultList = $this->entity->where('(' . $this->getName() . '=' . $dbvalue . ')');
                }
                else
                {
                    $resultList = $this->entity->where('(' . $this->entity->getPrimaryWhereCondition('!=') . ') ' . 'AND (' . $this->getName() . '=' . $dbvalue . ')');
                }

                if ($resultList->totalRows > 0)
                {
                    ErrorManager::addError(new BaseError(RM::translate('System.unique-value-required-for-column') . ' ' . $this->getName() . ' [' . $value . ']', 101));
                    return;
                }
            }
        }

        // check length
        if ($this->getColumnLen() > 0 && strlen($value) > $this->getColumnLen())
        {
            ErrorManager::addError(new BaseError(RM::translate('System.max-length-is-{maxLength}-for-column', array(
                                                                                                                    'maxLength' => $this->getColumnLen())) . ' ' . $this->getName() . ' [' . strlen($value) . ']', 102));
            return;
        }

        switch ($colType)
        {
            case DataType::INT: // numeric

                // check type
                if (! is_numeric($value))
                {
                    ErrorManager::addError(new BaseError(RM::translate('System.number-value-expected-for-column') . ' ' . $this->getName() . ' [' . $value . ']', 103));
                    return;
                }

                $this->setValue($value);
                break;

            case DataType::BOOLEAN: // boolean
                if (! in_array(strtolower($value), array(
                                                            false,
                                                            true,
                                                            '0',
                                                            '1',
                                                            'false',
                                                            'true',
                                                            'yes',
                                                            'no')))
                {
                    ErrorManager::addError(new BaseError(RM::translate('System.boolean-value-expected-for-column') . ' ' . $this->getName() . ' [' . $value . ']', 104));
                    return;
                }

                $this->setValue($this->to01Value($value));
                break;

            case DataType::DATE: // date

                if (DateTime::createFromFormat('Y-m-d', $value) === FALSE)
                {
                    ErrorManager::addError(new BaseError(RM::translate('System.invalid-date-value-for-column') . ' ' . $this->getName() . ' [' . $value . ']', 105)); // Expected Y-m-d
                    return;
                }

                $this->setValue($value);
                break;

            case DataType::DATETIME: // datetime

                if (! Strings::isEmpty($value) && DateTime::createFromFormat('Y-m-d H:i:s', $value) === FALSE)
                {
                    ErrorManager::addError(new BaseError(RM::translate('System.invalid-datetime-value-for-column') . ' ' . $this->getName() . ' [' . $value . ']'), 106); // expected Y-m-d H:i:s
                    return;
                }

                $this->setValue($value);
                break;

            case DataType::HASH: // hashed

                // check length
                if (strlen(trim($value)) > $this->getColumnLen())
                {
                    ErrorManager::addError(new BaseError(RM::translate('System.max-length-is-{maxLength}-for-column', array(
                                                                                                                            'maxLength' => $this->getColumnLen())) . ' ' . $this->getName() . ' [' . strlen($value) . ']', 107));
                    return;
                }

                $this->setValue($this->hash($value));
                break;

            case DataType::JSON: // json

                if (is_string($value)) $value = json_decode($value);

                if (! is_array($value) && ! is_object($value))
                {
                    ErrorManager::addError(new BaseError(RM::translate('System.valud-must-be-array-or-object-for-column') . ' ' . $this->getName() . ' [' . $value . ']', 108));
                    return;
                }

                // check length
                if (strlen(json_encode($value)) > $this->getColumnLen())
                {
                    ErrorManager::addError(new BaseError(RM::translate('System.max-length-is-{maxLength}-for-column', array(
                                                                                                                            'maxLength' => $this->getColumnLen())) . ' ' . $this->getName() . ' [' . strlen($value) . ']', 109));
                    return;
                }

                $this->setValue(json_encode($value));
                break;

            default:

                // check length
                if (strlen($value) > $this->getColumnLen())
                {
                    ErrorManager::addError(new BaseError(RM::translate('System.max-length-is-{maxLength}-for-column', array(
                                                                                                                            'maxLength' => $this->getColumnLen())) . ' ' . $this->getName() . ' [' . strlen($value) . ']', 110));
                    return;
                }
                $this->setValue($value);
                // echo "[".$this->name."=".$this->value."]";
                break;
        }

        $this->setChanged(true);
    }

    public function get()
    {
        if ($this->isNumeric() && $this->value == NULL)
        {
            return 0;
        }

        switch ($this->columnType)
        {
            case DataType::JSON:
                return json_decode($this->value);

            default:
                return $this->value;
        }
    }

    public function getAsBoolean(): bool
    {
        return ($this->value == 1) ? true : false;
    }

    // increment
    public function increment(): Column
    {
        $this->set($this->value - (- 1));
        $this->setChanged(true);

        return $this;
    }

    // decrement
    public function decrement(): Column
    {
        $this->set($this->value - 1);
        $this->setChanged(true);

        return $this;
    }

    // update with current date
    public function setCurrentDate(): Column
    {
        $this->set(Dates::getCurrentDate());
        $this->setChanged(true);

        return $this;
    }

    // update with current date time
    public function setCurrentDateTime(): Column
    {
        $this->set(Dates::getCurrentDateTime());
        $this->setChanged(true);

        return $this;
    }

    // replaceAll
    public function replaceAll(String $needle, String $replacer): Column
    {
        $this->value = Strings::replace($needle, $replacer, $this->value);
        $this->setChanged(true);

        return $this;
    }

    // capitalize
    public function capitalize(): Column
    {
        $this->value = ucfirst($this->value);
        $this->setChanged(true);

        return $this;
    }

    // capitalize first word only
    public function capitalizeFirstWord(): Column
    {
        $this->lowercase();
        $this->value = ucfirst($this->value);
        $this->setChanged(true);

        return $this;
    }

    // capitalize all words
    public function capitalizeAllWords(): Column
    {
        $this->lowercase();
        $this->value = ucwords($this->value);
        $this->setChanged(true);

        return $this;
    }

    // uppercase
    public function uppercase(): Column
    {
        $this->value = strtoupper($this->value);
        $this->setChanged(true);

        return $this;
    }

    // lowercase
    public function empty(): Column
    {
        if ($this->isNumeric())
        {
            $this->value = 0;
        }
        else
        {
            $this->value = '';
        }
        $this->setChanged(true);

        return $this;
    }

    // lowercase
    public function lowercase(): Column
    {
        $this->value = strtolower($this->value);
        $this->setChanged(true);

        return $this;
    }

    // +
    public function plus(int $value): Column
    {
        if ($this->columnType != DataType::INT)
        {
            throw new InvalidArgumentException("-0010:Column.minus() - Forbidden operation for type " . DataType::getTypeName($this->columnType));
        }

        $this->value = $this->value - (- trim($value));
        $this->setChanged(true);

        return $this;
    }

    // -
    public function minus(int $value): Column
    {
        if ($this->columnType != DataType::INT)
        {
            throw new InvalidArgumentException("-0011:Column.minus() - Forbidden operation for type " . DataType::getTypeName($this->columnType));
        }

        $this->value = $this->value - $value;
        $this->setChanged(true);

        return $this;
    }

    // > ?
    public function greaterThan(int $value): bool
    {
        if (! $this->isNumeric()) return false;
        return $this->value > $value;
    }

    // >= ?
    public function greaterOrEqual(int $value): bool
    {
        if (! $this->isNumeric()) return false;
        return $this->value >= $value;
    }

    // < ?
    public function lessThan(int $value): bool
    {
        if (! $this->isNumeric()) return false;
        return $this->value < $value;
    }

    // <= ?
    public function lessOrEqual(int $value): bool
    {
        if (! $this->isNumeric()) return false;
        return $this->value <= $value;
    }

    // = ?
    public function equals(String $string): bool
    {
        if ($this->getColumnType() == DataType::HASH) $string = $this->hash($string);
        return (trim($this->value) == trim($string));
    }

    // alias for = ?
    public function is(String $string): bool
    {
        return $this->equals($string);
    }

    // is true
    public function isTrue(): bool
    {
        return trim($this->value) == 1 || trim($this->value) == true;
    }

    // is true
    public function isFalse(): bool
    {
        return trim($this->value) != 1 || trim($this->value) != true;
    }

    // = ? (Ignore Case)
    public function equalsIgnoreCase(String $string): bool
    {
        if ($this->getColumnType() == DataType::HASH) $string = $this->hash($string);
        return (strtolower(trim($this->value)) == strtolower(trim($string)));
    }

    // alias for = ? (Ignore Case)
    public function isIgnoreCase(String $string): bool
    {
        return $this->equalsIgnoreCase($string);
    }

    // isnum ?
    public function isNumeric(): bool
    {
        return ($this->columnType == DataType::INT || $this->columnType == DataType::FLOAT || $this->columnType == DataType::BOOLEAN);
    }

    // isEmpty ?
    public function isEmpty(): bool
    {
        if ($this->getColumnType() == DataType::INT)
        {
            return ($this->value == NULL || strlen(trim($this->value)) == 0 || trim($this->value) == 0) ? true : false;
        }

        return ($this->value == NULL || strlen(trim($this->value)) == 0) ? true : false;
    }

    // inArray ?
    public function inArray(Array $array): bool
    {
        return in_array($this->value, $array);
    }

    // isOnOf ?
    public function oneOf(): bool
    {
        return $this->inArray(func_get_args());
    }

    // length ?
    public function length(): int
    {
        return strlen($this->value);
    }

    public static function cast(Column $column): ?Column
    {
        return $column;
    }

    // toString()
    public function __toString(): String
    {
        return ($this->get() == NULL) ? '' : $this->get();
    }

    private function hash(String $string): String
    {
        return (strlen(trim($string)) == 0) ? $string : md5($string);
    }

    private function to01Value($value): int
    {
        return in_array(strtolower($value), array(
                                                    true,
                                                    '1',
                                                    'true',
                                                    'yes')) ? 1 : 0;
    }
}