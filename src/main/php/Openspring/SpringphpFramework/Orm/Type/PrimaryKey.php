<?php

/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Orm\Type;

class PrimaryKey
{
    protected $strategy = NULL;

    public function __construct($strategy)
    {
        $this->setStrategy($strategy);
    }

    public function setStrategy(int $strategy): void
    {
        GenerationType::validate($strategy);
        
        $this->strategy = (in_array($strategy, array(GenerationType::AUTO,GenerationType::MANUAL))) ? $strategy : GenerationType::AUTO;
    }

    public function getStrategy(): int
    {
        return $this->strategy;
    }

    public function isAUTO(): bool
    {
        return ($this->strategy == GenerationType::AUTO) ? true : false;
    }

    public function isMANUAL(): bool
    {
        return ($this->strategy == GenerationType::MANUAL) ? true : false;
    }
}