<?php
/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Orm\Type;

use Openspring\SpringphpFramework\Enumeration\EnumerationType;

class DataType extends EnumerationType
{
    const GUID = 0;
	const VARCHAR = 1;
    const INT = 2;
    const FLOAT = 3;
    const DATE = 4;
    const DATETIME = 5;
    const BOOLEAN = 6;
    const HASH = 7;
    const HTML = 8;
    const XML = 9;
    const JSON = 10;

    public static function getTypeName(int $t): String
    {
        $types = array('GUID', 'VARCHAR', 'INT', 'FLOAT', 'DATE', 'DATETIME', 'BOOLEAN', 'HASH', 'HTML', 'XML', 'JSON');
        
        return $types[$t];
    }
}