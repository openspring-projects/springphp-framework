<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Orm\DataSource;

use Openspring\SpringphpFramework\Context\Environment;
use Openspring\SpringphpFramework\Exception\ElementNotFoundException;
use Openspring\SpringphpFramework\IO\Path;
use Openspring\SpringphpFramework\Type\Dictionary;
use Openspring\SpringphpFramework\Utils\Arrays;
use Openspring\SpringphpFramework\Utils\Cast;
use Openspring\SpringphpFramework\Utils\Objects;
use Openspring\SpringphpFramework\Utils\XMLUtils;
use Exception;

class DataSourcesManager
{
    protected $fconfigs = NULL;
    protected $fpersistence = NULL;
    protected $dataSourcesPool = NULL;
    protected $dataSourceConfigs = array();

    public function __construct()
    {
        $this->fpersistence = Path::getResources('persistence.xml');
        if ($this->parsePersistenceFile())
        {
            $this->connectTargetDataSources();
        }
    }

    protected function connectTargetDataSources(): void
    {
        $this->dataSourcesPool = new Dictionary();
        $databasesConfigIds = Environment::getDatabaseTargets();

        foreach ($databasesConfigIds as $dsName)
        {
            $dsConfig = Cast::asDataSourceConfig(Arrays::get($this->dataSourceConfigs, $dsName, null));
            if ($dsConfig == null)
            {
                throw new ElementNotFoundException('No datasource config found for id ' . $dsName);
            }

            $this->dataSourcesPool->add($dsName, new BasicDataSource($dsConfig));
        }
    }

    public function hasAnyDefaultConnection(): bool
    {
        return ($this->dataSourcesPool != null && $this->dataSourcesPool->hasAny());
    }

    public function getDataSource(?String $dsName = NULL): ?DataSource
    {
        if ($this->dataSourcesPool == null) return null;

        $dsName = ($dsName == NULL) ? $this->getDefaultDataSourceName() : $dsName;

        if ($dsName == null) return null;

        return $this->dataSourcesPool->get($dsName);
    }

    public function getDefaultDataSourceName($dsName = NULL): ?String
    {
        if ($dsName != NULL || $this->dataSourcesPool == null || $this->dataSourcesPool->isEmpty()) return $dsName;

        foreach (array_keys($this->dataSourcesPool->toArray()) as $key)
        {
            return (string) $key;
        }

        return null;
    }

    public function loadPersistenceFile(String $filePath): bool
    {
        $this->fpersistence = $filePath;

        if ($this->parsePersistenceFile())
        {
            $this->connectTargetDataSources();
            return true;
        }

        return false;
    }

    protected function parsePersistenceFile(): bool
    {
        $this->dataSourceConfigs = array();

        // persistence.xml path
        if (! file_exists($this->fpersistence)) return false;

        $this->fconfigs = @simplexml_load_file($this->fpersistence);
        if ($this->fconfigs === FALSE)
        {
            throw new Exception('invalid database persistence file ' . $this->fpersistence);
        }

        // dsn
        if (empty($this->fconfigs->datasources))
        {
            throw new Exception('Invalid persistence file');
        }

        // show sql
        $show_sql = false;
        if (! empty($this->fconfigs->show_sql))
        {
            //TODO: use this value
            $show_sql = $this->fconfigs->show_sql;
        }

        foreach ($this->fconfigs->datasources->children() as $dsconfig)
        {
            $dsNameAttribs = $dsconfig->attributes();
            $dsName = (string) $dsNameAttribs['id'];

            if (! isset($dsNameAttribs['id']))
            {
                throw new Exception('Datasource with no id declared in ' . $this->fpersistence);
            }

            $attributes = "@attributes";
            $item = XMLUtils::toObject($dsconfig);
            if (is_object($item->password)) $item->password = '';
            unset($item->$attributes);

            $dataSourceConfig = new DataSourceConfig();
            Objects::autoMapObject($item, $dataSourceConfig);
            
            $dataSourceConfig->setShowSQL($show_sql);
            
            $this->dataSourceConfigs[$dsName] = $dataSourceConfig;
        }

        return true;
    }
}