<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Orm\DataSource;

/**
 * Physical data source config object
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
class DataSourceConfig
{
    private $dialect = '';
    private $host = '';
    private $database = '';
    private $port = '';
    private $user = '';
    private $password = '';
    private $tablePrefix = '';
    private $showSQL = false;

    public function getDialect(): String
    {
        return $this->dialect;
    }

    public function getHost(): String
    {
        return $this->host;
    }

    public function getDatabase(): String
    {
        return $this->database;
    }

    public function getPort(): String
    {
        return $this->port;
    }

    public function getUser(): String
    {
        return $this->user;
    }

    public function getPassword(): String
    {
        return $this->password;
    }

    public function getTablePrefix(): String
    {
        return $this->tablePrefix;
    }

    public function setDialect(String $dialect)
    {
        $this->dialect = $dialect;
    }

    public function setHost(String $host)
    {
        $this->host = $host;
    }

    public function setDatabase(String $database)
    {
        $this->database = $database;
    }

    public function setPort(String $port)
    {
        $this->port = $port;
    }

    public function setUser(String $user)
    {
        $this->user = $user;
    }

    public function setPassword(String $password)
    {
        $this->password = $password;
    }

    public function setTablePrefix(String $tablePrefix)
    {
        $this->tablePrefix = $tablePrefix;
    }

    public function getShowSQL()
    {
        return $this->showSQL;
    }

    public function setShowSQL($showSQL)
    {
        $this->showSQL = $showSQL;
    }
}