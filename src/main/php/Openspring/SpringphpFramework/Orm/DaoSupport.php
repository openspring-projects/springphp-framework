<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Orm;

use Openspring\SpringphpFramework\Exception\KeyNotFoundException;
use Openspring\SpringphpFramework\Exception\NullPointerException;
use Openspring\SpringphpFramework\Exception\Data\InvalidDatabaseOperationException;
use Openspring\SpringphpFramework\Log\LogFactory;
use Openspring\SpringphpFramework\Manager\ErrorManager;
use Openspring\SpringphpFramework\Orm\Relation\PrimaryKeysManager;
use Openspring\SpringphpFramework\Orm\Type\DataType;
use Openspring\SpringphpFramework\Type\Any;
use Openspring\SpringphpFramework\Type\DynamicObject;
use Openspring\SpringphpFramework\Type\ResultList;
use Openspring\SpringphpFramework\Utils\Arrays;
use Openspring\SpringphpFramework\Utils\DatabaseUtils;
use Openspring\SpringphpFramework\Utils\Objects;
use Openspring\SpringphpFramework\Utils\Strings;
use Exception;
use Openspring\SpringphpFramework\Orm\Query\Select;

abstract class DaoSupport
{
    // private vars
    var $state = RecordState::INSERT;

    // I:Insert|E:Edited|S:Selected|D:Deleted
    protected $pkeysManager = NULL;
    protected $columns = NULL;
    protected $connection = NULL;

    // public vars
    protected $tableName = NULL;
    protected $databaseTableAlias = NULL;

    // logger
    protected $logger;
    protected $lastQuery = '';

    public function __construct($tableName, $gConnection = NULL)
    {
        if ($this->getConnection(true) == null) return;

        $this->logger = LogFactory::getLogger(self::class);
        $this->logger->debug("Creating new entity {$tableName}");

        if ($this->getConnection() != null)
        {
            $this->tablePrefix = $this->getConnection()
                ->getDataSourceConfig()
                ->getTablePrefix();
            $this->tableName = Strings::replace('#__', $this->tablePrefix, $tableName);
            $this->databaseTableAlias = DatabaseUtils::getEntityAlias($this->tableName);
        }

        $this->state = RecordState::INSERT;
    }

    protected function initialize($params = NULL): void
    {
        $this->pkeysManager = new PrimaryKeysManager($this->selectVariables(array(
                                                                                    Column::class), 'isPrimaryKey', true));

        $this->columns = $this->selectVariables(array(
                                                        Column::class));

        if ($params == NULL || $params['values'] == NULL) return;

        foreach ($params['values'] as $key => $value)
        {
            if (! isset($this->$key)) continue;
            $this->$key->set($value);
        }
    }

    public function getTableRealName(): String
    {
        return Strings::replace('#__', $this->tablePrefix, $this->tableName);
    }

    /**
     * Return last run query
     *
     * @return String
     */
    public function getLastQuery(): String
    {
        return $this->lastQuery;
    }

    /**
     * Edit current record
     */
    public function edit(): void
    {
        $this->state = RecordState::UPDATE;
    }

    private function getConnection(bool $silent = false): ?Connection
    {
        if ($this->connection == NULL)
        {
            $this->connection = DatabaseContext::getConnection();
            if ($this->connection == NULL)
            {
                if ($silent) return null;

                throw new NullPointerException('No default valid database connection available. The connection object is null');
            }
        }

        return $this->connection;
    }

    /**
     * Load record by id to current object
     *
     * @param String $field
     * @param String|int $value
     * @return boolean
     */
    public function loadById(String $field, $value)
    {
        return $this->loadByIds(array(
                                        $field => $value));
    }

    /**
     * Load record by ids to current object
     *
     * @param array $arrayCond
     * @return bool
     */
    public function loadByIds(Array $arrayCond): bool
    {
        $query = "SELECT * FROM " . $this->tableName . " WHERE" . $this->getWhereCondition($arrayCond);
        $resultList = $this->executeSQL($query, '', false);

        return $this->loadFromResultList($resultList);
    }

    /**
     * Load to current object by maximum and an extra where conditions
     * Will run this query:
     * - SELECT * FROM {$tableName} WHERE {$field} = (SELECT MAX({$field}) FROM {$tableName} {$where})
     *
     * @param
     *            $field
     * @param
     *            $where
     * @return true if record found and false if record not found
     */
    public function loadByMax(String $field, String $where = "")
    {
        $where = $this->toWhere($where);
        $field = $this->getCorrectFieldName($field);

        $query = "SELECT * FROM {$this->tableName} WHERE {$this->$field->getName()}=(SELECT MAX({$this->$field->getName()}) FROM {$this->tableName}{$where})";
        $resultList = $this->executeSQL($query, $where, false);

        return $this->loadFromResultList($resultList);
    }

    /**
     * Return next max value of an attribute
     * Return 0 if record found and max value if found
     *
     * @param
     *            $field
     * @return int
     *
     */
    public function getNextMaxValue(String $field, $whereCond = ''): int
    {
        return $this->getMaxValue($field, $whereCond) + 1;
    }

    /**
     * Return max value of an attribute(0 if no record found and max value if found)
     *
     * @param
     *            $field
     * @return number
     */
    public function getMaxValue(String $field, $whereCond = ''): int
    {
        $query = "SELECT MAX($field) MAX_VALUE FROM " . $this->tableName . $this->toWhere($whereCond);
        $resultList = $this->executeSQL($query, '', false);

        if ($resultList == null || $resultList->totalRows == 0)
        {
            return 0;
        }

        $maxValue = $resultList->first()->MAX_VALUE;

        return $maxValue ? $maxValue : 0;
    }

    /**
     * Return field value by condition.
     * If NULL value found or no matchs found, it returns $alt
     *
     * @param $field: field
     *            to return its value
     * @param $alt: value
     *            to return on NULL value
     * @return mixed: returns the stored value type
     */
    public function getFieldValueByCond(String $field, $whereCond = '', $alt = NULL)
    {
        $query = "SELECT $field value FROM " . $this->tableName . $this->toWhere($whereCond);
        $resultList = $this->executeSQL($query, '', false);

        if ($resultList == null || $resultList->totalRows == 0)
        {
            return $alt;
        }

        $value = $resultList->first()->value;

        return ! $value ? $alt : $value;
    }

    /**
     * Select only some fields of this table and return the result as a ResultList object
     *
     * @param $fields: fields
     *            names to select
     * @param $whereCond: where
     *            condition
     * @return ResultList: returns result as a ResultList object
     */
    public function select(Array $fields, $whereCond = '', $page = 0, $pageSize = 0): ResultList
    {
        foreach ($fields as &$field)
        {
            $field = Select::asCamlCased($field);
        }

        $selectFields = implode(',', $fields);
        $query = "SELECT $selectFields FROM " . $this->tableName . $this->toWhere($whereCond);
        $resultList = $this->executeSQL($query, '', false);

        if ($resultList == null || $resultList->totalRows == 0)
        {
            return new ResultList(array(), $page, $pageSize, 0, 0);
        }

        return $resultList;
    }

    /**
     * Checks if a value exists ?
     *
     * @param
     *            $arrayCond
     * @return column value (type depends on the column type)
     */
    public function existById(String $field, $value): bool
    {
        return $this->existByIds(array(
                                        $field => $value));
    }

    /**
     * Checks if a value exists ?
     *
     * @param
     *            $arrayCond
     * @return column value (type depends on the column type)
     */
    public function existByIds(array $arrayCond): bool
    {
        $query = "SELECT COUNT(1) TOTAL FROM " . $this->tableName . " WHERE" . $this->getWhereCondition($arrayCond);
        $resultList = $this->executeSQL($query, '', false);

        return ($resultList != null && $resultList->first()->TOTAL > 0);
    }

    /**
     * Make a search by an entire sql where clause condition
     *
     * @param
     *            $whereCond
     * @return true if record found and false if record not found
     */
    public function where(String $whereCond, String $orderBy = '', int $page = 0, int $pageSize = 0): ResultList
    {
        $fields = $this->getFields();
        $fselect = array();
        foreach ($fields as $key)
        {
            $fselect[] = $this->$key->getName() . ' ' . $key;
        }

        $sf = implode(',', $fselect);

        $query = "SELECT {$sf} FROM {$this->tableName}" . $this->toWhere($whereCond) . $this->toOrderBy($orderBy);
        $resultList = $this->executeSQL($query, '', false, $page, $pageSize);

        if ($resultList == null)
        {
            return DatabaseContext::getEmptyResultList();
        }

        return $resultList;
    }

    /**
     * Return value of specified column name
     *
     * @param $field column
     *            name
     * @return column value (type depends on the column type)
     */
    public function get(String $field)
    {
        return $this->$field->get();
    }

    /**
     * Set value of specified column name
     *
     * @param $field column
     *            name
     * @param $value column
     *            value
     * @return void
     *
     */
    public function set(String $field, $value)
    {
        $this->$field->set($value);
    }

    /**
     * Execute natively an sql crud query.
     * It means run an Insert, Update or Delete query
     *
     * @param String $query
     *            crud sql query to executeSQL
     * @return false if not resuls found and an associative array in the other case
     */
    public function executeCrudSQL(String $query): bool
    {
        $this->lastQuery = $this->getNativeQuery($query);
        $this->logger->debug($this->lastQuery);

        return $this->getConnection()->executeNativeCrudSQL($this->lastQuery);
    }

    // TODO: to test
    public function deleteFrom(String $tableName, String $where): bool
    {
        $query = 'DELETE FROM ' . $tableName . ' WHERE ' . $where;
        return $this->executeCrudSQL($query);
    }

    // TODO: to test
    public function countOf(String $tableName, String $where): int
    {
        $query = 'SELECT 1 FROM ' . $tableName . ' WHERE ' . $where;
        $resultList = $this->executeSQL($query);

        return $resultList->totalRows;
    }

    private function getNativeQuery(String $query): String
    {
        return Strings::replace('#__', $this->tablePrefix, $query);
    }

    /**
     * Execute a query
     *
     * @param String $query
     * @param String $where
     * @param bool $isCRUD
     * @param int $page
     * @param int $pageSize
     * @return ResultList|NULL
     */
    protected function executeSQL(String $query, bool $isCRUD = false, int $page = 0, int $pageSize = 0): ?ResultList
    {
        $this->lastQuery = $this->getNativeQuery($query);
        $this->logger->debug($this->lastQuery);

        if ($isCRUD) $this->getConnection()->executeNativeSQL('SET NAMES utf8');

        return $this->getConnection()->executeNativeSQL($this->lastQuery, $page, $pageSize);
    }

    /**
     * Load data to current object from a resultlist object
     *
     * @param ResultList $resultList
     * @return bool
     */
    protected function loadFromResultList(?ResultList $resultList): bool
    {
        if ($resultList == null || $resultList->totalRows == 0)
        {
            return false;
        }

        foreach ($resultList as $item)
        {
            $resultRecord = $item;
            foreach ($this->columns as $col)
            {
                if (! Objects::hasProperty($resultRecord, $col->getName()))
                {
                    throw new KeyNotFoundException('Field not found ' . $col->getName() . ' in current selection on table ' . $this->tableName);
                }

                $col->setValue($resultRecord->{$col->getName()});
            }
        }

        $this->state = RecordState::UPDATE;

        return true;
    }

    /**
     * Return current object operation mode (edit or insert)
     *
     * @return true if edit mode and false id insert mode
     */
    public function isEditMode(): bool
    {
        return ($this->state == RecordState::UPDATE) ? true : false;
    }

    /**
     * Persist an entity into the physical database.
     * If editMode is enabled then the entity will be updated
     *
     * @return bool return true on success and false on failure
     */
    public function save(): bool
    {
        return (! $this->isEditMode()) ? $this->createEntity() : $this->updateEntity();
    }

    /**
     * Create a new record
     *
     * @return bool return true on success and false on failure
     */
    private function createEntity()
    {
        $fields = "";
        $values = "";
        $query = "";

        $this->logger->debug("Creating new row...");

        foreach ($this->columns as $col)
        {
            if (($col->isPrimaryKey() && $col->getPrimaryKey()->isAUTO()) || ! $col->isChanged()) continue;

            $fields .= $col->getName() . ",";

            if ($col->get() === null)
            {
                $values .= "null,";
                continue;
            }
            if ($col->isNumeric() || $col->getColumnType() == DataType::BOOLEAN)
            {
                $values .= $col->get() . ",";
                continue;
            }
            switch ($col->getColumnType())
            {
                default:
                    $values .= "'" . DatabaseUtils::cleanString($col->get(), $col->getColumnType()) . "',";
                    break;
            }
        }

        // adapting vars
        $fields = substr($fields, 0, strlen($fields) - 1);
        $values = substr($values, 0, strlen($values) - 1);
        $query = "INSERT INTO " . $this->tableName . "($fields) VALUES($values)";
        $result = $this->executeCrudSQL($query);
        if ($this->pkeysManager->isAuto())
        {
            $this->pkeysManager->setAutoInsertId($this->getConnection()
                ->getLastInsertedRowId());
        }

        $this->state = RecordState::UPDATE;

        return $result;
    }

    /**
     * Update current record
     *
     * @return bool return true on success and false on failure
     */
    private function updateEntity()
    {
        $this->logger->debug("Updating row...");

        $query = "";
        $where = NULL;
        // constructing fields and values lists
        foreach ($this->columns as $col)
        {
            if (($col->isPrimaryKey()) || ! $col->isChanged()) continue;

            if ($col->get() == null)
            {
                $query .= $col->getName() . "=null,";
                continue;
            }

            if ($col->isNumeric() || $col->getColumnType() == DataType::BOOLEAN)
            {
                $query .= $col->getName() . "=" . $col->get() . ",";
                continue;
            }
            switch ($col->getColumnType())
            {
                default:
                    $query .= $col->getName() . "='" . DatabaseUtils::cleanString($col->get(), $col->getColumnType()) . "',";
                    break;
            }
        }

        $where = $this->pkeysManager->getWhereCondition();
        if (trim($query) == '') return true;
        $query = substr($query, 0, strlen($query) - 1);
        $query = "UPDATE " . $this->tableName . " SET {$query} WHERE ({$where})";
        $r = $this->executeCrudSQL($query);
        $this->state = RecordState::UPDATE;

        return $r;
    }

    /**
     * Delete current record
     *
     * @throws Exception
     * @return bool return true on success and false on failure
     */
    public function delete(): bool
    {
        if (! $this->isEditMode())
        {
            throw new InvalidDatabaseOperationException('Can not delete record out of edited state !' . $this->state);
        }

        $q = "DELETE FROM {$this->tableName} WHERE {$this->pkeysManager->getWhereCondition()}";
        $r = $this->executeCrudSQL($q);
        $this->state = RecordState::DELETE;

        return $r;
    }

    /**
     * Return true if any of passed sub tables has a child of the main table
     * Call this function if all subtables are joined to this object
     * table with one key (compound keys are not supported in this method)
     * For parameter $subTablesNames: names can be given as tablename.key_field
     * Exemples of use:
     * - $userBo->deleteCascade(['#__comments', '#__posts']);
     * - $userBo->deleteCascade(['esapi_comments', 'esapi_posts']);
     * - $userBo->deleteCascade(['comments', 'posts']);
     * - $userBo->deleteCascade(['comments.user_id', 'posts.user_id']);
     * - $userBo->deleteCascade(['comments.userId', 'posts.userId ']);
     *
     * @param array $subTablesNames
     *            array of subtables names.
     * @return bool return true on success and false on failure
     */
    public function hasAnyChildrenIn(array $subTablesNames = array()): bool
    {
        foreach ($subTablesNames as $subTableName)
        {
            $dobject = $this->disqualifyFieldName($subTableName);
            $where = $this->pkeysManager->getWhereCondition();

            if (! Strings::isEmpty($dobject->field))
            {
                $pkeys = $this->pkeysManager->getPrimaryKeys();

                $fpkey = (count($pkeys) > 0) ? Column::cast(reset($pkeys)) : null;
                if ($fpkey != null)
                {
                    $where = Strings::replace($fpkey->getName(), $dobject->field, $where);
                }
            }

            ErrorManager::addSimpleError($subTableName);

            $query = "SELECT 1 FROM {$dobject->table} WHERE {$where}";
            $resultList = $this->executeSQL($query);
            if ($resultList->hasAny()) return true;
        }

        return false;
    }

    /**
     * Delete current record with cascade constraints
     * Call this function if all subtables are joined to this object
     * table with one key (compound keys are not supported in this method)
     * For parameter $subTablesNames: names can be given as tablename.key_field
     * Exemples of use:
     * - $userBo->deleteCascade(['#__comments', '#__posts']);
     * - $userBo->deleteCascade(['esapi_comments', 'esapi_posts']);
     * - $userBo->deleteCascade(['comments', 'posts']);
     * - $userBo->deleteCascade(['comments.user_id', 'posts.user_id']);
     * - $userBo->deleteCascade(['comments.userId', 'posts.userId ']);
     *
     * @param array $subTablesNames
     *            array of subtables names.
     * @return bool return true on success and false on failure
     */
    public function deleteCascade(array $subTablesNames = array()): bool
    {
        $savedAutoCommitMode = $this->getConnection()->getAutoCommit();

        if (! $this->isEditMode())
        {
            throw new Exception('Can not delete record out of edited state !' . $this->state);
        }

        // enable transactional mode
        $this->getConnection()->setAutoCommit(true);

        // delete constraints
        foreach ($subTablesNames as $subTableName)
        {
            $dobject = $this->disqualifyFieldName($subTableName);
            $dobject->table = $this->extractTableNameOnly($dobject->table);
            $where = $this->pkeysManager->getWhereCondition();

            if (! Strings::isEmpty($dobject->field))
            {
                $pkeys = $this->pkeysManager->getPrimaryKeys();

                $fpkey = (count($pkeys) > 0) ? Column::cast($pkeys[0]) : null;
                if ($fpkey != null)
                {
                    $where = Strings::replace($fpkey->getName(), $dobject->field, $where);
                }
            }

            $query = "DELETE FROM {$dobject->table} WHERE {$where}";
            if (! $this->executeCrudSQL($query))
            {
                return false;
            }
        }

        // delete main record
        $query = "DELETE FROM {$this->tableName} WHERE {$this->pkeysManager->getWhereCondition()}";
        $deleted = $this->executeCrudSQL($query);
        $this->state = RecordState::DELETE;

        if ($deleted)
        {
            $this->getConnection()->commit();
            $this->getConnection()->setAutoCommit($savedAutoCommitMode);
        }

        return $deleted;
    }

    private function extractTableAlias(String $tableName): String
    {
        $items = explode(' ', $tableName);
        return (count($items) == 2) ? trim($items[1]) . '.' : '';
    }
    
    private function extractTableNameOnly(String $tableName): String
    {
        $items = explode(' ', $tableName);
        return trim($items[0]);
    }

    /**
     * Delete records of current entity by condition
     *
     * @param String $whereCond
     * @return bool return true on success and false on failure
     */
    public function deleteItems(String $whereCond): bool
    {
        $query = "DELETE FROM {$this->tableName} WHERE {$whereCond}";
        return $this->executeCrudSQL($query);
    }

    /**
     * Construct where condition from array config
     *
     * @return String
     */
    public function getPrimaryWhereCondition(String $operator = '='): String
    {
        return $this->pkeysManager->getWhereCondition($operator);
    }

    /**
     * Convert to simple array
     *
     * @return array
     */
    public function toArray(): array
    {
        $out = array();
        $fields = $this->getFields();

        foreach ($fields as $key)
        {
            $pv = $this->convert(Objects::get($this, $key));
            if ($pv !== FALSE) $out[$key] = $pv;
        }

        return $out;
    }

    public function toSimpleObject(): Any
    {
        return Arrays::toObject($this->toArray());
    }

    protected function disqualifyFieldName(String $name): DynamicObject
    {
        $name = Strings::replace('#__', $this->tablePrefix, $name);
        if (! Strings::startsWith($name, $this->tablePrefix)) $name = $this->tablePrefix . $name;

        $items = explode('.', $name);

        if (count($items) == 1) return new DynamicObject(array(
                                                                'table' => $name,
                                                                'field' => ''));

        return new DynamicObject(array(
                                        'table' => $items[0],
                                        'field' => $items[1]));
    }

    /**
     * Quote a value if necessary
     *
     * @param Column $attrib
     * @return String
     */
    protected function getDatabaseValue(Column $attrib): String
    {
        return ($this->$attrib->getColumnType() == DataType::VARCHAR) ? "'" . DatabaseUtils::cleanString($this->$attrib->get(), $this->$attrib->getColumnType()) . "'" : $this->$attrib->get();
    }

    /**
     * Return a valid where clause
     *
     * @param String $where
     * @return String
     */
    protected function toWhere(String $where): String
    {
        if (Strings::containIgnoreCase($where, 'where')) return ' ' . trim($where);

        return (! Strings::isEmpty($where)) ? ' WHERE ' . $where : '';
    }

    /**
     * Return a valid orderby clause
     *
     * @param String $where
     * @return String
     */
    protected function toOrderBy(String $orderBy): String
    {
        if (Strings::containIgnoreCase($orderBy, 'order by')) return ' ' . trim($orderBy);

        return (! Strings::isEmpty($orderBy)) ? ' ORDER BY ' . $orderBy : '';
    }

    /**
     * Construct where condition from array config
     *
     * @param array $array
     * @return String
     */
    protected function getWhereCondition(array $array): String
    {
        $where = '';
        foreach ($array as $key => $val)
        {
            $key = $this->getCorrectFieldName($key);
            $this->$key->setValue($val);

            $n = $this->$key->isNumeric();
            $name = $this->$key->getName();
            $value = ($n) ? $this->$key->get() : Strings::getSecuredDbInput($this->$key->get());

            if ($val === NULL)
            {
                $where .= ' AND ' . "$name IS NULL";
                continue;
            }

            if ($this->$key->getColumnType() == DataType::HASH)
            {
                $value = md5($value);
            }

            $where .= ' AND ' . (($n) ? "$name={$value}" : "$name='{$value}'");
        }

        return substr($where, 4, strlen($where));
    }

    protected function getCorrectFieldName(String $name): String
    {
        if (property_exists($this, $name)) return $name;

        return Strings::snakeIt($name);
    }

    /**
     * Get class variables by variable class type
     *
     * @param array $classArray
     * @param String $method
     * @param
     *            $value
     * @return NULL[]
     */
    protected function selectVariables(array $classArray, String $method = NULL, $value = NULL)
    {
        $out = array();
        $vars = get_object_vars($this);
        foreach (array_keys($vars) as $key)
        {
            if (gettype($this->$key) == 'object' && in_array(get_class($this->$key), $classArray))
            {
                if ($method != NULL && $this->$key->$method() != $value) continue;
                $out[$key] = $this->$key;
            }
        }
        return $out;
    }

    protected function getFields()
    {
        $fields = array();
        $vars = get_object_vars($this);

        foreach (array_keys($vars) as $key)
        {
            if (! is_object($this->$key) || get_class($this->$key) != Column::class) continue;
            $fields[] = $key;
        }

        return $fields;
    }

    protected function convert($o)
    {
        switch (gettype($o))
        {
            case 'object':
                if (method_exists($o, 'toArray')) return $o->toArray();
                else Objects::objectToArray($o);
            default:
                return $o;
        }
    }
}