<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Orm;

use Openspring\SpringphpFramework\Exception\ClassNotFoundException;
use Openspring\SpringphpFramework\Exception\InvalidClassImplementationException;
use Openspring\SpringphpFramework\Orm\DataSource\DataSourceConfig;
use Openspring\SpringphpFramework\Orm\Driver\DatabaseDriver;
use Openspring\SpringphpFramework\Orm\Query\QueryBuilder;
use Openspring\SpringphpFramework\Reflection\ClassReflection;
use Openspring\SpringphpFramework\Type\ResultList;

class Connection
{
    private $dsConfig = NULL;
    private $driver = NULL;

    public function __construct(DataSourceConfig $dsConfig)
    {
        $this->dsConfig = $dsConfig;
        $driverClassName = $dsConfig->getDialect();

        if (! class_exists($driverClassName))
        {
            throw new ClassNotFoundException('Database driver not found ' . $driverClassName);
        }

        if (! ClassReflection::implements($driverClassName, DatabaseDriver::class))
        {
            throw new InvalidClassImplementationException('Invalid database driver ' . $driverClassName . ' it must implements interface ' . DatabaseDriver::class);
        }

        $this->driver = new $driverClassName($this->getDataSourceConfig());
    }

    public function getDataSourceConfig(): DataSourceConfig
    {
        return $this->dsConfig;
    }

    public function getDriver(): ?DatabaseDriver
    {
        return $this->driver;
    }

    public function executeNativeSQL(String $sql, int $page = 0, $pageSize = 0, $totalRows = 0): ?ResultList
    {
        return $this->getDriver()->execute_SQL($sql, $page, $pageSize, $totalRows);
    }

    public function executeNativeCrudSQL(String $crudSQL): bool
    {
        return $this->getDriver()->execute_CRUD_SQL($crudSQL);
    }

    public function getLastInsertedRowId(): int
    {
        return $this->getDriver()->getLastInsertedRowId();
    }

    public function getQueryBuilder(): QueryBuilder
    {
        return new QueryBuilder($this);
    }

    public function getDefaultClientCharset(): String
    {
        return $this->getDriver()->getDefaultClientCharset();
    }

    public function setDefaultClientCharset(String $value): void
    {
        $this->getDriver()->setDefaultClientCharset($value);
    }

    public function setAutoCommit(bool $value): void
    {
        $this->getDriver()->setAutocommit($value);
    }
    
    public function disableAutoCommit(): void
    {
        $this->setAutoCommit(false);
    }
    
    public function enableAutoCommit(bool $commit = false): void
    {
        if ($commit) $this->commit();
        $this->setAutoCommit(true);
    }

    public function getAutoCommit(): bool
    {
        return $this->getDriver()->getAutocommit();
    }

    public function commit(): bool
    {
        return $this->getDriver()->commit();
    }

    public function rollback(): bool
    {
        return $this->getDriver()->rollback();
    }

    public function close(): void
    {
        if ($this->getDriver() == null) return;

        $this->getDriver()->close();
    }

    public function isClosed(): bool
    {
        if ($this->getDriver() == null) return true;

        return ! $this->getDriver()->isAlive();
    }
}