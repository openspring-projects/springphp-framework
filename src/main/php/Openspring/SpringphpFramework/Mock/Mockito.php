<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Mock;

use Openspring\SpringphpFramework\Context\ApplicationContext;
use Openspring\SpringphpFramework\Enumeration\MockitoCategoryType;
use Openspring\SpringphpFramework\Exception\Bean\NoSuchBeanDefinitionException;
use Throwable;

/**
 * Mockito
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
class Mockito
{
    protected static $mockitoStore = null;
    protected static $key = null;
    protected static $debug = false;

    public static function initialize(bool $debug = false): void
    {
        static::$mockitoStore = ApplicationContext::getBeanOf(MockitoStore::class);
        if (static::$mockitoStore == null)
        {
            throw new NoSuchBeanDefinitionException('No qualifying bean found of type ' . MockitoStore::class);
        }

        if ($debug) static::enableDebug();
    }

    public static function when(String $key): Mockito
    {
        static::$key = $key;

        return new static();
    }

    public static function thenReturn($value): Mockito
    {
        if (static::isDebugEnabled()) echo PHP_EOL . "[MOCKITO] " . static::$key;

        static::getMockitoStore()->forKey(static::$key)->returnValue($value, MockitoCategoryType::VALUE);

        static::$key = null;

        return new static();
    }

    public static function thenThrow(Throwable $throwable): Mockito
    {
        if (static::isDebugEnabled()) echo PHP_EOL . "[MOCKITO] " . static::$key;

        static::getMockitoStore()->forKey(static::$key)->returnValue($throwable, MockitoCategoryType::EXCEPTION);

        static::$key = null;

        return new static();
    }

    public static function get(String $key)
    {
        $melement = static::getMockitoStore()->findByKey($key);

        if ($melement == null) return null;

        switch ($melement->getCategory())
        {
            case MockitoCategoryType::EXCEPTION:
                throw new $melement->getValue();

            default:
                return $melement->getValue();
        }
    }

    public static function enableDebug(): void
    {
        static::$debug = true;
    }

    public static function isDebugEnabled(): bool
    {
        return static::$debug;
    }

    protected static function getMockitoStore(): MockitoStore
    {
        return static::$mockitoStore;
    }
}