<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Exception\Http;

use Openspring\SpringphpFramework\Exception\BaseException;
use Openspring\SpringphpFramework\Exception\HttpException;
use Openspring\SpringphpFramework\Http\HttpStatus;

class HttpServerException extends BaseException implements HttpException
{
    private $httpCode;
    private $handlable;

    public function __construct(String $message = 'Internal server exception occured', int $code = 0, int $httpCode = HttpStatus::INTERNAL_SERVER_ERROR, bool $handlable = true)
    {
        parent::__construct($message, $code, null);

        $this->httpCode = $httpCode;
        $this->handlable = $handlable;
    }

    public function getHttpCode(): int
    {
        return $this->httpCode;
    }

    public function isHandlable(): bool
    {
        return $this->handlable;
    }
}