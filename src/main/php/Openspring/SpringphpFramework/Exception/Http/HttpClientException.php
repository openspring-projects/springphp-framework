<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Exception\Http;

use Openspring\SpringphpFramework\Exception\BaseException;
use Openspring\SpringphpFramework\Exception\HttpException;

class HttpClientException extends BaseException implements HttpException
{
    private $header;

    public function __construct(String $message, int $code = 0, $header = null)
    {
        parent::__construct($message, $code, null);

        $this->header = $header;
    }

    public function getUrl(): String
    {
        return (isset($this->header['url'])) ? $this->header['url'] : null;
    }

    public function getContentType(): String
    {
        return (isset($this->header['content_type'])) ? $this->header['content_type'] : null;
    }

    public function getHttpCode(): int
    {
        return (isset($this->header['http_code'])) ? $this->header['http_code'] : 0;
    }

    public function getHeaderSize(): int
    {
        return (isset($this->header['header_size'])) ? $this->header['header_size'] : 0;
    }

    public function getRequestSize(): int
    {
        return (isset($this->header['request_size'])) ? $this->header['request_size'] : 0;
    }

    public function getFiletime(): String
    {
        return (isset($this->header['filetime'])) ? $this->header['filetime'] : null;
    }

    public function getSslVerifyResult(): String
    {
        return (isset($this->header['ssl_verify_result'])) ? $this->header['ssl_verify_result'] : null;
    }

    public function getRedirectCount(): int
    {
        return (isset($this->header['redirect_count'])) ? $this->header['redirect_count'] : 0;
    }

    public function getTotalTime(): String
    {
        return (isset($this->header['total_time'])) ? $this->header['total_time'] : null;
    }

    public function getNamelookupTime(): String
    {
        return (isset($this->header['namelookup_time'])) ? $this->header['namelookup_time'] : null;
    }

    public function getConnectTime(): String
    {
        return (isset($this->header['connect_time'])) ? $this->header['connect_time'] : null;
    }

    public function getPretransferTime(): String
    {
        return (isset($this->header['pretransfer_time'])) ? $this->header['pretransfer_time'] : null;
    }

    public function getSizeUpload(): int
    {
        return (isset($this->header['size_upload'])) ? $this->header['size_upload'] : 0;
    }

    public function getSizeDownload(): int
    {
        return (isset($this->header['size_download'])) ? $this->header['size_download'] : 0;
    }

    public function getSpeedDownload(): String
    {
        return (isset($this->header['speed_download'])) ? $this->header['speed_download'] : null;
    }

    public function getSpeedUpload(): String
    {
        return (isset($this->header['speed_upload'])) ? $this->header['speed_upload'] : null;
    }

    public function getDownloadContentLength(): int
    {
        return (isset($this->header['download_content_length'])) ? $this->header['download_content_length'] : 0;
    }

    public function getUploadContentLength(): int
    {
        return (isset($this->header['upload_content_length'])) ? $this->header['upload_content_length'] : 0;
    }

    public function getStarttransferTime(): String
    {
        return (isset($this->header['starttransfer_time'])) ? $this->header['starttransfer_time'] : null;
    }

    public function getRedirectTime(): String
    {
        return (isset($this->header['redirect_time'])) ? $this->header['redirect_time'] : null;
    }

    public function getCertinfo(): String
    {
        return (isset($this->header['certinfo'])) ? $this->header['certinfo'] : null;
    }

    public function getPrimaryIp(): String
    {
        return (isset($this->header['primary_ip'])) ? $this->header['primary_ip'] : null;
    }

    public function getPrimaryPort(): String
    {
        return (isset($this->header['primary_port'])) ? $this->header['primary_port'] : null;
    }

    public function getLocalIp(): String
    {
        return (isset($this->header['local_ip'])) ? $this->header['local_ip'] : null;
    }

    public function getLocalPort(): String
    {
        return (isset($this->header['local_port'])) ? $this->header['local_port'] : null;
    }

    public function getRedirectUrl(): String
    {
        return (isset($this->header['redirect_url'])) ? $this->header['redirect_url'] : null;
    }

    public function getRequestHeader(): String
    {
        return (isset($this->header['request_header'])) ? $this->header['request_header'] : null;
    }

    public function isHandlable(): bool
    {
        return true;
    }
}