<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Exception;

use Openspring\SpringphpFramework\Context\ApplicationContext;
use Openspring\SpringphpFramework\Context\Environment;
use Openspring\SpringphpFramework\Core\Bean\ControllerAdvice;
use Openspring\SpringphpFramework\Enumeration\MediaType;
use Openspring\SpringphpFramework\Http\HttpStatus;
use Openspring\SpringphpFramework\Http\Request;
use Openspring\SpringphpFramework\Http\Response;
use Openspring\SpringphpFramework\Http\ResponseEntity;
use Openspring\SpringphpFramework\IO\Path;
use Openspring\SpringphpFramework\Log\LogFactory;
use Openspring\SpringphpFramework\Utils\ExceptionUtils;
use Openspring\SpringphpFramework\Utils\Strings;
use Exception;
use ReflectionClass;
use Throwable;

/**
 * Default global application exception handler.
 * You can bypass it by defining a ControllerAdvicer bean
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
class ExceptionHandler implements IExceptionHandler
{
    protected $httpCode = HttpStatus::INTERNAL_SERVER_ERROR;
    protected const DEFAULT_CADVC_METHOD = 'exceptionHandler';
    protected $logger;

    public function handle(Throwable $e)
    {
        Response::setMediaType(MediaType::APPLICATION_JSON_VALUE);
        Request::setAccept(MediaType::APPLICATION_JSON_VALUE);
        Request::setContentType(MediaType::APPLICATION_JSON_VALUE);

        $handlable = true;
        $responseEntity = null;
        $controllerAdvice = ApplicationContext::getBeanOf(ControllerAdvice::class);

        $this->logger = LogFactory::getLogger(self::class);

        if (! ($e instanceof Exception))
        {
            $e = new Exception(ExceptionUtils::getTraces($e), $e->getCode());
        }

        if ($e instanceof HttpException)
        {
            $handlable = $e->isHandlable();
            $this->httpCode = $e->getHttpCode();
        }

        if ($controllerAdvice != null && $handlable)
        {
            $handler = $this->getTargetMethod($controllerAdvice, $e);
            $responseEntity = ResponseEntity::cast($controllerAdvice->$handler($e));
        }

        if ($responseEntity instanceof ResponseEntity)
        {
            Response::setResponseEntity($responseEntity);
            return Response::render();
        }

        if (Response::getMediaType() == MediaType::APPLICATION_JSON_VALUE)
        {
            return $this->jsonHandler($e);
        }

        return $this->htmlHandler($e);
    }

    private function getTargetMethod(ControllerAdvice $controllerAdvice, $e): String
    {
        $matches = array();
        $reflector = new ReflectionClass($controllerAdvice);
        $methods = $reflector->getMethods();
        foreach ($methods as $method)
        {
            $parameters = $method->getParameters();
            if (count($parameters) > 0)
            {
                $type = $parameters[0]->getType();
                if ($type != null)
                {
                    $clazz = $type->getName();
                    if ($e instanceof $clazz)
                    {
                        $matches[] = $method->name;
                    }
                }
            }
        }

        if (count($matches) == 1) return $matches[0];
        foreach ($matches as $match)
        {
            if ($match != self::DEFAULT_CADVC_METHOD) return $match;
        }
    }

    private function jsonHandler(Throwable $e)
    {
        $details = (Environment::isActiveProfileProd()) ? '' : ExceptionUtils::getTraces($e);
        Response::setResponse('[' . $e->getCode() . '] ' . $e->getMessage() . ': ' . $details, $this->httpCode);
        Response::render();
    }

    private function htmlHandler(Throwable $e)
    {
        // log error
        $this->logger->error(ExceptionUtils::getTraces($e));

        // dev error handler
        $output = $this->style('Exception in ', 'style="color:#2b91af"') . $this->clean($e->getFile()) . ':' . $this->style($e->getLine(), 'style="color:#2b91af;"') . ' ' . $this->style($e->getMessage(), 'style="color:red;"') . $this->getLastPHPError() . '<BR/>';
        foreach ($e->getTrace() as $trace)
        {
            if (! isset($trace['file'])) continue;
            $fn = basename($trace['file']);
            $output .= $this->style('at ', 'style="color:#2b91af;padding-left:30px;"') . $this->clean($trace['file']) . '.' . $trace['function'] . "($fn:" . $this->style($trace['line'], 'style="color:#2b91af;"') . ")" . $this->style(ExceptionUtils::getArgs($trace['args']), 'style="color:#2b91af;"') . '<BR/>';
        }

        $this->printout($output);
    }

    private function getLastPHPError()
    {
        return '';
        $errors = error_get_last();
        $out = '';
        foreach ($errors as $e)
        {
            $out .= $e['message'];
        }
        return $out;
    }

    private function clean($path)
    {
        $hmPath = Strings::replace('\\\\', '\\', Path::getRoot());
        $string = Strings::replace($hmPath, '', $path);
        $string = Strings::replace('/', '.', $string);
        $string = Strings::replace('\\', '.', $string);
        $string = Strings::replace('.php', '', $string);
        $string = Strings::replace('.php', '', $string);

        return $string;
    }

    private function style($s, $style = '')
    {
        return '<span ' . $style . '>' . $s . '</span>';
    }

    private function printout($output)
    {
        echo '<div style="background-color:#eff0f1;padding:15px;border-left:15px solid red;font-family:Consolas,Menlo,Monaco,Lucida Console,Liberation Mono,DejaVu Sans Mono,Bitstream Vera Sans Mono,Courier New,monospace,sans-serif;">' . $output . '</div>';
    }
}