<?php
/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Orm;

use Openspring\SpringphpFramework\Core\Initializable;
use Openspring\SpringphpFramework\Orm\DataSource\DataSourcesManager;
use Openspring\SpringphpFramework\Orm\Query\QueryBuilder;
use Openspring\SpringphpFramework\Orm\Query\WhereBuilder;
use Openspring\SpringphpFramework\Type\ResultList;

/**
 * Database context
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
class DatabaseContext implements Initializable
{
    private static $dataSourcesManager = NULL;

    public static function initialize(): void
    {
        static::setDataSourcesManager(NULL);
        
        $dataSourcesManager = new DataSourcesManager();
        if ($dataSourcesManager->hasAnyDefaultConnection())
        {
            static::setDataSourcesManager($dataSourcesManager);
        }        
    }

    public static function setDataSourcesManager(?DataSourcesManager $dataSourcesManager): void
    {
        static::$dataSourcesManager = $dataSourcesManager;
    }

    public static function getDataSourcesManager(): ?DataSourcesManager
    {
        return static::$dataSourcesManager;
    }

    public static function getConnection(?String $dsName = NULL): ?Connection
    {
        if (static::getDataSourcesManager() == null) return null;

        $dataSource = static::getDataSourcesManager()->getDataSource($dsName);
        if ($dataSource == null) return $dataSource;
        
        return $dataSource->getConnection();
    }

    public static function getEmptyResultList(): ResultList
    {
        return static::getResultList(array());
    }
	
	public static function getResultList(Array $array = null): ResultList
    {
        return new ResultList($array, 1, 10, 0, 0);
    }

    public static function getQueryBuilder(?String $dsName = NULL): ?QueryBuilder
    {
        return static::getConnection($dsName)->getQueryBuilder();
    }

    public static function getQueryBuilderWhere(?String $dsName = NULL, $where): ?QueryBuilder
    {
        return static::getQueryBuilder($dsName)->where($where);
    }

    public static function getWhereBuilder(): ?WhereBuilder
    {
        return new WhereBuilder();
    }
}