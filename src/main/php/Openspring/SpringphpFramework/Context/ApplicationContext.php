<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Context;

use Openspring\SpringphpFramework\Core\SpringphpApplication;
use Openspring\SpringphpFramework\Core\Stereotype\Bean;
use Openspring\SpringphpFramework\Core\Stereotype\BeanElement;
use Openspring\SpringphpFramework\Core\Stereotype\BeanInstanciator;
use Openspring\SpringphpFramework\Core\Stereotype\IBean;
use Openspring\SpringphpFramework\Enumeration\BeanScope;
use Openspring\SpringphpFramework\Exception\ClassNotFoundException;
use Openspring\SpringphpFramework\Exception\InvalidJsonStringException;
use Openspring\SpringphpFramework\Exception\NullPointerException;
use Openspring\SpringphpFramework\Exception\Bean\BeanCreationException;
use Openspring\SpringphpFramework\Exception\Bean\InvalidBeanDefinitionException;
use Openspring\SpringphpFramework\Exception\Bean\NoSuchBeanDefinitionException;
use Openspring\SpringphpFramework\IO\FileUtils;
use Openspring\SpringphpFramework\IO\Path;
use Openspring\SpringphpFramework\IO\ProjectFile;
use Openspring\SpringphpFramework\Reflection\ClassReflection;
use Openspring\SpringphpFramework\Type\DynamicObject;
use Openspring\SpringphpFramework\Utils\ClassUtils;
use Openspring\SpringphpFramework\Utils\Dates;
use Openspring\SpringphpFramework\Utils\Rest;
use Openspring\SpringphpFramework\Utils\Strings;
use Openspring\SpringphpFramework\Exception\Http\InternalServerErrorException;

/**
 * Global application context
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
class ApplicationContext
{
    protected static $beansInstanciationStack = array();
    protected static $beansDIStack = array();
    protected static $application = NULL;
    protected static $controllerList = array();
    protected static $startupDate;
    protected static $contextLoaded = false;
    protected static $allowBeanOverriding = true;

    // beans
    protected static $beansRepo = array();
    protected static $beans = array();
    protected static $mapping = null;
    public static $baseBeanDirs = array();
    public static $excludeBeanDirs = array();
    public static $currentMappingItem = null;

    public static function initialize(): void
    {
        static::$startupDate = Dates::getCurrentDateTime();
        ProjectFile::load();
        static::loadContext();
    }

    public static function addBeanAutowiringKey(String $hostClazz, String $wiredClazz): void
    {
        $key = '[' . $hostClazz . ' requires ' . $wiredClazz . ']';
        if (isset(self::$beansDIStack[$key]))
        {
            $stack = self::getCyclicDIPath();
            $error = 'Cyclic dependecy injection detected between classes ' . $hostClazz . ' and ' . $wiredClazz . '.';
            throw new BeanCreationException($error . ' Caused by stack ' . $stack .  '. The last DI restart the whole stack over and over. You need to resolve this');
        }
        self::$beansDIStack[$key] = $wiredClazz;
    }

    public static function getCyclicDIPath(): String
    {
        $stack = array();
        $c = count(self::$beansDIStack);
        if ($c == 0) return '';

        $clazz = end(self::$beansDIStack);
        $copy = false;
        foreach (self::$beansDIStack as $key => $value)
        {
            if ($clazz == $value) $copy = true;
            if ($copy) $stack[] = $key;
        }
        
        return implode(' ', $stack);
    }

    public static function setScanDirs(array $scanDirs, array $excludeDirs = array()): void
    {
        static::$baseBeanDirs = $scanDirs;
        static::$excludeBeanDirs = $excludeDirs;
    }

    public static function getScanBeanDirs(String $basePath): array
    {
        $dirs = array();
        foreach (static::$baseBeanDirs as $dir)
        {
            $dirs[] = FileUtils::standarizePath($basePath . '/' . $dir);
        }

        return $dirs;
    }

    public static function getExcludeBeanDirs(String $basePath): array
    {
        $dirs = array();
        foreach (static::$excludeBeanDirs as $dir)
        {
            $dirs[] = FileUtils::standarizePath($basePath . '/' . $dir);
        }

        return $dirs;
    }

    public static function isBeanOverridingAllowed(): bool
    {
        return ApplicationContext::$allowBeanOverriding;
    }

    public static function allowBeanOverriding(bool $allowBeanOverriding): void
    {
        ApplicationContext::$allowBeanOverriding = $allowBeanOverriding;
    }

    public static function getApplicationName(): String
    {
        return Environment::getApplicationName();
    }

    public static function getStartupDate(): String
    {
        return static::$startupDate;
    }

    public static function loadContext(bool $force = false): void
    {
        if ($force || ! static::$contextLoaded)
        {
            static::reloadContext('/.context');
        }
    }

    public static function setContext(String $ctxPath): void
    {
        static::reloadContext($ctxPath);
    }

    private static function reloadContext(String $ctxPath): void
    {
        $fcontextFile = Path::getResources($ctxPath);

        if (! file_exists($fcontextFile))
        {
            Rest::scanForRestControllers();
        }

        $ctx = json_decode(file_get_contents($fcontextFile));

        if ($ctx == null)
        {
            throw new InternalServerErrorException('We are sorry for this unmanaged behavior. Please reload your page and try again');
        }

        static::$beansRepo = property_exists($ctx, 'beans') ? $ctx->beans : array();
        $mapping = array();
        foreach ($ctx->mapping as $a)
        {
            if (count($a) != 8)
            {
                throw new InvalidJsonStringException('Invalid context file ' . $fcontextFile . ' at item ' . json_encode($a));
            }

            $mapping[] = new DynamicObject(array(
                                                    "methods" => $a[0],
                                                    "authGroups" => $a[1],
                                                    "authRoles" => $a[2],
                                                    "clazz" => $a[3],
                                                    "path" => $a[4],
                                                    "pathVariables" => $a[5],
                                                    "wcomp" => $a[6],
                                                    "action" => $a[7]));
        }

        static::setMapping($mapping);
        static::$contextLoaded = true;
    }

    public static function setApplication(SpringphpApplication $application)
    {
        self::$application = $application;
    }

    public static function getApplication(): ?SpringphpApplication
    {
        return self::$application;
    }

    public static function addBeanSingleton($bean): void
    {
        static::addBean($bean, BeanScope::SINGLETON);
    }

    public static function addBeanPrototype($clazz): void
    {
        static::addBean($clazz, BeanScope::PROTOTYPE);
    }

    public static function addBean($bean, int $type = BeanScope::SINGLETON): void
    {
        if (is_string($bean))
        {
            if (! class_exists($bean)) throw new ClassNotFoundException('No class definition found ' . $bean);
            if (! ClassReflection::implements($bean, IBean::class)) throw new InvalidBeanDefinitionException('Invalid bean ' . $bean . '. A valid bean must implements interface ' . IBean::class . ' or extends class ' . Bean::class);
        }

        if (static::getBeanOf($bean) != null)
        {
            if (! static::isBeanOverridingAllowed())
            {
                throw new BeanCreationException('Bean already exists ' . get_class($bean));
            }
        }

        $key = (! is_string($bean)) ? get_class($bean) : $bean;
        static::$beans[$key] = new BeanElement($key, $bean, $type);
    }

    public static function removeBean($clazz): void
    {
        foreach (static::getBeans() as $key => $beanElement)
        {
            $bean = static::getBeanInstance($clazz, $beanElement);
            if ($bean != null) unset(static::$beans[$key]);
        }
    }

    public static function getBeans(): ?array
    {
        return static::$beans;
    }

    public static function getBeansDIStack(): ?array
    {
        return static::$beansDIStack;
    }

    public static function getBeansInstanciationStack(): ?array
    {
        return static::$beansInstanciationStack;
    }

    public static function getBeanOf($clazz, bool $required = false): ?Bean
    {
        static::checkClass($clazz);

        foreach (static::getBeans() as $beanElement)
        {
            $bean = static::getBeanInstance($clazz, $beanElement);
            if ($bean != null)
            {
                $beanElement->setInstance($bean);
                return $bean;
            }
        }

        if ($required)
        {
            throw new NoSuchBeanDefinitionException('No such bean definition ' . $clazz);
        }

        return null;
    }

    public static function getBeansOf($clazz): ?array
    {
        static::checkClass($clazz);
        $beans = array();
        foreach (static::getBeans() as $beanElement)
        {
            $bean = static::getBeanInstance($clazz, $beanElement);
            if ($bean != null)
            {
                $beanElement->setInstance($bean);
                $beans[] = $bean;
            }
        }

        return $beans;
    }

    public static function checkClass($clazz): void
    {
        if (is_object($clazz)) return;
        if (Strings::isEmpty($clazz))
        {
            throw new NullPointerException('Bean class name is null');
        }
    }

    public static function getBeansRepoClass(String $className): ?String
    {
        foreach (static::$beansRepo as $clazz)
        {
            if (Strings::endsWith($clazz, '\\' . $className) || $clazz == $className) return $clazz;
        }

        return null;
    }

    private static function getBeanInstance($clazz, BeanElement $be)
    {
        if (is_string($clazz))
        {
            return static::instantiateClassIfMatched($clazz, $be);
        }

        if (! $be->isSingleton()) return null;

        return (static::classMatches($clazz, $be->getValue())) ? static::getBeanElementValue($be) : null;
    }

    private static function classMatches($clazz, $beValue): bool
    {
        if (! is_string($beValue)) // mixed instance
        {
            return $beValue->instanceOf($clazz);
        }
        else // means $beValue is string
        {
            $claz = is_string($clazz) ? $clazz : get_class($clazz);
            return ($beValue == $claz || is_subclass_of($beValue, $claz));
        }
    }

    private static function classMatchesByStrings(String $clazz, String $bsValue): ?IBean
    {
        return ($bsValue == $clazz || is_subclass_of($bsValue, $clazz));
    }

    private static function instantiateClassIfMatched(String $clazz, BeanElement $be): ?IBean
    {
        return ($be->getKey() == $clazz || is_subclass_of($be->getKey(), $clazz)) ? static::getBeanElementValue($be) : null;
    }

    private static function getBeanElementValue(BeanElement $beanElement)
    {
        if (! is_string($beanElement->getValue())) return $beanElement->getValue();

        $clazz = $beanElement->getKey();

        // create a new object
        if (count(ClassUtils::getConstructorParameters($clazz)) == 0)
        {
            if (Environment::isActiveProfileTest())
            {
                self::$beansInstanciationStack[] = 'new ' . $clazz;
            }

            return new $clazz();
        }

        if (! is_subclass_of($clazz, BeanInstanciator::class))
        {
            throw new BeanCreationException('Can not instantiate bean ' . $clazz . ': To resolve this problem, implement this interface ' . BeanInstanciator::class);
        }

        return $clazz::getInstance();
    }

    public static function getMapping(): array
    {
        return static::$mapping;
    }

    public static function setMapping(Array $mapping): void
    {
        static::$mapping = $mapping;
    }
}