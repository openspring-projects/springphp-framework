<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Context;

use Openspring\SpringphpFramework\Core\Initializable;
use Openspring\SpringphpFramework\Enumeration\ProfileType;
use Openspring\SpringphpFramework\IO\FileUtils;
use Openspring\SpringphpFramework\IO\PropertiesFile;
use Openspring\SpringphpFramework\Log\LogLevel;
use Openspring\SpringphpFramework\Utils\Strings;
use ErrorException;

/**
 * Application environnement properties
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
class Environment implements Initializable
{
    const DEFUALT_RUNCONFIG_PATH = 'src/main/resources/';
    const DEFUALT_TESTCONFIG_PATH = 'src/test/resources/';
    const SPRINGPHP_CONFIG_RUN = 'springphp.config.run';
    const SPRINGPHP_CONFIG_TEST = 'springphp.config.test';
    const SPRINGPHP_LOG_ENABLE = 'springphp.log.enable';
    const SPRINGPHP_LOG_FILE = 'springphp.log.file';
    const SPRINGPHP_LOG_FILE_MAXSIZE = 'springphp.log.file.maxsize';
    const SPRINGPHP_LOG_LEVEL = 'springphp.log.level';
    const SPRINGPHP_LOG_FORMAT = 'springphp.log.format';
    const SPRINGPHP_APPLICATION_NAME = 'springphp.application.name';
    const SPRINGPHP_PROFILE_ACTIVE = 'springphp.profile.active';
    const SPRINGPHP_PATH_ROOT = 'springphp.path.root';
    const SPRINGPHP_PATH_PUBLIC = 'springphp.path.public';
    const SPRINGPHP_PATH_SOURCES = 'springphp.path.sources';
    const SPRINGPHP_PATH_TESTS = 'springphp.path.tests';
    const SPRINGPHP_DATABASE_TARGETS = 'springphp.database.targets';
    const SPRINGPHP_APPLICATION_CONTEXT_PATH = 'springphp.application.context.path';
    const SPRINGPHP_AUTHENTIFICATION_JWT_KEY = 'springphp.authentification.jwt.key';
    private static $properties = array();

    public static function initialize(): void
    {
        if (Strings::isEmpty(self::getRunConfigPath())) self::setRunConfigPath(static::DEFUALT_RUNCONFIG_PATH);
        if (Strings::isEmpty(self::getTestConfigPath())) self::setTestConfigPath(static::DEFUALT_TESTCONFIG_PATH);
    }

    public static function getApplicationPropertiesFile(String $applicationPropertiesFilePath)
    {
        if (! file_exists($applicationPropertiesFilePath)) return null;
        return self::loadFile($applicationPropertiesFilePath, false);
    }

    public static function load(String $propertiesFileBaseDirectory = null): void
    {
        self::setAllProperty(static::getProperties($propertiesFileBaseDirectory));
    }

    public static function getProperties(String $propertiesFileBaseDirectory = null, ?String $envId = null): array
    {
        $configBase = self::getRootPath(self::getConfigPath());
        if (! Strings::isEmpty($propertiesFileBaseDirectory))
        {
            $configBase = $propertiesFileBaseDirectory;
            static::setTestConfigPath($propertiesFileBaseDirectory);
        }

        $envId = (Strings::isEmpty($envId)) ? strtolower(static::getActiveProfile()) : $envId;

        $envPropFile = FileUtils::standarizePath($configBase . "/{$envId}-application.properties");
        $defaultPropFile = FileUtils::standarizePath($configBase . "/application.properties");

        if (FileUtils::exists($envPropFile)) $defaultPropFile = $envPropFile;

        return self::loadFile($defaultPropFile);
    }

    private static function loadFile(String $configFile, bool $alterAppCore = true)
    {
        $propertiesFile = new PropertiesFile($configFile, true);

        return $propertiesFile->getProperties();
    }

    public static function setRunConfigPath($runConfigPath): void
    {
        self::$properties[self::SPRINGPHP_CONFIG_RUN] = $runConfigPath;
    }

    public static function getRunConfigPath()
    {
        return self::getProperty(self::SPRINGPHP_CONFIG_RUN);
    }

    public static function setTestConfigPath($testConfigPath): void
    {
        self::$properties[self::SPRINGPHP_CONFIG_TEST] = $testConfigPath;
    }

    public static function getTestConfigPath()
    {
        return self::getProperty(self::SPRINGPHP_CONFIG_TEST);
    }

    public static function getConfigPath()
    {
        return (self::isProfileTest()) ? self::getTestConfigPath() : self::getRunConfigPath();
    }

    public static function isProfileTest(): bool
    {
        return (self::getActiveProfile() == ProfileType::TEST);
    }

    public static function setActiveProfile(String $profile)
    {
        if (! ProfileType::isValid($profile))
        {
            throw new ErrorException('A valid profile must be one of these values: RUN, TEST. ' . $profile . ' given');
        }

        self::setProperty(self::SPRINGPHP_PROFILE_ACTIVE, $profile);
    }

    public static function isLogEnabled(): bool
    {
        return Strings::toBoolean(self::getProperty(self::SPRINGPHP_LOG_ENABLE, 'false'));
    }

    public static function setLogFile(String $fpath): void
    {
        self::setProperty(self::SPRINGPHP_LOG_FILE, $fpath);
    }

    public static function getLogFile()
    {
        return self::getProperty(self::SPRINGPHP_LOG_FILE, 'logs/server_#{date}.log');
    }

    public static function setLogFileMaxSize(int $sizeInKO): void
    {
        self::setProperty(self::SPRINGPHP_LOG_FILE_MAXSIZE, $sizeInKO);
    }

    public static function getLogFileMaxSize(): int
    {
        return self::getProperty(self::SPRINGPHP_LOG_FILE_MAXSIZE, 1024);
    }

    /**
     * supported values : [ERROR | WARNING | INFO | DEBUG]
     *
     * @param bool $value
     */
    public static function setLogLevel(String $value): void
    {
        self::setProperty(self::SPRINGPHP_LOG_LEVEL, $value);
    }

    public static function getLogLevel()
    {
        return self::getProperty(self::SPRINGPHP_LOG_LEVEL, LogLevel::INFO);
    }

    public static function enableLog(bool $value): void
    {
        self::setProperty(self::SPRINGPHP_LOG_ENABLE, $value ? 'true' : 'false');
    }

    public static function getLogEnable()
    {
        return self::getProperty(self::SPRINGPHP_LOG_ENABLE, 'false');
    }

    public static function getLogFormat()
    {
        return self::getProperty(self::SPRINGPHP_LOG_FORMAT, '[%d][%l][%c]%m');
    }

    // log output format: known var names: %d (date) ; %l (log level) ; %m (message)
    public static function setLogFormat(String $format): void
    {
        self::setProperty(self::SPRINGPHP_LOG_FORMAT, $format);
    }

    public static function getDisplayName()
    {
        return self::getProperty(self::SPRINGPHP_APPLICATION_NAME);
    }

    public static function getActiveProfile()
    {
        return self::getProperty(self::SPRINGPHP_PROFILE_ACTIVE);
    }

    public static function isActiveProfile(String $profile): bool
    {
        return (self::getActiveProfile() == $profile);
    }

    public static function isActiveProfileDev(): bool
    {
        return (self::getActiveProfile() == ProfileType::RUN_DEV);
    }

    public static function isActiveProfileProd(): bool
    {
        return (self::getActiveProfile() == ProfileType::RUN_PROD);
    }

    public static function isActiveProfileTest(): bool
    {
        return (self::getActiveProfile() == ProfileType::TEST);
    }

    public static function getRootPath($subDirectory = '')
    {
        return self::getProperty(self::SPRINGPHP_PATH_ROOT) . $subDirectory;
    }

    public static function setRootPath($rootPth = '')
    {
        self::setProperty(self::SPRINGPHP_PATH_ROOT, $rootPth);
    }

    public static function getPublicPath($subDirectory = '')
    {
        return self::getProperty(self::SPRINGPHP_PATH_PUBLIC) . $subDirectory;
    }

    public static function getMainSrcPath($subDirectory = '')
    {
        return self::getProperty(self::SPRINGPHP_PATH_SOURCES) . $subDirectory;
    }

    public static function getTestSrcPath($subDirectory = '')
    {
        return self::getProperty(self::SPRINGPHP_PATH_TESTS) . $subDirectory;
    }

    public static function getSourcesPath()
    {
        return (self::isProfileTest()) ? self::getTestSrcPath() : self::getMainSrcPath();
    }

    public static function getDatabaseTargets()
    {
        $value = self::getProperty(self::SPRINGPHP_DATABASE_TARGETS);
        if (Strings::isEmpty($value)) return array();

        return explode(',', $value);
    }

    public static function getContextRoot($subDirectory = '')
    {
        return self::getProperty(self::SPRINGPHP_APPLICATION_CONTEXT_PATH) . $subDirectory;
    }

    public static function getApplicationName()
    {
        return self::getProperty(self::SPRINGPHP_APPLICATION_NAME);
    }

    public static function getJWTKey(): String
    {
        return self::getProperty(self::SPRINGPHP_AUTHENTIFICATION_JWT_KEY);
    }

    public static function setAllProperty($props)
    {
        if ($props == null) return;
        foreach ($props as $name => $value)
        {
            self::setProperty($name, $value);
        }
    }

    public static function setProperty(String $pname, $pvalue)
    {
        self::$properties[$pname] = $pvalue;
    }

    public static function getProperty(String $property, $default = '')
    {
        return (self::propertyExists($property)) ? self::$properties[lcfirst($property)] : $default;
    }

    public static function getPropertyArray(String $property, $sep)
    {
        $value = (isset(self::$properties[lcfirst($property)])) ? self::$properties[lcfirst($property)] : '';
        return explode($sep, $value);
    }

    public static function propertyExists(String $property)
    {
        return (isset(self::$properties[lcfirst($property)]));
    }

    public static function propertyIs(String $property, $value)
    {
        return (isset(self::$properties[$property])) ? ((self::$properties[$property] == $value) ? true : false) : false;
    }
}