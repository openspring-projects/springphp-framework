<?php
/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Enumeration;

/**
 * Enumeration of exchange media type
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
class MediaType extends EnumerationType
{
    const ALL_VALUE = "*/*";
    const APPLICATION_ATOM_XML_VALUE = "application/atom+xml";
    const APPLICATION_CBOR_VALUE = "application/cbor";
    const APPLICATION_FORM_URLENCODED_VALUE = "application/x-www-form-urlencoded";
    const APPLICATION_JSON_VALUE = "application/json";
    const APPLICATION_JSON_UTF8_VALUE = "application/json;charset=UTF-8";
    const APPLICATION_OCTET_STREAM_VALUE = "application/octet-stream";
    const APPLICATION_PDF_VALUE = "application/pdf";
    const APPLICATION_PROBLEM_JSON_VALUE = "application/problem+json";
    const APPLICATION_PROBLEM_JSON_UTF8_VALUE = "application/problem+json;charset=UTF-8";
    const APPLICATION_PROBLEM_XML_VALUE = "application/problem+xml";
    const APPLICATION_RSS_XML_VALUE = "application/rss+xml";
    const APPLICATION_STREAM_JSON_VALUE = "application/stream+json";
    const APPLICATION_XHTML_XML_VALUE = "application/xhtml+xml";
    const APPLICATION_XML_VALUE = "application/xml";
    const IMAGE_GIF_VALUE = "image/gif";
    const IMAGE_JPEG_VALUE = "image/jpeg";
    const IMAGE_PNG_VALUE = "image/png";
    const MULTIPART_FORM_DATA_VALUE = "multipart/form-data";
    const MULTIPART_MIXED_VALUE = "multipart/mixed";
    const TEXT_EVENT_STREAM_VALUE = "text/event-stream";
    const TEXT_HTML_VALUE = "text/html";
    const TEXT_MARKDOWN_VALUE = "text/markdown";
    const TEXT_PLAIN_VALUE = "text/plain";
    const TEXT_XML_VALUE = "text/xml";
}