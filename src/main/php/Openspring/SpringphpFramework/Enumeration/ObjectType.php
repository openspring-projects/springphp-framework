<?php

/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Enumeration;

use Openspring\SpringphpFramework\Utils\Strings;

/**
 * Enumeration of annotation attribute type
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
class ObjectType extends EnumerationType
{
    const STRING = 'String';
    const INTEGER = 'Integer';
    const DOUBLE = 'Double';
    const FLOAT = 'Float';
    const NULL = 'Null';
    const ARRAY = 'Array';
    const OBJECT = 'Object';
    const CLAZZ = 'Class';
    const BOOLEAN = 'Boolean';
    const ITERABLE = 'Iterable';
    const RESOURCE = 'Resource';
    const NUMBER = 'Number';
    const CALLABLE = 'Callable';
    const CALLBACK = 'Callback';

    public static function getType($value): String
    {
        $type = gettype($value);
        $valueOf = static::valueOf($type, true);

        return (Strings::isEmpty($valueOf)) ? self::OBJECT : $valueOf;
    }
}