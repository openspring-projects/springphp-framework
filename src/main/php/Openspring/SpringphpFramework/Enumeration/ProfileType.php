<?php

/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Enumeration;

/**
 * Enumeration of application supported environnements
 * 
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
class ProfileType extends EnumerationType
{
    const RUN_DEFAULT = 'DEFAULT';
    const RUN_DEV = 'DEV';
	const RUN_QUALIF = 'QUALIF';
	const RUN_VAL = 'VAL';
	const RUN_PPROD = 'PPROD';
	const RUN_REHEARSAL = 'REHEARSAL';
	const RUN_PROD = 'PROD';
	const TEST = 'TEST';
}