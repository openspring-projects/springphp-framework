<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Enumeration;

use Openspring\SpringphpFramework\Exception\InvalidEnumerationException;
use Openspring\SpringphpFramework\Reflection\ClassReflection;

/**
 * Class defining enumeration type.
 * Class typed as enumerations should extend this class to profit from many features
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
abstract class EnumerationType
{

    /**
     * Return true if given value or array values
     * are part of class constants declaration
     *
     * @param mixed $value
     * @param bool $ignoreCase
     * @return bool
     */
    public static function isValid($value, bool $ignoreCase = false): bool
    {
        $array = self::toArray();
        if ($ignoreCase)
        {
            foreach ($array as &$const)
            {
                $const = strtolower($const);
            }
        }

        if (is_array($value))
        {
            foreach ($value as $v)
            {
                $cv = ($ignoreCase) ? strtolower($v) : $v;
                if (! in_array($cv, $array)) return false;
            }

            return true;
        }
        else
        {
            $value = ($ignoreCase) ? strtolower($value) : $value;
            return in_array($value, $array);
        }
    }

    /**
     * Return value of a given value as set in enumeration
     *
     * @param String $value
     * @param bool $ignoreCase
     * @return String
     */
    public static function valueOf(String $value, bool $ignoreCase = false): String
    {
        $value = ($ignoreCase) ? strtolower($value) : $value;
        $array = self::toArray();

        foreach ($array as $const)
        {
            $constv = ($ignoreCase) ? strtolower($const) : $const;
            if ($constv == $value) return $const;
        }

        return '';
    }

    /**
     * Raiser an exception of InvalidEnumerationException
     * if given value or array values are not part
     * of class constants declaration fully or partialy
     *
     * @param
     *            String|Array of string $value
     * @return bool
     */
    public static function validate($value, bool $ignoreCase = false): void
    {
        if (! self::isValid($value, $ignoreCase))
        {
            throw new InvalidEnumerationException("Invalid enumeration value " . $value . ' for type ' . get_called_class());
        }
    }

    /**
     * Return all class constants as array
     *
     * @return array
     */
    public static function toArray(): array
    {
        $consts = ClassReflection::getConstants(static::class);
        $array = array();
        foreach ($consts as $cvalue)
        {
            $array[] = (string) $cvalue;
        }

        return $array;
    }
}