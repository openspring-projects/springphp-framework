<?php

/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Enumeration;

/**
 * Enumeration of bean scope type
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
class FieldValidationAnnotation extends EnumerationType
{
    const REGEX = 'Regex';
    const SIZE = 'Size';
    const MIN = 'Min';
    const MAX = 'Max';
    const NOTNULL = 'NotNull';
    const NOTEMPTY = 'NotEmpty';
    const NOTBLANK = 'NotBlank';
    const EMAIL = 'Email';
    const URL = 'URL';
    const ASSERTBOOL = 'AssertBool';
    const ASSERTTRUE = 'AssertTrue';
    const ASSERTFALSE = 'AssertFalse';
    const ASSERTNUMBER = 'AssertNumber';
    const POSITIVE = 'Positive';
    const POSITIVEORZERO = 'PositiveOrZero';
    const NEGATIVE = 'Negative';
    const NEGATIVEORZERO = 'NegativeOrZero';
    const DATE = 'Date';
    const PAST = 'Past';
    const PASTORPRESENT = 'PastOrPresent';
    const FUTURE = 'Future';
    const FUTUREORPRESENT = 'FutureOrPresent';
    const INARRAY = 'InArray';
}