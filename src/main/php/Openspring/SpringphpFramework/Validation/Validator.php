<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Validation;

use Openspring\SpringphpFramework\Core\Stereotype\Annotations;
use Openspring\SpringphpFramework\Enumeration\FieldValidationAnnotation;
use Openspring\SpringphpFramework\Exception\Data\DataValidationException;
use Openspring\SpringphpFramework\Manager\RM;
use Openspring\SpringphpFramework\Reflection\ClassReflection;
use Openspring\SpringphpFramework\Type\DynamicObject;
use Openspring\SpringphpFramework\Utils\Objects;
use Openspring\SpringphpFramework\Utils\Strings;
use ReflectionClass;

/**
 * Data validation factory
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
class Validator implements IValidator
{
    private $violations = array();
    private $silentModeEnabled = false;

    public function __construct(bool $silentModeEnabled)
    {
        $this->silentModeEnabled = $silentModeEnabled;
    }

    public function validate(Validable $dto): array
    {
        $this->violations = array();
        $this->validateAnnotatedFields($dto, $this->silentModeEnabled);

        return $this->violations;
    }

    public function isValid(Validable $dto): bool
    {
        $this->violations = array();
        $this->validateAnnotatedFields($dto, true);

        return (count($this->violations) == 0);
    }

    public function getViolations(): array
    {
        return $this->violations;
    }

    public function getMessages(): array
    {
        $messages = array();
        foreach ($this->violations as $violation)
        {
            $messages[] = $violation->message;
        }

        return $messages;
    }

    protected function validateAnnotatedFields(Validable $dto, bool $silentModeEnabled): void
    {
        $class = new ReflectionClass($dto);

        // class all annotations distinctly selected
        $cannots = ClassReflection::getAllAnnotationsKeys($dto, FieldValidationAnnotation::toArray());

        // target annotations to find in field doc
        $tannots = Annotations::getValidationAnnotations($cannots);

        foreach ($class->getProperties() as $property)
        {
            $fdoc = $property->getDocComment();
            $fname = $property->name;

            // get field annotations
            $fannots = ClassReflection::getDocAnnotations($fdoc, $tannots, false);

            // validate field based on its found annotations configs
            foreach ($fannots as $key => $config)
            {
                $validator = $config->validator;

                // validate
                $result = $validator->isValid($fname, $dto->$fname, $config);
                if (! $result)
                {
                    if ($validator->isMetaOk())
                    {
                        $pmessage = Objects::get($config->passedParams, 'message', '');

                        $msg = 'Constraint @' . $key . ' failure on field ' . $fname . ' with data ' . $dto->$fname;
                        $pmsg = $pmessage;
                        if (! Strings::isEmpty($pmessage))
                        {
                            $param = Strings::getBetween($pmessage, '${', '}', false);
                            if (! Strings::isEmpty($param))
                            {
                                $pmsg = RM::translate($param);
                                if ($pmsg == $param)
                                {
                                    $pmsg = Strings::getStringPart($pmessage, ':', 1);
                                }
                            }
                        }

                        if (! Strings::isEmpty($pmsg)) $msg = $pmsg;
                    }
                    else
                    {
                        $msg = $validator->getMessagesAsString();
                    }

                    $violation = array(
                        'field' => $fname,
                        'value' => $dto->$fname,
                        'message' => $msg);
                    $this->violations[] = new DynamicObject($violation);

                    if (! $silentModeEnabled)
                    {
                        throw new DataValidationException($msg);
                    }
                }
            }
        }
    }
}