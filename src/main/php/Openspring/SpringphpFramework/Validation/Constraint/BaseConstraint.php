<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Validation;

use Openspring\SpringphpFramework\Enumeration\ObjectType;
use Openspring\SpringphpFramework\Utils\Objects;
use Openspring\SpringphpFramework\Utils\Strings;
use Openspring\SpringphpFramework\Annotation\ValidatorAnnotationDefiner;

/**
 * Interface of object validability
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
class BaseConstraint
{
    protected $messages = array();
    protected $metaOK = true;
    protected $continueValidation = true;

    public function validateRegex(String $field, $value, $pattern): bool
    {
        $matches = null;
        preg_match_all($pattern, $value, $matches, PREG_PATTERN_ORDER);

        return count($matches[0]) > 0;
    }

    /**
     * Check that validation configuration basicly is good
     *
     * @param ValidatorAnnotationDefiner $config
     */
    public function isMetaValid(String $field, $value, $config): bool
    {
        $this->continueValidation = true;
        $annotation = Strings::replace('(', '', $config->start);
        $this->messages = array();
        $baseMessage = 'Invalid arguements found for annotation ' . $config->start . ') on field ' . $field;

        if (! Objects::hasAllPropertiesNotEmpty($config->passedParams, array_keys($config->expectedParams)))
        {
            $this->messages[] = $baseMessage . '. Expected ' . json_encode($config->expectedParams);
            $this->metaOK = false;

            return false;
        }

        // check value type
        $stop = false;
        foreach ($config->target as $type)
        {
            $stop = false;
            if ($value === null || (is_string($value) && $value === ''))
            {
                $stop = true;
                $this->continueValidation = false;
                break;
            }

            // check data type
            switch ($type)
            {
                case ObjectType::STRING:
                    if (is_string($value)) $stop = true;
                    break;
                case ObjectType::INTEGER:
                    if (is_numeric($value)) $stop = true;
                    break;
                case ObjectType::ARRAY:
                    if (is_array($value)) $stop = true;
                    break;
                case ObjectType::BOOLEAN:
                    if (is_bool($value)) $stop = true;
                    break;
                case ObjectType::OBJECT:
                    if (is_object($value)) $stop = true;
                    break;
            }
            
            if ($stop) break;
        }

        if (! $stop)
        {
            $this->messages[] = 'Invalid Data type passed in field ' . $field . '. Expected ' . json_encode($config->target) . ' for annotation ' . $annotation;
            $this->metaOK = false;

            return false;
        }

        // check passed parameters
        foreach ($config->expectedParams as $param => $type)
        {
            $passedParams = $config->passedParams;
            switch ($type)
            {
                case ObjectType::STRING:
                    if (! is_string($passedParams->$param))
                    {
                        $this->messages[] = $baseMessage . '. parameter ' . $param . ' must be string';
                        $this->metaOK = false;

                        return false;
                    }
                    break;
                case ObjectType::INTEGER:
                    if (! is_numeric($passedParams->$param))
                    {
                        $this->messages[] = $baseMessage . '. parameter ' . $param . ' must be numeric';
                        $this->metaOK = false;

                        return false;
                    }
                    break;
            }
        }

        return true;
    }

    public function getMessages(): array
    {
        return $this->messages;
    }

    public function getMessagesAsString(): String
    {
        return implode(' ', $this->messages);
    }

    public function isMetaOk(): bool
    {
        return $this->metaOK;
    }
}