<?php

/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Validation\Validator\Simple\Mixed;

use Openspring\SpringphpFramework\Validation\BaseConstraint;
use Openspring\SpringphpFramework\Validation\Constraint;

/**
 * validates that the annotated property is a valid email address
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
class EmailConstraint extends BaseConstraint implements Constraint
{
    public function isValid(String $field, $value, $config): bool
    {
        if (!$this->isMetaValid($field, $value, $config)) return false;
        if (!$this->continueValidation) return true;
        
        return ! (! filter_var($value, FILTER_VALIDATE_EMAIL));
    }
}