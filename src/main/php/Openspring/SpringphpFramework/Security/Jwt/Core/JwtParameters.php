<?php
/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Security\Jwt\Core;

use Openspring\SpringphpFramework\Security\Jwt\Enumeration\TokenType;

class JwtParameters
{
    protected $publicKey;
    protected $privateKey;
    protected $algorithm;
    protected $issuer = '';
    protected $expiresIn = 300;
    protected $refreshExpiresIn = 300;
    protected $tokenType = TokenType::BEARER;

    public function __construct(String $publicKey, String $privateKey)
    {
        $this->privateKey = $privateKey;
        $this->publicKey = $publicKey;
    }

    public function getPublicKey(): String
    {
        return $this->publicKey;
    }

    public function getPrivateKey(): String
    {
        return $this->privateKey;
    }

    public function getAlgorithm(): String
    {
        return $this->algorithm;
    }

    public function getIssuer(): String
    {
        return $this->issuer;
    }

    public function getExpiresIn(): int
    {
        return $this->expiresIn;
    }

    public function getRefreshExpiresIn(): int
    {
        return $this->refreshExpiresIn;
    }

    public function setPublicKey(String $publicKey)
    {
        $this->publicKey = $publicKey;
    }

    public function setPrivateKey(String $privateKey)
    {
        $this->privateKey = $privateKey;
    }

    public function setAlgorithm(String $algorithm)
    {
        $this->algorithm = $algorithm;
    }

    public function setIssuer(String $issuer)
    {
        $this->issuer = $issuer;
    }

    public function setExpiresIn(int $expiresIn)
    {
        $this->expiresIn = $expiresIn;
    }

    public function setRefreshExpiresIn(int $refreshExpiresIn)
    {
        $this->refreshExpiresIn = $refreshExpiresIn;
    }

    public function getTokenType()
    {
        return $this->tokenType;
    }

    public function setTokenType($tokenType)
    {
        $this->tokenType = $tokenType;
    }
}