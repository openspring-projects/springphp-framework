<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Security\Jwt\Core;

use Openspring\SpringphpFramework\Exception\InvalidArgumentException;
use Openspring\SpringphpFramework\Security\Jwt\Contract\Encryptor;
use Openspring\SpringphpFramework\Security\Jwt\Enumeration\EncryptionAlgorithm;
use Exception;

class Jwt implements Encryptor
{

    public function encode($payload, String $key, String $algo = EncryptionAlgorithm::HS256): String
    {
        $header = $this->generateJwtHeader($payload, $algo);

        $segments = array(
            $this->encodeUrlSafeBase64(json_encode($header)),
            $this->encodeUrlSafeBase64(json_encode($payload)));

        $signing_input = implode('.', $segments);

        $signature = $this->sign($signing_input, $key, $algo);
        $segments[] = $this->encodeUrlSafeBase64($signature);

        return implode('.', $segments);
    }

    public function decode(String $encoded, String $key = null, bool $allowedAlgoOnly = true)
    {
        if (! strpos($encoded, '.'))
        {
            return false;
        }

        $tks = explode('.', $encoded);

        if (count($tks) != 3)
        {
            return false;
        }

        list ($headb64, $payloadb64, $cryptob64) = $tks;

        if (null === ($header = json_decode($this->decodeUrlSafeBase64($headb64), true)))
        {
            return false;
        }

        if (null === $payload = json_decode($this->decodeUrlSafeBase64($payloadb64), true))
        {
            return false;
        }

        $sig = $this->decodeUrlSafeBase64($cryptob64);

        if ((bool) $allowedAlgoOnly)
        {
            if (! isset($header['alg']))
            {
                return false;
            }

            // check if bool arg supplied here to maintain BC
            if (is_array($allowedAlgoOnly) && ! in_array($header['alg'], $allowedAlgoOnly))
            {
                return false;
            }

            if (! $this->verifySignature($sig, "$headb64.$payloadb64", $key, $header['alg']))
            {
                return false;
            }
        }

        return $payload;
    }

    public function encodeUrlSafeBase64($data): String
    {
        $b64 = base64_encode($data);
        $b64 = str_replace(array(
            '+',
            '/',
            "\r",
            "\n",
            '='), array(
            '-',
            '_'), $b64);

        return $b64;
    }

    public function decodeUrlSafeBase64(String $b64)
    {
        $b64 = str_replace(array(
            '-',
            '_'), array(
            '+',
            '/'), $b64);

        return base64_decode($b64);
    }

    protected function verifySignature(String $signature, $input, String $key, String $algo = EncryptionAlgorithm::HS256): bool
    {
        // use constants when possible, for HipHop support
        switch ($algo)
        {
            case EncryptionAlgorithm::HS256:
            case EncryptionAlgorithm::HS384:
            case EncryptionAlgorithm::HS512:
                return $this->hash_equals($this->sign($input, $key, $algo), $signature);

            case EncryptionAlgorithm::RS256:
                return openssl_verify($input, $signature, $key, defined('OPENSSL_ALGO_SHA256') ? OPENSSL_ALGO_SHA256 : 'sha256') === 1;

            case EncryptionAlgorithm::RS384:
                return @openssl_verify($input, $signature, $key, defined('OPENSSL_ALGO_SHA384') ? OPENSSL_ALGO_SHA384 : 'sha384') === 1;

            case EncryptionAlgorithm::RS512:
                return @openssl_verify($input, $signature, $key, defined('OPENSSL_ALGO_SHA512') ? OPENSSL_ALGO_SHA512 : 'sha512') === 1;

            default:
                throw new InvalidArgumentException("Unsupported or invalid signing algorithm.");
        }
    }

    protected function sign($input, String $key, String $algo = EncryptionAlgorithm::HS256): String
    {
        switch ($algo)
        {
            case EncryptionAlgorithm::HS256:
                return hash_hmac('sha256', $input, $key, true);

            case EncryptionAlgorithm::HS384:
                return hash_hmac('sha384', $input, $key, true);

            case EncryptionAlgorithm::HS512:
                return hash_hmac('sha512', $input, $key, true);

            case EncryptionAlgorithm::RS256:
                return $this->generateRSASignature($input, $key, defined('OPENSSL_ALGO_SHA256') ? OPENSSL_ALGO_SHA256 : 'sha256');

            case EncryptionAlgorithm::RS384:
                return $this->generateRSASignature($input, $key, defined('OPENSSL_ALGO_SHA384') ? OPENSSL_ALGO_SHA384 : 'sha384');

            case EncryptionAlgorithm::RS512:
                return $this->generateRSASignature($input, $key, defined('OPENSSL_ALGO_SHA512') ? OPENSSL_ALGO_SHA512 : 'sha512');

            default:
                throw new Exception("Unsupported or invalid signing algorithm.");
        }
    }

    protected function generateRSASignature($input, String $key, int $algo): String
    {
        $signature = '';
        if (! openssl_sign($input, $signature, $key, $algo))
        {
            $error = "Unable to sign data." . openssl_error_string();
            throw new Exception($error);
        }

        return $signature;
    }

    protected function generateJwtHeader($payload, String $algorithm): array
    {
        return array(
            'typ' => 'JWT',
            'alg' => $algorithm);
    }

    protected function hash_equals($a, $b): bool
    {
        if (function_exists('hash_equals'))
        {
            return hash_equals($a, $b);
        }
        $diff = strlen($a) ^ strlen($b);
        for ($i = 0; $i < strlen($a) && $i < strlen($b); $i ++)
        {
            $diff |= ord($a[$i]) ^ ord($b[$i]);
        }

        return $diff === 0;
    }
}