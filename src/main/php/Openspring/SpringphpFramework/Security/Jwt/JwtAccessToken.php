<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Security\Jwt;

use Openspring\SpringphpFramework\Context\ApplicationContext;
use Openspring\SpringphpFramework\Exception\Http\BadRequestException;
use Openspring\SpringphpFramework\Exception\Http\UnauthorizedException;
use Openspring\SpringphpFramework\Security\Jwt\Contract\AccessToken;
use Openspring\SpringphpFramework\Security\Jwt\Contract\Encryptor;
use Openspring\SpringphpFramework\Security\Jwt\Core\Jwt;
use Openspring\SpringphpFramework\Security\Jwt\Core\JwtParameterProvider;
use Openspring\SpringphpFramework\Security\Jwt\Core\JwtParameters;
use Openspring\SpringphpFramework\Security\Jwt\Enumeration\TokenElement;
use Openspring\SpringphpFramework\Security\Jwt\Enumeration\TokenPayload;
use Openspring\SpringphpFramework\Security\Jwt\Enumeration\TokenScope;
use Openspring\SpringphpFramework\Security\Jwt\Enumeration\TokenType;
use Openspring\SpringphpFramework\Utils\EncryptionUtils;

class JwtAccessToken implements AccessToken
{
    protected $clientId;
    protected $jwt = null;
    protected $jwtParameterProvider;
    protected $jwtParameters = null;
    protected $userClaims = array();

    public function __construct(array $userClaims = array())
    {
        $jwt = new Jwt();
        $this->setJwt($jwt);
        $this->setUserClaims($userClaims);
        $this->jwtParameterProvider = ApplicationContext::getBeanOf(JwtParameterProvider::class, true);
    }

    public function setJwt(Encryptor $encryptor): void
    {
        $this->jwt = $encryptor;
    }

    public function setUserClaims(array $userClaims = array()): void
    {
        $this->userClaims = $userClaims;
    }

    protected function getJwtParameters(): JwtParameters
    {
        if ($this->jwtParameters == null)
        {
            $this->jwtParameters = $this->jwtParameterProvider->getJwtParameters($this->clientId);
        }

        return $this->jwtParameters;
    }

    public function createAccessToken(String $clientId, String $userId, String $scope = TokenScope::OPENID_EMAIL_PROFILE, $addRefreshToken = true): array
    {
        $this->clientId = $clientId;
        $payload = $this->createTokenPayload($clientId, $userId, $scope);
        $access_token = $this->encodeToken($payload, $clientId);

        // token to return to the client
        $token = array(
            TokenElement::ACCESS_TOKEN => $access_token,
            TokenElement::EXPIRES_IN => time() + $this->getJwtParameters()->getExpiresIn(),
            TokenElement::TOKEN_TYPE => $this->getJwtParameters()->getTokenType(),
            TokenElement::SCOPE => $scope);

        if ($addRefreshToken)
        {
            $payload = $this->createRefrshPayload($clientId, $userId, $scope);
            $refresh_token = $this->encodeToken($payload, $clientId);

            $expires = 0;
            $expires = time() + $this->getJwtParameters()->getRefreshExpiresIn();

            $token[TokenElement::REFRESH_TOKEN] = $refresh_token;
            $token[TokenElement::REFRESH_EXPIRES_IN] = $expires;
        }

        return $token;
    }

    public function verifyTokenPayload($payload, bool $throwException = true): bool
    {
        $jwt = array_merge(array(
            'scope' => null,
            'iss' => null,
            'sub' => null,
            'uid' => null,
            'aud' => null,
            'exp' => null,
            'nbf' => null,
            'iat' => null,
            'jti' => null,
            'typ' => null), $payload);

        if (! isset($jwt['iss']))
        {
            if (! $throwException) return false;
            throw new BadRequestException("Invalid issuer (iss) provided");
        }

        if (! isset($jwt['sub']))
        {
            if (! $throwException) return false;
            throw new BadRequestException("Invalid subject (sub) provided");
        }

        if (! isset($jwt['exp']))
        {
            if (! $throwException) return false;
            throw new BadRequestException("Expiration (exp) time must be present");
        }

        // Check expiration
        if (ctype_digit($jwt['exp']))
        {
            if ($jwt['exp'] <= time())
            {
                if (! $throwException) return false;
                throw new UnauthorizedException("JWT has expired");
            }
        }
        else
        {
            if (! $throwException) return false;
            throw new BadRequestException("Expiration (exp) time must be a unix time stamp");
        }

        // Check the not before time
        if ($notBefore = $jwt['nbf'])
        {
            if (ctype_digit($notBefore))
            {
                if ($notBefore > time())
                {
                    if (! $throwException) return false;
                    throw new BadRequestException("JWT cannot be used before the Not Before (nbf) time");
                }
            }
            else
            {
                if (! $throwException) return false;
                throw new BadRequestException("Not Before (nbf) time must be a unix time stamp");
            }
        }

        return true;
    }

    protected function getUserClaims(): array
    {
        return $this->userClaims;
    }

    protected function encodeToken(array $token, ?String $clientId = null)
    {
        $private_key = $this->getJwtParameters()->getPrivateKey();
        $algorithm = $this->getJwtParameters()->getAlgorithm();

        return $this->jwt->encode($token, $private_key, $algorithm);
    }

    protected function createTokenPayload(String $clientId, String $userId, String $scope = TokenScope::OPENID_EMAIL_PROFILE): array
    {
        $expires = time() + $this->getJwtParameters()->getExpiresIn();
        $type = $this->getJwtParameters()->getTokenType();
        $aud = 'account';

        return $this->createPayload($clientId, $userId, $expires, $scope, $aud, $type);
    }

    protected function createRefrshPayload(String $clientId, String $userId, String $scope = TokenScope::OPENID_EMAIL_PROFILE): array
    {
        $expires = time() + $this->getJwtParameters()->getRefreshExpiresIn();
        $type = TokenType::REFRESH;
        $aud = $this->getJwtParameters()->getIssuer();

        return $this->createPayload($clientId, $userId, $expires, $scope, $aud, $type);
    }

    protected function createPayload(String $clientId, String $userId, int $expires, String $scope, String $aud, String $type): array
    {
        // token to encrypt
        $id = EncryptionUtils::newGuid(36);

        $payload = array(
            TokenPayload::ID => $id, // for BC (see #591)
            TokenPayload::JTI => $id,
            TokenPayload::ISS => $this->getJwtParameters()->getIssuer(),
            TokenPayload::AUD => $aud,
            TokenPayload::SUB => $clientId,
            TokenPayload::UID => $userId, // not in spec (customized property)
            TokenPayload::EXP => $expires,
            TokenPayload::IAT => time(),
            TokenElement::TOKEN_TYPE => $type,
            TokenElement::SCOPE => $scope);

        $payload = array_merge($this->getUserClaims(), $payload);

        return $payload;
    }
}