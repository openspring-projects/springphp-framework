<?php

/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Security\Jwt;

use Openspring\SpringphpFramework\Context\ApplicationContext;
use Openspring\SpringphpFramework\Core\Bean\UserClaims;
use Openspring\SpringphpFramework\Security\Jwt\Contract\Encryptor;
use Openspring\SpringphpFramework\Security\Jwt\Contract\IdToken;
use Openspring\SpringphpFramework\Security\Jwt\Core\Jwt;
use Openspring\SpringphpFramework\Security\Jwt\Core\JwtParameterProvider;
use Openspring\SpringphpFramework\Security\Jwt\Core\JwtParameters;
use Openspring\SpringphpFramework\Security\Jwt\Enumeration\TokenElement;
use Openspring\SpringphpFramework\Security\Jwt\Enumeration\TokenPayload;
use Openspring\SpringphpFramework\Security\Jwt\Enumeration\TokenScope;
use Openspring\SpringphpFramework\Security\Jwt\Enumeration\TokenType;
use Openspring\SpringphpFramework\Utils\EncryptionUtils;

class JwtIdToken implements IdToken
{
    protected $clientId;
    protected $jwt = null;
    protected $jwtParameterProvider;
    protected $jwtParameters = null;
    protected $userClaims = array();

    public function __construct(array $userClaims)
    {
        $jwt = new Jwt();
        $this->setJwt($jwt);
        $this->setUserClaims($userClaims);
        $this->jwtParameterProvider = ApplicationContext::getBeanOf(JwtParameterProvider::class, true);
    }

    public function setJwt(Encryptor $encryptor): void
    {
        $this->jwt = $encryptor;
    }

    public function setUserClaims(array $userClaims = array()): void
    {
        $this->userClaims = $userClaims;
    }

    public function addClaim(String $name, $value): UserClaims
    {
        if ($this->userClaims == null) $this->userClaims = array();
        $this->userClaims[$name] = $value;

        return $this;
    }

    public function addClaims(array $claims): UserClaims
    {
        foreach ($claims as $name => $value)
        {
            $this->addClaim($name, $value);
        }

        return $this;
    }

    protected function getJwtParameters(): JwtParameters
    {
        if ($this->jwtParameters == null)
        {
            $this->jwtParameters = $this->jwtParameterProvider->getJwtParameters($this->clientId);
        }

        return $this->jwtParameters;
    }

    public function createIdToken(String $clientId, String $userId, String $scope = TokenScope::OPENID): String
    {
        $this->clientId = $clientId;
        $payload = $this->createTokenPayload($clientId, $userId, $scope);

        return $this->encodeToken($payload, $clientId);
    }

    protected function getUserClaims(): array
    {
        return $this->userClaims;
    }

    protected function encodeToken(array $token, ?String $clientId = null)
    {
        $private_key = $this->getJwtParameters()->getPrivateKey();
        $algorithm = $this->getJwtParameters()->getAlgorithm();

        return $this->jwt->encode($token, $private_key, $algorithm);
    }

    protected function createTokenPayload(String $clientId, String $userId, String $scope = TokenScope::OPENID): array
    {
        $expires = time() + $this->getJwtParameters()->getExpiresIn();
        $type = TokenType::ID;
        $aud = 'account';

        return $this->createPayload($clientId, $userId, $expires, $scope, $aud, $type);
    }

    protected function createPayload(String $clientId, String $userId, int $expires, String $scope, String $aud, String $type): array
    {
        // token to encrypt
        $id = EncryptionUtils::newGuid(36);

        $payload = array(
            TokenPayload::ID => $id, // for BC (see #591)
            TokenPayload::JTI => $id,
            TokenPayload::ISS => $this->getJwtParameters()->getIssuer(),
            TokenPayload::AUD => $aud,
            TokenPayload::SUB => $userId,
            TokenPayload::EXP => $expires,
            TokenPayload::IAT => time(),
            TokenElement::TOKEN_TYPE => $type,
            TokenElement::SCOPE => $scope);

        $payload = array_merge($this->getUserClaims(), $payload);

        return $payload;
    }
}