<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Security\Jwt;

use Openspring\SpringphpFramework\Core\Initializable;
use Openspring\SpringphpFramework\Exception\Http\UnauthorizedException;
use Openspring\SpringphpFramework\Manager\ErrorManager;
use Openspring\SpringphpFramework\Security\CurrentUser;
use Openspring\SpringphpFramework\Type\Any;
use Openspring\SpringphpFramework\Utils\Objects;
use Openspring\SpringphpFramework\Utils\Strings;

/**
 * Global security class managing :
 * - action access by groups
 * - captcha
 */
class JwtAccessSecurity implements Initializable
{
    private static $accRoles = array();

    public static function initialize(): void
    {
        // Empty
    }

    /**
     * asign groups to an action to restrict access
     * by group using annotation @RequestMapping's value 'groupIn'
     */
    public static function authorize($actionId, $groups, $roles)
    {
        self::authorizeGroups($actionId, $groups);
        self::authorizeRoles($actionId, $roles);
    }

    /**
     * asign groups to an action to restrict access
     * by group using annotation @RequestMapping's value 'groupIn'
     */
    public static function authorizeGroups(String $actionId, Array $groupsIn)
    {
        if (empty($groupsIn)) return;

        $accessRule = self::getAccessRuleInstance($actionId);
        $accessRule->groups = $groupsIn;

        self::$accRoles[$actionId] = $accessRule;
    }

    /**
     * asign roles to an action to restrict access
     * by role using annotation @RequestMapping's value 'rolesIn'
     */
    public static function authorizeRoles(String $actionId, Array $rolesIn)
    {
        if (empty($rolesIn)) return;

        $accessRule = self::getAccessRuleInstance($actionId);
        $accessRule->roles = $rolesIn;

        self::$accRoles[$actionId] = $accessRule;
    }

    private static function getAccessRuleInstance($actionId)
    {
        $accessRule = new Any();
        if (isset(self::$accRoles[$actionId]))
        {
            $accessRule = self::$accRoles[$actionId];
        }
        else
        {
            $accessRule->type = 'allow';
            $accessRule->action = $actionId;
        }

        return $accessRule;
    }

    /**
     * verify if current connected user has right to run current action
     */
    public static function isAuthorized($action)
    {
        ErrorManager::clean();

        if (! isset(self::$accRoles[$action])) return true;

        if (! CurrentUser::isAuthentificated())
        {
            throw new UnauthorizedException("Not authorized (Not Logged)");
        }

        // check groups
        if (Objects::hasProperty(self::$accRoles[$action], 'groups'))
        {
            $groupId = CurrentUser::getGroupId();
            if (! in_array($groupId, self::$accRoles[$action]->groups) && ! in_array("*", self::$accRoles[$action]->groups)) return false;
        }

        // check roles
        if (Objects::hasProperty(self::$accRoles[$action], 'roles'))
        {
            $roles = CurrentUser::getRoles();
            $roleFound = false;
            if (! in_array("*", self::$accRoles[$action]->roles))
            {
                foreach ($roles as $role)
                {
                    if (in_array($role, self::$accRoles[$action]->roles))
                    {
                        $roleFound = true;
                        break;
                    }
                }
            }
            else
            {
                $roleFound = true;
            }

            if (! $roleFound) return false;
        }

        return true;
    }

    /**
     * verify the action user
     */
    public static function isThisUser(String $action_user_id): bool
    {
        return (CurrentUser::isAuthentificated() && CurrentUser::getUserId() == $action_user_id);
    }
}