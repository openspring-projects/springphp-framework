<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Security;

use Openspring\SpringphpFramework\Exception\NullPointerException;
use Openspring\SpringphpFramework\Utils\Strings;
use Openspring\SpringphpFramework\Exception\Http\UnauthorizedException;

class CurrentUser
{
    /**
     * @var IUser
     */
    private static $user;

    public static function setUser(IUser $user): void
    {
        self::$user = $user;
    }

    public static function getUser(bool $throwException = false): ?IUser
    {
        if ($throwException) self::checkUser();

        return self::$user;
    }

    public static function isAuthentificated(): bool
    {
        return (self::getUser() !== null && ! Strings::isEmpty(self::getUser()->getUserId()));
    }

    public static function getUserId(bool $throwException = true): String
    {
        return self::getUser($throwException)->getUserId();
    }

    public static function getGroupId(): int
    {
        return self::getUser(true)->getGroupId();
    }

    public static function getRoles(): array
    {
        return self::getUser(true)->getRoles();
    }

    public static function getLogin(): String
    {
        return self::getUser(true)->getLogin();
    }

    public static function getEmail(): String
    {
        return self::getUser(true)->getEmail();
    }

    public static function getFullName(): String
    {
        return self::getUser(true)->getFullName();
    }

    private static function checkUser(): void
    {
        if (! self::isAuthentificated())
        {
            throw new UnauthorizedException();
        }
    }
}