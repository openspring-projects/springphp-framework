<?php

/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Security;

/**
 * Definition of user details object
 * 
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
interface IUser
{
    public function setUserId(String $userId): void;
    public function getUserId(): String;
    
    public function setGroupId(int $groupId): void;
    public function getGroupId(): int;
    
    public function setLogin(String $login): void;
    public function getLogin(): String;
    
    public function setFirstName(String $firstName): void;
    public function getFirstName(): ?String;
    
    public function setLastName(String $lastName): void;
    public function getLastName(): String;
    
    public function setEmail(String $email): void;
    public function getEmail(): String;
    
    public function setRoles(array $roles = array()): void;
    public function getRoles(): array;
}