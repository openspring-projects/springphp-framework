<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Type;

use Openspring\SpringphpFramework\Utils\Strings;
use Openspring\SpringphpFramework\Utils\Arrays;

class StringPlus
{
    public $value = null;

    public function __construct(String $value)
    {
        $this->value = $value;
    }

    public function replaceAll(String $needle, String $replacer): ?String
    {
        return Strings::replace($needle, $replacer, $this->value);
    }

    public function capitalize(): ?String
    {
        return ucfirst($this->value);
    }

    public function capitalizeFirstWord(): ?String
    {
        return ucfirst($this->lowercase());
    }

    public function capitalizeAllWords(): ?String
    {
        return ucwords($this->lowercase());
    }

    public function uppercase(): ?String
    {
        return strtoupper($this->value);
    }

    public function lowercase(): ?String
    {
        return strtolower($this->value);
    }

    public function is(?String $value): bool
    {
        return $value == $this->value;
    }

    public function isTrue($strict = false): bool
    {
        if ($strict) return ($this->value == true);
        return in_array($this->value, Arrays::of(1, '1', true, TRUE));
    }

    public function isFalse($strict = false): bool
    {
        if ($strict) return ($this->value == false);
        return in_array($this->value, Arrays::of('', 0, '0', false, FALSE));
    }

    public function __toString()
    {
        return $this->value;
    }
}