<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Type;

use Openspring\SpringphpFramework\Utils\Strings;

class JString
{
    public $value = null;

    public function __construct(String $value)
    {
        $this->value = $value;
    }

    public function replaceAll(String $needle, String $replacer): JString
    {
        $this->value = Strings::replace($needle, $replacer, $this->value);

        return $this;
    }

    public function capitalize(): JString
    {
        $this->value = ucfirst($this->value);

        return $this;
    }

    public function capitalizeFirstWord(): JString
    {
        $this->lowercase();
        $this->value = ucfirst($this->value);

        return $this;
    }

    public function capitalizeAllWords(): JString
    {
        $this->lowercase();
        $this->value = ucwords($this->value);

        return $this;
    }

    public function uppercase(): JString
    {
        $this->value = strtoupper($this->value);

        return $this;
    }

    public function empty(?String $value = null): JString
    {
        $this->value = $value;

        return $this;
    }

    public function lowercase(): JString
    {
        $this->value = strtolower($this->value);

        return $this;
    }
    
    public function __toString()
    {
        return $this->value;
    }
}