<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Type;

use Openspring\SpringphpFramework\IO\FileUtils;
use Openspring\SpringphpFramework\Manager\RM;
use Exception;
use Openspring\SpringphpFramework\Utils\Strings;
use Openspring\SpringphpFramework\Exception\IO\FileNotFoundException;

class RequestTemporaryFile
{
    private $id = NULL;
    private $ext = NULL;
    private $path = NULL;
    private $enabled = false;
    private $useAdvSecurity = false;

    public function __construct($tempFileId = 0)
    {
        $this->inputId = $tempFileId;
        $this->path = (isset($_FILES[$this->inputId])) ? $_FILES[$this->inputId]['tmp_name'] : NULL;
        if (Strings::isEmpty($this->path))
        {
            throw new FileNotFoundException('Temporary uploaded file not found for input id '. $tempFileId);    
        }
        
        $this->ext = FileUtils::getExtension((isset($_FILES[$this->inputId])) ? $_FILES[$this->inputId]['name'] : NULL);
        $this->enabled = ($this->path == NULL) ? false : true;
        $this->useAdvSecurity = function_exists("mime_content_type");
    }

    public function exists()
    {
        return $this->enabled;
    }

    public function getSize()
    {
        return ($this->enabled) ? $_FILES[$this->inputId]['size'] : 0;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function getType()
    {
        return ($this->useAdvSecurity) ? mime_content_type($this->path) : "";
    }

    public function getExtension()
    {
        return $this->ext;
    }

    public function saveTo($directory, $diskFileName, $maxSize = 0, $allowedTypes = NULL, $overWrite = true)
    {
        $destFile = "{$directory}{$diskFileName}";

        // check file path
        if (! is_dir($directory))
        {
            throw new Exception('can not save file in directory ' . $directory . '. Directory not found');
            return false;
        }

        // can create ?
        if (! $overWrite && file_exists($destFile))
        {
            return new ActionResponse(false, 'TF-0001', RM::translate('Message.can-not-create-file-it-exists'));
        }

        // check file size
        if ($maxSize > 0 && $maxSize < $this->getSize())
        {
            return new ActionResponse(false, 'TF-0002', RM::translate('Message.can-not-create_file-it-is-oversized'));
        }

        // check file is allowed
        if ($this->useAdvSecurity && $allowedTypes != NULL && ! in_array($this->getType(), $allowedTypes))
        {
            return new ActionResponse(false, 'TF-0003', RM::translate('Message.can-not-create-file-its-type-not-allowed'));
        }

        // remove file if exists
        if ($overWrite && file_exists($destFile))
        {
            @unlink($destFile);
        }

        // save
        if (! copy($this->getPath(), $destFile))
        {
            return new ActionResponse(false, 'TF-0004', RM::translate('Message.cannot-mov-fil-uploaded'));
        }

        return new ActionResponse(true);
    }
}