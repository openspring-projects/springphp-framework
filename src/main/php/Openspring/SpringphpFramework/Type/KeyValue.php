<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Type;

/**
 * KeyValue type
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
class KeyValue
{
    var $key;
    var $value;

    public function __construct(String $key, ?String $value)
    {
        $this->key = $key;
        $this->value = $value;
    }
    
    public static function of(String $key, ?String $value)
    {
        return new KeyValue($key, $value);
    }

    public function getKey()
    {
        return $this->key;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setKey($key)
    {
        $this->key = $key;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }
}