<?php

/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Type;

class ActionResponse
{
	private $ok			= NULL;
	private $code		= NULL;
	private $message	= NULL;
	private $extra		= NULL;
	
	public function __construct($ok, $code = 0, $message = NULL, $extra = null)
	{
		$this->ok		= $ok;
		$this->code		= $code;
		$this->message	= $message;
		$this->extra 	= $extra;
	}

	public function isOk()
	{
		return $this->ok;
	}
	
	public function getCode()
	{
		return $this->code;
	}
	
	public function getMessage()
	{
		return $this->message;
	}

	public function getExtra()
	{
		return $this->extra;
	}
}