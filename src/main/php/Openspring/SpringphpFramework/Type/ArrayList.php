<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Type;

use Openspring\SpringphpFramework\Exception\ArrayIndexOutOfBoundsException;
use Openspring\SpringphpFramework\Exception\InvalidArgumentException;
use JsonSerializable;

/**
 * ArrayList type
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
class ArrayList extends Collection implements JsonSerializable
{

    public function __construct(Array $array = array())
    {
        parent::__construct($array);
    }

    public function hasIndex(int $index): bool
    {
        return (isset($this->array[$index]));
    }

    private function checkHasIndex(int $index): void
    {
        if (! $this->hasIndex($index))
        {
            throw new ArrayIndexOutOfBoundsException('Array index out of bound ' . $index);
        }
    }

    public function add($item): void
    {
        $this->array[] = $item;
    }

    public function remove(int $index): void
    {
        $this->checkHasIndex($index);
        unset($this->array[$index]);
    }

    public function addAll(Array $array): void
    {
        $this->array = $array;
    }

    public function removeAll(): void
    {
        $this->array = array();
    }

    public function get(int $index)
    {
        $this->checkHasIndex($index);
        return $this->array[$index];
    }

    public function set(int $index, $item): void
    {
        $this->checkHasIndex($index);
        $this->array[$index] = $item;
    }

    public function hasValue($value): bool
    {
        foreach ($this->array as $val)
        {
            if ($val == $value) return true;
        }

        return false;
    }

    public function containsObject(String $property, $value): bool
    {
        return ($this->find($property, $value) != null);
    }

    public function find(String $property, $value)
    {
        $first = $this->first();
        if (! property_exists($first, $property))
        {
            throw new InvalidArgumentException('can not find property ' . $property);
        }

        foreach ($this->array as $item)
        {
            if ($item->$property == $value) return $item;
        }

        return null;
    }

    public function jsonSerialize()
    {
        return $this->array;
    }
}