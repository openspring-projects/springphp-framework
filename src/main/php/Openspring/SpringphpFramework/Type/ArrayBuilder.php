<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Type;

use JsonSerializable;

/**
 * ArrayBuilder type
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
class ArrayBuilder implements JsonSerializable
{
    protected $array = array();

    public static function of($value): ArrayBuilder
    {
        $arrayPlus = new ArrayBuilder();
        $arrayPlus->addValue($value);
        return $arrayPlus;
    }
    
    public static function ofkv(String $key, $value): ArrayBuilder
    {
        $arrayPlus = new ArrayBuilder();
        $arrayPlus->addKeyValue($key, $value);
        return $arrayPlus;
    }
    
    public function addKeyValue(String $key, $value): ArrayBuilder
    {
        $this->array[$key] = $value;
        return $this;
    }
    
    public function addValue($value): ArrayBuilder
    {
        $this->array[] = $value;
        return $this;
    }
    
    public function toArray()
    {
        return $this->array;
    }

    public function __toString()
    {
        return $this->array;
    }

    public function jsonSerialize()
    {
        return $this->array;
    }
}