<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Type;

use Openspring\SpringphpFramework\Exception\ArrayIndexOutOfBoundsException;
use Openspring\SpringphpFramework\Exception\InvalidArgumentException;

/**
 * Dictionnary type
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
class Dictionary extends Map
{

    public function __construct(Array $array = array())
    {
        parent::__construct($array);
    }

    public function hasKey(String $key): bool
    {
        return (isset($this->array[$key]));
    }

    private function checkHasNotKey(String $key): void
    {
        if ($this->hasKey($key))
        {
            throw new ArrayIndexOutOfBoundsException('Dictionnary has already this key ' . $key);
        }
    }

    private function checkHasKey(String $key): void
    {
        if (! $this->hasKey($key))
        {
            throw new ArrayIndexOutOfBoundsException('Dictionnary key out of bound ' . $key);
        }
    }

    public function add(String $key, $item): void
    {
        $this->checkHasNotKey($key);
        $this->array[$key] = $item;
    }

    public function remove(String $key): void
    {
        $this->checkHasKey($key);
        unset($this->array[$key]);
    }

    public function addAll(Array $array): void
    {
        $this->array = $array;
    }

    public function removeAll(): void
    {
        $this->array = array();
    }

    public function get(String $key)
    {
        $this->checkHasKey($key);
        return $this->array[$key];
    }

    public function getAll(): array
    {
        return $this->array;
    }

    public function set(String $key, $item): void
    {
        $this->checkHasKey($key);
        $this->array[$key] = $item;
    }

    public function containsObject(String $property, $value): bool
    {
        return ($this->find($property, $value) != null);
    }

    public function hasValue($value): bool
    {
        foreach ($this->array as $val)
        {
            if ($val == $value) return true;
        }

        return false;
    }

    public function find(String $property, $value)
    {
        $first = $this->first();
        if (! property_exists($first, $property))
        {
            throw new InvalidArgumentException('can not find property ' . $property);
        }

        foreach ($this->array as $item)
        {
            if ($item->$property == $value) return $item;
        }

        return null;
    }
}