<?php
/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Type;

use Iterator;

/**
 * Collection type
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
abstract class Collection implements Iterator
{
    protected $position = 0;
    protected $array = array();

    public function __construct(Array $array = array())
    {
        $this->array = $array;
        $this->position = 0;
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function current()
    {
        return $this->array[$this->position];
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        ++$this->position;
    }

    public function valid()
    {
        return isset($this->array[$this->position]);
    }

    public function size(): int
    {
        return count($this->array);
    }

    public function hasAny(): bool
    {
        return ($this->size() > 0);
    }
	
    public function isEmpty(): bool
    {
        return !$this->hasAny();
    }

    public function first()
    {
        return reset($this->array);
    }

    public function last()
    {
        return end($this->array);
    }
	
	public function clear(): void
    {
        $this->array = array();
    }
    
    public function toArray(): Array
    {
        return $this->array;
    }
}