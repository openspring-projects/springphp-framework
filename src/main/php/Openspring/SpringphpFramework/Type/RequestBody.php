<?php
/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Type;

class RequestBody
{
    protected $body;

    public function __construct($body)
    {
        $this->body = $body;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function getInt(String $paramName, int $default = 0): ?int
    {
        return $this->getValue($paramName, $default);
    }

    public function getBool(String $paramName, bool $default = false): ?bool
    {
        return $this->getValue($paramName, $default);
    }

    public function getDate(String $paramName, String $default = ''): ?String
    {
        return $this->getValue($paramName, $default);
    }

    public function getArray(String $paramName, Array $default = array()): ?array
    {
        return $this->getValue($paramName, $default);
    }

    public function getObject(String $paramName, Object $default = null): ?Object
    {
        return $this->getValue($paramName, $default);
    }

    public function getString(String $paramName, String $default = ''): ?String
    {
        return $this->getValue($paramName, $default);
    }

    protected function getValue(String $property, $default = '')
    {
        if (! is_object($this->body)) return $default;
        return property_exists($this->body, $property) ? $this->body->$property : $default;
    }
}