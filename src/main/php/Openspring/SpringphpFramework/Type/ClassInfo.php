<?php

/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Type;

class ClassInfo
{    
    var $name = '';
	var $qualifiedName = '';
	var $comments = '';
	var $methods = array();

    public function __construct(String $name='', String $qualifiedName='', String $comments = '')
	{
		$this->name = $name;
		$this->qualifiedName = $qualifiedName;
		$this->comments = $comments;
	}

	public function getName(): String {
		return $this->name;
	}

	public function getQualifiedName(): String {
		return $this->qualifiedName;
	}

	public function getComments(): String {
		return $this->comments;
	}

	public function getMethods(): array {
	    return $this->methods;
	}
	
	public static function castToClassMethod(ClassMethod $classMethod): ?ClassMethod {
	    return $classMethod;
	}
}