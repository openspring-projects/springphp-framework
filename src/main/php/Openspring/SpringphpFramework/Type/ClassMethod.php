<?php
/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Type;

use Openspring\SpringphpFramework\Annotation\RequestMapping\DeleteMapping;
use Openspring\SpringphpFramework\Annotation\RequestMapping\GetMapping;
use Openspring\SpringphpFramework\Annotation\RequestMapping\PatchMapping;
use Openspring\SpringphpFramework\Annotation\RequestMapping\PostMapping;
use Openspring\SpringphpFramework\Annotation\RequestMapping\PutMapping;
use Openspring\SpringphpFramework\Annotation\RequestMapping\RequestMapping;
use Openspring\SpringphpFramework\Utils\Strings;

class ClassMethod
{
    var $name = '';
    var $modifier = '';
    var $comments = '';
    var $annotations = array();
    var $parameters = array();

    public function __construct(String $name, String $type, String $comments, array $parameters = array())
    {
        $this->name = $name;
        $this->modifier = $type;
        $this->comments = $comments;
        $this->parameters = $parameters;

        $this->setAnnotations();
    }

    function getName(): String
    {
        return $this->name;
    }

    public function isPrivate(): bool
    {
        return $this->modifier == 'private';
    }

    public function isProtected(): bool
    {
        return $this->modifier == 'protected';
    }

    public function isPublic(): bool
    {
        return $this->modifier == 'public';
    }

    public function getComments(): String
    {
        return $this->comments;
    }

    public function setAnnotations(): void
    {
        if (Strings::isEmpty($this->comments)) return;

        $docAnnotations = explode('@', $this->comments);
        foreach ($docAnnotations as $docAnnotation)
        {
            $docAnnotation = '@' . $docAnnotation;
            $annotation = Strings::getBetween($docAnnotation, '@', '(', false);
            if ($annotation == '') continue;

            switch ($annotation)
            {
                case 'RequestMapping':
                    $this->addAnnotation(RequestMapping::class, $docAnnotation);
                    break;

                case 'GetMapping':
                    $this->addAnnotation(GetMapping::class, $docAnnotation);
                    break;

                case 'PostMapping':
                    $this->addAnnotation(PostMapping::class, $docAnnotation);
                    break;

                case 'PutMapping':
                    $this->addAnnotation(PutMapping::class, $docAnnotation);
                    break;

                case 'PatchMapping':
                    $this->addAnnotation(PatchMapping::class, $docAnnotation);
                    break;

                case 'DeleteMapping':
                    $this->addAnnotation(DeleteMapping::class, $docAnnotation);
                    break;

                default:
                    break;
            }
        }
    }

    private function addAnnotation($clazz, ?String $docAnnotation): void
    {
        $mapping = new $clazz($docAnnotation, $this->modifier);
        if ($mapping->isValid())
        {
            $this->annotations[] = $mapping->castToRequestMapping();
        }
    }

    public function getAnnotations(): array
    {
        return $this->annotations;
    }

    public function getParameters(): array
    {
        return $this->parameters;
    }

    public static function castToMethodParameter(MethodParameter $mParam): ?MethodParameter
    {
        return $mParam;
    }

    public static function castToRequestMapping(RequestMapping $requestMapping): ?RequestMapping
    {
        return $requestMapping;
    }
}