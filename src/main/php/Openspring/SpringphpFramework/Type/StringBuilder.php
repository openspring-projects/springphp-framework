<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Type;

use Exception;

class StringBuilder
{
    public $sb = array();

    public function __construct($intial_string = null)
    {
        if ($intial_string != null)
        {
            $this->append($intial_string);
        }
    }

    public function append($newLine, $beforeBreaks = 0, $afterBreaks = 0)
    {
        for ($i = 0; $i < $beforeBreaks; $i ++)
        {
            array_push($this->sb, '');
        }

        array_push($this->sb, $newLine);

        for ($i = 0; $i < $afterBreaks; $i ++)
        {
            array_push($this->sb, '');
        }

        return $this;
    }

    public function removeFirst()
    {
        $this->remove(array_key_first($this->sb));
    }

    public function removeLast()
    {
        $this->remove(array_key_last($this->sb));
    }

    public function remove($key)
    {
        if (! isset($this->sb[$key])) throw new Exception('Key not found in target array');

        unset($this->sb[$key]);
    }

    public function clean($intial_string = '')
    {
        $this->sb = array();
        return $this->append($intial_string);
    }

    public function toString($separator = '')
    {
        return implode($separator, $this->sb);
    }
}