<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Type;

/**
 * Create object from array
 * Exemple:
 * - $item = new DynamicObject(array('name' => "khalid, 'age' => 36))
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
class DynamicObject
{
    public function __construct(Array $items = array())
    {
        foreach ($items as $key => $value)
        {
            $this->$key = $value;
        }
    }

    public static function init(String $propertyName, $value): DynamicObject
    {
        $new = new DynamicObject();
        
        $new->addProperty($propertyName, $value);
        
        return $new;
    }

    public function addProperty(String $propertyName, $value): DynamicObject
    {
        $this->$propertyName = $value;

        return $this;
    }
}