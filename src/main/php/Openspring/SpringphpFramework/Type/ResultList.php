<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Type;

use JsonSerializable;
use Openspring\SpringphpFramework\Orm\Query\Transformer;

class ResultList extends ArrayList implements JsonSerializable
{
    public $page = 0;
    public $pageSize = 0;
    public $totalPages = 0;
    public $totalRows = 0;

    public function __construct(Array $array, $page, $pageSize, $totalRows, $totalPages)
    {
        parent::__construct($array);

        $this->page = $page;
        $this->pageSize = $pageSize;
        $this->totalPages = $totalPages;
        $this->totalRows = $totalRows;
    }

    public function hasItems()
    {
        return ($this->totalRows > 0);
    }

    public function transform(Transformer $transformer): void
    {
        foreach ($this->array as $element)
        {
            $transformer->Transform($element);
        }
    }

    public function jsonSerialize()
    {
        $dynItem = new DynamicObject();

        $dynItem->addProperty('page', $this->page);
        $dynItem->addProperty('pageSize', $this->pageSize);
        $dynItem->addProperty('totalPages', $this->totalPages);
        $dynItem->addProperty('totalRows', $this->totalRows);
        $dynItem->addProperty('data', $this->array);

        return $dynItem;
    }
}