<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Utils;

use Openspring\SpringphpFramework\Exception\IllegalArgumentException;
use Openspring\SpringphpFramework\Orm\Type\DataType;

class Strings
{
    /**
     * Default charset is UTF-8
     *
     * @var string
     */
    public static $encoding = 'UTF-8';

    /**
     * Empty string constant
     */
    const EMPTY = '';

    public static function snakeIt(String $name, $uppercaseFirstChar = false): String
    {
        $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $name)));

        if (! $uppercaseFirstChar)
        {
            $str[0] = strtolower($str[0]);
        }

        return $str;
    }

    public static function isEmpty(?String $string)
    {
        return ($string === null || (is_string($string) && trim($string) == ''));
    }

    public static function isAnyEmpty()
    {
        $vars = func_get_args();
        foreach ($vars as $var)
        {
            if (self::isEmpty($var)) return true;
        }

        return false;
    }

    public static function nvl($param, $nvl)
    {
        return ($param == NULL) ? $nvl : $param;
    }

    public static function equalIgnoreCase($string1, $string2)
    {
        return (trim(strtolower($string1)) == trim(strtolower($string2)));
    }

    public static function equal($string1, $string2)
    {
        return (trim($string1) == trim($string2));
    }

    /**
     * Return true if a string contains an other substring.
     * The test is not case sensitive
     *
     * @param String $haystack
     * @param String $needle
     * @return String|NULL
     */
    public static function containIgnoreCase(?String $haystack, ?String $needle): bool
    {
        return (strpos(trim(strtolower($haystack)), trim(strtolower($needle))) !== FALSE);
    }

    /**
     * Extract a substring between 2 substrings in a given string
     *
     * @param String $string
     * @param String $leftLimiter
     * @param String $rightLimiter
     * @return String|NULL
     */
    public static function getBetween(?String $string, ?String $leftLimiter, ?String $rightLimiter, bool $keepLimiters = true, bool $trim = true)
    {
        $pos1 = strpos($string, $leftLimiter);
        if ($pos1 === false) return false;

        $pos2 = strpos($string, $rightLimiter, $pos1 + 1);
        if ($pos2 === false)
        {
            if ($rightLimiter == PHP_EOL)
            {
                $pos2 = strlen($string) - $pos1;
            }
        }

        if ($pos2 <= $pos1) return false;

        $out = substr($string, $pos1, ($pos2 - $pos1) + strlen($rightLimiter));

        if (! $keepLimiters)
        {
            $out = static::replace($leftLimiter, '', $out);
            $out = static::replace($rightLimiter, '', $out);
        }

        return (! $trim) ? $out : trim($out);
    }

    // TODO: to test
    public static function getBetweenInv(?String $string, ?String $leftLimiter, ?String $rightLimiter, bool $keepLimiters = true, bool $trim = true)
    {
        $pos1 = strpos($string, $leftLimiter);
        if ($pos1 === false) return false;

        $pos2 = strripos($string, $rightLimiter, $pos1 + 1);
        if ($pos2 === false)
        {
            if ($rightLimiter == PHP_EOL)
            {
                $pos2 = strlen($string) - $pos1;
            }
        }

        if ($pos2 <= $pos1) return false;

        $out = substr($string, $pos1, ($pos2 - $pos1) + strlen($rightLimiter));

        if (! $keepLimiters)
        {
            $out = static::replace($leftLimiter, '', $out);
            if ($rightLimiter != PHP_EOL)
            {
                $out = substr($out, 0, strlen($out) - strlen($rightLimiter));
            }
        }

        return (! $trim) ? $out : trim($out);
    }

    public static function getUpTo(?String $string, ?String $toLimiter, bool $returnStringIfNoneFound = true): ?String
    {
        $pos1 = strpos($string, $toLimiter);
        if ($pos1 === false) return $returnStringIfNoneFound ? $string : '';

        return substr($string, 0, $pos1);
    }

    /**
     * Return true if a string contains an other substring.
     * The test is case sensitive
     *
     * @param String $haystack
     * @param String $needle
     * @return String|NULL
     */
    public static function contain(?String $haystack, ?String $needle): bool
    {
        return (strpos($haystack, $needle) !== FALSE);
    }

    /**
     * Return true if a string contains all the given other substrings.
     * The test is case sensitive
     *
     * @param String $haystack
     * @throws IllegalArgumentException
     * @return bool
     */
    public static function containMultiple(?String $haystack): bool
    {
        $argsNum = func_num_args();
        $args = func_get_args();

        if ($argsNum == 1)
        {
            throw new IllegalArgumentException('Minimum args required are 2. one passed');
        }

        $i = - 1;
        foreach ($args as $arg)
        {
            if (++ $i == 0) continue;

            if (! self::contain($haystack, $arg)) return false;
        }

        return true;
    }

    /**
     * Return true if a string contains all the given other substrings.
     * The test is not case sensitive
     *
     * @param String $haystack
     * @throws IllegalArgumentException
     * @return bool
     */
    public static function containMultipleIgnoreCase(?String $haystack): bool
    {
        $argsNum = func_num_args();
        $args = func_get_args();

        if ($argsNum == 1)
        {
            throw new IllegalArgumentException('Minimum args required are 2. one passed');
        }

        $i = - 1;
        foreach ($args as $arg)
        {
            if (++ $i == 0) continue;

            if (! self::containIgnoreCase($haystack, $arg)) return false;
        }

        return true;
    }

    public static function toBoolean($val, bool $alt = false): bool
    {
        if (in_array($val, array(
                                    '1',
                                    'true'))) return true;
        if (in_array($val, array(
                                    '0',
                                    'false'))) return false;

        return $alt;
    }

    public static function startsWith($haystack, $needle)
    {
        return (strpos($haystack, $needle) === 0);
    }

    public static function endsWith($haystack, $needle)
    {
        return (strpos(strrev($haystack), strrev($needle)) === 0);
    }

    public static function bornedWith($haystack, $startNeedle, $endNeedle): bool
    {
        if (! self::startsWith($haystack, $startNeedle)) return false;
        if (! self::endsWith($haystack, $endNeedle)) return false;

        return true;
    }

    /**
     * Return a given string token based on a given delimiter and index to return
     * -1 means return the last item
     *
     * @param String $string
     * @param String $delim
     * @param int $indx
     * @param String $alt
     * @return String|NULL
     */
    public static function getStringPart(?String $string, String $delim, int $indx, String $alt = ''): ?String
    {
        if ($string == null) return $alt;

        $parts = explode($delim, $string);

        if ($indx == - 1) ($indx = count($parts) - 1);

        return (isset($parts[$indx])) ? $parts[$indx] : $alt;
    }

    public static function stringToArray($string, $delim)
    {
        return explode($delim, $string);
    }

    public static function getCssAttribute($target, $attribute, $alt = '', $delim1 = ';', $delim2 = ':')
    {
        $parts = explode($delim1, $target);
        foreach ($parts as $part)
        {
            $comb = explode($delim2, $part);
            if (trim($comb[0]) == $attribute)
            {
                return (isset($comb[1])) ? $comb[1] : $alt;
            }
        }
        return $alt;
    }

    public static function replace($find, $replace, $string)
    {
        return strtr($string, array(
                                    $find => $replace));
    }

    public static function replaceMultiple($findReplaceArray = array(), String $string)
    {
        return strtr($string, $findReplaceArray);
    }

    public static function removeChars(String $string, $chars = array())
    {
        foreach ($chars as $char)
        {
            $string = self::replace($char, '', $string);
        }

        return $string;
    }

    public static function replaceAll($string, $args)
    {
        return strtr($string, $args);
    }

    /**
     * Replace all occurences with no case sensitive
     *
     * @param unknown $string
     * @param array $args
     * @return String
     */
    public static function ireplaceAll($string, array $args): String
    {
        $out = $string;
        foreach ($args as $key => $value)
        {
            $out = str_ireplace($key, $value, $out);
        }

        return $out;
    }

    public static function removeAll(?String $string, Array $args): ?String
    {
        if (self::isEmpty($string)) return $string;

        foreach ($args as $arg)
        {
            $string = static::replace($arg, '', $string);
        }

        return $string;
    }

    public static function removeFirstChars($string, $count = 1)
    {
        return substr($string, $count, strlen($string));
    }

    public static function removeLastChars($string, $count = 1)
    {
        return substr($string, 0, strlen($string) - $count);
    }

    public static function stringToWord($sentence, $size = 0)
    {
        if ($sentence == null) return null;
        $string = static::replace(' ', '_', $sentence);
        $string = preg_replace('/[^A-Za-z0-9\_]/', '', $string);
        $end = strtolower(preg_replace('/_+/', '_', $string));

        return ($size > 0) ? substr($end, 0, $size) : $end;
    }

    /**
     * Return substring from the needle pos to the end
     */
    public static function substringToEnd($string, $needle)
    {
        $pos = strpos($string, $needle);
        if ($pos == 0) return '';

        return substr($string, $pos, strlen($string));
    }

    /**
     * converts bit string to array by mapping 0 to false and 1 to true
     */
    public static function bitsToArray($bit_string)
    {
        $outArray = str_split($bit_string);

        foreach ($outArray as $key => $value)
        {
            $outArray[$key] = ($value == 0) ? false : true;
        }

        return $outArray;
    }

    /**
     * Quote a value if necessary
     *
     * @param
     *            ($an) attribute name
     * @return String
     *
     */
    public static function getSecuredDbInput($unsafe_value, $type = DataType::VARCHAR): String
    {
        // TODO: fix this shit
        // return mysqli_real_escape_string(DatabaseContext::getConnection()->getDriver()->getConnectionIdentifier(), $unsafe_value);
        return addslashes($unsafe_value);
    }

    /**
     * Strip all whitespaces from the given string.
     *
     * @param string $string
     *            The string to strip
     * @return string
     */
    public static function stripSpace($string): string
    {
        return preg_replace('/\s+/', '', $string);
    }

    /**
     * Parse text by lines
     *
     * @param string $text
     * @param bool $toAssoc
     * @return array
     */
    public static function parseLines($text, $toAssoc = true): array
    {
        $text = htmlspecialchars_decode($text);
        $text = self::clean($text, false, false, false);

        $text = str_replace([
                                "\n",
                                "\r",
                                "\r\n",
                                PHP_EOL], "\n", $text);
        $lines = explode("\n", $text);

        $result = [];
        if (! empty($lines))
        {
            foreach ($lines as $line)
            {
                $line = trim($line);

                if ($line === '')
                {
                    continue;
                }

                if ($toAssoc)
                {
                    $result[$line] = $line;
                }
                else
                {
                    $result[] = $line;
                }
            }
        }

        return $result;
    }

    /**
     * Make string safe
     * - Remove UTF-8 chars
     * - Remove all tags
     * - Trim
     * - Add Slashes (opt)
     * - To lower (opt)
     *
     * @param string $string
     * @param bool $toLower
     * @param bool $addSlashes
     * @param bool $removeAccents
     * @return string
     */
    public static function clean($string, $toLower = false, $addSlashes = false, $removeAccents = true): string
    {
        if ($removeAccents)
        {
            $string = self::removeAccents($string);
        }

        $string = strip_tags($string);
        $string = trim($string);

        if ($addSlashes)
        {
            $string = addslashes($string);
        }

        if ($toLower)
        {
            $string = self::low($string);
        }

        return $string;
    }

    /**
     * Converts all accent characters to ASCII characters.
     * If there are no accent characters, then the string given is just returned.
     *
     * @param string $string
     *            Text that might have accent characters
     * @param string $language
     *            Specifies a priority for a specific language.
     * @return string Filtered string with replaced "nice" characters
     */
    public static function removeAccents($string, $language = ''): string
    {
        if (! preg_match('/[\x80-\xff]/', $string))
        {
            return $string;
        }
        return self::downCode($string, $language);
    }

    public static function downCode($string, $locale = null)
    {
        if (! preg_match('/[\x80-\xff]/', $string))
        {
            return $string;
        }

        $chars = [
                    // Decompositions for Latin-1 Supplement
                    'ª' => 'a',
                    'º' => 'o',
                    'À' => 'A',
                    'Á' => 'A',
                    'Â' => 'A',
                    'Ã' => 'A',
                    'Ä' => 'A',
                    'Å' => 'A',
                    'Æ' => 'AE',
                    'Ç' => 'C',
                    'È' => 'E',
                    'É' => 'E',
                    'Ê' => 'E',
                    'Ë' => 'E',
                    'Ì' => 'I',
                    'Í' => 'I',
                    'Î' => 'I',
                    'Ï' => 'I',
                    'Ð' => 'D',
                    'Ñ' => 'N',
                    'Ò' => 'O',
                    'Ó' => 'O',
                    'Ô' => 'O',
                    'Õ' => 'O',
                    'Ö' => 'O',
                    'Ù' => 'U',
                    'Ú' => 'U',
                    'Û' => 'U',
                    'Ü' => 'U',
                    'Ý' => 'Y',
                    'Þ' => 'TH',
                    'ß' => 's',
                    'à' => 'a',
                    'á' => 'a',
                    'â' => 'a',
                    'ã' => 'a',
                    'ä' => 'a',
                    'å' => 'a',
                    'æ' => 'ae',
                    'ç' => 'c',
                    'è' => 'e',
                    'é' => 'e',
                    'ê' => 'e',
                    'ë' => 'e',
                    'ì' => 'i',
                    'í' => 'i',
                    'î' => 'i',
                    'ï' => 'i',
                    'ð' => 'd',
                    'ñ' => 'n',
                    'ò' => 'o',
                    'ó' => 'o',
                    'ô' => 'o',
                    'õ' => 'o',
                    'ö' => 'o',
                    'ø' => 'o',
                    'ù' => 'u',
                    'ú' => 'u',
                    'û' => 'u',
                    'ü' => 'u',
                    'ý' => 'y',
                    'þ' => 'th',
                    'ÿ' => 'y',
                    'Ø' => 'O',
                    // Decompositions for Latin Extended-A
                    'Ā' => 'A',
                    'ā' => 'a',
                    'Ă' => 'A',
                    'ă' => 'a',
                    'Ą' => 'A',
                    'ą' => 'a',
                    'Ć' => 'C',
                    'ć' => 'c',
                    'Ĉ' => 'C',
                    'ĉ' => 'c',
                    'Ċ' => 'C',
                    'ċ' => 'c',
                    'Č' => 'C',
                    'č' => 'c',
                    'Ď' => 'D',
                    'ď' => 'd',
                    'Đ' => 'D',
                    'đ' => 'd',
                    'Ē' => 'E',
                    'ē' => 'e',
                    'Ĕ' => 'E',
                    'ĕ' => 'e',
                    'Ė' => 'E',
                    'ė' => 'e',
                    'Ę' => 'E',
                    'ę' => 'e',
                    'Ě' => 'E',
                    'ě' => 'e',
                    'Ĝ' => 'G',
                    'ĝ' => 'g',
                    'Ğ' => 'G',
                    'ğ' => 'g',
                    'Ġ' => 'G',
                    'ġ' => 'g',
                    'Ģ' => 'G',
                    'ģ' => 'g',
                    'Ĥ' => 'H',
                    'ĥ' => 'h',
                    'Ħ' => 'H',
                    'ħ' => 'h',
                    'Ĩ' => 'I',
                    'ĩ' => 'i',
                    'Ī' => 'I',
                    'ī' => 'i',
                    'Ĭ' => 'I',
                    'ĭ' => 'i',
                    'Į' => 'I',
                    'į' => 'i',
                    'İ' => 'I',
                    'ı' => 'i',
                    'Ĳ' => 'IJ',
                    'ĳ' => 'ij',
                    'Ĵ' => 'J',
                    'ĵ' => 'j',
                    'Ķ' => 'K',
                    'ķ' => 'k',
                    'ĸ' => 'k',
                    'Ĺ' => 'L',
                    'ĺ' => 'l',
                    'Ļ' => 'L',
                    'ļ' => 'l',
                    'Ľ' => 'L',
                    'ľ' => 'l',
                    'Ŀ' => 'L',
                    'ŀ' => 'l',
                    'Ł' => 'L',
                    'ł' => 'l',
                    'Ń' => 'N',
                    'ń' => 'n',
                    'Ņ' => 'N',
                    'ņ' => 'n',
                    'Ň' => 'N',
                    'ň' => 'n',
                    'ŉ' => 'n',
                    'Ŋ' => 'N',
                    'ŋ' => 'n',
                    'Ō' => 'O',
                    'ō' => 'o',
                    'Ŏ' => 'O',
                    'ŏ' => 'o',
                    'Ő' => 'O',
                    'ő' => 'o',
                    'Œ' => 'OE',
                    'œ' => 'oe',
                    'Ŕ' => 'R',
                    'ŕ' => 'r',
                    'Ŗ' => 'R',
                    'ŗ' => 'r',
                    'Ř' => 'R',
                    'ř' => 'r',
                    'Ś' => 'S',
                    'ś' => 's',
                    'Ŝ' => 'S',
                    'ŝ' => 's',
                    'Ş' => 'S',
                    'ş' => 's',
                    'Š' => 'S',
                    'š' => 's',
                    'Ţ' => 'T',
                    'ţ' => 't',
                    'Ť' => 'T',
                    'ť' => 't',
                    'Ŧ' => 'T',
                    'ŧ' => 't',
                    'Ũ' => 'U',
                    'ũ' => 'u',
                    'Ū' => 'U',
                    'ū' => 'u',
                    'Ŭ' => 'U',
                    'ŭ' => 'u',
                    'Ů' => 'U',
                    'ů' => 'u',
                    'Ű' => 'U',
                    'ű' => 'u',
                    'Ų' => 'U',
                    'ų' => 'u',
                    'Ŵ' => 'W',
                    'ŵ' => 'w',
                    'Ŷ' => 'Y',
                    'ŷ' => 'y',
                    'Ÿ' => 'Y',
                    'Ź' => 'Z',
                    'ź' => 'z',
                    'Ż' => 'Z',
                    'ż' => 'z',
                    'Ž' => 'Z',
                    'ž' => 'z',
                    'ſ' => 's',
                    // Decompositions for Latin Extended-B
                    'Ș' => 'S',
                    'ș' => 's',
                    'Ț' => 'T',
                    'ț' => 't',
                    // Euro Sign
                    '€' => 'E',
                    // GBP (Pound) Sign
                    '£' => '',
                    // Vowels with diacritic (Vietnamese)
                    // unmarked
                    'Ơ' => 'O',
                    'ơ' => 'o',
                    'Ư' => 'U',
                    'ư' => 'u',
                    // grave accent
                    'Ầ' => 'A',
                    'ầ' => 'a',
                    'Ằ' => 'A',
                    'ằ' => 'a',
                    'Ề' => 'E',
                    'ề' => 'e',
                    'Ồ' => 'O',
                    'ồ' => 'o',
                    'Ờ' => 'O',
                    'ờ' => 'o',
                    'Ừ' => 'U',
                    'ừ' => 'u',
                    'Ỳ' => 'Y',
                    'ỳ' => 'y',
                    // hook
                    'Ả' => 'A',
                    'ả' => 'a',
                    'Ẩ' => 'A',
                    'ẩ' => 'a',
                    'Ẳ' => 'A',
                    'ẳ' => 'a',
                    'Ẻ' => 'E',
                    'ẻ' => 'e',
                    'Ể' => 'E',
                    'ể' => 'e',
                    'Ỉ' => 'I',
                    'ỉ' => 'i',
                    'Ỏ' => 'O',
                    'ỏ' => 'o',
                    'Ổ' => 'O',
                    'ổ' => 'o',
                    'Ở' => 'O',
                    'ở' => 'o',
                    'Ủ' => 'U',
                    'ủ' => 'u',
                    'Ử' => 'U',
                    'ử' => 'u',
                    'Ỷ' => 'Y',
                    'ỷ' => 'y',
                    // tilde
                    'Ẫ' => 'A',
                    'ẫ' => 'a',
                    'Ẵ' => 'A',
                    'ẵ' => 'a',
                    'Ẽ' => 'E',
                    'ẽ' => 'e',
                    'Ễ' => 'E',
                    'ễ' => 'e',
                    'Ỗ' => 'O',
                    'ỗ' => 'o',
                    'Ỡ' => 'O',
                    'ỡ' => 'o',
                    'Ữ' => 'U',
                    'ữ' => 'u',
                    'Ỹ' => 'Y',
                    'ỹ' => 'y',
                    // acute accent
                    'Ấ' => 'A',
                    'ấ' => 'a',
                    'Ắ' => 'A',
                    'ắ' => 'a',
                    'Ế' => 'E',
                    'ế' => 'e',
                    'Ố' => 'O',
                    'ố' => 'o',
                    'Ớ' => 'O',
                    'ớ' => 'o',
                    'Ứ' => 'U',
                    'ứ' => 'u',
                    // dot below
                    'Ạ' => 'A',
                    'ạ' => 'a',
                    'Ậ' => 'A',
                    'ậ' => 'a',
                    'Ặ' => 'A',
                    'ặ' => 'a',
                    'Ẹ' => 'E',
                    'ẹ' => 'e',
                    'Ệ' => 'E',
                    'ệ' => 'e',
                    'Ị' => 'I',
                    'ị' => 'i',
                    'Ọ' => 'O',
                    'ọ' => 'o',
                    'Ộ' => 'O',
                    'ộ' => 'o',
                    'Ợ' => 'O',
                    'ợ' => 'o',
                    'Ụ' => 'U',
                    'ụ' => 'u',
                    'Ự' => 'U',
                    'ự' => 'u',
                    'Ỵ' => 'Y',
                    'ỵ' => 'y',
                    // Vowels with diacritic (Chinese, Hanyu Pinyin)
                    'ɑ' => 'a',
                    // macron
                    'Ǖ' => 'U',
                    'ǖ' => 'u',
                    // acute accent
                    'Ǘ' => 'U',
                    'ǘ' => 'u',
                    // caron
                    'Ǎ' => 'A',
                    'ǎ' => 'a',
                    'Ǐ' => 'I',
                    'ǐ' => 'i',
                    'Ǒ' => 'O',
                    'ǒ' => 'o',
                    'Ǔ' => 'U',
                    'ǔ' => 'u',
                    'Ǚ' => 'U',
                    'ǚ' => 'u',
                    // grave accent
                    'Ǜ' => 'U',
                    'ǜ' => 'u'];
        // Used for locale-specific rules
        if ('de_DE' == $locale || 'de_DE_formal' == $locale || 'de_CH' == $locale || 'de_CH_informal' == $locale)
        {
            $chars['Ä'] = 'Ae';
            $chars['ä'] = 'ae';
            $chars['Ö'] = 'Oe';
            $chars['ö'] = 'oe';
            $chars['Ü'] = 'Ue';
            $chars['ü'] = 'ue';
            $chars['ß'] = 'ss';
        }
        elseif ('da_DK' === $locale)
        {
            $chars['Æ'] = 'Ae';
            $chars['æ'] = 'ae';
            $chars['Ø'] = 'Oe';
            $chars['ø'] = 'oe';
            $chars['Å'] = 'Aa';
            $chars['å'] = 'aa';
        }
        elseif ('ca' === $locale)
        {
            $chars['l·l'] = 'll';
        }
        elseif ('sr_RS' === $locale || 'bs_BA' === $locale)
        {
            $chars['Đ'] = 'DJ';
            $chars['đ'] = 'dj';
        }
        $string = strtr($string, $chars);

        return $string;
    }

    /**
     * Convert >, <, ', " and & to html entities, but preserves entities that are already encoded.
     *
     * @param string $string
     *            The text to be converted
     * @param bool $encodedEntities
     * @return string
     */
    public static function htmlEnt($string, $encodedEntities = false): string
    {
        if ($encodedEntities)
        {
            // @codeCoverageIgnoreStart
            if (defined('HHVM_VERSION'))
            {
                $transTable = get_html_translation_table(HTML_ENTITIES, ENT_QUOTES);
            }
            else
            {
                /**
                 * @noinspection PhpMethodParametersCountMismatchInspection
                 */
                $transTable = get_html_translation_table(HTML_ENTITIES, ENT_QUOTES, self::$encoding);
            }
            // @codeCoverageIgnoreEnd

            $transTable[chr(38)] = '&';

            $regExp = '/&(?![A-Za-z]{0,4}\w{2,3};|#[\d]{2,3};)/';

            return preg_replace($regExp, '&amp;', strtr($string, $transTable));
        }

        return htmlentities($string, ENT_QUOTES, self::$encoding);
    }

    /**
     * Get unique string
     *
     * @param string $prefix
     * @return string
     */
    public static function unique($prefix = 'unique'): string
    {
        $prefix = rtrim(trim($prefix), '-');
        $random = random_int(10000000, 99999999);

        $result = $random;
        if ($prefix)
        {
            $result = $prefix . '-' . $random;
        }

        return $result;
    }

    /**
     * Generate readable random string
     *
     * @param int $length
     * @param bool $isReadable
     * @return string
     */
    public static function random($length = 10, $isReadable = true): string
    {
        $result = '';

        if ($isReadable)
        {
            $vowels = [
                        'a',
                        'e',
                        'i',
                        'o',
                        'u',
                        '0'];
            $consonants = [
                            'b',
                            'c',
                            'd',
                            'f',
                            'g',
                            'h',
                            'j',
                            'k',
                            'l',
                            'm',
                            'n',
                            'p',
                            'r',
                            's',
                            't',
                            'v',
                            'w',
                            'x',
                            'y',
                            'z',
                            '1',
                            '2',
                            '3',
                            '4',
                            '5',
                            '6',
                            '7',
                            '8',
                            '9'];

            $max = $length / 2;

            for ($pos = 1; $pos <= $max; $pos ++)
            {
                $result .= $consonants[random_int(0, count($consonants) - 1)];
                $result .= $vowels[random_int(0, count($vowels) - 1)];
            }
        }
        else
        {
            $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

            for ($pos = 0; $pos < $length; $pos ++)
            {
                $result .= $chars[mt_rand() % strlen($chars)];
            }
        }

        return $result;
    }

    /**
     * Pads a given string with zeroes on the left.
     *
     * @param int $number
     *            The number to pad
     * @param int $length
     *            The total length of the desired string
     * @return string
     */
    public static function zeroPad($number, $length): string
    {
        return str_pad($number, $length, '0', STR_PAD_LEFT);
    }

    /**
     * Truncate a string to a specified length without cutting a word off.
     *
     * @param string $string
     *            The string to truncate
     * @param integer $length
     *            The length to truncate the string to
     * @param string $append
     *            Text to append to the string IF it gets truncated, defaults to '...'
     * @return string
     */
    public static function truncateSafe($string, $length, $append = '...'): string
    {
        $result = self::sub($string, 0, $length);
        $lastSpace = self::rPos($result, ' ');

        if ($lastSpace !== false && $string !== $result)
        {
            $result = self::sub($result, 0, $lastSpace);
        }

        if ($result !== $string)
        {
            $result .= $append;
        }

        return $result;
    }

    /**
     * Truncate the string to given length of characters.
     *
     * @param string $string
     *            The variable to truncate
     * @param integer $limit
     *            The length to truncate the string to
     * @param string $append
     *            Text to append to the string IF it gets truncated, defaults to '...'
     * @return string
     */
    public static function limitChars($string, $limit = 100, $append = '...'): string
    {
        if (self::len($string) <= $limit)
        {
            return $string;
        }

        return rtrim(self::sub($string, 0, $limit)) . $append;
    }

    /**
     * Truncate the string to given length of words.
     *
     * @param string $string
     * @param int $limit
     * @param string $append
     * @return string
     */
    public static function limitWords($string, $limit = 100, $append = '...'): string
    {
        $matches = array();

        preg_match('/^\s*+(?:\S++\s*+){1,' . $limit . '}/u', $string, $matches);

        if (! Arrays::keyExists(0, $matches) || self::len($string) === self::len($matches[0]))
        {
            return $string;
        }

        return rtrim($matches[0]) . $append;
    }

    /**
     * Check if a given string matches a given pattern.
     *
     * @param string $pattern
     *            Pattern of string expected
     * @param string $string
     *            String that need to be matched
     * @param bool $caseSensitive
     * @return bool
     */
    public static function like($pattern, $string, $caseSensitive = true): bool
    {
        if ($pattern === $string)
        {
            return true;
        }

        // Preg flags
        $flags = $caseSensitive ? '' : 'i';

        // Escape any regex special characters
        $pattern = preg_quote($pattern, '#');

        // Unescaped * which is our wildcard character and change it to .*
        $pattern = str_replace('\*', '.*', $pattern);

        return (bool) preg_match('#^' . $pattern . '$#' . $flags, $string);
    }

    /**
     * Check is mbstring loaded
     *
     * @return bool
     */
    public static function isMBString(): bool
    {
        static $isLoaded;

        if (null === $isLoaded)
        {
            $isLoaded = extension_loaded('mbstring');

            if ($isLoaded)
            {
                mb_internal_encoding(self::$encoding);
            }
        }

        return $isLoaded;
    }

    /**
     * Get string length
     *
     * @param
     *            $string
     * @return int
     */
    public static function len($string): int
    {
        if (self::isMBString())
        {
            return mb_strlen($string, self::$encoding);
        }

        return strlen($string); // @codeCoverageIgnore
    }

    /**
     * Find position of first occurrence of string in a string
     *
     * @param string $haystack
     * @param string $needle
     * @param int $offset
     * @return int
     */
    public static function pos($haystack, $needle, $offset = 0)
    {
        if (self::isMBString())
        {
            return mb_strpos($haystack, $needle, $offset, self::$encoding);
        }

        return strpos($haystack, $needle, $offset); // @codeCoverageIgnore
    }

    /**
     * Find position of last occurrence of a string in a string
     *
     * @param string $haystack
     * @param string $needle
     * @param int $offset
     * @return int
     */
    public static function rPos($haystack, $needle, $offset = 0)
    {
        if (self::isMBString())
        {
            return mb_strrpos($haystack, $needle, $offset, self::$encoding);
        }

        return strrpos($haystack, $needle, $offset); // @codeCoverageIgnore
    }

    /**
     * Finds position of first occurrence of a string within another, case insensitive
     *
     * @param string $haystack
     * @param string $needle
     * @param int $offset
     * @return int
     */
    public static function iPos($haystack, $needle, $offset = 0)
    {
        if (self::isMBString())
        {
            return mb_stripos($haystack, $needle, $offset, self::$encoding);
        }

        return stripos($haystack, $needle, $offset); // @codeCoverageIgnore
    }

    /**
     * Finds first occurrence of a string within another
     *
     * @param string $haystack
     * @param string $needle
     * @param bool $beforeNeedle
     * @return string
     */
    public static function strStr($haystack, $needle, $beforeNeedle = false)
    {
        if (self::isMBString())
        {
            return mb_strstr($haystack, $needle, $beforeNeedle, self::$encoding);
        }

        return strstr($haystack, $needle, $beforeNeedle); // @codeCoverageIgnore
    }

    /**
     * Finds first occurrence of a string within another, case insensitive
     *
     * @param string $haystack
     * @param string $needle
     * @param bool $beforeNeedle
     * @return string
     */
    public static function iStr($haystack, $needle, $beforeNeedle = false)
    {
        if (self::isMBString())
        {
            return mb_stristr($haystack, $needle, $beforeNeedle, self::$encoding);
        }

        return stristr($haystack, $needle, $beforeNeedle); // @codeCoverageIgnore
    }

    /**
     * Finds the last occurrence of a character in a string within another
     *
     * @param string $haystack
     * @param string $needle
     * @param bool $part
     * @return string
     */
    public static function rChr($haystack, $needle, $part = null)
    {
        if (self::isMBString())
        {
            return mb_strrchr($haystack, $needle, $part, self::$encoding);
        }

        return strrchr($haystack, $needle); // @codeCoverageIgnore
    }

    /**
     * Get part of string
     *
     * @param string $string
     * @param int $start
     * @param int $length
     * @return string
     */
    public static function sub($string, $start, $length = 0)
    {
        if (self::isMBString())
        {
            if (0 === $length)
            {
                $length = self::len($string);
            }

            return mb_substr($string, $start, $length, self::$encoding);
        }

        return substr($string, $start, $length); // @codeCoverageIgnore
    }

    /**
     * Make a string lowercase
     *
     * @param string $string
     * @return string
     */
    public static function low($string): string
    {
        if (self::isMBString())
        {
            return mb_strtolower($string, self::$encoding);
        }

        return strtolower($string); // @codeCoverageIgnore
    }

    /**
     * Make a string uppercase
     *
     * @param string $string
     * @return string
     *
     * @SuppressWarnings(PHPMD.ShortMethodName)
     */
    public static function up($string): string
    {
        if (self::isMBString())
        {
            return mb_strtoupper($string, self::$encoding);
        }

        return strtoupper($string); // @codeCoverageIgnore
    }

    /**
     * Count the number of substring occurrences
     *
     * @param string $haystack
     * @param string $needle
     * @return int
     */
    public static function subCount($haystack, $needle)
    {
        if (self::isMBString())
        {
            return mb_substr_count($haystack, $needle, self::$encoding);
        }

        return substr_count($haystack, $needle); // @codeCoverageIgnore
    }

    /**
     * Checks if the $haystack starts with the text in the $needle.
     *
     * @param string $haystack
     * @param string $needle
     * @param bool $caseSensitive
     * @return bool
     */
    public static function isStart($haystack, $needle, $caseSensitive = false): bool
    {
        if ($caseSensitive)
        {
            return $needle === '' || self::pos($haystack, $needle) === 0;
        }

        return $needle === '' || self::iPos($haystack, $needle) === 0;
    }

    /**
     * Checks if the $haystack ends with the text in the $needle.
     * Case sensitive.
     *
     * @param string $haystack
     * @param string $needle
     * @param bool $caseSensitive
     * @return bool
     */
    public static function isEnd($haystack, $needle, $caseSensitive = false): bool
    {
        if ($caseSensitive)
        {
            return $needle === '' || self::sub($haystack, - self::len($needle)) === $needle;
        }

        $haystack = self::low($haystack);
        $needle = self::low($needle);

        return $needle === '' || self::sub($haystack, - self::len($needle)) === $needle;
    }

    /**
     * Trim whitespaces and other special chars
     *
     * @param string $value
     * @param bool $extendMode
     * @return string
     */
    public static function trim($value, $extendMode = false): string
    {
        $result = (string) trim($value);

        if ($extendMode)
        {
            $result = trim($result, chr(0xE3) . chr(0x80) . chr(0x80));
            $result = trim($result, chr(0xC2) . chr(0xA0));
            $result = trim($result);
        }

        return $result;
    }

    /**
     * Escape string before save it as xml content
     *
     * @param
     *            $string
     * @return mixed
     */
    public static function escXml($string)
    {
        $string = preg_replace('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $string);

        $string = str_replace([
                                '&',
                                '<',
                                '>',
                                '"',
                                "'"], [
                                        '&amp;',
                                        '&lt;',
                                        '&gt;',
                                        '&quot;',
                                        '&apos;'], $string);

        return $string;
    }

    /**
     * Escape UTF-8 strings
     *
     * @param string $string
     * @return string
     */
    public static function esc($string): string
    {
        return htmlspecialchars($string, ENT_NOQUOTES, self::$encoding);
    }

    /**
     * Convert camel case to human readable format
     *
     * @param string $input
     * @param string $separator
     * @param bool $toLower
     *            *
     * @return string
     */
    public static function splitCamelCase($input, $separator = '_', $toLower = true): string
    {
        $original = $input;

        $output = preg_replace([
                                    '/(?<=[^A-Z])([A-Z])/',
                                    '/(?<=[^0-9])([0-9])/'], '_$0', $input);
        $output = preg_replace('#_{1,}#', $separator, $output);

        $output = trim($output);
        if ($toLower)
        {
            $output = strtolower($output);
        }

        if ('' === $output)
        {
            return $original;
        }

        return $output;
    }

    /**
     * Convert test name to human readable string
     *
     * @param string $input
     * @return mixed|string
     */
    public static function testName2Human($input)
    {
        $original = $input;
        $input = self::getClassName($input);

        /**
         * @noinspection NotOptimalRegularExpressionsInspection
         */
        if (! preg_match('#^tests#i', $input))
        {
            $input = preg_replace('#^(test)#i', '', $input);
        }

        $input = preg_replace('#(test)$#i', '', $input);
        $output = preg_replace([
                                    '/(?<=[^A-Z])([A-Z])/',
                                    '/(?<=[^0-9])([0-9])/'], ' $0', $input);
        $output = trim(str_replace('_', ' ', $output));

        $output = implode(' ', array_filter(array_map(function ($item)
        {
            $item = ucwords($item);
            $item = trim($item);

            return $item;
        }, explode(' ', $output))));

        if (strcasecmp($original, $output) === 0)
        {
            return $original;
        }

        if ('' === $output)
        {
            return $original;
        }

        return $output;
    }

    /**
     * Generates a universally unique identifier (UUID v4) according to RFC 4122
     * Version 4 UUIDs are pseudo-random!
     * Returns Version 4 UUID format: xxxxxxxx-xxxx-4xxx-Yxxx-xxxxxxxxxxxx where x is
     * any random hex digit and Y is a random choice from 8, 9, a, or b.
     *
     * @see http://stackoverflow.com/questions/2040240/php-function-to-generate-v4-uuid
     *
     * @return string
     */
    public static function uuid(): string
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x', 
            // 32 bits for "time_low"
            random_int(0, 0xffff), random_int(0, 0xffff), 
            // 16 bits for "time_mid"
            random_int(0, 0xffff), 
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            random_int(0, 0x0fff) | 0x4000, 
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            random_int(0, 0x3fff) | 0x8000, 
            // 48 bits for "node"
            random_int(0, 0xffff), random_int(0, 0xffff), random_int(0, 0xffff));
    }

    /**
     * Get class name without namespace
     *
     * @param mixed $object
     * @param bool $toLower
     * @return mixed|string
     */
    public static function getClassName($object, $toLower = false)
    {
        $className = $object;
        if (is_object($object))
        {
            $className = get_class($object);
        }

        $result = $className;
        if (strpos($className, '\\') !== false)
        {
            $className = explode('\\', $className);
            reset($className);
            $result = end($className);
        }

        if ($toLower)
        {
            $result = strtolower($result);
        }

        return $result;
    }

    /**
     * Increments a trailing number in a string.
     * Used to easily create distinct labels when copying objects. The method has the following styles:
     * - default: "Label" becomes "Label (2)"
     * - dash: "Label" becomes "Label-2"
     *
     * @param string $string
     *            The source string.
     * @param string $style
     *            The the style (default|dash).
     * @param integer $next
     *            If supplied, this number is used for the copy, otherwise it is the 'next' number.
     * @return string
     */
    public static function inc($string, $style = 'default', $next = 0): string
    {
        $styles = [
                    'dash' => [
                                '#-(\d+)$#',
                                '-%d'],
                    'default' => [
                                    [
                                        '#\((\d+)\)$#',
                                        '#\(\d+\)$#'],
                                    [
                                        ' (%d)',
                                        '(%d)']]];

        $styleSpec = $styles[$style] ?? $styles['default'];

        // Regular expression search and replace patterns.
        if (is_array($styleSpec[0]))
        {
            $rxSearch = $styleSpec[0][0];
            $rxReplace = $styleSpec[0][1];
        }
        else
        {
            $rxSearch = $rxReplace = $styleSpec[0];
        }

        // New and old (existing) sprintf formats.
        if (is_array($styleSpec[1]))
        {
            $newFormat = $styleSpec[1][0];
            $oldFormat = $styleSpec[1][1];
        }
        else
        {
            $newFormat = $oldFormat = $styleSpec[1];
        }

        // Check if we are incrementing an existing pattern, or appending a new one.
        $matches = array();
        if (preg_match($rxSearch, $string, $matches))
        {
            $next = empty($next) ? ($matches[1] + 1) : $next;
            $string = preg_replace($rxReplace, sprintf($oldFormat, $next), $string);
        }
        else
        {
            $next = empty($next) ? 2 : $next;
            $string .= sprintf($newFormat, $next);
        }

        return $string;
    }

    /**
     * Splits a string of multiple queries into an array of individual queries.
     * Single line or line end comments and multi line comments are stripped off.
     *
     * @param string $sql
     *            Input SQL string with which to split into individual queries.
     * @return array
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public static function splitSql($sql): array
    {
        $start = 0;
        $open = false;
        $comment = false;
        $endString = '';
        $end = strlen($sql);
        $queries = [];
        $query = '';

        /**
         * @noinspection ForeachInvariantsInspection
         */
        for ($i = 0; $i < $end; $i ++)
        {
            $current = $sql[$i];
            $current2 = substr($sql, $i, 2);
            $current3 = substr($sql, $i, 3);
            $lenEndString = strlen($endString);
            $testEnd = substr($sql, $i, $lenEndString);

            if ($current === '"' || $current === "'" || $current2 === '--' || ($current2 === '/*' && $current3 !== '/*!' && $current3 !== '/*+') || ($current === '#' && $current3 !== '#__') || ($comment && $testEnd === $endString))
            {
                // Check if quoted with previous backslash
                $num = 2;

                while ($sql[$i - $num + 1] === '\\' && $num < $i)
                {
                    $num ++;
                }

                // Not quoted
                if ($num % 2 === 0)
                {
                    if ($open)
                    {
                        if ($testEnd === $endString)
                        {
                            if ($comment)
                            {
                                $comment = false;
                                if ($lenEndString > 1)
                                {
                                    $i += ($lenEndString - 1);
                                    $current = $sql[$i];
                                }
                                $start = $i + 1;
                            }
                            $open = false;
                            $endString = '';
                        }
                    }
                    else
                    {
                        $open = true;
                        if ('--' === $current2)
                        {
                            $endString = "\n";
                            $comment = true;
                        }
                        elseif ('/*' === $current2)
                        {
                            $endString = '*/';
                            $comment = true;
                        }
                        elseif ('#' === $current)
                        {
                            $endString = "\n";
                            $comment = true;
                        }
                        else
                        {
                            $endString = $current;
                        }
                        if ($comment && $start < $i)
                        {
                            $query .= substr($sql, $start, $i - $start);
                        }
                    }
                }
            }

            if ($comment)
            {
                $start = $i + 1;
            }

            if (($current === ';' && ! $open) || $i === $end - 1)
            {
                if ($start <= $i)
                {
                    $query .= substr($sql, $start, $i - $start + 1);
                }
                $query = trim($query);

                if ($query)
                {
                    if ($current !== ';')
                    {
                        $query .= ';';
                    }
                    $queries[] = $query;
                }

                $query = '';
                $start = $i + 1;
            }
        }

        return $queries;
    }

    /**
     * Converts any accent characters to their equivalent normal characters and converts any other non-alphanumeric
     * characters to dashes, then converts any sequence of two or more dashes to a single dash.
     * This function generates
     * slugs safe for use as URLs, and if you pass true as the second parameter, it will create strings safe for
     * use as CSS classes or IDs.
     *
     * @param string $string
     *            A string to convert to a slug
     * @param string $separator
     *            The string to separate words with
     * @param boolean $cssMode
     *            Whether or not to generate strings safe for CSS classes/IDs (Default to false)
     * @return string
     */
    public static function toSafeURLString($string, $separator = '-', $cssMode = false): string
    {
        $slug = preg_replace('/([^a-z0-9]+)/', $separator, strtolower(self::removeAccents($string)));
        $slug = trim($slug, $separator);
        if ($cssMode)
        {
            $digits = [
                        'zero',
                        'one',
                        'two',
                        'three',
                        'four',
                        'five',
                        'six',
                        'seven',
                        'eight',
                        'nine'];
            if (is_numeric($slug[0]))
            {
                $slug = $digits[$slug[0]] . substr($slug, 1);
            }
        }
        return $slug;
    }
}