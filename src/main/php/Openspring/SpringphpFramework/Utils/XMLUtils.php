<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Utils;

use Exception;

class XMLUtils
{

    public static function toObject($sxo)
    {
        return json_decode(json_encode($sxo));
    }

    public static function checkRequiredProperty($target, $prop)
    {
        if (! self::hasProperty($target, $prop)) throw new Exception("can not find '{$prop}' property in application.properties");
        return true;
    }

    public static function hasProperty($target, $prop)
    {
        return (! empty($target->$prop));
    }

    public static function getPropertyAttribute($attributes, $attribute, $alt = '')
    {
        return (! isset($attributes[$attribute])) ? $alt : trim((string) $attributes[$attribute]);
    }

    public static function getRequiredPropertyAttribute($attributes, $attribute)
    {
        if (! isset($attributes[$attribute])) throw new Exception("can not find required attribute '{$attribute}' on a required item in application.properties");
        return trim((string) $attributes[$attribute]);
    }
}