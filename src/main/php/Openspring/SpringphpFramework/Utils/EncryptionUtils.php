<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Utils;

use Exception;

/**
 * Crypting utils
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
class EncryptionUtils
{
    // DECLARE THE REQUIRED VARIABLES
    public static $ENC_METHOD = "AES-256-CBC";

    // THE ENCRYPTION METHOD.
    public static $ENC_KEY = "SOME_RANDOM_KEY";

    // ENCRYPTION KEY
    public static $ENC_IV = "SOME_RANDOM_IV";

    // ENCRYPTION IV.
    public static $ENC_SALT = "xS$";

    /**
     * Returns a unique string
     *
     * @return String
     */
    public static function newGuid(int $length = 32): String
    {
        $guid = md5(uniqid('', TRUE));
        $hyphenedGuid = substr($guid, 0, 8) . '-' . substr($guid, 8, 4) . '-' . substr($guid, 12, 4) . '-' . substr($guid, 16, 4) . '-' . substr($guid, 20, 12);
        $bracedGuid = '{' . $hyphenedGuid . '}';

        switch ($length)
        {
            case 36:
                return $hyphenedGuid;
            case 38:
                return $bracedGuid;
            default:
                return $guid;
        }
    }

    /**
     * Returns a unique number of 16 numbers
     *
     * @return int
     */
    public static function newUNumber(): int
    {
        $n1 = time();
        $n2 = rand(0, 9999);

        return str_pad($n1 . $n2, 16, '0', STR_PAD_RIGHT);
    }

    /**
     * DECLARE REQUIRED VARIABLES TO CLASS CONSTRUCTOR
     *
     * @param String $METHOD
     * @param String $KEY
     * @param String $IV
     * @param String $SALT
     * @return string
     */
    function __construct($METHOD = NULL, $KEY = NULL, $IV = NULL, $SALT = NULL)
    {
        try
        {
            // Setting up the Encryption Method when needed.
            self::$ENC_METHOD = (isset($METHOD) && ! empty($METHOD) && $METHOD != NULL) ? $METHOD : self::$ENC_METHOD;

            // Setting up the Encryption Key when needed.
            self::$ENC_KEY = (isset($KEY) && ! empty($KEY) && $KEY != NULL) ? $KEY : self::$ENC_KEY;

            // Setting up the Encryption IV when needed.
            self::$ENC_IV = (isset($IV) && ! empty($IV) && $IV != NULL) ? $IV : self::$ENC_IV;

            // Setting up the Encryption IV when needed.
            self::$ENC_SALT = (isset($SALT) && ! empty($SALT) && $SALT != NULL) ? $SALT : self::$ENC_SALT;
        } catch (Exception $e)
        {
            return "Caught exception: " . $e->getMessage();
        }
    }

    /**
     * THIS FUNCTION WILL ENCRYPT THE PASSED STRING
     *
     * @param String $string
     * @return String
     */
    public static function encrypt(String $string): String
    {
        try
        {
            $output = false;
            $key = hash('sha256', self::$ENC_KEY);
            // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
            $iv = substr(hash('sha256', self::$ENC_IV), 0, 16);
            $output = openssl_encrypt($string, self::$ENC_METHOD, $key, 0, $iv);
            $output = base64_encode($output);
            return $output;
        } catch (Exception $e)
        {
            return "Caught exception: " . $e->getMessage();
        }
    }

    /**
     * THIS FUNCTION WILL DECRYPT THE ENCRYPTED STRING.
     *
     * @param String $string
     * @return String
     */
    public static function decrypt(String $string): String
    {
        try
        {
            $output = false;
            // hash
            $key = hash('sha256', self::$ENC_KEY);
            // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
            $iv = substr(hash('sha256', self::$ENC_IV), 0, 16);

            $output = openssl_decrypt(base64_decode($string), self::$ENC_METHOD, $key, 0, $iv);
            return $output;
        } catch (Exception $e)
        {
            return "Caught exception: " . $e->getMessage();
        }
    }

    /**
     * THIS FUNCTION FOR PASSWORDS ONLY, BECAUSE IT CANNOT BE DECRYPTED IN FUTURE.
     *
     * @param String $input
     * @return boolean|string
     */
    public static function hashPassword(String $input): String
    {
        try
        {
            if (! isset($input) || $input == null || empty($input))
            {
                return false;
            }

            // GENERATE AN ENCRYPTED PASSWORD SALT
            $SALT = self::Encrypt(self::$ENC_SALT);
            $SALT = md5($SALT);
            // PERFORM MD5 ENCRYPTION ON PASSWORD SALT.

            // ENCRYPT PASSWORD
            $input = md5(self::Encrypt(md5($input)));
            $input = self::Encrypt($input);
            $input = md5($input);

            // PERFORM ANOTHER ENCRYPTION FOR THE ENCRYPTED PASSWORD + SALT.
            $Encrypted = self::Encrypt($SALT) . self::Encrypt($input);
            $Encrypted = sha1($Encrypted . $SALT);

            // RETURN THE ENCRYPTED PASSWORD AS MD5
            return md5($Encrypted);
        } catch (Exception $e)
        {
            return "Caught exception: " . $e->getMessage();
        }
    }
}