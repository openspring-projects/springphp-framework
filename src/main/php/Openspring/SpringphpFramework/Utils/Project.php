<?php

/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Utils;

use Openspring\SpringphpFramework\Context\Environment;
use Openspring\SpringphpFramework\IO\DirectoryUtils;
use Openspring\SpringphpFramework\IO\FileUtils;
use Openspring\SpringphpFramework\Type\Any;

class Project
{
    static $props = array();

    public static function build(Any $projectInfo, String $envId, array $props = array())
    {
        $configPath = 'src/main/resources/';
        static::$props = $props;
        $projectPath = $projectInfo->targetPath ;

        $dist = FileUtils::standarizePath($projectInfo->dist);
        if (! FileUtils::isAbsolutePath($dist))
        {
            $dist = FileUtils::standarizePath($projectPath . '/' . $projectInfo->dist);
        }

        if (! FileUtils::parentExists($dist))
        {
            return "Build target directory parent not found " . FileUtils::getFileParent($dist);
        }

        $contextRoot = $props[Environment::SPRINGPHP_APPLICATION_CONTEXT_PATH];
        $targetContextRoot = $projectInfo->context_root;

        if (! DirectoryUtils::exists($projectPath)) return "Project folder not found " . $projectPath;

        // source
        $mainSrcPathValue = static::getProperty(Environment::SPRINGPHP_PATH_SOURCES);
        $sourcesPath = static::fixPath($projectPath . $mainSrcPathValue);
        $resourcesPath = static::fixPath($projectPath . '/' . $configPath);
        $persistenceFilePath = $resourcesPath . 'persistence.xml';
        $contextFilePath = $resourcesPath . '.context';
        $composerFilePath = $projectPath . 'composer.json';

        if (DirectoryUtils::exists($dist)) DirectoryUtils::removeDirectory($dist, TRUE);

        @mkdir($dist);

        // dest
        $targetBaseDir = static::fixPath($projectPath . '/target');
        if (DirectoryUtils::exists($dist)) $targetBaseDir = $dist;

        $targetBinPath = static::fixPath($targetBaseDir . '/bin');
        $targetResourcesPath = static::fixPath($targetBaseDir . '/WEB-INF/');
        $targetPersistenceFilePath = $targetBaseDir . '/WEB-INF/persistence.xml';
        $targetContextFilePath = $targetBaseDir . '/WEB-INF/.context';
        $targetVendorPath = static::fixPath($targetResourcesPath . '/lib');

        // create bin directory if does not exists and empty it
        DirectoryUtils::createIfNotExists($targetBaseDir, true);
        DirectoryUtils::createIfNotExists($targetBinPath);
        DirectoryUtils::createIfNotExists($targetResourcesPath);
        DirectoryUtils::createIfNotExists($targetVendorPath);

        // copy src to bin
        if (! DirectoryUtils::exists($sourcesPath)) return "Source folder not found " . $sourcesPath;
        DirectoryUtils::copyDir($sourcesPath, $targetBinPath);

        // copy base files
        $indexFilePath = static::fixPath($projectPath . '/index.php');
        $htaccessFilePath = static::fixPath($projectPath . '/.htaccess');

        if (! file_exists($indexFilePath)) return "index.php file not found " . $indexFilePath;
        if (! file_exists($htaccessFilePath)) return ".htaccess file not found " . $htaccessFilePath;

        $params = array();
        $params[$mainSrcPathValue] = "bin/";
        $params['/vendor/'] = "/WEB-INF/lib/";
        $params[$configPath] = "WEB-INF/";
        FileUtils::createFileFromTemplate($indexFilePath, static::fixPath($targetBaseDir . '/index.php'), $params);

        $params = array();
        $params[$contextRoot] = $targetContextRoot;
        FileUtils::createFileFromTemplate($htaccessFilePath, static::fixPath($targetBaseDir . '/.htaccess'), $params);

        // copy config: application.properties
        $applicationPropertiesFile = static::fixPath($resourcesPath . '/application.properties');
        $applicationPropertiesFileEnv = Strings::replace('application.properties', "application-{$envId}.properties", $applicationPropertiesFile);
        if (file_exists($applicationPropertiesFileEnv))
        {
            FileUtils::copyFile($applicationPropertiesFileEnv, static::fixPath($targetResourcesPath . '/application.properties'));
        }
        else
        {
            if (! file_exists($applicationPropertiesFile)) return "application-{$envId}.properties nor application.properties file not found in project directory";
            FileUtils::copyFile(static::fixPath($resourcesPath . '/application.properties'), static::fixPath($targetResourcesPath . '/application.properties'));
        }

        // copy config: persistence.xml
        $persistFile = $persistenceFilePath;
        $persistFileProd = Strings::replace('persistence.xml', "persistence-{$envId}.xml", $persistFile);
        if (file_exists($persistFileProd))
        {
            FileUtils::copyFile($persistFileProd, $targetPersistenceFilePath);
        }
        else
        {
            if (! file_exists($persistFile)) return "persistence-{$envId}.xml nor persistence.xml file not found in " . Strings::replace('persistence.xml', '', $persistFile);
            FileUtils::copyFile(static::fixPath($persistenceFilePath), $targetPersistenceFilePath);
        }
        
        // copy config: .context
        if (file_exists($contextFilePath))
        {
            FileUtils::copyFile($contextFilePath, $targetContextFilePath);
        }
        else
        {
            return "Context file not found in " . Strings::replace('.context', '', $contextFilePath);
        }

        // remove dev dependencies from package.json and run composer install commands
        if (! file_exists($composerFilePath)) return "composer.json file not found " . $composerFilePath;

        $vendorDir = 'vendor-dir';
        $psr4 = "psr-4";
        
        $composer = json_decode(file_get_contents($composerFilePath));
        $composer->config->$vendorDir = 'WEB-INF/lib';
        if (property_exists($composer, 'autoload'))
        {
            if (property_exists($composer->autoload, $psr4))
            {
                foreach($composer->autoload->$psr4 as $key => $value)
                {
                    $composer->autoload->$psr4->$key = 'bin/';
                }
            }
        }

        file_put_contents($targetBaseDir . 'composer.json', json_encode($composer));

        // run composer --no-dev command on targetBaseDir
        chdir($targetBaseDir);
        static::execInBackground("composer install --no-dev");
        static::execInBackground("composer dump-autoload --optimize");
        
        // remove temporary files
        FileUtils::removeFile($targetBaseDir . 'composer.json');
        FileUtils::removeFile($targetBaseDir . 'composer.lock');
        

        return true;
    }
    
    private static function execInBackground(String $cmd)
    {
        if (substr(php_uname(), 0, 7) == "Windows"){
            pclose(popen("start /B /wait $cmd", "r"));
        }
        else
        {
            exec("$cmd > /dev/null &");
        }
    }

    private static function getProperty($name)
    {
        return (! isset(static::$props[$name])) ? '' : static::$props[$name];
    }

    private static function fixPath($path)
    {
        return Strings::replace('//', '/', $path);
    }
}