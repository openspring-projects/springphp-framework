<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Utils;

use Openspring\SpringphpFramework\Annotation\RequestMapping\RequestMapping;
use Openspring\SpringphpFramework\Context\ApplicationContext;
use Openspring\SpringphpFramework\Context\Environment;
use Openspring\SpringphpFramework\Core\Initializable;
use Openspring\SpringphpFramework\Exception\BaseException;
use Openspring\SpringphpFramework\Exception\IO\IOException;
use Openspring\SpringphpFramework\IO\DirectoryUtils;
use Openspring\SpringphpFramework\IO\FileUtils;
use Openspring\SpringphpFramework\IO\Path;
use Openspring\SpringphpFramework\IO\ProjectFile;
use Openspring\SpringphpFramework\Reflection\ClassReflection;
use Openspring\SpringphpFramework\Type\Any;
use Openspring\SpringphpFramework\Type\StringBuilder;
use Openspring\SpringphpFramework\Web\RestController;

class Rest implements Initializable
{

    public static function initialize(): void
    {
        // Empty
    }

    public static function scanForRestControllers($targetProjectPath = null, $targetCompSubPath = null, String $contextRoot = null)
    {
        $container = new Any();

        // add beans
        $allBeans = static::scanForBeans($targetProjectPath);
        $container->beans = $allBeans;

        // add restcontrollers
        $contextRoot = ($contextRoot == null) ? Environment::getContextRoot() : $contextRoot;

        // open springphp container file for modifications
        $fcontainer = (Strings::isEmpty($targetProjectPath)) ? Path::getResources('/.context') : $targetProjectPath . '/.context';
        $container->mapping = array();
        $container->beans = array();

        foreach ($allBeans as $clazzQName)
        {
            try
            {
                if (! ClassReflection::extends($clazzQName, RestController::class))
                {
                    $container->beans[] = $clazzQName;
                    continue;
                }

                $classInfo = ClassReflection::parseClass($clazzQName);
                if ($classInfo == null || count($classInfo->methods) == 0) continue;

                $wcomp = $classInfo->name;

                // rewrite urls
                foreach ($classInfo->getMethods() as $method)
                {
                    $requestMapping = Cast::asRequestMapping(ClassReflection::getAnnotation($method, RequestMapping::class));

                    if ($requestMapping == null) continue;

                    $action = ucfirst($method->getName());
                    $methods = $requestMapping->getMethod();
                    $authGroups = $requestMapping->getAuthorizedGroups();
                    $authRoles = $requestMapping->getAuthorizedRoles();

                    if ($requestMapping->isValid())
                    {
                        $mappingItem = $requestMapping->getMappingItem($methods, $authGroups, $authRoles, $classInfo->getQualifiedName(), $wcomp, $action, $contextRoot, $method->getParameters());
                        $mappingItem->path = self::getRightPath($mappingItem->path, $method->getParameters());
                        $container->mapping[] = $mappingItem;
                    }
                }
            } catch (BaseException $e)
            {
                $e->addDetails('In class ' . $clazzQName);

                throw $e;
            }
        }

        // save file to main/resources/.context
        $jsonData = json_encode($container);
        if (file_put_contents($fcontainer, $jsonData) === FALSE)
        {
            throw new IOException('can not create context file ' . $fcontainer);
        }

        // create htaccess file
        self::createHtaccess($targetProjectPath);

        return $fcontainer;
    }

    protected static function getRightPath(String $path, Array $margs): String
    {
        foreach ($margs as $marg)
        {
            $pattern = $marg->getType() == 'int' ? '([0-9]+)' : '([^\/]+)';
            $path = Strings::replace('$' . $marg->getName(), $pattern, $path);
        }

        $parts = explode('/', $path);
        $newParts = array();
        $newPart = '';
        foreach ($parts as $part)
        {
            $newParts[] = (Strings::startsWith($part, '$')) ? '([^\/]+)' : $part;
        }

        $newPath = implode('/', $newParts);

        return $newPath;
    }

    public static function scanForBeans(?String $targetProjectPath): array
    {
        $base = (! Strings::isEmpty($targetProjectPath)) ? $targetProjectPath : Environment::getRootPath();
        $baseDirs = ApplicationContext::getScanBeanDirs($base);
        $excludeDirs = ApplicationContext::getExcludeBeanDirs($base);

        // get all candidates files
        $candidateFiles = array();
        foreach ($baseDirs as $baseDir)
        {
            $files = DirectoryUtils::getFilesRecursively($baseDir, '*.php');
            $candidateFiles = array_merge($candidateFiles, $files);
        }

        // excludes
        foreach ($excludeDirs as $excludeDir)
        {
            foreach ($candidateFiles as $key => $candidateFile)
            {
                if (Strings::startsWith($candidateFile, $excludeDir))
                {
                    unset($candidateFiles[$key]);
                }
            }
        }

        // go for scanning now
        $beans = array();
        foreach ($candidateFiles as $key => $candidateFile)
        {
            $beanClazz = ClassReflection::getBeanClassNameFromFile($candidateFile);
            if ($beanClazz != null)
            {
                $beans[] = $beanClazz;
            }
        }

        return $beans;
    }

    private static function createHtaccess($targetProjectPath = null): bool
    {
        $htaccess = ($targetProjectPath == null) ? Path::getRoot('.htaccess') : $targetProjectPath . '.htaccess';

        if (FileUtils::exists($htaccess)) return true;

        $sb = new StringBuilder();
        $contextRoot = Strings::replace('/', '', Environment::getContextRoot());

        $sb->append('RewriteEngine On', 0, 0);
        $sb->append('SetEnvIf Authorization "(.*)" HTTP_AUTHORIZATION=$1', 1, 0);
        $sb->append('RewriteRule ^(.*)$ /' . $contextRoot . '/index.php [L,END]', 1, 0);

        if (file_put_contents($htaccess, $sb->toString("\r\n")) === FALSE)
        {
            throw new IOException('can not create httaccess file ' . $htaccess);
        }

        return $htaccess;
    }

    public static function generateDocsForRestControllers($targetProjectPath = null, $targetCompSubPath = null, $projectName = null, $targetBuidDistPath = null)
    {
        $sb = new StringBuilder();
        $compPath = ($targetProjectPath == null) ? Path::getControllers() : $targetProjectPath . $targetCompSubPath;
        $paths = DirectoryUtils::getFilesRecursively($compPath, '*Controller.php');
        $projectName = ($projectName == null) ? Environment::getDisplayName() : $projectName;

        // add styles
        $sb->append('<html>');
        $sb->append('<head>');
        $sb->append('<style>');
        $sb->append('html * {font-family: monospace;}');
        $sb->append('.sub-title-1 {font-size:13px;color:#5d5656;}');
        $sb->append('.page-title {border: 1px solid #03A9F4;background-color: #03A9F4;padding: 7px;color: #f8f8f8;font-size: 30px;border-radius: 7px;}');
        $sb->append('.comp-head {font-size: 25px;margin-bottom: 15px;color: #636161;font-weight: bold;padding: 5px;border-bottom: 1px solid #e8e8e8;}');
        $sb->append('.comp-doc {background-color: #f9f9f9;color: #000000;font-size: 25px;padding: 0px 2px;border: 1px solid #f3eeee;margin: 0px;}');
        $sb->append('.class-doc {font-size: 16px;color: #888282;margin: 5px;padding: 0px;}');
        $sb->append('.api-key {min-width: 50px !important;display: inline-block;text-align: center;font-size: 16px;color: #f1f1f1;border-radius: 7px;padding: 3px;}');

        $sb->append('.api-post {background-color: #49cc90 !important;}');
        $sb->append('.api-put {background-color: #fca130 !important;}');
        $sb->append('.api-delete {background-color: #f93e3e !important;}');
        $sb->append('.api-get {background-color: #61affe !important;}');

        $sb->append('.api-value {background-color: #9c3818;font-size: 15px;color: #fdfdfd;border-radius: 7px;padding: 2px;}');
        $sb->append('.api-ws-doc {padding-left: 10px; font-weight: normal; font-size: 14px;}');
        $sb->append('.api-ws {background-color: #e1f0fd;border: 1px solid #b3dff3;padding: 5px;font-size: 16px;border-radius: 7px;margin-bottom: 7px;}');
        $sb->append('.api-desc {color: #6d6d6d;font-size: 16px;padding: 5px;margin-bottom: 2px;}');
        $sb->append('</style>');
        $sb->append('</head>');
        $sb->append('<body>');
        // add static parts
        $sb->append('<h3 class="page-title">REST API Documentation for project ' . $projectName . ' ' . ProjectFile::getVersion() . '');
        $sb->append('<br/><span class="sub-title-1">Generated on ' . Dates::getCurrentDateTime() . '<span></h3>', 0, 1);

        foreach ($paths as $path)
        {
            // For test Only: if (!Strings::endsWith($path, "SMSController.php")) continue;

            $classInfo = ClassReflection::parse($path);
            if ($classInfo == null || count($classInfo->methods) == 0) continue;

            // get class name
            $wcomp = Strings::replace('Controller', '', $classInfo->getName());

            // rewrite urls
            if (count($classInfo->getMethods()) == 0) continue;

            $sb->append("<h4 class='comp-head'>Controller $wcomp</h4>", 1);
            $sb->append('<div class="class-doc">' . self::getCleanDoc($classInfo->getComments()) . '</div>');
            foreach ($classInfo->getMethods() as $method)
            {
                $requestMapping = ClassReflection::getAnnotation($method, RequestMapping::class);

                if ($requestMapping == null) continue;

                if ($requestMapping->isValid())
                {
                    $fparams = '';
                    foreach ($method->getParameters() as $param)
                    {
                        $fparams .= '<li>' . $param->getType() . ' ' . $param->getName() . '</li>';
                    }

                    $sb->append('<div class="api-ws">');
                    foreach ($requestMapping->getMethod() as $m)
                    {
                        $sb->append('<span class="api-key api-' . strtolower(trim($m)) . '">' . $m . '</span> ');
                    }

                    $sb->append($requestMapping->getValue() . '<span class="api-ws-doc">' . self::getCleanDoc($requestMapping->getDoc()) . '</span>' . '</div>');
                    $sb->append('<div><strong>Parameters</strong><ul>' . self::untagDoc(Strings::removeLastChars($fparams, 2)) . '</ul></div>');
                    $sb->append('</div>');
                }
            }
        }
        $sb->append('</body>');
        $sb->append('</html>');

        $docRoot = $targetBuidDistPath;

        if (Strings::isEmpty($docRoot))
        {
            $docRoot = $targetProjectPath . 'target/';
        }

        DirectoryUtils::createIfNotExists($docRoot, true);

        $docRoot .= 'doc/';
        DirectoryUtils::createIfNotExists($docRoot, true);

        file_put_contents($docRoot . 'index.html', Strings::replace('$', '', $sb->toString('')));

        return $docRoot . 'index.html';
    }

    private static function getCleanDoc($dirtyDoc)
    {
        $doc = trim(Strings::replaceAll($dirtyDoc, array(
                                                            '/**' => '',
                                                            '*/' => '',
                                                            '*' => '')));
        $doc = Strings::replace("\n", "<br/>", $doc);

        return self::untagDoc($doc);
    }

    private static function untagDoc($doc)
    {
        return Strings::replaceAll($doc, array(
                                                'J-Response' => '<a href="j-response.html">J-Response</a>',
                                                'J-File' => '<a href="j-file.html">J-File</a>',
                                                'requestBody' => '<a href="request-body.html">requestBody</a>'));
    }
}