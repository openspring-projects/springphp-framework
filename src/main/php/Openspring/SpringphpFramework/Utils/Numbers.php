<?php
/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Utils;

date_default_timezone_set('America/New_York');

/**
 * Utility class for numbers
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
class Numbers
{
    /**
     *
     * @param String $number
     * @param number $decimal
     * @param string $separator
     * @return string
     */
    public static function formatDecimal(String $number, int $decimal = 2, String $separator = ','): String
    {
        return number_format((float) $number, $decimal, $separator, ' ');
    }
}