<?php

/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Openspring\SpringphpFramework\Utils;

class PHPClassUtils
{

    public static function getClassFullNameFromFile($filePathName)
    {
        return self::getClassNamespaceFromFile($filePathName) . self::getClassNameFromFile($filePathName);
    }

    public static function getClassObjectFromFile($filePathName)
    {
        $classString = self::getClassFullNameFromFile($filePathName);
        $object = new $classString();

        return $object;
    }

    protected static function getClassNamespaceFromFile($filePathName)
    {
        $namespace = '';
        if ($file = fopen($filePathName, "r"))
        {
            while (! feof($file))
            {
                $line = fgets($file);
                if (Strings::startsWith($line, 'namespace'))
                {
                    $namespace = Strings::replace('namespace', '', $line);
                    $namespace = Strings::replace(';', '', $namespace);
                    $namespace = trim($namespace);
                    break;
                }
            }
            fclose($file);
        }

        return trim($namespace) !== '' ? '\\' . $namespace . '\\' : '';
    }

    protected static function getFullClassNameFromFile($filePathName)
    {
        $namespace = '';
        $clazz = '';
        if ($file = fopen($filePathName, "r"))
        {
            while (! feof($file))
            {
                $line = fgets($file);
                if (Strings::startsWith($line, 'namespace'))
                {
                    $namespace = Strings::replace('namespace', '', $line);
                    $namespace = Strings::replace(';', '', $namespace);
                    $namespace = trim($namespace);
                    continue;
                }

                if (Strings::containIgnoreCase($line, "class "))
                {
                    $parts = explode(" ", trim($line));
                    $clazz = trim($parts[1]);
                    if (strlen($namespace)) $namespace = "\\" . $namespace . "\\";
                    break;
                }
            }
            fclose($file);
        }
        echo "<br/>### " . $namespace . $clazz;
        return $namespace . $clazz;
    }

    public static function getClassNameFromFile($filePathName)
    {
        $php_code = file_get_contents($filePathName);

        $classes = array();
        $tokens = token_get_all($php_code);
        $count = count($tokens);
        for ($i = 2; $i < $count; $i ++)
        {
            if ($tokens[$i - 2][0] == T_CLASS && $tokens[$i - 1][0] == T_WHITESPACE && $tokens[$i][0] == T_STRING)
            {

                $class_name = $tokens[$i][1];
                $classes[] = $class_name;
            }
        }

        return $classes[0];
    }
}
