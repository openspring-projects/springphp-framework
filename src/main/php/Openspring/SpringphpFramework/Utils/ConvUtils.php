<?php
/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Utils;


/**
 * Common Conversion utils
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
class ConvUtils
{
    const KB = 1024;
    const MB = 1048576;
    const GB = 1073741824;
    const TB = 1099511627776;

    /**
     * Convert html to utf8 text
     *
     * @param String $html
     * @return String
     */
    public static function htmlToUTF8(String $html): String
    {
        return htmlentities($html, ENT_NOQUOTES, 'UTF-8', false);
    }

    /**
     * Convert string to boolean
     *
     * @param String $value
     * @return bool
     */
    public static function toBool(String $value): bool
    {
        return (in_array(strtolower($value), array('yes','true','1'))) ? true : false;
    }

    /**
     * Convert boolean to int (0 for false and 1 for true)
     *
     * @param bool $bool
     * @return int
     */
    public static function boolToInt(bool $bool): int
    {
        return ($bool) ? 1 : 0;
    }

    /**
     * Convert string to its hexadecimal value
     *
     * @param String $string
     * @return String
     */
    public static function stringToHex(String $string): String
    {
        $hex = '';
        for ($i = 0; $i < strlen($string); $i ++)
        {
            $hex .= dechex(ord($string[$i]));
        }

        return $hex;
    }

    /**
     * Convert hexadecimal value back to its normal value
     *
     * @param String $hex
     * @return String
     */
    public static function hexToString(String $hex): String
    {
        $string = '';
        for ($i = 0; $i < strlen($hex) - 1; $i += 2)
        {
            $string .= chr(hexdec($hex[$i] . $hex[$i + 1]));
        }

        return $string;
    }
}