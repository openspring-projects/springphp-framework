<?php

/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Utils;

use ReflectionClass;
use ReflectionMethod;

class ClassUtils
{
    /**
     * Return a class constructor parameters
     * 
     * @param mixed $clazz
     * @return array|NULL
     */
    public static function getConstructorParameters($clazz): ?array
    {
        $constructor = static::getConstructor($clazz);
        if ($constructor == null) return array();
        return $constructor->getParameters();
    }

    /**
     * Return a class constructor
     * 
     * @param mixed $clazz
     * @return ReflectionMethod|NULL
     */
    public static function getConstructor($clazz): ?ReflectionMethod
    {
        $class = new ReflectionClass($clazz);

        return $class->getConstructor();
    }

    /**
     * Return setter name of a given property
     *
     * @param String $property
     * @return String
     */
    public static function getSetter(String $property): String
    {
        return 'set' . ucfirst($property);
    }

    /**
     * Return getter name of a given property
     *
     * @param String $property
     * @return String
     */
    public static function getGetter(String $property): String
    {
        return 'get' . ucfirst($property);
    }
}