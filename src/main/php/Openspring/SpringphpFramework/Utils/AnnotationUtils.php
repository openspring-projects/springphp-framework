<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Utils;

use Openspring\SpringphpFramework\Annotation\AnnotationAttribute;
use Openspring\SpringphpFramework\Annotation\AnnotationElement;
use Openspring\SpringphpFramework\Exception\ElementNotFoundException;
use Openspring\SpringphpFramework\Exception\InvalidJsonStringException;
use Openspring\SpringphpFramework\Annotation\AnnotationArgumentProcessor;

class AnnotationUtils
{

    public static function getElements(String $doc): array
    {
        $annotationsElements = array();

        if (Strings::isEmpty($doc))
        {
            return $annotationsElements;
        }

        if (! Strings::contain($doc, '@'))
        {
            return $annotationsElements;
        }

        $annotsDocs = Arrays::explode('@', $doc, true);
        foreach ($annotsDocs as $annotDoc)
        {
            if (! Strings::contain($annotDoc, '@')) continue;
            $annotationsElements[] = self::getElement($annotDoc);
        }

        return $annotationsElements;
    }

    public static function getElement(String $annotDoc): AnnotationElement
    {
        $annotName = Strings::getBetween($annotDoc, '@', '(', false);
        if (Strings::isEmpty($annotName))
        {
            throw new ElementNotFoundException('No valid annotation found in doc ' . $annotDoc);
        }

        // set annotation name
        $annotationElement = new AnnotationElement($annotName);
        $jsonArray = array();
        $json = Strings::getBetween($annotDoc, '(', ')', false);
        // inset AnnotationArgumentProcessor here
        if (! Strings::isEmpty($json))
        {
            //$jsonArray = json_decode($json, true);
            // try get it the other way
            $aaProc = new AnnotationArgumentProcessor($json);
            $jsonArray = $aaProc->decodeJson(true);
            if ($jsonArray == NULL || count($jsonArray) == 0)
            {
                throw new InvalidJsonStringException('Invalid annotation arguments found in doc ' . $annotDoc);
            }
        }

        // set annotation attributes
        foreach ($jsonArray as $name => $value)
        {
            $name = Strings::removeAll($name, array('"',"'"));
            if (is_string($value)) $value = Strings::removeAll($value, array('"',"'"));
            $annotationElement->addAttribute(new AnnotationAttribute($name, $value));
        }

        return $annotationElement;
    }

    public static function castToAnnotationElement($annotationElement): ?AnnotationElement
    {
        return $annotationElement;
    }
}