<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Utils;

use Openspring\SpringphpFramework\Orm\Type\DataType;

class DatabaseUtils
{
    /**
     * Quote a value if necessary
     *
     * @param String $str
     * @param String $type
     * @return String
     */
    public static function cleanString(String $str, String $type = DataType::VARCHAR): String
    {
        $len = strlen($str);
        $escapeCount = 0;
        $cleanString = '';

        for ($offset = 0; $offset < $len; $offset ++)
        {
            switch ($c = $str[$offset])
            {
                case "'":
                    if ($escapeCount % 2 == 0) $cleanString .= "\\";
                    $escapeCount = 0;
                    $cleanString .= $c;
                    break;
                case '"':
                    if ($escapeCount % 2 == 0) $cleanString .= "\\";
                    $escapeCount = 0;
                    $cleanString .= $c;
                    break;
                case '\\':
                    $escapeCount ++;
                    $cleanString .= $c;
                    break;
                default:
                    $escapeCount = 0;
                    $cleanString .= $c;
            }
        }

        if (in_array($type, array(
            DataType::HTML,
            DataType::XML)))
        {
            $cleanString = Convertors::htmlToUTF8($cleanString);
        }

        return $cleanString;
    }

    /**
     * Convert an array to sql in operator
     * Example: array(1, 2, 3) would be converted as (1, 2, 3)
     *
     * @param array $array
     * @param bool $isstring
     * @return String
     */
    public static function arrayToInOperator(array $array, bool $isstring = false): String
    {
        return ($isstring) ? "('" . implode("','", $array) . "')" : '(' . implode(',', $array) . ')';
    }

    /**
     * Generate an alias for an entity name
     *
     * @param String $ename
     * @return String
     */
    public static function getEntityAlias(String $ename): String
    {
        $out = 0;
        for ($i = 0; $i < strlen($ename); $i ++)
        {
            $out += ord(substr($ename, $i, 1));
        }
        
        return '_tbl' . $out;
    }
}