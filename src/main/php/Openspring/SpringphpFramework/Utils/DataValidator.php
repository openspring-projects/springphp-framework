<?php
/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Utils;

/**
 * Data validation helper
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
class DataValidator
{
    /**
     * Check if phone number is valid
     *
     * @param String $phone
     * @param int $minLength
     * @param int $maxLength
     * @return bool
     */
    public static function isPhoneNumber(?String $phone, bool $ignoreFormat = true, int $minLength = 10, int $maxLength = 25): bool
    {
        $cleanNumber = $phone;
        
        // clean phone number
        if ($ignoreFormat)
        {
            $cleanNumber = trim(Strings::removeChars($phone, array('.', '-', ' ', '+', '-', '(', ')')));
        }
        
        if (!is_numeric($cleanNumber)) return false;

        return !(strlen($cleanNumber) < $minLength || strlen($cleanNumber) > $maxLength);
    }

    /**
     * Check if email is valid
     *
     * @param String $email
     * @return bool
     */
    public static function isEmail(?String $email): bool
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
}