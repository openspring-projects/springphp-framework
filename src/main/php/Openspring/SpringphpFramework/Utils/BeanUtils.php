<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Utils;

use Openspring\SpringphpFramework\Context\ApplicationContext;
use Openspring\SpringphpFramework\Core\Bean\HttpMessageConverter;
use Openspring\SpringphpFramework\Exception\Bean\NoSuchBeanDefinitionException;
use Openspring\SpringphpFramework\Exception\Http\MediaTypeNotAcceptableException;
use Openspring\SpringphpFramework\Http\Converter\JsonMessageConverter;
use Openspring\SpringphpFramework\Http\Converter\TextHTMLMessageConverter;

/**
 * Common beans utils
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
class BeanUtils
{

    /**
     * Return a convenable http message converter for given media type
     *
     * @param String $mediaType
     * @throws NoSuchBeanDefinitionException
     * @return HttpMessageConverter
     */
    public static function getHttpMessageConverterBean(String $mediaType): HttpMessageConverter
    {
        $mcs = ApplicationContext::getBeansOf(HttpMessageConverter::class);
        if (count($mcs) == 0)
        {
            ApplicationContext::addBeanSingleton(new TextHTMLMessageConverter());
            ApplicationContext::addBeanSingleton(new JsonMessageConverter());

            $mcs = ApplicationContext::getBeansOf(HttpMessageConverter::class);
        }

        if (count($mcs) == 0)
        {
            throw new NoSuchBeanDefinitionException('No bean found of type ' . HttpMessageConverter::class);
        }

        $mc = Cast::asMessageConverter(Arrays::search($mcs, $mediaType, 'mediaType'));
        if ($mc == null)
        {
            throw new MediaTypeNotAcceptableException('No bean found of type ' . HttpMessageConverter::class . ' for ' . $mediaType . ' type conversion');
        }

        return $mc;
    }
}