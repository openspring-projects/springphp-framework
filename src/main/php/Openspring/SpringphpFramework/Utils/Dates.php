<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Utils;

use function Openspring\SpringphpFramework\Utils\Dates\pluralize;
use DateTime;
use DateTimeZone;
date_default_timezone_set('Africa/Casablanca');

class Dates
{
    const MINUTE = 60;
    const HOUR = 3600;
    const DAY = 86400;
    const WEEK = 604800; // 7 days
    const MONTH = 2592000; // 30 days
    const YEAR = 31536000; // 365 days
    const SQL_DATE_FORMAT = 'Y-m-d';
    const SQL_DATE_TIME_FORMAT = 'Y-m-d H:i:s';
    const SQL_TIME_FORMAT = 'H:i:s';
    const SQL_NULL = '0000-00-00 00:00:00';

    public static function validateDate(String $date, String $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);

        return ($d && $d->format($format) === $date);
    }

    public static function createDate(String $date = null, String $format = 'Y-m-d'): ?DateTime
    {
        if ($date == null) return new DateTime();

        $d = DateTime::createFromFormat($format, $date);

        if (! $d || $d->format($format) !== $date) return null;

        return $d;
    }

    public static function getNowDateTime(): DateTime
    {
        return new DateTime('now');
    }

    public static function getMonthLastDay(int $month = 0, int $year = 0): int
    {
        $todayDate = new DateTime('now');

        if ($month == 0) $month = self::getMonth($todayDate);
        if ($year == 0) $year = self::getYear($todayDate);

        return cal_days_in_month(CAL_GREGORIAN, $month, $year);
    }

    public static function getCurrentDateTimePlus(int $days): ?DateTime
    {
        $futureDate = DateTime::createFromFormat('Y-m-d', Dates::getCurrentDate());
        $futureDate->modify('+' . $days . ' day');

        return $futureDate;
    }

    public static function getCurrentDateTimeMinus(int $days): ?DateTime
    {
        $pastDate = DateTime::createFromFormat('Y-m-d', Dates::getCurrentDate());
        $pastDate->modify('-' . $days . ' day');

        return $pastDate;
    }

    public static function getYear(DateTime $date = null): int
    {
        if ($date == null) $date = self::getNowDateTime();
        return intval($date->format('Y'));
    }

    public static function getMonth(DateTime $date = null): int
    {
        if ($date == null) $date = self::getNowDateTime();
        return intval($date->format('m'));
    }

    public static function getWeek(DateTime $date = null): int
    {
        if ($date == null) $date = self::getNowDateTime();
        return intval($date->format('w'));
    }

    public static function getDay(DateTime $date = null): int
    {
        if ($date == null) $date = self::getNowDateTime();
        return intval($date->format('d'));
    }

    public static function getHour(DateTime $date = null): int
    {
        if ($date == null) $date = self::getNowDateTime();
        return intval($date->format('H'));
    }

    public static function getMinute(DateTime $date = null): int
    {
        if ($date == null) $date = self::getNowDateTime();
        return intval($date->format('i'));
    }

    public static function getSecond(DateTime $date = null): int
    {
        if ($date == null) $date = self::getNowDateTime();
        return intval($date->format('s'));
    }

    public static function getTimeAsSeconds(DateTime $date = null): int
    {
        if ($date == null) $date = self::getNowDateTime();
        $h = self::getHour($date);
        $m = self::getMinute($date);
        $s = self::getSecond($date);

        if ($h == 0) $h = 24;

        return ($h * 60 * 60) + ($m * 60) + $s;
    }

    public static function getDateTimeAsSeconds(DateTime $date = null): int
    {
        if ($date == null) $date = self::getNowDateTime();
        return intval($date->format('U'));
    }

    public static function getDayName(DateTime $date): String
    {
        return $date->format('D');
    }

    public static function getDate(DateTime $date): String
    {
        return $date->format('Y-m-d');
    }

    public static function getTime(DateTime $date): String
    {
        return $date->format('H:i:s');
    }

    public static function getDateTime(DateTime $date): String
    {
        return $date->format('Y-m-d H:i:s');
    }

    /**
     * Convert to timestamp
     *
     * @param string|DateTime $time
     * @param bool $currentIsDefault
     * @return int
     */
    public static function toStamp($time = null, $currentIsDefault = true): int
    {
        if ($time instanceof DateTime)
        {
            return $time->format('U');
        }
        if (null !== $time)
        {
            $time = is_numeric($time) ? (int) $time : strtotime($time);
        }
        if (! $time)
        {
            $time = $currentIsDefault ? time() : 0;
        }
        return $time;
    }

    /**
     * @param mixed $time
     * @param null $timeZone
     * @return DateTime
     */
    public static function factory($time = null, $timeZone = null): DateTime
    {
        $timeZone = self::timezone($timeZone);
        if ($time instanceof DateTime)
        {
            return $time->setTimezone($timeZone);
        }
        $dateTime = new DateTime('@' . self::toStamp($time));
        $dateTime->setTimezone($timeZone);
        return $dateTime;
    }

    /**
     * Return a DateTimeZone object based on the current timezone.
     *
     * @param mixed $timezone
     * @return DateTimeZone
     */
    public static function timezone($timezone = null): DateTimeZone
    {
        if ($timezone instanceof DateTimeZone)
        {
            return $timezone;
        }
        $timezone = $timezone ?: date_default_timezone_get();
        return new DateTimeZone($timezone);
    }

    /**
     * Check if string is date
     *
     * @param string $date
     * @return bool
     *
     * @SuppressWarnings(PHPMD.ShortMethodName)
     */
    public static function is($date): bool
    {
        $time = strtotime($date);
        return $time > 0;
    }

    /**
     * Convert time for sql format
     *
     * @param null|int $time
     * @return string
     */
    public static function sql($time = null): string
    {
        return self::factory($time)->format(self::SQL_DATE_TIME_FORMAT);
    }

    /**
     * @param string|int $date
     * @param string $format
     * @return string
     */
    public static function human($date, $format = 'd M Y H:i'): string
    {
        return self::factory($date)->format($format);
    }

    /**
     * Returns true if date passed is within this week.
     *
     * @param string|int $time
     * @return bool
     */
    public static function isThisWeek($time): bool
    {
        return (self::factory($time)->format('W-Y') === self::factory()->format('W-Y'));
    }

    /**
     * Returns true if date passed is within this month.
     *
     * @param string|int $time
     * @return bool
     */
    public static function isThisMonth($time): bool
    {
        return (self::factory($time)->format('m-Y') === self::factory()->format('m-Y'));
    }

    /**
     * Returns true if date passed is within this year.
     *
     * @param string|int $time
     * @return bool
     */
    public static function isThisYear($time): bool
    {
        return (self::factory($time)->format('Y') === self::factory()->format('Y'));
    }

    /**
     * Returns true if date passed is tomorrow.
     *
     * @param string|int $time
     * @return bool
     */
    public static function isTomorrow($time): bool
    {
        return (self::factory($time)->format('Y-m-d') === self::factory('tomorrow')->format('Y-m-d'));
    }

    /**
     * Returns true if date passed is today.
     *
     * @param string|int $time
     * @return bool
     */
    public static function isToday($time): bool
    {
        return (self::factory($time)->format('Y-m-d') === self::factory()->format('Y-m-d'));
    }

    /**
     * Returns true if date passed was yesterday.
     *
     * @param string|int $time
     * @return bool
     */
    public static function isYesterday($time): bool
    {
        return (self::factory($time)->format('Y-m-d') === self::factory('yesterday')->format('Y-m-d'));
    }

    /**
     * Return current data as string using format 'Y-m-d'
     *
     * @return String
     */
    public static function getCurrentDate(String $format = 'Y-m-d'): String
    {
        return date($format);
    }

    /**
     * Return current datetime as string using format 'Y-m-d H:i:s'
     *
     * @return String
     */
    public static function getCurrentDateTime(String $format = 'Y-m-d H:i:s'): String
    {
        return date($format); // return 2019-01-20 05:19:13.000000
    }

    /**
     * TODO: Fix this function
     */
    public static function getCurrentMilliseconds()
    {
        return microtime(true) * 1000;
    }

    /**
     * TODO: Fix this function
     */
    public static function getCurrentSeconds()
    {
        $ms = self::getCurrentMilliseconds();

        return ($ms == 0) ? 0 : $ms / 100;
    }

    public static function getSeconds($ms)
    {
        return ($ms == 0) ? 0 : 100 / $ms;
    }

    public static function isDate($date, $format = 'Y-m-d H:i:s')
    {
        if ($date == null || trim($date) == '') return false;
        $d = DateTime::createFromFormat($format, $date);
        return ($d && $d->format($format) == $date);
    }

    public static function formatDate($date, $fformat = 'Y-m-d H:i:s', $tformat = 'd/m/Y H:i:s')
    {
        $d = DateTime::createFromFormat($fformat, $date);
        if ($d) return $d->format($tformat);
        return $date;
    }

    /**
     * Return difference between two dates as IntervalDate
     * Ex: Dates::getBetweenDates(fd, sd); => substruct sd from fd ans return difference as IntervalDate
     */
    public static function getBetweenDates(String $startDate, String $endDate)
    {
        $date1 = new DateTime($startDate);
        $date2 = new DateTime($endDate);

        return $date1->diff($date2);
    }

    /**
     * Return difference between two dates as IntervalDate
     * Ex: $diff = Dates::diffDatesInSecondes(Dates::getCurrentDate(), $someFutureDate); => x > 0 in secondes
     */
    public static function diffDatesInSecondes(String $startDate, String $endDate)
    {
        // Convert them to timestamps.
        $date1Timestamp = strtotime($startDate);
        $date2Timestamp = strtotime($endDate);

        // Calculate the difference.
        return ($date2Timestamp - $date1Timestamp);
    }

    /**
     * Return difference between two dates in seconds
     * Ex: Dates::getBetweenDatesInSeconds('2019-08-16 05:00:00', '2019-08-16 05:01:00')
     * returns 60
     */
    public static function getBetweenDatesInSeconds(DateTime $startDateTime, DateTime $endDateTime): int
    {
        // Convert them to timestamps.
        $date1Timestamp = strtotime($startDateTime->format('Y-m-d'));
        $date2Timestamp = strtotime($endDateTime->format('Y-m-d'));

        // Calculate the difference.
        return ($date2Timestamp - $date1Timestamp);
    }

    public function ago($datetime)
    {
        $interval = date_create('now')->diff($datetime);
        $suffix = ($interval->invert ? ' ago' : '');

        if ($interval->y >= 1) return $this->pluralize($interval->y, 'year') . $suffix;
        if ($interval->m >= 1) return $this->pluralize($interval->m, 'month') . $suffix;
        if ($interval->d >= 1) return $this->pluralize($interval->d, 'day') . $suffix;
        if ($interval->h >= 1) return $this->pluralize($interval->h, 'hour') . $suffix;
        if ($interval->i >= 1) return $this->pluralize($interval->i, 'minute') . $suffix;

        return pluralize($interval->s, 'second') . $suffix;
    }

    private function pluralize($count, $text)
    {
        return $count . (($count == 1) ? (" $text") : (" ${text}s"));
    }
}