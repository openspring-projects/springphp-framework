<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Utils;

use Openspring\SpringphpFramework\Type\Any;

class Arrays
{

    // TODO: to test
    public static function ofStrings($arr, bool $acceptEmpty = true): bool
    {
        if (! is_array($arr)) return false;
        if (count($arr) == 0) return $acceptEmpty;
        return array_sum(array_map('is_string', $arr)) == count($arr);
    }

    // TODO: to test
    public static function of()
    {
        $array = array();
        $values = func_get_args();
        foreach ($values as $value)
        {
            $array[] = $value;
        }

        return $array;
    }

    // TODO: to test
    public static function ofkv()
    {
        $array = array();
        $values = func_get_args();
        foreach ($values as $value)
        {
            $array[$value->getKey()] = $value->getValue();
        }

        return $array;
    }

    /**
     * Return a key value safely
     *
     * @param array $array
     * @param String|int $key
     * @param String $default
     * @return mixed
     */
    public static function get(Array $array, String $key, ?String $default = '')
    {
        return (isset($array[$key])) ? $array[$key] : $default;
    }

    /**
     * Return an array item value after isset test
     * It is a safe return
     *
     * @param
     *            $var
     * @param
     *            $default
     * @return string
     */
    public static function getItem(&$var, $default = null)
    {
        if (isset($var))
        {
            return $var;
        }
        return $default;
    }

    /**
     * Return trimmed array.
     * Removes all false values from array like false, 0, emptystring and null
     */
    public static function trim($array1 = array()): array
    {
        return array_filter($array1);
    }

    /**
     * Return true if array is empty.
     * false values are counted like false, 0, emptystring and null
     */
    public static function isEmpty($array1 = array()): bool
    {
        return (count($array1) == 0);
    }

    /**
     * Return an array of a specific object property in a given object array
     */
    public static function getColumn($array1, String $columnName): array
    {
        $column = array();
        foreach ($array1 as $item)
        {
            $column[] = $item->$columnName;
        }

        return $column;
    }

    /**
     * Return intersection values between 2 primitive typed arrays
     */
    public static function getIntersectionValues($array1 = array(), $array2 = array())
    {
        return array_intersect($array1, $array2);
    }

    /**
     * Return values existing in array1 and not in array2
     * Arrays must be of primitive data
     */
    public static function getDiffValues($array1 = array(), $array2 = array())
    {
        $diffItems = array();
        foreach ($array1 as $value)
        {
            if (! in_array($value, $array2))
            {
                $diffItems[] = $value;
            }
        }

        return $diffItems;
    }

    /**
     * Remove the duplicates from an array.
     *
     * @param array $array
     * @param bool $keepKeys
     * @return array
     */
    public static function unique($array, $keepKeys = false): array
    {
        if ($keepKeys)
        {
            $array = array_unique($array);
        }
        else
        {
            // This is faster version than the builtin array_unique().
            // http://stackoverflow.com/questions/8321620/array-unique-vs-array-flip
            // http://php.net/manual/en/function.array-unique.php
            $array = array_keys(array_flip($array));
        }

        return $array;
    }

    /**
     * Check is key exists
     *
     * @param string $key
     * @param mixed $array
     * @param bool $returnValue
     * @return mixed
     */
    public static function keyExists($key, $array, $returnValue = false)
    {
        $isExists = array_key_exists((string) $key, (array) $array);

        if ($returnValue)
        {
            if ($isExists)
            {
                return $array[$key];
            }

            return null;
        }

        return $isExists;
    }

    /**
     * Check is value exists in the array
     *
     * @param string $value
     * @param mixed $array
     * @param bool $returnKey
     * @return mixed
     *
     * @SuppressWarnings(PHPMD.ShortMethodName)
     */
    public static function valueExists($value, array $array, $returnKey = false)
    {
        $inArray = in_array($value, $array, true);

        if ($returnKey)
        {
            if ($inArray)
            {
                return array_search($value, $array, true);
            }

            return null;
        }

        return $inArray;
    }

    /**
     * Returns the first element in an array.
     *
     * @param array $array
     * @return mixed
     */
    public static function firstItem(array $array)
    {
        return reset($array);
    }

    /**
     * Returns the last element in an array.
     *
     * @param array $array
     * @return mixed
     */
    public static function lastItem(array $array)
    {
        return end($array);
    }

    /**
     * Returns the first key in an array.
     *
     * @param array $array
     * @return int|string
     */
    public static function firstKey(array $array)
    {
        reset($array);
        return key($array);
    }

    /**
     * Returns the last key in an array.
     *
     * @param array $array
     * @return int|string
     */
    public static function lastKey(array $array)
    {
        end($array);
        return key($array);
    }

    /**
     * Flatten a multi-dimensional array into a one dimensional array.
     *
     * @param array $array
     *            The array to flatten
     * @param boolean $preserveKeys
     *            Whether or not to preserve array keys. Keys from deeply nested arrays will
     *            overwrite keys from shallow nested arrays
     * @return array
     */
    public static function flat(array $array, $preserveKeys = true): array
    {
        $flattened = [];

        array_walk_recursive($array, function ($value, $key) use (&$flattened, $preserveKeys)
        {
            if ($preserveKeys && ! is_int($key))
            {
                $flattened[$key] = $value;
            }
            else
            {
                $flattened[] = $value;
            }
        });

        return $flattened;
    }

    /**
     * Searches for a given value in an array of arrays, objects and scalar values.
     * You can optionally specify
     * a field of the nested arrays and objects to search in.
     *
     * @param array $array
     *            The array to search
     * @param mixed $search
     *            The value to search for
     * @param bool $field
     *            The field to search in, if not specified all fields will be searched
     * @return null|mixed null on failure or the array element on success
     */
    public static function search(array $array, $search, $field = false)
    {
        // *grumbles* stupid PHP type system
        $search = (string) $search;
        foreach ($array as $key => $elem)
        {
            // *grumbles* stupid PHP type system

            $key = (string) $key;

            if ($field)
            {
                $value = Objects::get($elem, $field);
                if (is_object($elem) && $value == $search)
                {
                    return $array[$key];
                }

                if (is_array($elem) && $elem[$field] == $search)
                {
                    return $array[$key];
                }

                if (is_scalar($elem) && $elem == $search)
                {
                    return $array[$key];
                }
            }
            else
            {
                if (is_object($elem))
                {
                    $elem = (array) $elem;
                    /**
                     * @noinspection NotOptimalIfConditionsInspection
                     */
                    if (in_array($search, $elem, false))
                    {
                        return $array[$key];
                    }
                }
                elseif (is_array($elem) && in_array($search, $elem, false))
                {
                    return $array[$key];
                }
                elseif (is_scalar($elem) && $elem == $search)
                {
                    return $array[$key];
                }
            }
        }

        return null;
    }

    /**
     * Returns an array containing all the elements of arr1 after applying
     * the callback function to each one.
     *
     * @param string $callback
     *            Callback function to run for each element in each array
     * @param array $array
     *            An array to run through the callback function
     * @param boolean $onNoScalar
     *            Whether or not to call the callback function on non scalar values
     *            (Objects, resources, etc)
     * @return array
     */
    public static function mapDeep(array $array, $callback, $onNoScalar = false): array
    {
        foreach ($array as $key => $value)
        {
            if (is_array($value))
            {
                $args = [
                            $value,
                            $callback,
                            $onNoScalar];
                $array[$key] = call_user_func_array([
                                                        __CLASS__,
                                                        __FUNCTION__], $args);
            }
            elseif (is_scalar($value) || $onNoScalar)
            {
                $array[$key] = $callback($value);
            }
        }

        return $array;
    }

    /**
     * Check is array is type assoc
     *
     * @param
     *            $array
     * @return bool
     */
    public static function isAssoc($array): bool
    {
        return array_keys($array) !== range(0, count($array) - 1);
    }

    /**
     * Add cell to the start of assoc array
     *
     * @param array $array
     * @param string $key
     * @param mixed $value
     * @return array
     */
    public static function unshiftAssoc(array &$array, $key, $value): array
    {
        $array = array_reverse($array, true);
        $array[$key] = $value;
        $array = array_reverse($array, true);

        return $array;
    }

    /**
     * Get one field from array of arrays (array of objects)
     *
     * @param array $arrayList
     * @param string $fieldName
     * @return array
     */
    public static function getField($arrayList, $fieldName = 'id'): array
    {
        $result = [];

        if (! empty($arrayList) && is_array($arrayList))
        {
            foreach ($arrayList as $option)
            {
                if (is_array($option))
                {
                    $result[] = $option[$fieldName];
                }
                elseif (is_object($option))
                {
                    if (isset($option->{$fieldName}))
                    {
                        $result[] = $option->{$fieldName};
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Group array by key
     *
     * @param array $arrayList
     * @param string $key
     * @return array
     */
    public static function groupByKey(array $arrayList, $key = 'id'): array
    {
        $result = [];

        foreach ($arrayList as $item)
        {
            if (is_object($item))
            {
                if (isset($item->{$key}))
                {
                    $result[$item->{$key}][] = $item;
                }
            }
            elseif (is_array($item))
            {
                if (self::keyExists($key, $item))
                {
                    $result[$item[$key]][] = $item;
                }
            }
        }

        return $result;
    }

    /**
     * Recursive array mapping
     *
     * @param \Closure $function
     * @param array $array
     * @return array
     */
    public static function map($function, $array): array
    {
        $result = [];

        foreach ($array as $key => $value)
        {
            if (is_array($value))
            {
                $result[$key] = self::map($function, $value);
            }
            else
            {
                $result[$key] = $function($value);
            }
        }

        return $result;
    }

    /**
     * Sort an array by keys based on another array
     *
     * @param array $array
     * @param array $orderArray
     * @return array
     */
    public static function sortByArray(array $array, array $orderArray): array
    {
        return array_merge(array_flip($orderArray), $array);
    }

    /**
     * Add some prefix to each key
     *
     * @param array $array
     * @param string $prefix
     * @return array
     */
    public static function addEachKey(array $array, $prefix): array
    {
        $result = [];

        foreach ($array as $key => $item)
        {
            $result[$prefix . $key] = $item;
        }

        return $result;
    }

    /**
     * Convert assoc array to comment style
     *
     * @param array $data
     * @return string
     */
    public static function toComment(array $data): string
    {
        $result = [];
        foreach ($data as $key => $value)
        {
            $result[] = $key . ': ' . $value . ';';
        }

        return implode(PHP_EOL, $result);
    }

    /**
     * Wraps its argument in an array unless it is already an array
     *
     * @example Arr.wrap(null) # => []
     *          Arr.wrap([1, 2, 3]) # => [1, 2, 3]
     *          Arr.wrap(0) # => [0]
     * @param mixed $object
     * @return array
     */
    public static function wrap($object): array
    {
        if (null === $object)
        {
            return [];
        }

        if (is_array($object) && ! self::isAssoc($object))
        {
            return $object;
        }

        return [
                    $object];
    }

    /**
     * Implode nested array elements
     *
     * @param string $glue
     * @param array $array
     * @return string
     */
    public static function implode($glue, array $array): string
    {
        $result = '';

        foreach ($array as $item)
        {
            if (is_array($item))
            {
                $result .= self::implode($glue, $item) . $glue;
            }
            else
            {
                $result .= $item . $glue;
            }
        }

        if ($glue)
        {
            $result = Strings::sub($result, 0, 0 - Strings::len($glue));
        }

        return $result;
    }

    /**
     * Implode nested array elements
     *
     * @param string $glue
     * @param array $array
     * @return string
     */
    public static function explode(String $delimiter, String $string, $keepDelimiter = false): array
    {
        $result = explode($delimiter, $string);
        if (! $keepDelimiter) return $result;

        $first = false;
        foreach ($result as &$item)
        {
            if (! $first)
            {
                $first = true;
                continue;
            }

            $item = $delimiter . $item;
        }

        return $result;
    }

    /**
     * Return true if second array contains the first array
     *
     * @param array $find
     * @param array $array
     * @return bool
     */
    public static function contains(Array $find, Array $array): bool
    {
        return count(array_intersect($find, $array)) == count($find);
    }

    /**
     * Convert array to object
     *
     * @param array $array
     * @return Any
     */
    public static function toObject(Array $array): Any
    {
        $object = new Any();
        foreach ($array as $key => $value)
        {
            $object->$key = $value;
        }

        return $object;
    }
}