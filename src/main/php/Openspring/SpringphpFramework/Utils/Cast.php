<?php
/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Utils;

use Openspring\SpringphpFramework\Annotation\AnnotationAttribute;
use Openspring\SpringphpFramework\Annotation\AnnotationElement;
use Openspring\SpringphpFramework\Annotation\RequestMapping\RequestMapping;
use Openspring\SpringphpFramework\Annotation\Rest\MappingItem;
use Openspring\SpringphpFramework\Core\Bean\HttpMessageConverter;
use Openspring\SpringphpFramework\Core\Bean\JwtProcessor;
use Openspring\SpringphpFramework\Orm\DataSource\DataSourceConfig;
use Openspring\SpringphpFramework\Web\PathVariable;

/**
 * Casting class
 * 
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
class Cast
{
    public static function asAnnotationElement($annotationElement): ?AnnotationElement
    {
        return $annotationElement;
    }

    public static function asAnnotationAttribute($annotationAttribute): ?AnnotationAttribute
    {
        return $annotationAttribute;
    }
    
    public static function asRequestMapping($requestMapping): ?RequestMapping
    {
        return $requestMapping;
    }
    
    public static function asMessageConverter($messageConverter): ?HttpMessageConverter
    {
        return $messageConverter;
    }
    
    public static function asDataSourceConfig($dataSourceConfig): ?DataSourceConfig
    {
        return $dataSourceConfig;
    }
    
    public static function asMappingItem($mappingItem): ?MappingItem
    {
        $mi = MappingItem::New();
        Objects::autoMapObject($mappingItem, $mi);
        
        return $mi;
    }
    
    public static function asPathVariable($pathVariable): ?PathVariable
    {
        return $pathVariable;
    }
    
    public static function asJWTProcessor($jwtManager): ?JwtProcessor
    {
        return $jwtManager;
    }
    
    public static function asType($value, string $type)
    {
        if ($type == 'int') return self::asInt($value);
        if ($type == 'bool') return self::asBool($value);
        
        return self::asString($value);
    }
    
    public static function asString($value): ?String
    {
        return $value;
    }
    
    public static function asInt($value): int
    {
        return $value == null ? 0 : $value;
    }
    
    public static function asBool($value): bool
    {
        return Convertors::toBool($value);
    }
}