<?php
/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Utils;

use Throwable;

class ExceptionUtils
{
    public static function getTraces(Throwable $e)
    {
        $output = 'Exception in ' . $e->getFile() . ':' . $e->getLine() . ' ' . $e->getMessage() . PHP_EOL;
        foreach ($e->getTrace() as $trace)
        {
            $file = self::cleanPath(basename(Arrays::get($trace, 'file')));
            $class = Arrays::get($trace, 'class');
            $args = self::getArgs(Arrays::get($trace, 'args'));
            $function = Arrays::get($trace, 'function');
            $line = Arrays::get($trace, 'line');

            $fcPath = (Strings::isEmpty($file)) ? $class : $file;

            $output .= "	at {$fcPath} . {$function}({$fcPath}: {$line}) $args" . PHP_EOL;
        }

        return $output;
    }

    public static function cleanPath($path)
    {
        return $path;
    }

    public static function getArgs($args)
    {
        if (! is_array($args) || count($args) == 0)
        {
            return '';
        }

        $out = '';
        foreach ($args as $arg)
        {
            $out .= (is_object($arg)) ? "@object, " : ((is_array($arg)) ? "@array, " : "'$arg', ");
        }

        if (strlen($out) > 3)
        {
            $out = substr($out, 0, strlen($out) - 2);
        }

        return ' with ' . count($args) . ' args (' . $out . ')';
    }
}