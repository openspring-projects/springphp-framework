<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Utils;

use Openspring\SpringphpFramework\Exception\InvalidArgumentException;
use Openspring\SpringphpFramework\Reflection\ClassReflection;
use Openspring\SpringphpFramework\Type\Any;
use Exception;
use ReflectionMethod;

class Objects
{

    /**
     * Return true if given object is null
     *
     * @param Object $object
     * @return bool
     */
    public static function isNull(?Object $object): bool
    {
        return ($object == null);
    }

    /**
     * Return true if given object is not null
     *
     * @param Object $object
     * @return bool
     */
    public static function isNotNull(?Object $object): bool
    {
        return ! self::isNull($object);
    }

    public static function isMethodPublic($class, $method)
    {
        if (method_exists($class, $method))
        {
            $reflection = new ReflectionMethod($class, $method);

            return ($reflection->isPublic());
        }
    }

    public static function get($o, $prop, $defaultValue = '')
    {
        try
        {
            if (! is_object($o))
            {
                return $defaultValue;
            }

            $getter = ClassUtils::getGetter($prop);
            if (method_exists($o, $getter) && self::isMethodPublic($o, $getter))
            {
                return $o->$getter();
            }

            if (property_exists($o, $prop))
            {
                return $o->$prop;
            }

            return $defaultValue;
        } catch (Exception $e)
        {
            return $defaultValue;
        }
    }

    public static function setMethod(&$o, String $prop, $value, bool $createPropertyIfNoMatchFound = false)
    {
        if (! is_object($o))
        {
            throw new InvalidArgumentException('Parameter 1 must be an object');
        }

        $setter = ClassUtils::getSetter($prop);
        if (method_exists($o, $setter))
        {
            return $o->$setter(self::getVarValue($value));
        }

        if (property_exists($o, $prop))
        {
            return ($o->$prop = self::getVarValue($value));
        }

        if ($createPropertyIfNoMatchFound)
        {
            return ($o->$prop = self::getVarValue($value));
        }
    }

    public static function getVarValue($var)
    {
        return $var;
    }

    public static function getMethod($o, $prop)
    {
        if (! is_object($o))
        {
            throw new InvalidArgumentException('Parameter 1 must be an object');
        }

        $get = 'get' . lcfirst($prop);
        return (method_exists($o, $get)) ? $o->$get() : ((property_exists($o, $prop)) ? $o->$prop : '');
    }

    /**
     * Convert object to array
     *
     * @param Object $o
     * @throws InvalidArgumentException
     * @return array
     */
    public static function objectToArray(Object $o): array
    {
        if (! is_object($o))
        {
            throw new InvalidArgumentException('Parameter 1 must be an object');
        }

        return json_decode(json_encode($o), true);
    }

    /**
     * Return true if given object has test property
     *
     * @param Object $target
     * @param String $property
     * @return bool
     */
    public static function hasProperty(?Object $target, String $property): bool
    {
        if ($target == null || $property == null)
        {
            return false;
        }

        return (property_exists($target, $property));
    }

    /**
     * Return true if given object has all test properties
     *
     * @param Object $target
     * @param array $properties
     * @return bool
     */
    public static function hasAllProperties(?Object $target, Array $properties): bool
    {
        if ($target == null || $properties == null)
        {
            return false;
        }

        foreach ($properties as $prop)
        {
            if (! property_exists($target, $prop)) return false;
        }

        return true;
    }

    public static function hasAllPropertiesNotEmpty(?Object $target, Array $properties): bool
    {
        $hasAllProperties = static::hasAllProperties($target, $properties);
        if (! $hasAllProperties) return false;

        foreach ($properties as $var)
        {
            if (Strings::isEmpty($target->$var)) return false;
        }

        return true;
    }

    /**
     * Smart object properties auto-mapping
     * The mapping is done on the $dest object its self
     *
     * @param Object $src
     * @param Object $dest
     *            PASSED BY REFERENCE
     * @param array $ignore
     * @param array $mapping
     */
    public static function autoMapObject(?Object $src, ?Object &$dest, Array $mapping = array(), Array $ignore = array(), bool $createProps = false, $jumpNULL = false): void
    {
        if ($src === null || $dest === null)
        {
            return;
        }

        foreach ($src as $prop => $value)
        {
            if ($jumpNULL && $value == NULL) continue;
            
            if (in_array($prop, $ignore)) continue;

            $mapProp = Arrays::get($mapping, $prop, $prop);
            $setMapProp = ClassUtils::getSetter($mapProp);

            // case of value is an object or array
            if (is_object($value) || is_array($value))
            {
                if (ClassReflection::isPropertyPublic($dest, $mapProp))
                {
                    $dest->$mapProp = $value;
                }
                else
                {
                    if (method_exists($dest, $setMapProp))
                    {
                        try
                        {
                            $dest->$setMapProp($value);
                        } catch (Exception $e)
                        {
                            // Ignore
                        }
                    }
                }
                continue;
            }

            // set string or int value
            self::setMethod($dest, $mapProp, $value, $createProps);
        }
    }

    /**
     * Smart array items auto-mapping
     * The mapping is done on the $dest array its self
     *
     * @param array $src
     * @param array $dest
     *            PASSED BY REFERENCE
     * @param array $ignore
     */
    public static function autoMapArray(?Array $src, ?Array &$dest, Array $mapping = array(), Array $ignore = array()): void
    {
        if ($src === null || $dest === null)
        {
            return;
        }

        foreach ($src as $key => $value)
        {
            if (Arrays::keyExists($key, $ignore)) continue;

            $mKey = Arrays::get($mapping, $key, $key);

            if (is_object($value))
            {
                $dest[$mKey] = new Any();
                self::autoMapObject($value, $dest[$mKey], $ignore, $mapping, true);
            }
            else if (is_array($value))
            {
                $dest[$mKey] = array();
                self::autoMapArray($value, $dest[$mKey], $ignore, $mapping);
            }
            else
            {
                $dest[$mKey] = $src[$key];
            }
        }
    }

    /**
     * Return safely an object property value
     *
     * @param Object $object
     * @param String $property
     * @param string $defaultValue
     *
     * @return mixed
     */
    public static function getProperty(Object $object, String $property, $defaultValue = '')
    {
        if (! is_object($object) || $property == null)
        {
            return $defaultValue;
        }

        return (property_exists($object, $property)) ? $object->$property : $defaultValue;
    }

    /**
     * Return safely an array property value
     *
     * @param array $array
     * @param String $property
     * @param string $defaultValue
     *
     * @return mixed
     */
    public static function getArrayProperty(Array $array, String $property, $defaultValue = '')
    {
        if (! is_array($array) || $property == null)
        {
            return $defaultValue;
        }

        return (isset($array[$property])) ? $array[$property] : $defaultValue;
    }

    /**
     * Return safely an array item by index
     *
     * @param array $array
     * @param int $index
     * @param string $defaultValue
     *
     * @return mixed
     */
    public static function getArrayItem(Array $array, int $index, $defaultValue = '')
    {
        if (! is_array($array) || ! is_numeric($index))
        {
            return $defaultValue;
        }

        return (isset($array[$index])) ? $array[$index] : $defaultValue;
    }

    /**
     * Find by property value items in a given array
     * Example of use:
     * $item1 = new Any();
     * $item1->name = 'khalid';
     * $item1->age = 36;
     * $item2 = new Any();
     * $item2->name = 'hamid';
     * $item2->age = 40;
     * $item3 = new Any();
     * $item3->name = 'rachid';
     * $item3->age = 55;
     * $array = array($item1, $item2, $item3);
     * Objects::findArrayItem($array, 'name', 'khalid');
     * This return $item1
     *
     * @param array $array
     * @param String $property
     * @param mixed $value
     * @return mixed
     */
    public static function findArrayItem(Array $array, String $property, $value)
    {
        if (! is_array($array))
        {
            return null;
        }

        foreach ($array as $item)
        {
            if ($item->$property == $value)
            {
                return $item;
            }
        }

        return null;
    }
}