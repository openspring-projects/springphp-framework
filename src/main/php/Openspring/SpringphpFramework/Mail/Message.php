<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Mail;

use Openspring\SpringphpFramework\Exception\InvalidArgumentException;
use Openspring\SpringphpFramework\Exception\IO\FileNotFoundException;
use Openspring\SpringphpFramework\Utils\Arrays;
use Openspring\SpringphpFramework\Utils\DataValidator;
use Openspring\SpringphpFramework\Utils\Strings;

/**
 * Mail Message type
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
class Message
{
    private $from;
    private $to = array();
    private $cc = array();
    private $bcc = array();
    private $replyTo;
    private $subject;
    private $body;
    private $files = array();

    public function getFrom(): ?String
    {
        return $this->from;
    }

    public function getTos(): array
    {
        return $this->to;
    }

    public function getCcs(): array
    {
        return $this->cc;
    }

    public function getBccs(): array
    {
        return $this->bcc;
    }

    public function getReplyTos(): ?String
    {
        return $this->replyTo;
    }

    public function getSubject(): ?String
    {
        return $this->subject;
    }

    public function getBody(): ?String
    {
        return $this->body;
    }

    public function getAttachements(): array
    {
        return $this->files;
    }

    public function setFrom(?String $from): void
    {
        $this->from = $from;
    }

    public function setTo(Array $tos): void
    {
        foreach ($tos as $to)
        {
            if (! DataValidator::isEmail($to)) throw new InvalidArgumentException('Invalid email ' . $to);
        }

        $this->to = $tos;
    }

    public function addTo(?String $to): void
    {
        if (! DataValidator::isEmail($to)) throw new InvalidArgumentException('Invalid email ' . $to);

        $this->to[] = $to;
    }

    public function setCc(Array $ccs): void
    {
        foreach ($ccs as $cc)
        {
            if (! DataValidator::isEmail($cc)) throw new InvalidArgumentException('Invalid email ' . $cc);
        }

        $this->cc = $ccs;
    }

    public function addCc(?String $cc): void
    {
        if (! DataValidator::isEmail($cc)) throw new InvalidArgumentException('Invalid email ' . $cc);

        $this->cc[] = $cc;
    }

    public function setBcc(Array $bccs): void
    {
        foreach ($bccs as $bcc)
        {
            if (! DataValidator::isEmail($bcc)) throw new InvalidArgumentException('Invalid email ' . $bcc);
        }

        $this->bcc = $bccs;
    }

    public function addBcc(?String $bcc): void
    {
        if (! DataValidator::isEmail($bcc)) throw new InvalidArgumentException('Invalid email ' . $bcc);

        $this->bcc[] = $bcc;
    }

    public function setReplyTo(?String $replyTo): void
    {
        if (Strings::isEmpty($replyTo)) return;
        
        if (! DataValidator::isEmail($replyTo)) throw new InvalidArgumentException('Invalid email ' . $replyTo);

        $this->replyTo = $replyTo;
    }

    public function setSubject(?String $subject): void
    {
        $this->subject = $subject;
    }

    public function setBody(?String $body): void
    {
        $this->body = $body;
    }

    public function addAttachement(?String $filename): void
    {
        if (! file_exists($filename))
        {
            throw new FileNotFoundException("Filenot found " . $filename);
        }

        $this->files[] = $filename;
    }

    public function hasAttachements(): bool
    {
        return (! Arrays::isEmpty($this->getAttachements()));
    }

    public function hasCcs(): bool
    {
        return (! Arrays::isEmpty($this->getCcs()));
    }

    public function hasBccs(): bool
    {
        return (! Arrays::isEmpty($this->getBccs()));
    }

    public function hasTos(): bool
    {
        return (! Arrays::isEmpty($this->getTos()));
    }
}