<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Mail;

use Openspring\SpringphpFramework\IO\FileUtils;
use Openspring\SpringphpFramework\Utils\Arrays;
use Openspring\SpringphpFramework\Utils\Strings;
use ErrorException;
use Exception;

class MailSender
{
    const DEFAULT_TEMPLATE = '<table><tr><td>{body}</td></tr></table>';
    
    private $messageTemplate;
    private $message;
    private $mimeBoundary;

    public function __construct(Message $message, ?String $messageTemplate = null)
    {
        $this->message = $message;
        $this->mimeBoundary = "==Multipart_Boundary_x" . md5(uniqid(time())) . "x";
        $this->messageTemplate = ($messageTemplate == null) ? self::DEFAULT_TEMPLATE : $messageTemplate;

        // check message
        if ($this->message == null)
        {
            throw new ErrorException('Message is null');
        }
    }

    public function getMessageTemplate()
    {
        return $this->messageTemplate;
    }

    public function setMessageTemplate($messageTemplate)
    {
        $this->messageTemplate = $messageTemplate;
    }

    public function getHeaders()
    {
        $headers = "MIME-Version: 1.0" . PHP_EOL;

        if ($this->message->hasAttachements())
        {
            $headers .= "Content-Type: multipart/mixed; charset=utf-8" . PHP_EOL;
            $headers .= "boundary=" . $this->mimeBoundary . PHP_EOL;
        }
        else
        {
            $headers = 'MIME-Version: 1.0' . PHP_EOL;
            $headers .= 'Content-type: text/html; charset=utf-8' . PHP_EOL;
        }

        // set from
        $headers .= "From: " . $this->message->getFrom() . PHP_EOL;

        // check data
        if (Strings::isEmpty($this->message->getFrom()))
        {
            throw new ErrorException('No from set');
        }

        // check data
        if (! $this->message->hasTos())
        {
            throw new ErrorException('No recipients set');
        }

        // check data
        if (Strings::isEmpty($this->message->getBody()))
        {
            throw new ErrorException('No body set');
        }

        // add cc
        if ($this->message->hasCcs())
        {
            $headers .= "Cc: " . Arrays::implode(",", $this->message->getCcs()) . PHP_EOL;
        }

        // add bcc
        if ($this->message->hasBccs())
        {
            $headers .= "Bcc: " . Arrays::implode(",", $this->message->getBccs()) . PHP_EOL;
        }

        // add reply-to
        if (! Strings::isEmpty($this->message->getReplyTos()))
        {
            $headers .= "Reply-To: " . $this->message->getReplyTos() . PHP_EOL;
        }

        return $headers;
    }

    public function getComposedMessage(String $trackingPixelLink = '')
    {
        $message = Strings::replace('{body}', $this->message->getBody() . $trackingPixelLink, $this->getMessageTemplate());
        if ($this->message->hasAttachements())
        {
            // multipart boundary
            $message = "This is a multi-part message in MIME format." . PHP_EOL . PHP_EOL . "--" . $this->mimeBoundary . PHP_EOL . "Content-Type: text/html; charset=\"iso-8859-1\"" . PHP_EOL . "Content-Transfer-Encoding: 7bit" . PHP_EOL . PHP_EOL . $message . PHP_EOL . PHP_EOL;
            $message .= "--" . $this->mimeBoundary . PHP_EOL;

            // preparing attachments
            $files = $this->message->getAttachements();
            for ($x = 0; $x < count($files); $x ++)
            {
                $file = fopen($files[$x], "rb");
                $data = fread($file, filesize($files[$x]));
                fclose($file);
                $data = chunk_split(base64_encode($data));

                $message .= "Content-Type: application/octet-stream; name=\"" . FileUtils::getFileName($files[$x]) . "\"" . PHP_EOL . "Content-Disposition: attachment; filename=\"" . $files[$x] . "\"" . PHP_EOL . "Content-Transfer-Encoding: base64" . PHP_EOL . PHP_EOL . $data . PHP_EOL . PHP_EOL;

                $message .= "--" . $this->mimeBoundary . PHP_EOL;
            }
        }

        return $message;
    }

    public function sendMail()
    {
        $result = false;

        try
        {
            $result = @mail($this->getTos(), $this->message->getSubject(), $this->getComposedMessage(), $this->getHeaders());
        } catch (Exception $e)
        {
            $result = false;
        }

        return $result;
    }

    public function getTos(): String
    {
        return Arrays::implode(",", $this->message->getTos());
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getMimeBoundary()
    {
        return $this->mimeBoundary;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function setMimeBoundary($mimeBoundary)
    {
        $this->mimeBoundary = $mimeBoundary;
    }

    /**
     * Send emails using gmail stmp and the passed accounts
     *
     * @param String $login
     * @param String $password
     */
    public static function enableTestMode(String $login = "springphpframework@gmail.com", String $password = 'SpringPHP.00')
    {
        ini_set("smtp_server", "smtp.gmail.com");
        ini_set("smtp_port", "25");
        ini_set("error_logfile", "error.log");
        ini_set("debug_logfile", "debug.log");
        ini_set("auth_username", $login);
        ini_set("auth_password", $password);
        ini_set("force_sender", "springphpframework@gmail.com");
    }
}