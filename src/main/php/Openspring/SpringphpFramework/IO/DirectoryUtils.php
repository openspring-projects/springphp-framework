<?php

/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\IO;

use Openspring\SpringphpFramework\Exception\IO\DirectoryNotFoundException;
use Openspring\SpringphpFramework\Exception\IO\IOException;
use Openspring\SpringphpFramework\Utils\Strings;
use FilesystemIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

/**
 * Directories utilities
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
class DirectoryUtils
{

    /**
     * Return true if given path does exists on physical drive
     *
     * @param String $path
     * @return bool
     */
    public static function exists(String $path): bool
    {
        return is_dir($path);
    }

    /**
     * Create a directory if it does not exist and clean it if wanted
     *
     * @param String $path
     * @param bool $clean
     * @return bool
     */
    public static function createIfNotExists(String $path, bool $clean = false): bool
    {
        $result = false;

        if (Strings::isEmpty($path)) return false;

        if (! file_exists($path))
        {
            $result = @mkdir($path, 0777, true);
        }

        if ($result && $clean) static::emptyDirectory($path);

        return $result;
    }

    /**
     * Remove an empty directory if no force option is passed.
     * If asked to force, then it will be removed with its content
     *
     * @param String $path
     * @param bool $force
     * @return bool
     */
    public static function removeDirectory(String $path, bool $force = false): bool
    {
        if (! static::exists($path)) return true;

        if ($force)
        {
            foreach (scandir($path) as $file)
            {
                if ('.' === $file || '..' === $file) continue;

                if (is_dir("$path/$file")) static::removeRecursively("$path/$file", $force);
                else unlink("$path/$file");
            }
        }

        if (! static::isEmpty($path)) return false;

        @rmdir($path);

        return true;
    }

    private static function removeRecursively($path)
    {
        static::emptyDirectory($path);

        return @rmdir($path);
    }

    /**
     * Return true if given directory is empty
     *
     * @param String $path
     * @return bool
     */
    public static function isEmpty(String $path): bool
    {
        return ! (new FilesystemIterator($path))->valid();
    }

    /**
     * Delete a given directory contents recursively
     *
     * @param String $path
     * @return bool
     */
    public static function emptyDirectory(String $path): bool
    {
        if (! static::removeFiles($path)) return false;

        $it = new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS);
        $it = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);

        foreach ($it as $file)
        {
            rmdir($file->getPathname());
        }

        return true;
    }

    private static function removeFiles(String $path): bool
    {
        $it = new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS);
        $it = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);

        foreach ($it as $file)
        {
            if (! $file->isDir()) unlink($file->getPathname());
        }

        return true;
    }

    /**
     * Copy a given directory to a given destination
     *
     * @param String $source
     * @param String $dest
     * @return bool
     */
    public static function copyDir(String $source, String $dest): bool
    {
        // echo PHP_EOL . "==> ** copying " . $source . ' TO ' . $dest;
        $path = opendir($source);
        static::createIfNotExists($dest);

        static::checkSourceDestinationDirectory($source, $dest);

        while (false !== ($file = readdir($path)))
        {
            if (($file != '.') && ($file != '..'))
            {
                if (is_dir($source . '/' . $file))
                {
                    // echo PHP_EOL . "==> " . $source . '/' . $file;
                    static::copyDir($source . '/' . $file, $dest . '/' . $file);
                }
                else
                {
                    copy($source . '/' . $file, $dest . '/' . $file);
                }
            }
        }

        closedir($path);

        return true;
    }

    /**
     * Return true if given directory is wriatable
     *
     * @param String $path
     * @return boolean
     */
    public static function writable(String $path): bool
    {
        return (self::exists($path) && is_writable($path));
    }

    /**
     * Return a given directory files list
     *
     * @param String $pattern
     *            php glob function pattern
     * @param String $path
     * @param int $flags
     *            php glob function flags
     * @return array
     */
    public static function getFilesRecursively(String $path, String $pattern = '*', int $flags = 0): array
    {
        if (! DirectoryUtils::exists($path))
        {
            throw new DirectoryNotFoundException('Directory not found ' . $path);
        }

        $targetBasePath = FileUtils::standarizePath($path . '/');
        $targetPath = FileUtils::standarizePath($targetBasePath . '/' . $pattern);

        $paths = glob($targetBasePath . '*', GLOB_MARK | GLOB_ONLYDIR | GLOB_NOSORT);
        $files = glob($targetPath, $flags);

        foreach ($paths as $subpath)
        {
            $files = array_merge($files, self::getFilesRecursively($subpath, $pattern, $flags));
        }

        return $files;
    }

    /**
     * Return the last updated file in a given directory
     *
     * @param String $pattern
     *            php glob function pattern
     * @param String $path
     * @param int $flags
     *            php glob function flags
     * @return array
     */
    public static function getLastUpatedFile(String $path, String $pattern = '*', $flags = 0): ?String
    {
        $files = self::getFilesRecursively($path, $pattern, $flags);
        $files = array_combine($files, array_map("filemtime", $files));

        arsort($files);

        return key($files);
    }

    public static function getLastUpatedFileInPaths(array $paths, String $pattern = '*', $flags = 0): ?String
    {
        $files = array();
        foreach ($paths as $path)
        {
            $_files = self::getFilesRecursively($path, $pattern, $flags);
            $_files = array_combine($_files, array_map("filemtime", $_files));
            
            $files = array_merge($files, $_files);
        }

        arsort($files);

        return key($files);
    }

    private static function checkSourceDestinationDirectory(String $source, String $dest): void
    {
        if (! static::exists($source))
        {
            throw new DirectoryNotFoundException('source directory does not exist ' . $source);
        }

        if (! static::exists($dest))
        {
            throw new DirectoryNotFoundException('destination directory does not exist ' . $dest);
        }

        if (! DirectoryUtils::writable($dest))
        {
            throw new IOException('destination directory is not writable ' . $dest);
        }
    }
}