<?php
/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\IO;

use Openspring\SpringphpFramework\Context\Environment;
use Openspring\SpringphpFramework\Exception\IO\IOException;
use Openspring\SpringphpFramework\Utils\XMLUtils;
use Exception;

class ProjectFile
{
    private static $properties = array();
    private static $fproject = NULL;
    private static $xml = NULL;
    const XML = '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL .
                '<project>' . PHP_EOL .
                '    <variables>' . PHP_EOL .
                '    	<property name="version" value="1.0.0"/>' . PHP_EOL .
                '    	<property name="cssid" value="0"/>' . PHP_EOL .
                '    </variables>' . PHP_EOL .
                '</project>';

    public static function load()
    {
        if (! Environment::isActiveProfileDev()) return;

        self::$fproject = Path::getResources('.project');

        // .project path
        if (! file_exists(self::$fproject))
        {
                if (file_put_contents(self::$fproject, static::XML) === FALSE)
                {
                    throw new IOException('can not create project settings file ' . self::$fproject);
                }
        }

        self::$xml = @simplexml_load_file(self::$fproject);
        if (self::$xml === FALSE)
        {
            throw new Exception('invalid project file');
        }

        // load .project properties
        XMLUtils::checkRequiredProperty(self::$xml, 'variables');
        foreach (self::$xml->variables->children() as $property)
        {
            $name = XMLUtils::getRequiredPropertyAttribute($property->attributes(), 'name');
            $value = XMLUtils::getRequiredPropertyAttribute($property->attributes(), 'value');

            self::setProperty($name, $value);
        }
    }

    public static function getControllerScanSID()
    {
        return self::getProperty('cssid');
    }

    public static function getVersion()
    {
        return self::getProperty('version');
    }

    /**
     * generic properties
     */
    public static function setProperty($pname, $pvalue, $save = false)
    {
        self::$properties[$pname] = $pvalue;
        if ($save)
        {
            foreach (self::$xml->variables->children() as $property)
            {
                $name = XMLUtils::getRequiredPropertyAttribute($property->attributes(), 'name');
                if ($name == $pname)
                {
                    $property->attributes()->value = $pvalue;
                    self::$xml->asXML(self::$fproject);
                    return;
                }
            }
        }
    }

    public static function getProperty($property)
    {
        return (self::propertyExists($property)) ? self::$properties[lcfirst($property)] : '';
    }

    public static function getPropertyArray($property, $sep)
    {
        $value = (isset(self::$properties[lcfirst($property)])) ? self::$properties[lcfirst($property)] : '';
        return explode($sep, $value);
    }

    public static function propertyExists($property)
    {
        return (isset(self::$properties[lcfirst($property)]));
    }

    public static function propertyIs($property, $value)
    {
        return (isset(self::$properties[$property])) ? ((self::$properties[$property] == $value) ? true : false) : false;
    }
}