<?php

/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\IO;

use Openspring\SpringphpFramework\Context\Environment;
use Openspring\SpringphpFramework\Core\Initializable;
use Openspring\SpringphpFramework\Utils\Strings;

class Path implements Initializable
{

    public static function initialize(): void
    {
    }

    public static function getRoot($subPath = '')
    {
        return FileUtils::standarizePath(Environment::getRootPath($subPath));
    }

    public static function getResources($subPath = '')
    {
        $configPath = Environment::getConfigPath();
        if (Strings::isEmpty($configPath))
        {
            $configPath = dirname(Environment::getSourcesPath(), 2);
        }

        return FileUtils::standarizePath(self::getRoot($configPath . $subPath));
    }

    public static function getPublic($subPath = '')
    {
        return FileUtils::standarizePath(Environment::getRootPath(Environment::getPublicPath($subPath)));
    }

    public static function getSrc($subpath = '')
    {
        return FileUtils::standarizePath(self::getRoot(Environment::getMainSrcPath($subpath)));
    }
}