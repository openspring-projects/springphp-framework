<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\IO;

use Exception;

class PropertiesFile
{
    var $fpath;
    var $properties = array();

    public function __construct(String $filePath, bool $silentMode = false, String $encoding = 'UTF-8')
    {
        $this->fpath = $filePath;
        if (! file_exists($this->fpath))
        {
            if (! $silentMode) throw new Exception('can not find properties file in path ' . $this->fpath);
        }

        $this->parse();
    }

    private function parse(String $encoding = 'UTF-8')
    {
        $input = FileUtils::getFileContent($this->fpath, $encoding);
        $lines = explode("\n", $input);
        foreach ($lines as $line)
        {
            $line = trim($line);
            if (empty($line) || '#' === substr($line, 0, 1))
            {
                continue;
            }

            $limit = strpos($line, '=');
            $code = substr($line, 0, $limit);
            $value = substr($line, $limit + 1);
            $this->properties[$code] = $value;
        }
    }

    public function getProperties()
    {
        return $this->properties;
    }
}