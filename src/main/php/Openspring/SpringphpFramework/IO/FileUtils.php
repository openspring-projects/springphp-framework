<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\IO;

use Openspring\SpringphpFramework\Enumeration\MemeType;
use Openspring\SpringphpFramework\Exception\InvalidArgumentException;
use Openspring\SpringphpFramework\Exception\NullPointerException;
use Openspring\SpringphpFramework\Exception\IO\FileNotFoundException;
use Openspring\SpringphpFramework\Exception\IO\IOException;
use Openspring\SpringphpFramework\Utils\Strings;
use Exception;
use Openspring\SpringphpFramework\Exception\IO\DirectoryNotFoundException;

class FileUtils
{

    /**
     * Copy a file from an url
     *
     * @require allow_url_fopen: On
     * @param String $url
     * @param String $destPath
     * @return bool
     */
    public static function copyFileFromUrl(String $url, String $destinationFile): bool
    {
        if (! static::parentExists($destinationFile))
        {
            throw new DirectoryNotFoundException('Can not find directory ' . $destinationFile);
        }

        $fstream = file_get_contents($url);

        return (@file_put_contents($destinationFile, $fstream) !== false);
    }

    public static function isAbsolutePath(?String $path)
    {
        if (Strings::isEmpty($path)) throw new NullPointerException("Empty path");

        return $path[0] === DIRECTORY_SEPARATOR || preg_match('~\A[A-Z]:(?![^/\\\\])~i', $path) > 0;
    }

    /**
     * Return a file content in a specific encoding
     *
     * @param String $fileName
     * @param String $encoding
     * @throws FileNotFoundException
     * @return String
     */
    public static function getFileContent(String $fileName, String $encoding = 'UTF-8'): String
    {
        if (! static::exists($fileName))
        {
            throw new FileNotFoundException('File not found ' . $fileName);
        }

        $content = file_get_contents($fileName);

        return mb_convert_encoding($content, $encoding);
    }

    public static function getResourcesFileContent(String $fpath, String $encoding = 'UTF-8'): String
    {
        return self::getFileContent(Path::getResources($fpath));
    }

    /**
     * Return true if file exists
     *
     * @param String $fpath
     * @return bool
     */
    public static function exists(String $fpath): bool
    {
        if (! self::isFile($fpath)) return false;
        return file_exists($fpath);
    }

    protected static function isFile(String $fpath): bool
    {
        return ! (is_dir($fpath));
    }

    /**
     * Return true if a file parent exists
     *
     * @param String $fpath
     * @param int $up
     * @return bool
     */
    public static function parentExists(String $fpath, int $up = 1): bool
    {
        if (! self::isFile($fpath)) return false;
        $fparentPath = static::getFileParent($fpath, $up);

        return DirectoryUtils::exists($fparentPath);
    }

    /**
     * Return a file last modified timestamp
     *
     * @param String $fpath
     * @return int
     */
    public static function getLastUpdatedTime(String $fpath): int
    {
        return filemtime($fpath);
    }

    /**
     * Return images meme types
     *
     * @return array
     */
    public static function getFileRealType(String $fpath): String
    {
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $ftype = finfo_file($finfo, $fpath);
        finfo_close($finfo);

        return $ftype;
    }

    /**
     * Return images meme types
     *
     * @return array
     */
    public static function getImageMemeTypes(): array
    {
        return array(
                        MemeType::PNG,
                        MemeType::JPEG,
                        MemeType::JPG,
                        MemeType::GIF,
                        MemeType::BMP);
    }

    /**
     * Return a file extension
     *
     * @param String $fname
     * @return String
     */
    public static function getExtension(String $fname): String
    {
        $fname = static::getFileName($fname);
        if (Strings::isEmpty($fname))
        {
            return '';
        }

        $ext = pathinfo($fname, PATHINFO_EXTENSION);

        if (Strings::isEmpty($ext)) return $fname;

        return $ext;
    }

    /**
     * Return a file name
     *
     * @param String $fpath
     * @return String
     */
    public static function getFileName(String $fpath): String
    {
        $target = Strings::replace('\\', '/', $fpath);
        return Strings::getStringPart($target, '/', - 1);
    }

    public static function getFileNameWithoutExtension(String $fpath): String
    {
        $target = Strings::replace('\\', '/', $fpath);
        $fname = Strings::getStringPart($target, '/', - 1);

        $ext = static::getExtension($fname);
        if ($ext == $fname) return $fname;
        $len = strlen('.' . $ext);

        return substr($fname, 0, - $len);
    }

    /**
     * Return a file parent directory path
     *
     * @param String $fpath
     * @param int $up
     * @return String
     */
    public static function getFileParent(String $fpath, int $up = 1): String
    {
        return dirname($fpath, $up);
    }

    /**
     * Return a file name extension changed
     *
     * @param String $fname
     * @param String $newExt
     * @return String
     */
    public static function changeFileExtension(String $fnameOrPath, String $newExt): String
    {
        $fext = static::getExtension($fnameOrPath);
        $len = strlen($fext);

        return substr($fnameOrPath, 0, - $len) . $newExt;
    }

    /**
     * Return true if the given input name does exists in upload list
     *
     * @param String $inputName
     * @return bool
     */
    public static function isUploadedFileExists(String $inputName): bool
    {
        return (isset($_FILES[$inputName]));
    }

    /**
     * Save the uploaded file and return its name
     *
     * @param String $inputName
     * @param String $dest
     * @param String $fname
     * @throws IOException
     *
     * @return String saved file name
     */
    public static function saveUploadedFile(String $inputName, String $dest, String $fileName = null): String
    {
        $tmpFile = $_FILES[$inputName];

        // get file info
        $info = pathinfo($tmpFile['name']);

        // get the extension of the file
        $ext = $info['extension'];
        $newFileName = ($fileName == null) ? "F_" . uniqid() . "." . $ext : $fileName;

        // save file
        $targetPath = static::standarizePath($dest . "/" . $newFileName);
        $uploadedFilePath = $tmpFile['tmp_name'];

        $result = @move_uploaded_file($uploadedFilePath, $targetPath);
        $error = $tmpFile['error'];

        if (! $result)
        {
            throw new IOException('Error moving file ' . $tmpFile['tmp_name'] . ' to ' . $targetPath . ': ' . $error);
        }

        return $newFileName;
    }

    /**
     * Rename a given file
     *
     * @param String $fpath
     * @param String $newName
     * @return bool
     */
    public static function renameFile(String $fpath, String $newName): bool
    {
        if (! self::isFile($fpath)) return '';

        $nfpath = static::standarizePath(static::getFileParent($fpath) . '/' . $newName);

        return static::moveFile($fpath, $nfpath);
    }

    /**
     * Move a file to a new physical location
     *
     * @param String $source
     * @param String $dest
     * @throws FileNotFoundException
     * @return bool
     */
    public static function moveFile(String $source, String $dest): bool
    {
        if (! self::isFile($source)) return '';

        static::checkSourceDestinationFile($source, $dest);

        return (@rename($source, $dest) === true);
    }

    /**
     * Copy a given file to a given destination
     *
     * @param String $source
     * @param String $dest
     * @return bool
     */
    public static function copyFile(String $source, String $dest): bool
    {
        if (! self::isFile($source)) return '';
        static::checkSourceDestinationFile($source, $dest);

        return (@copy($source, $dest) === true);
    }

    /**
     * Remove and delete a file
     *
     * @param String $fpath
     * @param bool $ignoreErrors
     * @throws FileNotFoundException
     * @return bool
     */
    public static function removeFile(String $fpath, bool $ignoreErrors = true): bool
    {
        if (! static::exists($fpath) || ! is_file($fpath))
        {
            if ($ignoreErrors) return true;

            throw new FileNotFoundException('file does not exist ' . $fpath);
        }

        return (@unlink($fpath) === true);
    }

    /**
     * @param String $templateFile
     * @param String $destinationFile
     * @param
     *            Array | Object $params
     * @throws Exception
     * @return bool
     */
    public static function createFileFromTemplate(String $templateFile, String $destinationFile, $params = null): bool
    {
        if (! static::exists($templateFile))
        {
            throw new Exception('can not find template file ' . $templateFile);
        }

        if (! is_array($params) && ! is_object($params))
        {
            throw new InvalidArgumentException('Third parameter: expected array or simple object. ' . gettype($params) . ' given');
        }

        $content = file_get_contents($templateFile);
        if ($params != null)
        {
            foreach ($params as $key => $value)
            {
                if (! is_scalar($value)) continue;

                $content = Strings::replace($key, $value, $content);
            }
        }

        return (@file_put_contents($destinationFile, $content) !== false);
    }

    /**
     * Parse csv file and return an array of data
     *
     * @param string $csvFile
     * @param string $delimiter
     * @param string $enclosure
     * @param bool $hasHeader
     * @return array
     */
    public static function parseCsv(String $csvFile, String $delimiter = ';', String $enclosure = '"', bool $hasHeader = true, int $maxLength = 10000000): array
    {
        if (! static::exists($csvFile))
        {
            throw new FileNotFoundException("File not found " . $csvFile);
        }

        $result = [];
        $headerKeys = [];
        $rowCounter = 0;
        if (($handle = fopen($csvFile, 'rb')) !== false)
        {
            while (($row = fgetcsv($handle, $maxLength, $delimiter, $enclosure)) !== false)
            {
                if ($rowCounter === 0 && $hasHeader)
                {
                    $headerKeys = $row;
                }
                else
                {
                    if ($hasHeader)
                    {
                        $assocRow = [];
                        foreach ($headerKeys as $colIndex => $colName)
                        {
                            $assocRow[$colName] = $row[$colIndex];
                        }
                        $result[] = $assocRow;
                    }
                    else
                    {
                        $result[] = $row;
                    }
                }
                $rowCounter ++;
            }
            fclose($handle);
        }

        return $result;
    }

    /**
     * Standarize path by replacing \ by /
     *
     * @param String $path
     * @return String
     */
    public static function standarizePath(String $path): String
    {
        return Strings::replaceMultiple(array(
                                                '\\' => '/',
                                                '//' => '/'), $path);
    }

    protected static function checkSourceDestinationFile(String $source, String $dest): void
    {
        if (! static::exists($source))
        {
            throw new FileNotFoundException('Can not find source file ' . $source);
        }

        if (static::exists($dest))
        {
            throw new FileNotFoundException('Destination file exists ' . $dest);
        }

        if (! DirectoryUtils::writable(static::getFileParent($dest)))
        {
            throw new IOException('Destination file parent directory is not writable ' . $dest);
        }
    }
}