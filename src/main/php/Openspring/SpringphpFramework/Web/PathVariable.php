<?php
/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Web;

use JsonSerializable;

/**
 * Mapping variable
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
class PathVariable implements JsonSerializable
{
    private $param;
    private $type;
    private $pos;

    public function __construct(String $param, String $type, int $pos)
    {
        $this->param = $param;
        $this->type = $type;
        $this->pos = $pos;
    }

    public function getParam()
    {
        return $this->param;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getPos()
    {
        return $this->pos;
    }

    public function setParam($param)
    {
        $this->param = $param;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function setPos($pos)
    {
        $this->pos = $pos;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}