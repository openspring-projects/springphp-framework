<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Web;

use Openspring\SpringphpFramework\Annotation\RequestMapping\RequestMapping;
use Openspring\SpringphpFramework\Context\ApplicationContext;
use Openspring\SpringphpFramework\Core\Stereotype\Bean;
use Openspring\SpringphpFramework\Enumeration\MediaType;
use Openspring\SpringphpFramework\Exception\Http\InternalServerErrorException;
use Openspring\SpringphpFramework\Exception\Http\NotFoundException;
use Openspring\SpringphpFramework\Exception\Http\UnauthorizedException;
use Openspring\SpringphpFramework\Exception\Http\UnsupportedMediaTypeException;
use Openspring\SpringphpFramework\Exception\IO\FileNotFoundException;
use Openspring\SpringphpFramework\Http\HttpMethod;
use Openspring\SpringphpFramework\Http\Request;
use Openspring\SpringphpFramework\Http\Response;
use Openspring\SpringphpFramework\Http\ResponseEntity;
use Openspring\SpringphpFramework\Http\URL;
use Openspring\SpringphpFramework\Log\LogFactory;
use Openspring\SpringphpFramework\Reflection\ClassReflection;
use Openspring\SpringphpFramework\Security\Jwt\JwtAccessSecurity;
use Openspring\SpringphpFramework\Type\ClassMethod;
use Openspring\SpringphpFramework\Type\RequestBody;
use Openspring\SpringphpFramework\Type\RequestCookie;
use Openspring\SpringphpFramework\Type\RequestHeader;
use Openspring\SpringphpFramework\Type\RequestTemporaryFile;
use Openspring\SpringphpFramework\Utils\Cast;
use Openspring\SpringphpFramework\Utils\Objects;
use Openspring\SpringphpFramework\Utils\Strings;
use Openspring\SpringphpFramework\Validation\Validable;
use Openspring\SpringphpFramework\Validation\ValidatorFactory;
use Exception;
use ReflectionMethod;

/**
 * This class must be extended by applications
 * controllers to expose rest api
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
abstract class RestController extends Bean
{
    // vars
    protected $action = NULL;
    protected $capAction = NULL;
    protected $logger;

    protected function initParent()
    {
        $this->logger = LogFactory::getLogger(self::class);
        ApplicationContext::addBeanSingleton($this);
        if (method_exists($this, 'initialize')) $this->initialize();
    }

    public function getNakedName(): String
    {
        return Strings::replace('Controller', '', get_class($this));
    }

    public function setAction(String $action): void
    {
        $this->action = lcfirst($action);
        $this->Action = ucfirst($action);
    }

    // run wcomp
    public function runAction(String $action = ''): bool
    {
        $this->initParent();

        // set action to run
        $this->action = lcfirst($action);
        $this->Action = ucfirst($action);

        $this->logger->debug('Running action...');

        // check if action not empty and action exists as a granted public method on this class
        if (Strings::isEmpty($this->action))
        {
            throw new Exception('Call to invalid resource', 11);
        }

        // run current action
        $function = method_exists($this, $this->action) ? $this->action : NULL;
        if (method_exists($this, 'finalize')) $this->finalize();

        $requestMappingItem = NULL;
        if ($function != NULL)
        {
            $item = ClassReflection::getMethodAnnotation($this, $function, RequestMapping::class);
            $requestMappingItem = ClassMethod::castToRequestMapping($item);
            JwtAccessSecurity::authorize($function, $requestMappingItem->getAuthorizedGroups(), $requestMappingItem->getAuthorizedRoles());
        }

        if ($function == NULL || ! $requestMappingItem->isGranted())
        {
            if (strtolower($this->action) == strtolower(ApplicationContext::getApplication()->action))
            {
                if ($requestMappingItem == NULL || $requestMappingItem->isValid())
                {
                    $error = 'Ressource not found ' . URL::getCurrentURI(false);
                    $this->logger->debug($error);
                    throw new NotFoundException($error);
                }
                else
                {
                    $error = $requestMappingItem->getError() . ' ' . URL::getCurrentURI(false);
                    $this->logger->debug($error);
                    throw new InternalServerErrorException($error);
                }
            }

            return false;
        }

        $this->logger->debug('Running action #' . $function);

        /**
         * call action
         * check if user authorized to run this action ?
         */

        if (! JwtAccessSecurity::isAuthorized($function))
        {
            $this->logger->debug('Action ' . $action . ' denied');
            $this->logger->debug('Not authorized');

            throw new UnauthorizedException('Not authorized');
        }

        // process Content-Type header
        if (!in_array(Request::getMethod(), array(HttpMethod::GET)))
        {
            $contentType = strtolower(trim($requestMappingItem->getConsumes()));
            $requestContentType = strtolower(trim(Request::getContentType()));
            if ($requestContentType != MediaType::ALL_VALUE && $requestContentType != $contentType)
            {
                throw new UnsupportedMediaTypeException('Unsupported Http Content-Type Header submitted ' . $requestContentType . ': supported is ' . $contentType);
            }
        }

        // process Accept Header
        $accept = strtolower(trim($requestMappingItem->getProduces()));
        $requestAccept = explode(',', strtolower(trim(Request::getAccept())));
        if (! in_array(MediaType::ALL_VALUE, $requestAccept) && ! in_array($accept, $requestAccept))
        {
            throw new UnsupportedMediaTypeException('Unsupported Http Accept Header submitted ' . Request::getAccept() . ': supported is ' . $accept);
        }

        Response::setMediaType($accept);
        $this->runWithParamsInjection($this, $function, true);

        return true;
    }

    /**
     * Inject parameters
     *
     * @param
     *            $clazz
     * @param String $function
     * @param bool $render
     */
    public function runWithParamsInjection($clazz, String $function, bool $render): void
    {
        $args = array();
        $f = new ReflectionMethod($clazz, $function);

        foreach ($f->getParameters() as $param)
        {
            $pname = trim($param->name);
            $type = ClassReflection::extractMethodParameterType($param);

            // bind playload
            switch ($type)
            {
                case RequestBody::class:
                    $args[] = new RequestBody(Request::getRequestBody());
                    break;
                case RequestHeader::class:
                    $args[] = new RequestHeader(Request::getHeaderParameter($pname));
                    break;
                case RequestCookie::class:
                    $args[] = new RequestCookie(Request::getCookieParameter($pname));
                    break;
                case RequestTemporaryFile::class:
                    // $paramLen = strlen($pname);
                    // $uploadFileInputId = ($paramLen > 4) ? substr($pname, 0, $paramLen - 4) : 0;
                    $temporaryFile = new RequestTemporaryFile($pname);
                    if (! $temporaryFile->exists())
                    {
                        throw new FileNotFoundException('Temporary request file ' . $pname . ' not found');
                    }
                    $args[] = $temporaryFile;
                    break;
                default:

                    if (ClassReflection::isClass($type))
                    {
                        $requestBody = Request::getRequestBody();
                        $object = new $type();
                        Objects::autoMapObject($requestBody, $object);

                        // validate data
                        if (ClassReflection::implements($type, Validable::class))
                        {
                            $validator = ValidatorFactory::getStrictValidator();
                            $validator->validate($object);
                        }

                        $args[] = $object;
                    }
                    else
                    {
                        $args[] = Cast::asType(Request::getParameter($pname, null), $type);
                    }
                    break;
            }
        }

        $result = call_user_func_array(array(
            $clazz,
            $function), $args);

        if ($render) $this->render($result);
    }

    private function render($result): void
    {
        if ($result instanceof ResponseEntity)
        {
            $responseEntity = ResponseEntity::cast($result);

            Response::setResponseEntity($responseEntity);
            Response::render();
            return;
        }

        Response::setResponse($result);
        Response::render();
    }
}