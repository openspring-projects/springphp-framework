<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Web;

use Openspring\SpringphpFramework\Core\Bean\HttpMessageConverter;
use Openspring\SpringphpFramework\Core\Stereotype\Component;
use Openspring\SpringphpFramework\Exception\Http\HttpClientException;
use Openspring\SpringphpFramework\Exception\Http\MediaTypeNotAcceptableException;
use Openspring\SpringphpFramework\Http\HttpHeaders;
use Openspring\SpringphpFramework\Http\HttpMethod;
use Openspring\SpringphpFramework\Http\HttpStatusCategory;
use Openspring\SpringphpFramework\Utils\Arrays;
use Openspring\SpringphpFramework\Utils\BeanUtils;
use Openspring\SpringphpFramework\Utils\Cast;
use Openspring\SpringphpFramework\Utils\Strings;

/**
 * Synchronous client to perform HTTP requests, exposing a simple, template
 * method API over underlying HTTP client libraries.
 * <p>The RestTemplate offers templates for common scenarios by HTTP method</p>
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
class RestTemplate extends Component
{
    protected $sslVerifyHost = false;
    protected $sslVerifyPeer = false;
    protected $connectionTimeout = 240;
    protected $timeout = 240;
    protected $userAgent = "PostmanRuntime/7.20.1";
    protected $httpMessageConverters = array();

    public function addMessageConvertor(HttpMessageConverter $httpMc): RestTemplate
    {
        $this->httpMessageConverters[] = $httpMc;

        return $this;
    }

    public function getConnectionTimeout(): int
    {
        return $this->connectionTimeout;
    }

    public function getTimeout(): int
    {
        return $this->timeout;
    }

    public function getUserAgent(): String
    {
        return $this->userAgent;
    }

    public function setConnectionTimeout($connectionTimeout): RestTemplate
    {
        $this->connectionTimeout = $connectionTimeout;
        return $this;
    }

    public function setTimeout($timeout): RestTemplate
    {
        $this->timeout = $timeout;
        return $this;
    }

    public function setUserAgent($userAgent): RestTemplate
    {
        $this->userAgent = $userAgent;
        return $this;
    }

    public function getSslVerifyHost(): bool
    {
        return $this->sslVerifyHost;
    }

    public function getSslVerifyPeer(): bool
    {
        return $this->sslVerifyPeer;
    }

    public function setSslVerifyHost($sslVerifyHost): RestTemplate
    {
        $this->sslVerifyHost = $sslVerifyHost;
        return $this;
    }

    public function setSslVerifyPeer($sslVerifyPeer): RestTemplate
    {
        $this->sslVerifyPeer = $sslVerifyPeer;
        return $this;
    }

    public function post(String $url, ?HttpHeaders $httpHeaders = null, $data = null)
    {
        return $this->curl($url, 'POST', $httpHeaders, $data);
    }

    public function get(String $url, ?HttpHeaders $httpHeaders = null, $data = null)
    {
        return $this->curl($url, 'GET', $httpHeaders, $data);
    }

    public function put(String $url, ?HttpHeaders $httpHeaders = null, $data = null)
    {
        return $this->curl($url, 'PUT', $httpHeaders, $data);
    }

    public function delete(String $url, ?HttpHeaders $httpHeaders = null, $data = null)
    {
        return $this->curl($url, 'DELETE', $httpHeaders, $data);
    }

    public function exchange(String $method, String $url, ?HttpHeaders $httpHeaders = null, $data = null)
    {
        return $this->curl($url, 'DELETE', $httpHeaders, $data);
    }

    protected function curl(String $url, String $method, ?HttpHeaders $httpHeaders, $data = null)
    {
        $headers = array();
        if ($httpHeaders != null)
        {
            $pHeaders = $httpHeaders->getHeaders();
            foreach ($pHeaders as $header => $value)
            {
                $headers[] = $header . ': ' . $value;
            }
        }

        HttpMethod::validate($method);
        $ch = curl_init();
        $options = array(
                            CURLOPT_URL => $url,
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => $method,
                            CURLOPT_HTTPHEADER => array_merge(array(
                                                                    "Accept: */*",
                                                                    "Accept-Encoding: gzip, deflate",
                                                                    "Cache-Control: no-cache",
                                                                    "Connection: keep-alive",
                                                                    "User-Agent: " . $this->getUserAgent(),
                                                                    "cache-control: no-cache"), $headers));

        if (! in_array($method, array(
                                        HttpMethod::GET,
                                        HttpMethod::DELETE)))
        {
            if ($data == null) $data = '';

            $jdata = json_encode($data);
            $jdataLength = strlen($jdata);

            $options[CURLOPT_POSTFIELDS] = $jdata;
            $options[CURLOPT_HTTPHEADER][] = "Content-Length: " . $jdataLength;
            $options[CURLOPT_HTTPHEADER][] = "Content-Type: application/json";
        }

        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);

        $err = curl_errno($ch);
        $errmsg = curl_error($ch);
        $header = curl_getinfo($ch);

        $httpCode = $header['http_code'];
        $contentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
        $contentType = trim(Strings::getStringPart($contentType, ';', 0));

        $httpCodeCateg = HttpStatusCategory::getStatusCategory($httpCode);

        curl_close($ch);

        if ($httpCodeCateg != 2)
        {
            throw new HttpClientException($errmsg, $err, $header);
        }

        // convert data using the right message convertor
        $contentMediaType = $this->getHttpMessageConverter($contentType);

        return $contentMediaType->read($response);
    }

    protected function getHttpMessageConverter(String $mediaType): HttpMessageConverter
    {
        $httpMc = Cast::asMessageConverter(Arrays::search($this->httpMessageConverters, $mediaType, 'mediaType'));
        if ($httpMc == null)
        {
            $httpMc = BeanUtils::getHttpMessageConverterBean($mediaType);
        }

        if (! is_array($httpMc)) $httpMc = array(
                                                    $httpMc);

        $mc = Cast::asMessageConverter(Arrays::search($httpMc, $mediaType, 'mediaType'));
        if ($mc == null)
        {
            throw new MediaTypeNotAcceptableException('No bean found of type ' . HttpMessageConverter::class . ' for ' . $mediaType . ' type conversion');
        }

        return $mc;
    }
}