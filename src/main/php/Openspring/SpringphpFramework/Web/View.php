<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Web;

use Openspring\SpringphpFramework\Core\Initializable;
use Openspring\SpringphpFramework\Http\URL;
use Openspring\SpringphpFramework\Utils\Strings;
use Exception;

class View implements Initializable
{
    public static $templateFile = NULL;
    public static $html = NULL;

    public static function initialize(): void
    {
    }

    public static function getRedirect($url)
    {
        return '<meta http-equiv="refresh" content="0;URL=' . $url . '">';
    }

    public static function setTemplate($comp, $tmp, array $params = NULL)
    {
        self::$templateFile = $tmp;
        if (! file_exists(self::$templateFile)) throw new Exception('can find template ' . self::$templateFile);

        self::newBuffer();
        include self::$templateFile;
        self::$html = self::getBuffer();

        // replace params
        if ($params != NULL)
        {
            foreach ($params as $key => $value)
            {
                self::$html = Strings::replace($key, $value, self::$html);
            }
        }
    }

    public static function renderView($comp, $viewPath, $params = NULL, $region = NULL)
    {
        $viewPath = $viewPath;
        if (! file_exists($viewPath)) throw new Exception('can find view ' . $viewPath);

        // include view
        self::newBuffer();
        include $viewPath;
        $buffer = self::untag(self::getBuffer());

        // add content to region
        if ($region != NULL && self::$templateFile != NULL)
        {
            self::$html = self::untag(self::$html);
            $rules = array(
                '{' . $region . '}' => $buffer);
            echo Strings::replaceAll(self::$html, $rules);
        }
        else
        {
            echo $buffer;
        }
    }

    public static function render($content, $region = NULL)
    {
        if ($region != NULL && self::$templateFile != NULL)
        {
            self::$html = self::untag(self::$html);
            $rules = array(
                '{' . $region . '}' => $content);
            echo Strings::replaceAll(self::$html, $rules);
        }
        else
        {
            echo $content;
        }
    }

    public static function untag($content)
    {
        $rules = array(
            '{baseURL}' => URL::getContextRoot());

        return Strings::replaceAll($content, $rules);
    }

    public static function newBuffer()
    {
        ob_start();
    }

    public static function getBuffer()
    {
        $content = ob_get_contents();
        ob_end_clean();

        return $content;
    }
}