<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Utils;

use Openspring\SpringphpFramework\Annotation\Rest\MappingItem;
use Openspring\SpringphpFramework\Context\ApplicationContext;
use Openspring\SpringphpFramework\Context\Environment;
use Openspring\SpringphpFramework\Exception\Http\HttpServerException;
use Openspring\SpringphpFramework\Exception\Http\NotFoundException;
use Openspring\SpringphpFramework\Http\HttpStatus;
use Openspring\SpringphpFramework\Http\Request;
use Openspring\SpringphpFramework\Http\URL;
use Openspring\SpringphpFramework\IO\DirectoryUtils;
use Openspring\SpringphpFramework\IO\FileUtils;
use Openspring\SpringphpFramework\IO\ProjectFile;

class Router
{

    public static function route()
    {
        // rewrite htaccess if changes found
        if (Environment::isActiveProfileDev())
        {
            $scanDirs = ApplicationContext::getScanBeanDirs(Environment::getRootPath());
            $oldSSID = ProjectFile::getControllerScanSID();
            $lastUpdated = DirectoryUtils::getLastUpatedFileInPaths($scanDirs, "*.php");
            $newSSID = FileUtils::getLastUpdatedTime($lastUpdated);

            if ($oldSSID != $newSSID)
            {
                Rest::scanForRestControllers();
                ProjectFile::setProperty('cssid', $newSSID, true);
                ApplicationContext::loadContext(true);
            }
        }

        $ctxRoot = Environment::getContextRoot();
        $currentURI = URL::getCurrentURI(false);
        if (! Strings::startsWith($currentURI, $ctxRoot))
        {
            throw new HttpServerException('URI not found ' . $currentURI, 0, HttpStatus::NOT_FOUND, false);
        }

        $currentURL = FileUtils::standarizePath(Strings::replace($ctxRoot, '/', $currentURI));
        $requestMethod = strtoupper(Request::getMethod());

        // find exact match
        $mappings = ApplicationContext::getMapping();

        // find all possible matchs
        $matches = array();
        $foundMappingItem = NULL;
        // $matchedMappingItems = array();
        $matchFound = false;
        $az = uniqid();
        $a0 = uniqid();
        foreach ($mappings as $mappingItem)
        {
            $methods = $mappingItem->methods;
            if (! in_array($requestMethod, $methods)) continue;

            $path = Strings::replace('([^\/]+)', $az, $mappingItem->path);
            $path = Strings::replace('([0-9]+)', $a0, $path);

            $pattern = '/^\/' . Strings::replace('/', '\/', $path) . '$/';

            $pattern = Strings::replace($az, '([^\/]+)', $pattern);
            $pattern = Strings::replace($a0, '([0-9]+)', $pattern);

            preg_match($pattern, $currentURL, $matches);

            if (count($matches) > 0)
            {
                $foundMappingItem = MappingItem::New();
                Objects::autoMapObject($mappingItem, $foundMappingItem);
                break;
            }
        }

        // find exact matchs
        if ($foundMappingItem == NULL)
        {
            throw new NotFoundException('Ressource not found ' . $currentURI);
        }

        // save into context found mapping item
        ApplicationContext::$currentMappingItem = $foundMappingItem;

        // set vars and run action
        $pathVariablesValues = explode('/', $currentURL);
        if (count($pathVariablesValues) > 0) unset($pathVariablesValues[0]);
        foreach ($foundMappingItem->getPathVariables() as $pathVariable)
        {
            $_GET[$pathVariable->param] = $pathVariablesValues[$pathVariable->pos];
            $_REQUEST[$pathVariable->param] = $pathVariablesValues[$pathVariable->pos];
        }

        $_REQUEST['wcomp'] = $foundMappingItem->getClazz();
        $_REQUEST['action'] = $foundMappingItem->getAction();
    }
}