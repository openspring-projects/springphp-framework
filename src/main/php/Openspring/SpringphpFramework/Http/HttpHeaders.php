<?php
/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Http;

use Openspring\SpringphpFramework\Type\Dictionary;

/**
 * Http headers management
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
class HttpHeaders
{
    const ACCEPT = "Accept";
    const ACCEPT_CHARSET = "Accept-Charset";
    const ACCEPT_ENCODING = "Accept-Encoding";
    const ACCEPT_LANGUAGE = "Accept-Language";
    const ACCEPT_RANGES = "Accept-Ranges";
    const ACCESS_CONTROL_ALLOW_CREDENTIALS = "Access-Control-Allow-Credentials";
    const ACCESS_CONTROL_ALLOW_HEADERS = "Access-Control-Allow-Headers";
    const ACCESS_CONTROL_ALLOW_METHODS = "Access-Control-Allow-Methods";
    const ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
    const ACCESS_CONTROL_EXPOSE_HEADERS = "Access-Control-Expose-Headers";
    const ACCESS_CONTROL_MAX_AGE = "Access-Control-Max-Age";
    const ACCESS_CONTROL_REQUEST_HEADERS = "Access-Control-Request-Headers";
    const ACCESS_CONTROL_REQUEST_METHOD = "Access-Control-Request-Method";
    const AGE = "Age";
    const ALLOW = "Allow";
    const AUTHORIZATION = "Authorization";
    const CACHE_CONTROL = "Cache-Control";
    const CONNECTION = "Connection";
    const CONTENT_ENCODING = "Content-Encoding";
    const CONTENT_DISPOSITION = "Content-Disposition";
    const CONTENT_LANGUAGE = "Content-Language";
    const CONTENT_LENGTH = "Content-Length";
    const CONTENT_LOCATION = "Content-Location";
    const CONTENT_RANGE = "Content-Range";
    const CONTENT_TYPE = "Content-Type";
    const COOKIE = "Cookie";
    const DATE = "Date";
    const ETAG = "ETag";
    const EXPECT = "Expect";
    const EXPIRES = "Expires";
    const FROM = "From";
    const HOST = "Host";
    const IF_MATCH = "If-Match";
    const IF_MODIFIED_SINCE = "If-Modified-Since";
    const IF_NONE_MATCH = "If-None-Match";
    const IF_RANGE = "If-Range";
    const IF_UNMODIFIED_SINCE = "If-Unmodified-Since";
    const LAST_MODIFIED = "Last-Modified";
    const LINK = "Link";
    const LOCATION = "Location";
    const MAX_FORWARDS = "Max-Forwards";
    const ORIGIN = "Origin";
    const PRAGMA = "Pragma";
    const PROXY_AUTHENTICATE = "Proxy-Authenticate";
    const PROXY_AUTHORIZATION = "Proxy-Authorization";
    const RANGE = "Range";
    const REFERER = "Referer";
    const RETRY_AFTER = "Retry-After";
    const SERVER = "Server";
    const SET_COOKIE = "Set-Cookie";
    const SET_COOKIE2 = "Set-Cookie2";
    const TE = "TE";
    const TRAILER = "Trailer";
    const TRANSFER_ENCODING = "Transfer-Encoding";
    const UPGRADE = "Upgrade";
    const USER_AGENT = "User-Agent";
    const VARY = "Vary";
    const VIA = "Via";
    const WARNING = "Warning";
    const WWW_AUTHENTICATE = "WWW-Authenticate";
    
    protected $headers;

    public function __construct()
    {
        $this->setHeaders(new Dictionary());
    }

    public function setAccept(String $value): void
    {
        $this->setHeader(self::ACCEPT, $value);
    }

    public function setAcceptCharset(String $value): void
    {
        $this->setHeader(self::ACCEPT_CHARSET, $value);
    }

    public function setAcceptEncoding(String $value): void
    {
        $this->setHeader(self::ACCEPT_ENCODING, $value);
    }

    public function setAcceptLanguage(String $value): void
    {
        $this->setHeader(self::ACCEPT_LANGUAGE, $value);
    }

    public function setAuthorization(String $value): void
    {
        $this->setHeader(self::AUTHORIZATION, $value);
    }

    public function setContentType(String $value): void
    {
        $this->setHeader(self::CONTENT_TYPE, $value);
    }

    public function setUserAgent(String $value): void
    {
        $this->setHeader(self::USER_AGENT, $value);
    }

    public function setLocation(String $value): void
    {
        $this->setHeader(self::LOCATION, $value);
    }

    public function getAccept(): String
    {
        return $this->getHeader(self::ACCEPT);
    }

    public function getAcceptCharset(): String
    {
        return $this->getHeader(self::ACCEPT_CHARSET);
    }

    public function getAcceptEncoding(): String
    {
        return $this->getHeader(self::ACCEPT_ENCODING);
    }

    public function getAcceptLanguage(): String
    {
        return $this->getHeader(self::ACCEPT_LANGUAGE);
    }

    public function getAuthorization(): String
    {
        return $this->getHeader(self::AUTHORIZATION);
    }

    public function getContentType(): String
    {
        return $this->getHeader(self::CONTENT_TYPE);
    }

    public function getUserAgent(): String
    {
        return $this->getHeader(self::USER_AGENT);
    }

    public function getLocation(): String
    {
        return $this->getHeader(self::LOCATION);
    }

    public function setHeader(String $name, String $value): void
    {
        $this->headers->add($name, $value);
    }

    public function getHeader(String $name): String
    {
        return $this->headers->get($name);
    }

    public function setHeaders(Dictionary $headers): void
    {
        $this->headers = $headers;
    }

    public function getHeaders(): ?Dictionary
    {
        return $this->headers;
    }
}