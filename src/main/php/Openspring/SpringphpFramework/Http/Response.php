<?php

/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Http;

use Openspring\SpringphpFramework\Context\ApplicationContext;
use Openspring\SpringphpFramework\Context\Environment;
use Openspring\SpringphpFramework\Core\Initializable;
use Openspring\SpringphpFramework\Core\Bean\ResponseSerializer;
use Openspring\SpringphpFramework\Enumeration\MediaType;
use Openspring\SpringphpFramework\Exception\InvalidValueException;
use Openspring\SpringphpFramework\Utils\BeanUtils;

/**
 * Global application response control class
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
class Response implements Initializable
{
    private static $transformer = null;
    private static $body = null;
    private static $mediaType = null;
    private static $httpStatus = HttpStatus::OK;
    
    /**
     * @var HttpCookies
     */
    private static $cookies = null;
    
    private static $headers = null;

    public static function initialize(): void
    {
        self::$body = null;
        self::$mediaType = null;
        self::$httpStatus = HttpStatus::OK;
    }

    /**
     * Return response headers
     *
     * @return HttpHeaders|NULL
     */
    public static function getHeaders(): ?HttpHeaders
    {
        return Response::$headers;
    }

    /**
     * Set response headers
     *
     * @param HttpHeaders $headers
     */
    public static function setHeaders(?HttpHeaders $headers): void
    {
        Response::$headers = $headers;
    }

    /**
     * Return all response defined cookies
     *
     * @return HttpCookies|NULL
     */
    public static function getCookies(): ?HttpCookies
    {
        return self::$cookies;
    }

    /**
     * Define cookies for response
     *
     * @param HttpCookies $cookies
     */
    public static function setCookies(?HttpCookies $cookies)
    {
        self::$cookies = $cookies;
    }
    
    /**
     * Add cookie to cookies
     *
     * @param HttpCookies $cookies
     */
    public static function addCookie(HttpCookie $cookie)
    {
        if (self::$cookies == null)
        {
            self::$cookies = new HttpCookies();
        }
        
        self::$cookies->getCookies()->add($cookie);
    }

    /**
     * Return http status code
     *
     * @return String
     */
    public static function getHttpStatus(): int
    {
        return self::$httpStatus;
    }

    /**
     * Set http status code
     *
     * @param String $httpStatus
     */
    public static function setHttpStatus(int $httpStatus): void
    {
        self::$httpStatus = $httpStatus;
    }

    /**
     * Set supported response media type
     *
     * @param String $mediaType
     */
    public static function setMediaType(String $mediaType): void
    {
        self::$mediaType = $mediaType;
    }

    /**
     * Return true if media type is Native
     *
     * @return bool
     */
    public static function isMediaTypeTextHTML(): bool
    {
        return (self::$mediaType == MediaType::TEXT_HTML_VALUE);
    }

    /**
     * Return media type
     *
     * @return String
     */
    public static function getMediaType(): ?String
    {
        return self::$mediaType;
    }
    
    /**
     * Return true if media type has been set
     *
     * @return bool
     */
    public static function isMediaTypeSet(): bool
    {
        return (self::$mediaType != null);
    }

    /**
     * Add object to output array
     *
     * @param String $name
     * @param mixed $value
     */
    public static function setBody($value): void
    {
        self::$body = $value;
    }

    /**
     * Return output object by name
     *
     * @param String $name
     * @return NULL|mixed
     */
    public static function getBody()
    {
        return self::$body;
    }

    /**
     * Set ResponseEntity into reponse
     *
     * @param ResponseEntity $responseEntity
     * @param String $status
     */
    public static function setResponseEntity(ResponseEntity $responseEntity): void
    {
        self::setHeaders($responseEntity->getHeaders());
        self::setCookies($responseEntity->getCookies());
        self::setResponse($responseEntity->getBody(), $responseEntity->getHttpStatusCode());
    }

    /**
     * Respond with http status code 201
     *
     * @param mixed $body
     */
    public static function created($body): void
    {
        self::setResponseEntity(new ResponseEntity($body, HttpStatus::CREATED));
    }

    /**
     * Respond with http status code 200
     *
     * @param mixed $body
     */
    public static function ok($body): void
    {
        self::setResponseEntity(new ResponseEntity($body, HttpStatus::OK));
    }

    /**
     * Respond with http status code 400
     *
     * @param mixed $body
     */
    public static function badRequest($body): void
    {
        self::setResponseEntity(new ResponseEntity($body, HttpStatus::BAD_REQUEST));
    }

    /**
     * Respond with http status code 401
     *
     * @param mixed $body
     */
    public static function unauthorized($body): void
    {
        self::setResponseEntity(new ResponseEntity($body, HttpStatus::UNAUTHORIZED));
    }

    /**
     * Respond with http status code 403
     *
     * @param mixed $body
     */
    public static function forbidden($body): void
    {
        self::setResponseEntity(new ResponseEntity($body, HttpStatus::FORBIDDEN));
    }

    /**
     * Respond with http status code 404
     *
     * @param mixed $body
     */
    public static function notFound($body): void
    {
        self::setResponseEntity(new ResponseEntity($body, HttpStatus::NOT_FOUND));
    }

    /**
     * Set response content
     *
     * @param String $status
     * @param mixed $body
     * @param mixed $error
     */
    public static function setResponse($body, String $httpStatus = HttpStatus::OK): void
    {
        self::setHttpStatus($httpStatus);
        self::setBody($body);
    }

    /**
     * Render output to client.
     * No set of content after this method run is allowed
     */
    public static function render(): void
    {
        // is action supported media type valid ?
        if (self::isMediaTypeSet() && ! MediaType::isValid(self::getMediaType()))
        {
            throw new InvalidValueException('Unsupported Media type defined for response: ' . self::getMediaType());
        }

        // transform response if bean of type xxx if defined
        self::$transformer = ApplicationContext::getBeanOf(ResponseSerializer::class);
        if (self::$transformer != null)
        {
            $body = self::$transformer->before(self::getHttpStatus(), self::getBody());
        } else {
            $body = self::getBody();
        }
        
        if ($body !== null)
        {
            $httpMc = BeanUtils::getHttpMessageConverterBean(self::getMediaType());
            static::print($httpMc->write($body));
        }

        static::print("");
    }

    private static function print(?String $string): void
    {
        // set http code
        http_response_code(self::getHttpStatus());

        // set headers
        $headers = self::getHeaders();
        //header(HttpHeaders::CONTENT_TYPE . ": " . self::getMediaType(), true);
        if ($headers != null)
        {
            $items = $headers->getHeaders();
            if ($items->size() > 0)
            {
                foreach ($items as $name => $value)
                {
                    header("$name: $value", true);
                }
            }
        }

        // set cookies if found
        $cookies = self::getCookies();
        if ($cookies != null)
        {
            $httpCookies = $cookies->getCookies();
            if ($httpCookies->size() > 0)
            {
                foreach ($httpCookies as $hc)
                {
                    $expiresIn = time() + $hc->getExpiresIn();
                    setcookie($hc->getName(), $hc->getValue(), $expiresIn, $hc->getPath(), $hc->getDomain(), $hc->getSecure(), $hc->getHttpOnly());
                }
            }
        }

        // flush buffer
        if (self::$transformer != null)
        {
            $string = self::$transformer->after(self::getHttpStatus(), $string);
        }
        
        echo ($string);
        
        if (! Environment::isActiveProfileTest()) exit(0);
    }
}