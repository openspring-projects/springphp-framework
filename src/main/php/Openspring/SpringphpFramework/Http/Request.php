<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Http;

use Openspring\SpringphpFramework\Context\ApplicationContext;
use Openspring\SpringphpFramework\Core\Initializable;
use Openspring\SpringphpFramework\Core\Bean\DefaultValues;
use Openspring\SpringphpFramework\Enumeration\MediaType;
use Openspring\SpringphpFramework\Exception\InvalidValueException;
use Openspring\SpringphpFramework\Utils\BeanUtils;

/**
 * Current request bridge
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
class Request implements Initializable
{
    private static $requestBodyInput = 'php://input';
    public static $requestBody = NULL;
    public static $contentType = MediaType::TEXT_HTML_VALUE;
    public static $accept = null;

    public static function initialize(): void
    {
        self::$requestBody = NULL;
        self::$contentType = static::getHeaderParameter(HttpHeaders::CONTENT_TYPE);
        $defaultConfig = ApplicationContext::getBeanOf(DefaultValues::class);

        if (! MediaType::isValid(self::$contentType))
        {
            $default = ($defaultConfig == null) ? MediaType::TEXT_HTML_VALUE : $defaultConfig->getDefaultContentType();

            // set default value
            static::setContentType($default);
        }
    }

    /**
     * Return current http method
     *
     * @return string
     */
    public static function getMethod(): ?String
    {
        return trim(strtoupper($_SERVER['REQUEST_METHOD']));
    }

    /**
     * Set request request content type
     *
     * @param String $contentType
     */
    public static function setContentType(String $contentType): void
    {
        if (! MediaType::isValid($contentType))
        {
            throw new InvalidValueException('Unsupported value ' . $contentType);
        }

        self::$contentType = $contentType;
    }

    /**
     * Return request content type
     *
     * @return String
     */
    public static function getContentType(): ?String
    {
        return self::$contentType;
    }

    /**
     * Set request request accept header
     *
     * @param String $contentType
     */
    public static function setAccept(String $accept): void
    {
        if (! MediaType::isValid($accept))
        {
            throw new InvalidValueException('Unsupported Header Accept value ' . $accept);
        }

        self::$accept = $accept;
    }

    /**
     * Return request accept header
     *
     * @return String
     */
    public static function getAccept(): ?String
    {
        if (self::$accept == null)
        {
            self::$accept = self::getHeaderParameter(HttpHeaders::ACCEPT);
        }

        return self::$accept;
    }

    /**
     * Return current request parameters
     *
     * @return array
     */
    public static function getParameters(): ?array
    {
        return $_REQUEST;
    }

    /**
     * Return current request post parameters
     *
     * @return array
     */
    public static function getPostParameters(): ?array
    {
        return $_POST;
    }

    /**
     * Return current request get parameters
     *
     * @return array
     */
    public static function getGetParameters(): ?array
    {
        return $_GET;
    }

    /**
     * Return current request cookies
     *
     * @return array
     */
    public static function getCookieParameters(): ?array
    {
        return $_COOKIE;
    }

    /**
     * Add a cookie
     *
     * @param String $variableName
     *            target variable name
     * @param String $value
     */
    public static function setCookieParameter(String $variableName, ?String $value): void
    {
        setcookie($variableName, $value, time() + 8640000, "/");
    }

    /**
     * Return a specific current request parameter.
     * This method search the parameter even in request body
     *
     * @param String $variableName
     *            target variable name
     * @param String $default
     *            default value in case undefined or null
     * @return String
     */
    public static function getAnyParameter(String $variableName, ?String $default = ''): ?String
    {
        $variableName = self::getGetParameter($variableName, $default);
        return ($variableName != $default) ? $variableName : self::getRequestBodyParameter($variableName, $default);
    }

    /**
     * Return a specific current request parameter
     *
     * @param String $variableName
     *            target variable name
     * @param String $default
     *            default value in case undefined or null
     * @return String
     */
    public static function getParameter(String $variableName, ?String $default = ''): ?String
    {
        return trim((isset($_REQUEST[$variableName])) ? $_REQUEST[$variableName] : $default);
    }

    /**
     * Return a specific current request post or get parameter
     * It starts with post then search in get
     *
     * @param String $variableName
     *            target variable name
     * @param String $default
     *            default value in case undefined or null
     * @return String|NULL
     */
    public static function getPostGetParameter(String $variableName, ?String $default = ''): ?String
    {
        return (Request::getPostParameter($variableName, NULL) != NULL) ? Request::getPostParameter($variableName, NULL) : Request::getGetParameter($variableName, $default);
    }

    /**
     * Return a specific current request get or post parameter.
     * It starts with get then search in post
     *
     * @param String $variableName
     *            target variable name
     * @param String $default
     *            default value in case undefined or null
     * @return String|NULL
     */
    public static function getGetPostParameter(String $variableName, ?String $default = ''): ?String
    {
        return (Request::getGetParameter($variableName, NULL) != NULL) ? Request::getGetParameter($variableName, NULL) : Request::getPostParameter($variableName, $default);
    }

    /**
     * Return a specific current request post parameter
     *
     * @param String $variableName
     *            target variable name
     * @param String $default
     *            default value in case undefined or null
     * @return String|NULL
     */
    public static function getPostParameter(String $variableName, ?String $default = ''): ?String
    {
        return trim((isset($_POST[$variableName])) ? $_POST[$variableName] : $default);
    }

    /**
     * Return a specific current request get parameter
     *
     * @param String $variableName
     *            target variable name
     * @param String $default
     *            default value in case undefined or null
     * @return String|NULL
     */
    public static function getGetParameter(String $variableName, ?String $default = ''): ?String
    {
        return trim((isset($_GET[$variableName])) ? $_GET[$variableName] : $default);
    }

    /**
     * Return a specific current request cookie parameter
     *
     * @param String $variableName
     *            target variable name
     * @param String $default
     *            default value in case undefined or null
     * @return String|NULL
     */
    public static function getCookieParameter(String $variableName, ?String $default = ''): ?String
    {
        return trim((isset($_COOKIE[$variableName])) ? $_COOKIE[$variableName] : $default);
    }

    /**
     * Return the request body
     *
     * @return mixed
     */
    public static function getRequestBody()
    {
        $content = file_get_contents(static::$requestBodyInput);
        if (self::$requestBody == NULL)
        {
            self::$requestBody = BeanUtils::getHttpMessageConverterBean(self::getContentType())->read($content);
        }

        return self::$requestBody;
    }

    /**
     * Set request body input
     *
     * @return void
     */
    public static function setRequestBodyInput(String $requestBodyInput): void
    {
        static::$requestBodyInput = $requestBodyInput;
    }

    /**
     * Return a specific request body parameter
     *
     * @param String $variableName
     *            target variable name
     * @param String $default
     *            default value in case undefined or null
     * @return mixed
     */
    public static function getRequestBodyParameter(String $variableName, ?String $default = '')
    {
        self::getRequestBody();
        if (self::$requestBody == NULL) return $default;
        return (property_exists(self::$requestBody, $variableName)) ? self::$requestBody->$variableName : $default;
    }

    /**
     * Return a header parameter
     *
     * @param String $variableName
     *            target variable name
     * @param String $default
     *            default value in case undefined or null
     * @return String|NULL
     */
    public static function getHeaderParameter(String $variableName, ?String $default = ''): ?String
    {
        $headers = null;

        if (function_exists('apache_request_headers'))
        {
            $headers = apache_request_headers();
        }
        else
        {
            return (isset($_SERVER[$variableName])) ? $_SERVER[$variableName] : $default;
        }

        return (isset($headers[$variableName])) ? $headers[$variableName] : $default;
    }
}