<?php

/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Http;

use Openspring\SpringphpFramework\Enumeration\EnumerationType;

class HttpStatusCategory extends EnumerationType
{
    const INFO = 'INFO';
    const SUCCESS = 'SUCCESS';
    const REDIRECT = 'REDIRECT';
    const ERROR = 'ERROR';
    const CLIENT_ERROR = 'CLIENT_ERROR';
    const SERVER_ERROR = 'SERVER_ERROR';

    public static function getTypeOf(int $typeCode)
    {
        switch ($typeCode) {
            case 1:
                return self::INFO;
            case 2:
                return self::SUCCESS;
            case 3:
                return self::REDIRECT;
            case 4:
                return self::ERROR;
            case 5:
                return self::ERROR;
        }
    }

    public static function getStatusCategory(int $httpCode): int
    {
        if ($httpCode > 599) return 6;
        if ($httpCode > 499) return 5;
        if ($httpCode > 399) return 4;
        if ($httpCode > 299) return 3;
        if ($httpCode > 199) return 2;
        if ($httpCode > 99) return 1;

        return 6;
    }
}