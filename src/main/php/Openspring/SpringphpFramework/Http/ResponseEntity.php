<?php

/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Http;

class ResponseEntity
{
    var $httpStatusCode = HttpStatus::OK;
    var $cookies = null;
    var $headers = null;
    var $body = null;

    public function __construct($body, int $httpStatusCode = HttpStatus::OK, HttpCookies $cookies = null, ?HttpHeaders $headers = null)
    {
        $this->body = $body;
        $this->httpStatusCode = $httpStatusCode;
        $this->cookies = $cookies;
        $this->headers = $headers;
    }

    public static function NEW(int $httpStatusCode = HttpStatus::OK)
    {
        return new ResponseEntity(null, $httpStatusCode);
    }

    public function getStatusCategory()
    {
        return HttpStatusCategory::getStatusCategory($this->httpStatusCode);
    }

    public function getCookies(): ?HttpCookies
    {
        return $this->cookies;
    }

    public function getHeaders(): ?HttpHeaders
    {
        return $this->headers;
    }

    public function isInformational()
    {
        return ($this->httpStatusCode > 99 && $this->httpStatusCode < 200);
    }

    public function isSuccess()
    {
        return ($this->httpStatusCode > 199 && $this->httpStatusCode < 300);
    }

    public function isRedirection()
    {
        return ($this->httpStatusCode > 299 && $this->httpStatusCode < 400);
    }

    public function isClientError()
    {
        return ($this->httpStatusCode > 399 && $this->httpStatusCode < 500);
    }

    public function isServerError()
    {
        return ($this->httpStatusCode > 499 && $this->httpStatusCode < 600);
    }

    public function getHttpStatusCode()
    {
        return $this->httpStatusCode;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function setHttpStatusCode($httpStatusCode)
    {
        $this->httpStatusCode = $httpStatusCode;
    }

    public function setBody($body)
    {
        $this->body = $body;
    }

    public static function cast($responseEntity): ResponseEntity
    {
        return $responseEntity;
    }
}