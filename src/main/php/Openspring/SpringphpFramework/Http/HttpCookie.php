<?php

/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Http;

/**
 * Global application cookies control class
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
class HttpCookie
{
    protected $name;
    protected $value;
    protected $expiresIn = 300;
    protected $path = '/';
    protected $domain;
    protected $secure = false;
    protected $httpOnly = false;

    public function __construct(String $name, String $value, int $expiresIn, String $path, String $domain)
    {
        $this->name = $name;
        $this->value = $value;
        $this->expiresIn = $expiresIn;
        $this->path = $path;
        $this->domain = $domain;
        $this->secure = false;
        $this->httpOnly = false;
    }

    public function getName(): String
    {
        return $this->name;
    }

    public function getValue(): String
    {
        return $this->value;
    }

    public function getExpiresIn(): int
    {
        return $this->expiresIn;
    }

    public function getPath(): ?String
    {
        return $this->path;
    }

    public function getDomain(): ?String
    {
        return $this->domain;
    }

    public function getSecure(): ?bool
    {
        return $this->secure;
    }

    public function getHttpOnly(): bool
    {
        return $this->httpOnly;
    }

    public function setName(String $name)
    {
        $this->name = $name;
    }

    public function setValue(String $value): void
    {
        $this->value = $value;
    }

    public function setExpiresIn(int $expiresIn): void
    {
        $this->expiresIn = $expiresIn;
    }

    public function setPath($path): void
    {
        $this->path = $path;
    }

    public function setDomain(?String $domain): void
    {
        $this->domain = $domain;
    }

    public function setSecure(?bool $secure): void
    {
        $this->secure = $secure;
    }

    public function setHttpOnly(bool $httpOnly): void
    {
        $this->httpOnly = $httpOnly;
    }
}