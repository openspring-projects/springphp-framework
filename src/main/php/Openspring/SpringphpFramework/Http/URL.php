<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Http;

use Openspring\SpringphpFramework\Context\Environment;
use Openspring\SpringphpFramework\Core\Initializable;
use Openspring\SpringphpFramework\Type\StringBuilder;
use Openspring\SpringphpFramework\IO\FileUtils;
use Openspring\SpringphpFramework\Utils\Strings;

class URL implements Initializable
{
    private static $host = NULL;
    private static $domain = NULL;
    private static $subdomain = NULL;
    private static $returnURL = NULL;

    public static function initialize(): void
    {
        $host = $_SERVER['HTTP_HOST'];

        if (is_numeric(trim(Strings::replace('.', '', $host))))
        {
            self::$domain = $host;
            self::$subdomain = '';
        }
        else
        {
            $parts = explode('.', strtolower($host));
            $count = count($parts);
            switch ($count)
            {
                case 3:
                    self::$domain = $parts[1] . '.' . $parts[2];
                    self::$subdomain = $parts[0];
                    break;
                case 2:
                    self::$domain = $parts[0] . '.' . $parts[1];
                    break;
                default:
                    self::$domain = $parts[0];
                    break;
            }
        }

        self::$host = $host;
        self::$returnURL = Request::getParameter('return_url', '');
    }

    public static function getCurrentURL()
    {
        $currentURL = self::getBase() . self::getCurrentURI();

        return $currentURL;
    }

    public static function getCurrentURI(bool $withQueryString = true)
    {
        $uri = $_SERVER['REQUEST_URI'];
        $url = ($withQueryString) ? $uri : explode('?', $uri)[0];

        return FileUtils::standarizePath($url);
    }

    public static function getProtocol()
    {
        return strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, strpos($_SERVER["SERVER_PROTOCOL"], '/')));
    }

    public static function getRemoteIP()
    {
        return $_SERVER["REMOTE_ADDR"];
    }

    public static function getServerPort()
    {
        return $_SERVER['SERVER_PORT'];
    }

    public static function getRemoteHttpUserAgent()
    {
        return isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "N/A";
    }

    public static function getHost()
    {
        return self::$host;
    }

    public static function getDomain()
    {
        return self::$domain;
    }

    public static function getSubdomain()
    {
        return self::$subdomain;
    }

    public static function getBase($subURLPath = '')
    {
        $sub = Strings::isEmpty($subURLPath) ? '' : '/' . $subURLPath;
        return self::getProtocol() . '://' . FileUtils::standarizePath(self::getHost() . $sub);
    }

    public static function getContextRoot($subURLPath = '')
    {
        return self::getBase() . Environment::getContextRoot($subURLPath);
    }

    public static function getPublic($subPathURL = '')
    {
        return self::getBase() . Environment::getPublicPath($subPathURL);
    }

    public static function getHttpQuery($params = array())
    {
        $sb = new StringBuilder();
        foreach ($params as $param => $value)
        {
            $sb->append($param . '=' . $value);
        }

        return $sb->toString('&');
    }
}