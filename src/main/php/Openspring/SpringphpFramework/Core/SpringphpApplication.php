<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Core;

use Openspring\SpringphpFramework\Context\ApplicationContext;
use Openspring\SpringphpFramework\Context\Environment;
use Openspring\SpringphpFramework\Core\Bean\EnvironmentPostProcessor;
use Openspring\SpringphpFramework\Core\Bean\HttpCors;
use Openspring\SpringphpFramework\Core\Bean\HttpInterceptor;
use Openspring\SpringphpFramework\Core\Bean\HttpMessageConverter;
use Openspring\SpringphpFramework\Core\Bean\IJwtProcessor;
use Openspring\SpringphpFramework\Core\Bean\ResourceBundleMessageSource;
use Openspring\SpringphpFramework\Core\Stereotype\Annotable;
use Openspring\SpringphpFramework\Core\Stereotype\Bean;
use Openspring\SpringphpFramework\Core\Stereotype\Configuration;
use Openspring\SpringphpFramework\Exception\ErrorHandler;
use Openspring\SpringphpFramework\Exception\ExceptionHandler;
use Openspring\SpringphpFramework\Exception\InvalidArgumentException;
use Openspring\SpringphpFramework\Exception\NullPointerException;
use Openspring\SpringphpFramework\Exception\Bean\NoSuchBeanDefinitionException;
use Openspring\SpringphpFramework\Exception\Http\NotFoundException;
use Openspring\SpringphpFramework\Exception\IO\DirectoryNotFoundException;
use Openspring\SpringphpFramework\Http\Request;
use Openspring\SpringphpFramework\Http\Response;
use Openspring\SpringphpFramework\Http\URL;
use Openspring\SpringphpFramework\Http\Converter\JsonMessageConverter;
use Openspring\SpringphpFramework\Http\Converter\TextHTMLMessageConverter;
use Openspring\SpringphpFramework\IO\DirectoryUtils;
use Openspring\SpringphpFramework\IO\Path;
use Openspring\SpringphpFramework\Log\LogFactory;
use Openspring\SpringphpFramework\Log\Logger;
use Openspring\SpringphpFramework\Log\Implementation\Log4p;
use Openspring\SpringphpFramework\Manager\ErrorReporting;
use Openspring\SpringphpFramework\Manager\RM;
use Openspring\SpringphpFramework\Orm\DatabaseContext;
use Openspring\SpringphpFramework\Orm\DataSource\DataSourcesManager;
use Openspring\SpringphpFramework\Orm\Query\Pipe;
use Openspring\SpringphpFramework\Reflection\ClassReflection;
use Openspring\SpringphpFramework\Security\Jwt\JwtAccessSecurity;
use Openspring\SpringphpFramework\Utils\Arrays;
use Openspring\SpringphpFramework\Utils\Objects;
use Openspring\SpringphpFramework\Utils\PathResolver;
use Openspring\SpringphpFramework\Utils\Rest;
use Openspring\SpringphpFramework\Utils\Router;
use Openspring\SpringphpFramework\Utils\Strings;
use Openspring\SpringphpFramework\Web\RestController;
use Openspring\SpringphpFramework\Web\View;

/**
 * Application abstract class.
 * A real world application must extend this class
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
abstract class SpringphpApplication implements Annotable
{
    protected $controllerAdvice = null;
    protected $continue = true;
    protected $logger;

    public function __construct(String $rootPath, String $runConfigPath = null, String $testConfigPath = null)
    {
        Environment::setRunConfigPath($runConfigPath);
        Environment::setTestConfigPath($testConfigPath);
        
        Environment::load();
        $this->logger = LogFactory::getLogger(self::class);
        
        $this->enableAnnotations();
        
        $this->logger->info("Setting default config path for RUN configurations as " . $runConfigPath);
        $this->logger->info("Setting default config path for TEST configurations as " . $testConfigPath);
        
        $this->logger->info("Setting root path as " . $rootPath . '/');
        Environment::setRootPath($rootPath . '/');

        $this->logger->info("Setting handlers for errors and exceptions ");
        
        @set_error_handler(array(new ErrorHandler(), 'handle'));
        $this->logger->debug("Error handler set as " . ErrorHandler::class);
        
        @set_exception_handler(array(new ExceptionHandler(), 'handle'));
        $this->logger->debug("Exception handler set as " . ExceptionHandler::class);

        $this->logger->debug("Saving application reference for future use in Context");
        ApplicationContext::setApplication($this);
    }

    public function enableAnnotations(): void
    {
        if (Environment::isActiveProfileProd()) return;

        $scanDirs = array();
        $ignoreDirs = array();
        
        //$this->logger->info("Scanning main application annotations");

        $annots = ClassReflection::getClassAnnotations(get_class($this));
        if (Arrays::KeyExists('ComponentScan', $annots))
        {
            $csValue = $annots['ComponentScan']->passedParams;
            if ($csValue == null)
            {
                //$this->logger->error('Main class annotation ScanCoponent has some invalid arguments');
                throw new InvalidArgumentException('Main class annotation ScanCoponent has some invalid arguments');
            }

            // baseDirs
            if (Objects::hasProperty($csValue, 'baseDirs'))
            {
                $baseDirs = $csValue->baseDirs;
                if (is_array($baseDirs))
                {
                    foreach ($baseDirs as $baseDir)
                    {
                        $fbaseDir = Environment::getRootPath($baseDir);
                        if (! DirectoryUtils::exists($fbaseDir))
                        {
                            //$this->logger->error('Can find directory ' . $fbaseDir . ' passed to @ComponentScan');
                            throw new DirectoryNotFoundException('Can find directory ' . $fbaseDir . ' passed to @ComponentScan');
                        }

                        $scanDirs[] = $baseDir;
                    }
                    //$this->logger->info('Scan directories found');
                    //$this->logger->debug($scanDirs);
                }
            }

            // excludeDirs
            if (Objects::hasProperty($csValue, 'excludeDirs'))
            {
                $excludeDirs = $csValue->excludeDirs;
                if (is_array($excludeDirs))
                {
                    foreach ($excludeDirs as $excludeDir)
                    {
                        if (Strings::isEmpty($excludeDir)) continue;

                        $fexcludeDir = Environment::getRootPath($excludeDir);
                        if (! DirectoryUtils::exists($fexcludeDir))
                        {
                            //$this->logger->error('Can find directory ' . $fexcludeDir . ' passed to @ComponentScan');
                            throw new DirectoryNotFoundException('Can find directory ' . $fexcludeDir . ' passed to @ComponentScan');
                        }

                        $ignoreDirs[] = $excludeDir;
                    }
                    
                    //$this->logger->info('Scan excluded directories found');
                    //$this->logger->debug($ignoreDirs);
                }
            }

            // work for componenet scan annotation goes here
            ApplicationContext::setScanDirs($scanDirs, $ignoreDirs);
        }

        if (Arrays::KeyExists('PropertySource', $annots))
        {
            $psValue = $annots['PropertySource'];
            $value = Strings::getBetween($psValue->passedParams, '"', '"', false);
            $psrcPath = PathResolver::resolve($value);
            
            Environment::setRunConfigPath($psrcPath);
            
            //$this->logger->info('PropertySource set as ' . $psrcPath);
        }

        if (count($scanDirs) == 0)
        {
            //$this->logger->error('Main class must be annotated with a valid ScanCoponent annotation');
            throw new NullPointerException('Main class must be annotated with a valid ScanCoponent annotation');
        }
    }

    public function execute(): SpringphpApplication
    {
        $this->logger->info('Bean injection and configuration started');
        
        // init application root logger
        if (ApplicationContext::getBeanOf(Logger::class) == null)
        {
            $this->logger->info('No logging bean set. Using default ' . Log4p::class);
            ApplicationContext::addBeanPrototype(Log4p::class);
        }

        // checking cors in case of OPTIONS method
        $corsBean = ApplicationContext::getBeanOf(HttpCors::class);
        $this->logger->info('Checking cors bean ' . ($corsBean == null ? 'not found' : 'found'));
        if ($corsBean != null)
        {
            if (! $corsBean->allowOrigins()) exit(0);
        }

        $this->logger->info('Initializing environnement');
        Environment::initialize();

        $this->logger->info('Setting up default beans configuration');
        $this->setDefaultConfiguration();

        $this->logger->info('Initializing application context');
        ApplicationContext::initialize();

        // load resources messages
        $this->logger->info('Loading resources bundles');
        $resourceBundleMessageSource = ApplicationContext::getBeanOf(ResourceBundleMessageSource::class);
        if ($resourceBundleMessageSource != null)
        {
            $resourceBundleMessageSource->load();
            $this->logger->debug('Bundles found and loadd');
        }

        // initialize static classes
        $this->logger->info('Initializing static classes');
        $this->initializeStaticClasses();

        if (! Environment::isActiveProfileProd())
        {
            $this->logger->info('Error reporting enabled');
            ErrorReporting::enable();
        }

        // run after preHandle
        $envPostProcessor = ApplicationContext::getBeansOf(EnvironmentPostProcessor::class);
        $this->logger->info('Overriding environmnt properties by calling post processor environment bean');
        foreach ($envPostProcessor as $apflHandler)
        {
            $apflHandler->postProcessEnvironment($this);
        }

        // route current request
        $this->logger->info('Routing request');
        Router::route();

        // check default controller
        $this->wcomp = Request::getParameter('wcomp', '');
        $this->Wcomp = ucfirst($this->wcomp);
        $this->action = Request::getParameter('action', '');
        $this->Action = ucfirst($this->action);
        
        $this->logger->info('Calling ' . $this->wcomp . '.' . $this->action);

        // connect to databases
        $this->logger->debug('Initializing database connections');
        DatabaseContext::initialize();
        DatabaseContext::setDataSourcesManager(new DataSourcesManager());
        $this->logger->info('Database connection routines executed');

        // run jwt if any bean defined
        $this->logger->debug('Initializing jwt processor');
        $jwtManager = ApplicationContext::getBeanOf(IJwtProcessor::class);
        if ($jwtManager != null)
        {
            $this->logger->info('Jwt processor found and run');
            $jwtManager->process();
        }

        // run interceptors preHandle
        $this->logger->info('Initializing http interceptors prehandle');
        $interceptors = ApplicationContext::getBeansOf(HttpInterceptor::class);
        foreach ($interceptors as $interceptor)
        {
            $this->logger->debug('Http interceptor prehandle run');
            if ($interceptor->preHandle() == false) return Response::render();
        }

        // process current request
        $this->logger->info('Runnig request action');
        $this->processRequest();

        // run interceptors postHandle
        $this->logger->info('Initializing http interceptors prehandle');
        foreach ($interceptors as $interceptor)
        {
            $this->logger->debug('Http interceptor prehandle run');
            if ($interceptor->postHandle() == false) return Response::render();
        }
        
        $this->logger->info('Bean injection and configuration finished');

        return $this;
    }

    private function setDefaultConfiguration(): void
    {
        // define default message converters
        $mcs = ApplicationContext::getBeansOf(HttpMessageConverter::class);
        if (count($mcs) < 2)
        {
            $this->logger->debug('Http messages convertors not set. Default set');
            ApplicationContext::removeBean(TextHTMLMessageConverter::class);
            ApplicationContext::removeBean(JsonMessageConverter::class);

            $this->addConfiguration(new TextHTMLMessageConverter());
            $this->addConfiguration(new JsonMessageConverter());
        }
    }

    public function getBean($bean): ?Bean
    {
        return ApplicationContext::getBeanOf($bean);
    }

    public function getBeans($bean): ?Array
    {
        return ApplicationContext::getBeansOf($bean);
    }

    public function addConfiguration(Configuration $bean): SpringphpApplication
    {
        $this->logger->debug('Adding configuration of ' . get_class($bean));
        ApplicationContext::addBeanPrototype($bean);

        return $this;
    }

    public function getResourceBundleMessageSource(): ResourceBundleMessageSource
    {
        if ($this->resourceBundleMessageSource == null)
        {
            $this->logger->error("No qualifying bean found of type " . ResourceBundleMessageSource::class);
            throw new NoSuchBeanDefinitionException("No qualifying bean found of type " . ResourceBundleMessageSource::class);
        }

        return $this->resourceBundleMessageSource;
    }

    public function initializeStaticClasses(): void
    {
        Path::initialize();
        Request::initialize();
        Response::initialize();
        Rest::initialize();
        JwtAccessSecurity::initialize();
        URL::initialize();
        View::initialize();
        RM::initialize();
        Pipe::initialize();
    }

    // Switch to subcontroller
    public function processRequest(): bool
    {
        $this->logger->info("Starting rest controller " . $this->wcomp);
        return $this->startController($this->wcomp, $this->action);
    }

    public function startController(String $wcomp, String $action = ''): bool
    {
        $controller = $this->createController($wcomp);
        if ($controller === false) return false;
        $controller->runAction($action);
        $this->logger->debug("Rest controller " . $this->wcomp . " started");

        return true;
    }

    public function createController(String $restControllerClass): RestController
    {
        $this->logger->debug('Creating REST controller ' . $restControllerClass);

        if (Strings::isEmpty($restControllerClass))
        {
            $this->logger->error('Can not create controller from NULL. No valid class has been passed as a controller class');
            throw new NullPointerException('Can not create controller from NULL. No valid class has been passed as a controller class');
        }

        if (! class_exists($restControllerClass))
        {
            $this->logger->error('Controller creation failure. Class not found ' . $restControllerClass);
            throw new NotFoundException('Controller creation failure. Class not found ' . $restControllerClass);
        }

        $this->logger->debug('Rest controller ' . $restControllerClass . ' created');
        
        return new $restControllerClass();
    }
}