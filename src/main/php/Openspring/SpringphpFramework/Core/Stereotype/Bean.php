<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Core\Stereotype;

use Openspring\SpringphpFramework\Context\ApplicationContext;
use Openspring\SpringphpFramework\Context\Environment;
use Openspring\SpringphpFramework\Reflection\ClassReflection;
use Openspring\SpringphpFramework\Utils\Arrays;
use Openspring\SpringphpFramework\Utils\Strings;
use Openspring\SpringphpFramework\Utils\ClassUtils;

/**
 * Bean definition
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
abstract class Bean implements IBean, Annotable
{

    /**
     * {@inheritdoc}
     * @see \Openspring\SpringphpFramework\Core\Stereotype\IBean::instanceOf()
     */
    public function instanceOf($clazz): bool
    {
        $parentClazz = (! is_string($clazz)) ? get_class($clazz) : $clazz;

        return ($this instanceof $parentClazz);
    }

    /**
     * {@inheritdoc}
     * @see \Openspring\SpringphpFramework\Core\Stereotype\Annotable::enableAnnotations()
     */
    public function enableAnnotations(): void
    {
        $vars = ClassReflection::getAnnotatedFileds(get_class($this));
        foreach ($vars as $property => $annots)
        {
            // DI by @Autowired and @Var annotations
            if (Arrays::keyExists('Autowired', $annots) && Arrays::keyExists('Var', $annots))
            {
                $diClazz = $annots['Var'];
                $required = $annots['Autowired'];

                ApplicationContext::addBeanAutowiringKey(get_class($this), $diClazz);

                $bean = ApplicationContext::getBeanOf($diClazz);
                if ($bean == null)
                {
                    ApplicationContext::addBeanSingleton($diClazz);
                    $bean = ApplicationContext::getBeanOf($diClazz, $required);
                }

                // inject now
                $this->$property = $bean;
            }

            // DI by @Value annotation
            if (Arrays::keyExists('Value', $annots))
            {
                $propertyValue = $annots['Value'];
                $propertyName = Strings::getBetween($propertyValue, '${', '}', false);
                if ($propertyName !== false)
                {
                    $propertyValue = Environment::getProperty($propertyName);
                }

                // inject now
                $this->$property = $propertyValue;
            }
        }
    }
}