<?php

/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Core\Stereotype;

use Openspring\SpringphpFramework\Enumeration\BeanScope;

/**
 * Bean element type
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
class BeanElement
{
    private $key;
    private $value;
    private $type;

    public function __construct(String $key, $value, int $type = BeanScope::SINGLETON)
    {
        $this->setKey($key);
        $this->setValue($value);
        $this->setType($type);
    }

    public function getKey(): String
    {
        return $this->key;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function setKey(String $key): void
    {
        $this->key = $key;
    }

    public function setValue($value): void
    {
        $this->value = $value;
    }

    public function setType(int $type): void
    {
        BeanScope::validate($type);
        $this->type = $type;
    }

    public function isSingleton(): bool
    {
        return ($this->type == BeanScope::SINGLETON);
    }

    public function isPrototype(): bool
    {
        return ($this->type == BeanScope::PROTOTYPE);
    }
    
    public function setInstance($instance): void
    {
        if (!is_string($this->getValue())) return;
        if ($this->isSingleton()) $this->setValue($instance);
    }
}