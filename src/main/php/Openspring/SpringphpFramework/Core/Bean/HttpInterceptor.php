<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Core\Bean;

/**
 * Http request interceptor
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
interface HttpInterceptor
{

    /**
     * This method is run on http request reception.
     * It is one of the first things to run
     *
     * @return bool
     */
    public function preHandle(): bool;

    /**
     * This method is run on the response last phase
     *
     * @return bool
     */
    public function postHandle(): bool;
}