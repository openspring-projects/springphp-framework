<?php
/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Core\Bean;

use Openspring\SpringphpFramework\Security\IUser;
use Openspring\SpringphpFramework\Security\UserDetailsProvider;
use Openspring\SpringphpFramework\Security\Jwt\JwtAccessToken;
use Openspring\SpringphpFramework\Security\Jwt\Core\JwtParameters;
use Openspring\SpringphpFramework\Security\Jwt\Enumeration\TokenScope;

/**
 * JWT authentification operations
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
interface IJwtProcessor extends UserClaims
{
    public function getClientId(): String;

    public function addRefreshToken(bool $value): IJwtProcessor;

    public function isAddRefreshTokenEnabled(): bool;

    public function setJwtAccessToken(JwtAccessToken $jwtAccessToken): IJwtProcessor;

    public function setUserRepository(UserDetailsProvider $userRepo): IJwtProcessor;

    public function setUserDto(IUser $userDto): IJwtProcessor;

    public function setJwtParameters(JwtParameters $encryptParams): IJwtProcessor;

    /**
     * Process Authorization if found in header
     *
     * @return IJwtProcessor
     */
    public function process(?String $token = null, String $tokenName = 'token', bool $throwException = true): IJwtProcessor;

    /**
     * Return token unsigned
     */
    public function getPayload();

    /**
     * Set an unsigned token
     *
     * @param mixed $token
     * @return IJwtProcessor
     */
    public function setPayload($token): IJwtProcessor;

    /**
     * Set token string
     *
     * @param String $signedToken
     * @return IJwtProcessor
     */
    public function setAccessToken(String $encoded): IJwtProcessor;

    /**
     * Create a new access token
     *
     * @param String $userId
     * @param String $scope
     * @return array
     */
    public function createAccessToken(String $userId, String $scope = TokenScope::OPENID_EMAIL_PROFILE): array;

    /**
     * Return true if token is valid
     *
     * @return bool
     */
    public function isValidToken(): bool;
}