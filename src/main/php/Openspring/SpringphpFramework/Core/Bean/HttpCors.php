<?php

/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Core\Bean;

use Openspring\SpringphpFramework\Core\Stereotype\Configuration;
use Openspring\SpringphpFramework\Http\HttpMethod;
use Openspring\SpringphpFramework\Utils\Strings;

/**
 * Http CORS
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
class HttpCors extends Configuration
{
    protected $allowOrigins = null;
    
    public function __construct($allowOrigins = null)
    {
        $this->allowOrigins = $allowOrigins;
    }
    
    public function allowOrigins(): bool
    {
        // Allow from any origin
        if (isset($_SERVER['HTTP_ORIGIN']))
        {
            $allowOrigins = (! Strings::isEmpty($this->allowOrigins)) ? $this->allowOrigins : $_SERVER['HTTP_ORIGIN'];
            // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
            // you want to allow, and if so:
            header("Access-Control-Allow-Origin: {$allowOrigins}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400'); // cache for 1 day
        }
        
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == HttpMethod::OPTIONS)
        {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            {
                // may also be using PUT, PATCH, HEAD etc
                header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, PATCH, HEAD, OPTIONS");
            }
            
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            {
                // header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            }
            
            return false;
        }
        
        return true;
    }
}