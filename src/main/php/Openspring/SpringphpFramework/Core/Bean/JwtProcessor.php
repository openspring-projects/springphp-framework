<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Core\Bean;

use Openspring\SpringphpFramework\Context\ApplicationContext;
use Openspring\SpringphpFramework\Core\Stereotype\Configuration;
use Openspring\SpringphpFramework\Exception\Http\BadRequestException;
use Openspring\SpringphpFramework\Exception\Http\ForbiddenException;
use Openspring\SpringphpFramework\Exception\Http\UnauthorizedException;
use Openspring\SpringphpFramework\Http\Request;
use Openspring\SpringphpFramework\Security\CurrentUser;
use Openspring\SpringphpFramework\Security\IUser;
use Openspring\SpringphpFramework\Security\PostTokenValidator;
use Openspring\SpringphpFramework\Security\UserDetailsProvider;
use Openspring\SpringphpFramework\Security\Jwt\JwtAccessToken;
use Openspring\SpringphpFramework\Security\Jwt\Core\Jwt;
use Openspring\SpringphpFramework\Security\Jwt\Core\JwtParameterProvider;
use Openspring\SpringphpFramework\Security\Jwt\Core\JwtParameters;
use Openspring\SpringphpFramework\Security\Jwt\Enumeration\TokenScope;
use Openspring\SpringphpFramework\Utils\Strings;

/**
 * Jwt openid-connect implementation
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
class JwtProcessor extends Configuration implements IJwtProcessor
{
    protected const AUTHORIZATION = 'Authorization';
    protected const USER = 'user';
    protected $clientId;
    protected $jwt;
    protected $jwtParameterProvider;
    protected $jwtParameters;
    protected $jwtAccessToken;
    protected $tokenName = 'token';
    protected $userDto = null;
    protected $payload = array();
    protected $addRefreshToken = false;
    protected $userRepo;
    protected $postValidator;

    public function __construct(String $clientId)
    {
        $this->clientId = $clientId;

        $this->jwtParameterProvider = ApplicationContext::getBeanOf(JwtParameterProvider::class, true);
        $this->userRepo = ApplicationContext::getBeanOf(UserDetailsProvider::class, true);
        $this->postValidator = ApplicationContext::getBeanOf(PostTokenValidator::class);

        $this->setJwt(new Jwt());
        $this->setJwtAccessToken(new JwtAccessToken());
    }

    protected function getJwtParameters(): JwtParameters
    {
        if ($this->jwtParameters == null)
        {
            $this->jwtParameters = $this->jwtParameterProvider->getJwtParameters($this->clientId);
        }

        return $this->jwtParameters;
    }

    public function getClientId(): String
    {
        return $this->clientId;
    }

    public function addRefreshToken(bool $value): IJwtProcessor
    {
        $this->addRefreshToken = $value;
        return $this;
    }

    public function isAddRefreshTokenEnabled(): bool
    {
        return $this->addRefreshToken;
    }

    public function setJwtAccessToken(JwtAccessToken $jwtAccessToken): IJwtProcessor
    {
        $this->jwtAccessToken = $jwtAccessToken;
        return $this;
    }

    public function setJwt(Jwt $jwt): IJwtProcessor
    {
        $this->jwt = $jwt;
        return $this;
    }

    public function setUserRepository(UserDetailsProvider $userRepo): IJwtProcessor
    {
        $this->userRepo = $userRepo;
        return $this;
    }

    public function setUserDto(IUser $userDto): IJwtProcessor
    {
        $this->userDto = $userDto;
        CurrentUser::setUser($this->userDto);
        return $this;
    }

    public function setJwtParameters(JwtParameters $jwtParameters): IJwtProcessor
    {
        $this->jwtParameters = $jwtParameters;
        return $this;
    }

    public function process(?String $token = null, String $tokenName = 'token', bool $throwException = true): IJwtProcessor
    {
        $this->tokenName = $tokenName;
        $this->payload = array();
        $mappingItem = ApplicationContext::$currentMappingItem;

        // if ($mappingItem != null)
        // {
        // if (count($mappingItem->getAuthGroups()) == 0 && count($mappingItem->getAuthRoles()) == 0)
        // {
        // return $this;
        // }
        // }

        // get token from request header
        if ($token == null)
        {
            $token = Request::getHeaderParameter(static::AUTHORIZATION);
        }

        // get token from request cookies
        if (Strings::isEmpty($token))
        {
            $token = Request::getCookieParameter($this->tokenName);
        }

        if (Strings::isEmpty($token)) return $this;

        // decrypt token
        $this->setAccessToken($token);
        if (! $this->isValidToken($throwException))
        {
            if (! $throwException) return $this;
            throw new UnauthorizedException();
        }

        // is user blocked ?
        if ($this->userRepo->isUserBlocked())
        {
            throw new UnauthorizedException('This user bas been blocked for some security reasons. Please contact your administrator');
        }

        // run post token validation if bean defined
        if ($this->postValidator != null && ! $this->postValidator->validate($this->getPayload()))
        {
            throw new UnauthorizedException('Invalid token');
        }

        return $this;
    }

    public function getPayload()
    {
        return $this->payload;
    }

    public function setPayload($payload): IJwtProcessor
    {
        $this->payload = $payload;
        return $this;
    }

    public function setAccessToken(String $encoded): IJwtProcessor
    {
        $encoded = Strings::replace('bearer ', '', $encoded);
        $encoded = Strings::replace('Bearer ', '', $encoded);
        $encoded = Strings::replace('BEARER ', '', $encoded);

        // set client id if empty
        if (Strings::isEmpty($this->clientId))
        {
            $encodedBody = Strings::getStringPart($encoded, '.', 1, '');
            $body = json_decode($this->jwt->decodeUrlSafeBase64($encodedBody));
            $this->clientId = ($body != null && property_exists($body, 'sub')) ? $body->sub : '';
        }

        $this->payload = $this->jwt->decode($encoded, $this->getJwtParameters()
            ->getPublicKey());

        if ($this->payload == false)
        {
            throw new ForbiddenException('Invalid jwt or public key');
        }

        if (! isset($this->payload['uid']))
        {
            throw new BadRequestException('Token payload contains no user_id');
        }

        $userId = $this->payload['uid'];
        $userDto = $this->userRepo->getUserById($userId);

        $this->setUserDto($userDto);

        return $this;
    }

    public function createAccessToken(String $userId, String $scope = TokenScope::OPENID_EMAIL_PROFILE): array
    {
        $this->jwtAccessToken->setUserClaims($this->getPayload());
        return $this->jwtAccessToken->createAccessToken($this->getClientId(), $userId, $scope, $this->isAddRefreshTokenEnabled());
    }

    public function isValidToken(bool $throwException = true): bool
    {
        if (! is_array($this->payload))
        {
            if (! $throwException) return false;
            throw new BadRequestException('Invalid token payload submitted');
        }

        return $this->jwtAccessToken->verifyTokenPayload($this->payload, $throwException);
    }

    public function addClaim(String $name, $value): UserClaims
    {
        if ($this->payload == null) $this->payload = array();
        $this->payload[$name] = $value;

        return $this;
    }

    public function addClaims(array $claims): UserClaims
    {
        foreach ($claims as $name => $value)
        {
            $this->addClaim($name, $value);
        }

        return $this;
    }
}