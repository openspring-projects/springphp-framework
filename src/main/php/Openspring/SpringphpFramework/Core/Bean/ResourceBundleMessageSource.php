<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Core\Bean;

use Openspring\SpringphpFramework\Core\Stereotype\Configuration;
use Openspring\SpringphpFramework\Http\Request;
use Openspring\SpringphpFramework\IO\Path;
use Openspring\SpringphpFramework\IO\PropertiesFile;
use Openspring\SpringphpFramework\Utils\Arrays;
use Openspring\SpringphpFramework\Utils\Strings;

/**
 * Resource bundle bessage source object.
 * This class manage internationalization of the application
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
class ResourceBundleMessageSource extends Configuration
{
    private $baseDir = "";
    private $baseNames = array(
                                "native-messages");
    private $resources = array();
    private $defaultLocale = "en";
    private $encoding = 'UTF-8';
    private $locale = "en";

    public function __construct()
    {
        $this->reloadLocale();
    }

    public function addBaseName(String $baseName): ResourceBundleMessageSource
    {
        $this->baseNames[] = $baseName;
        return $this;
    }

    public function getBaseNames(): array
    {
        return $this->baseNames;
    }

    public function load(): void
    {
        $this->baseDir = Path::getResources();
        $this->resources = array();

        foreach ($this->baseNames as $baseName)
        {
            $propFile = $this->baseDir . $baseName . "_" . $this->locale . ".properties";
            if (file_exists($propFile))
            {
                $props = new PropertiesFile($propFile, false, $this->getEncoding());
                $this->resources = array_merge($props->getProperties(), $this->resources);
            }
        }
    }

    public function getEncoding()
    {
        return $this->encoding;
    }

    public function setEncoding($encoding)
    {
        $this->encoding = $encoding;
    }

    /**
     * Return the message of the given code.
     * Exemple of use:
     * 1 - we have this english bundle: M002=Hi {name}, you are {age}
     * 2 - we can get this message as Hi khalid, you are 36
     * 3 - by calling this function like this: $resourcesMessages->getMessage('M002', 'khalid', 36)
     *
     * @param String $messageCode
     * @return String|NULL
     */
    public function getMessage(?String $messageCode): ?String
    {
        $m = isset($this->resources[$messageCode]) ? $this->resources[$messageCode] : $messageCode;
        $argsCount = func_num_args();
        if ($argsCount == 1) return $m;

        // decode message
        $args = func_get_args();
        $words = explode(' ', $m);
        $i = 1;
        foreach ($words as &$w)
        {
            if ($i >= $argsCount) break;

            if (Strings::containIgnoreCase($w, '{', '}'))
            {
                $param = Strings::getBetween($w, '{', '}');
                if (! Strings::isEmpty($param))
                {
                    $w = Strings::replace($param, $args[$i], $w);
                    $i ++;
                }
            }
        }

        return Arrays::implode(' ', $words);
    }

    public function getDefaultLocale()
    {
        return $this->defaultLocale;
    }

    public function getLocale()
    {
        return $this->locale;
    }

    public function setDefaultLocale($defaultLocale)
    {
        $this->defaultLocale = $defaultLocale;
        $this->reloadLocale();
    }

    private function reloadLocale()
    {
        $supportedLocales = Request::getHeaderParameter("Accept-Language", null);
        if (Strings::isEmpty($supportedLocales))
        {
            $this->locale = $this->defaultLocale;
        }
        else
        {
            $locales = explode(',', $supportedLocales);
            if (count($locales) == 1)
            {
                $this->locale = $locales[0];
            }
            else
            {
                $this->locale = $this->defaultLocale;
            }
        }
    }

    public function setLocale($locale)
    {
        $this->locale = $locale;
    }
}