<?php

/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Test;

use Openspring\SpringphpFramework\Context\ApplicationContext;
use Openspring\SpringphpFramework\Context\Environment;
use Openspring\SpringphpFramework\Core\SpringphpApplication;
use Openspring\SpringphpFramework\Core\SpringphpApplicationBoot;
use Openspring\SpringphpFramework\Core\Bean\JwtProcessor;
use Openspring\SpringphpFramework\Enumeration\ProfileType;
use Openspring\SpringphpFramework\Http\HttpMethod;
use Openspring\SpringphpFramework\Http\Request;
use Openspring\SpringphpFramework\IO\FileUtils;
use Openspring\SpringphpFramework\IO\Path;
use Openspring\SpringphpFramework\Mock\Mockito;
use Openspring\SpringphpFramework\Mock\MockitoStore;
use Openspring\SpringphpFramework\Security\IUser;
use Openspring\SpringphpFramework\Security\UserDetailsProvider;
use Openspring\SpringphpFramework\Security\Jwt\Core\JwtParameters;
use Openspring\SpringphpFramework\Utils\Router;
use Openspring\SpringphpFramework\Security\Jwt\Core\JwtParameterProvider;

class TestRunner
{
    protected static $clientId = 'espi-client';
    protected static $userId = '14665064ed5a42a29574784a8663c8d3';
    protected static $jwtParameters;
    protected static $jwtParameterProvider;

    public static function setClientId(String $clientId): void
    {
        TestRunner::$clientId = $clientId;
    }

    public static function setUserId(String $userId): void
    {
        TestRunner::$userId = $userId;
    }

    public static function setJwtParameterProvider(JwtParameterProvider $jwtParameterProvider): void
    {
        TestRunner::$jwtParameterProvider = $jwtParameterProvider;
    }
    
    public static function getJwtParameters(): JwtParameters
    {
        if (static::$jwtParameters == null)
        {
            static::$jwtParameters = static::$jwtParameterProvider->getJwtParameters(static::$clientId);
        }
        
        return static::$jwtParameters;
    }

    public static function setUserRepository(UserDetailsProvider $userRepository): void
    {
        TestRunner::$userRepository = $userRepository;
    }

    public static function setSpringphpContext(bool $injectMockito = false): void
    {
        Environment::setActiveProfile(ProfileType::TEST);

        Environment::setRootPath(self::getRootPath());
        Environment::setTestConfigPath('src/test/resources/');
        ApplicationContext::setContext('context.json');
        Request::setRequestBodyInput(Path::getResources('request-body.json'));

        // inject mock
        if ($injectMockito)
        {
            ApplicationContext::addBeanSingleton(new MockitoStore());
        }

        Environment::load();
        ApplicationContext::initialize();
        if ($injectMockito) Mockito::initialize();
    }

    public static function runHttpMethod(String $httpMethod, String $uri, bool $routeURI = false, bool $injectMockito = false): void
    {
        static::setSpringphpContext($injectMockito);

        $_SERVER['REQUEST_URI'] = FileUtils::standarizePath('/' . $uri);
        $_SERVER['HTTP_HOST'] = 'localhost';
        $_SERVER['Accept'] = 'application/json';
        $_SERVER['Content-Type'] = 'application/json';
        $_SERVER["SERVER_PROTOCOL"] = 'http';
        $_SERVER['REQUEST_METHOD'] = $httpMethod;
        $_SERVER["REMOTE_ADDR"] = '127.0.0.1';
        $_SERVER['HTTP_USER_AGENT'] = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4';

        if ($routeURI) Router::route();
    }

    public static function startApplication(SpringphpApplication $application, bool $withJWT = false): void
    {
        self::authentificateUser($withJWT);

        SpringphpApplicationBoot::run($application);
    }

    public static function bootstrapApplication(SpringphpApplication $application, bool $withJWT = false, String $httpMethod = null, String $uri = null, bool $injectMockito = false): void
    {
        if ($uri == null) $uri = 'esapi/account/update';
        if ($httpMethod == null) $httpMethod = HttpMethod::PUT;

        static::runHttpMethod($httpMethod, $uri, false, $injectMockito);

        self::authentificateUser($withJWT);
        SpringphpApplicationBoot::run($application);
    }

    public static function authentificateUser(bool $withJWT = false): void
    {
        if (isset($_SERVER['Authorization'])) unset($_SERVER['Authorization']);

        if ($withJWT) $_SERVER['Authorization'] = self::getMockedToken();
    }

    public static function getMockedToken(): String
    {
        $jwtManagerBean = new JwtProcessor(static::$clientId);
        $array = $jwtManagerBean->createAccessToken(static::$userId);

        return $array['access_token'];
    }

    public static function buildMockedIUser(IUser $mockUserDto): IUser
    {
        $mockUserDto->setUserId(static::$userId);
        $mockUserDto->setGroupId(2);
        $mockUserDto->setLogin('kha.elabbadi@gmail.com');
        $mockUserDto->setEmail("kha.elabbadi@gmail.com");
        $mockUserDto->setFirstName('Khalid');
        $mockUserDto->setLastName('ELABBADI');

        return $mockUserDto;
    }

    public static function mockFileUpload(String $param = 'users', String $ext = 'json'): void
    {
        $resourcesPath = Path::getResources();

        $_FILES = [
            $param => [
                'tmp_name' => $resourcesPath . "io/{$param}-tmp.{$ext}",
                'name' => "{$param}-tmp.{$ext}",
                'type' => $ext,
                'size' => 16000,
                'error' => 0]];
    }

    public static function resetMockFileUpload(): void
    {
        $_FILES = array();
    }

    public static function getRootPath(String $subPath = ''): String
    {
        return FileUtils::standarizePath(dirname(__DIR__, 6)) . '/' . $subPath;
    }
}