<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Log;

use Openspring\SpringphpFramework\Context\ApplicationContext;
use Openspring\SpringphpFramework\Context\Environment;
use Openspring\SpringphpFramework\IO\DirectoryUtils;
use Openspring\SpringphpFramework\IO\FileUtils;
use Openspring\SpringphpFramework\Log\Implementation\Log4p;
use Openspring\SpringphpFramework\Reflection\ExtendedReflectionClass;
use Openspring\SpringphpFramework\Utils\Dates;
use Openspring\SpringphpFramework\Utils\Strings;
use Exception;
use Openspring\SpringphpFramework\Utils\EncryptionUtils;

/**
 * Global log factory class
 *
 * @author Khalid ELABBADI
 * @email khalid.elabbadii@gmail.com
 */
class LogFactory
{
    protected static $loggers = [];
    protected static $logFile = null;

    public static function getLogger($clazz): Logger
    {
        $logger = null;

        $class = new ExtendedReflectionClass($clazz);
        $dns = LogUtils::getDottedNamespace($class->getNamespaceName());
        $paramProvider = new LoggerParameterProvider();

        $fmaxSize = Environment::getLogFileMaxSize();
        if (self::$logFile == null)
        {
            self::$logFile = self::getFilePath(Environment::getLogFile(), $fmaxSize);
        }

        $paramProvider->setClazzName($class->getShortName());
        $paramProvider->setNamespace($dns);
        $paramProvider->setEnabled(Environment::isLogEnabled());
        $paramProvider->setLevel(LogUtils::getLevelByName(Environment::getLogLevel()));
        $paramProvider->setFileMaxSize($fmaxSize);
        $paramProvider->setFilePath(self::$logFile);
        $paramProvider->setPattern(Environment::getLogFormat());

        if (isset(self::$loggers[$dns])) return self::$loggers[$dns];

        // find an implementation of Logger (bean)
        $loggerClass = ApplicationContext::getBeanOf(Logger::class);
        if ($loggerClass == null)
        {
            ApplicationContext::addBeanSingleton(Log4p::class);
            $logger = new Log4p();
            $logger->setLoggerParameterProvider($paramProvider);
        }
        else
        {
            $logger = new $loggerClass();
            $logger->setLoggerParameterProvider($paramProvider);
        }

        self::$loggers[$dns] = $logger;

        return $logger;
    }

    protected static function getFilePath(String $filePath, int $maxSize): String
    {
        $filePath = Strings::replace('\\', '/', $filePath);
        $replaceConfig = array(
            '-' => '',
            ' ' => '',
            ':' => '');
        $dt = Strings::replace('-', '', Dates::getCurrentDate());
        $dtm = Strings::replaceAll(Dates::getCurrentDateTime(), $replaceConfig);

        $filePath = Strings::replace('%d', $dt, $filePath);
        $filePath = Strings::replace('%dt', $dtm, $filePath);

        $fname = FileUtils::getFileName($filePath);
        $fileDirPath = FileUtils::standarizePath(dirname($filePath));
        $realDirPath = FileUtils::standarizePath(realpath($fileDirPath));

        if (! Strings::isEmpty($realDirPath) && $fileDirPath != $realDirPath) $fileDirPath = $realDirPath;

        if (! DirectoryUtils::exists($fileDirPath))
        {
            DirectoryUtils::createIfNotExists(dirname($filePath));
        }

        if (! DirectoryUtils::writable($fileDirPath))
        {
            throw new Exception("Log file directory does not exist or readonly " . $fileDirPath . " (You can turn off this feature by setting springphp.log.enable to false in application.properties)");
        }

        $fpath = FileUtils::standarizePath($fileDirPath . '/' . $fname);

        if (FileUtils::exists($fpath))
        {
            $fsize = filesize($fpath) / 1024;
            if ($maxSize < $fsize)
            {
                FileUtils::moveFile($fpath, $fpath . '.backup.' . uniqid() . '.log');
            }
        }
        
        return $fpath;
    }
}