<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Log\Implementation;

use Openspring\SpringphpFramework\Context\Environment;
use Openspring\SpringphpFramework\Core\Stereotype\Component;
use Openspring\SpringphpFramework\IO\DirectoryUtils;
use Openspring\SpringphpFramework\IO\Path;
use Openspring\SpringphpFramework\Log\LogLevel;
use Openspring\SpringphpFramework\Log\Logger;
use Openspring\SpringphpFramework\Log\LoggerParameterProvider;
use Openspring\SpringphpFramework\Log\Appender\FileAppender;
use Openspring\SpringphpFramework\Utils\Dates;
use Openspring\SpringphpFramework\Utils\Strings;
use Exception;
use Openspring\SpringphpFramework\Log\LogUtils;
use Openspring\SpringphpFramework\Exception\IO\DirectoryNotFoundException;
use Openspring\SpringphpFramework\IO\FileUtils;

class Log4p extends Component implements Logger
{
    protected $filePath = NULL;
    protected $level = LogLevel::INFO;
    protected $fileAppender;
    protected $paramProvider;

    public function setLoggerParameterProvider(LoggerParameterProvider $paramProvider): void
    {
        $this->paramProvider = $paramProvider;
        $this->setLevel($this->paramProvider->getLevel());
        $this->setFilePath($this->paramProvider->getFilePath());
    }

    public function debug($item): void
    {
        if (! $this->isLevelLoggable(LogLevel::DEBUG)) return;
        $this->buildAndLogMessage($item, LogLevel::DEBUG);
    }

    public function info($item): void
    {
        if (! $this->isLevelLoggable(LogLevel::INFO)) return;
        $this->buildAndLogMessage($item, LogLevel::INFO);
    }

    public function warn($item): void
    {
        if (! $this->isLevelLoggable(LogLevel::WARN)) return;
        $this->buildAndLogMessage($item, LogLevel::WARN);
    }

    public function error($item): void
    {
        if (! $this->isLevelLoggable(LogLevel::ERROR)) return;
        $this->buildAndLogMessage($item, LogLevel::ERROR);
    }

    public function isDebugEnabled(): bool
    {
        return $this->isLevelLoggable(LogLevel::DEBUG);
    }

    public function isInfoEnabled(): bool
    {
        return $this->isLevelLoggable(LogLevel::INFO);
    }

    public function isWarnEnabled(): bool
    {
        return $this->isLevelLoggable(LogLevel::WARN);
    }

    public function isErrorEnabled(): bool
    {
        return $this->isLevelLoggable(LogLevel::ERROR);
    }

    public function setFilePath(String $filePath): void
    {
        $this->filePath = $filePath;
        $this->fileAppender = new FileAppender($this->filePath);
    }

    public function getFileAppender(): FileAppender
    {
        return $this->fileAppender;
    }

    public function getFilePath(): String
    {
        return $this->filePath;
    }

    public function setLevel(int $level): void
    {
        $this->level = $level;
    }

    public function getLevel(): int
    {
        return $this->level;
    }

    public function isLevelLoggable(int $targetLevel): bool
    {
        return ($targetLevel >= $this->level);
    }

    protected function buildAndLogMessage($item, int $level): void
    {
        if (! Environment::isLogEnabled()) return;
        
        $pattern = $this->paramProvider->getPattern();
        $cname = $this->paramProvider->getClazzName();
        $dns = $this->paramProvider->getNamespace();
        $levelName = LogUtils::getLevelName($level);

        $date = date("Y-m-d H:s:i");
        $output = Strings::replaceAll($pattern, array(
            '%d' => $date,
            '%ns' => $dns . '.' . $cname,
            '%c' => $cname,
            '%l' => $levelName,
            '%m' => $this->itemToString($item)));

        $this->fileAppender->append($output);
    }

    protected function itemToString($item): String
    {
        $text = $item;
        if (! is_string($item))
        {
            if (is_object($item) && method_exists($item, '__toString'))
            {
                $text = $item->__toString();
            }
            else
            {
                $text = json_encode($item);
            }
        }

        return $text;
    }
}