<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Log;

/**
 * Default Logger Parameters Provider
 *
 * @author Khalid ELABBADI
 * @email khalid.elabbadii@gmail.com
 */
class LoggerParameterProvider
{
    protected $clazzName;
    protected $namespace;
    protected $enabled;
    protected $level;
    protected $filePath;
    protected $fileMaxSize;
    protected $pattern;

    public function setClazzName(String $value): void
    {
        $this->clazzName = $value;
    }

    public function getClazzName(): String
    {
        return $this->clazzName;
    }

    public function setNamespace(String $value): void
    {
        $this->namespace = $value;
    }

    public function getNamespace(): String
    {
        return $this->namespace;
    }

    public function setEnabled(bool $value): void
    {
        $this->enabled = $value;
    }

    public function getEnabled(): bool
    {
        return $this->enabled;
    }

    public function setLevel(int $value): void
    {
        $this->level = $value;
    }

    public function getLevel(): int
    {
        return $this->level;
    }

    public function setFilePath(String $value): void
    {
        $this->filePath = $value;
    }

    public function getFilePath(): String
    {
        return $this->filePath;
    }
    
    public function setFileMaxSize(int $sizeInKO): void
    {
        $this->fileMaxSize = $sizeInKO;
    }
    
    public function getFileMaxSize(): int
    {
        return $this->fileMaxSize;
    }

    public function setPattern(String $value): void
    {
        $this->pattern = $value;
    }

    public function getPattern(): String
    {
        return $this->pattern;
    }
}