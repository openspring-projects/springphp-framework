<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Log;

use Openspring\SpringphpFramework\Log\Appender\FileAppender;

interface Logger
{
    public function setLoggerParameterProvider(LoggerParameterProvider $paramProvider): void;
    
    public function isDebugEnabled(): bool;

    public function debug($element): void;

    public function isInfoEnabled(): bool;

    public function info($element): void;

    public function isWarnEnabled(): bool;

    public function warn($element): void;

    public function isErrorEnabled(): bool;

    public function error($element): void;

    public function setLevel(int $level): void;

    public function getLevel(): int;

    public function setFilePath(String $filePath): void;

    public function getFilePath(): String;

    public function getFileAppender(): FileAppender;

    public function isLevelLoggable(int $targetLevel): bool;
}