<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Reflection;

use Openspring\SpringphpFramework\Utils\Arrays;
use Openspring\SpringphpFramework\Utils\Strings;
use ReflectionClass;
use RuntimeException;

class ExtendedReflectionClass extends ReflectionClass
{
    protected $useStatements = [];
    const USE_PATTERN = '/use[\s]+(.*);/';

    public function getUseStatements(): array
    {
        if (count($this->useStatements) == 0)
        {
            $this->reloadUseStatements();
        }

        return $this->useStatements;
    }

    public function getUseStatement(String $cname): String
    {
        return Arrays::get($this->getUseStatements(), $cname);
    }

    protected function reloadUseStatements()
    {
        if (! $this->isUserDefined())
        {
            throw new RuntimeException('Must parse use statements from user defined classes.');
        }

        $source = file_get_contents($this->getFileName());

        $source = Strings::replaceAll($source, array(
            PHP_EOL => ' ',
            '\r\n' => ' ',
            '\r' => ' ',
            ';' => ';' . PHP_EOL));

        $matchs = ClassReflection::getTextPregmatch($source, static::USE_PATTERN);
        foreach ($matchs as $match)
        {
            $useItem = trim(Strings::replace(';', '', $match));
            $useItem = Strings::replaceAll($useItem, array(
                ' AS ' => ' as ',
                ' As ' => ' as ',
                ' aS ' => ' as '));

            $brutClazzName = trim(Strings::getStringPart($useItem, '\\', - 1));
            $brutClazzName = Strings::replace('use ', '', $brutClazzName);

            if (Strings::contain($brutClazzName, '{'))
            {
                $brutClazzName = Strings::replaceAll($brutClazzName, array(
                    '{' => '',
                    '}' => ''));
                $base = trim(Strings::getBetween($useItem, 'use ', '{', false));
                $classes = explode(',', $brutClazzName);
                foreach ($classes as $classe)
                {
                    $realClazzName = trim(Strings::getStringPart(trim($classe), ' as ', 0));
                    $clazzName = trim(Strings::getStringPart(trim($classe), ' as ', - 1));
                    $this->useStatements[$clazzName] = Strings::replaceAll($base . $realClazzName, array(
                        ' ' => ''));
                }
            }
            else
            {
                $base = '';
                $useItem = trim(Strings::replace('use ', '', $useItem));
                $posOfLastAntislash = strripos($useItem, '\\');
                if ($posOfLastAntislash > 0)
                {
                    $base = substr($useItem, 0, $posOfLastAntislash + 1);
                }

                $realClazzName = trim(Strings::getStringPart(trim($brutClazzName), ' as ', 0));
                $clazzName = trim(Strings::getStringPart($brutClazzName, ' as ', - 1));
                $this->useStatements[$clazzName] = Strings::replaceAll($base . $realClazzName, array(
                    ' ' => ''));
            }
        }
    }
}