<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Reflection;

use Openspring\SpringphpFramework\Annotation\Annotation;
use Openspring\SpringphpFramework\Annotation\AnnotationArgumentProcessor;
use Openspring\SpringphpFramework\Context\ApplicationContext;
use Openspring\SpringphpFramework\Core\Stereotype\Annotations;
use Openspring\SpringphpFramework\Core\Stereotype\IBean;
use Openspring\SpringphpFramework\Enumeration\ObjectType;
use Openspring\SpringphpFramework\Exception\InvalidArgumentException;
use Openspring\SpringphpFramework\IO\FileUtils;
use Openspring\SpringphpFramework\Type\ClassInfo;
use Openspring\SpringphpFramework\Type\ClassMethod;
use Openspring\SpringphpFramework\Type\MethodParameter;
use Openspring\SpringphpFramework\Utils\Arrays;
use Openspring\SpringphpFramework\Utils\Objects;
use Openspring\SpringphpFramework\Utils\Strings;
use Exception;
use ReflectionClass;
use ReflectionProperty;

/**
 * This class load class info from php file
 *
 * @author Khalid ELABBADI
 * @email khalid.elabbadii@gmail.com
 */
class ClassReflection
{
    const T_COMMA = ';';
    const T_BRACE_OPEN = '{';
    const T_BRACE_CLOSE = '}';
    const ANNOTATION_PATTERN = '/@[a-zA-Z]+[\(]*(.*)[\)]*/';

    public static function isClass($clazz): bool
    {
        $clz = (is_object($clazz)) ? get_class($clazz) : $clazz;

        return class_exists($clz);
    }

    public static function isInterface($clazz): bool
    {
        return interface_exists($clazz);
    }

    public static function isClassOrInterface($clazz): bool
    {
        $isClass = static::isClass($clazz);
        if ($isClass) return true;

        return static::isInterface($clazz);
    }

    public static function getAllAnnotationsKeys($clazz, array $pick = array()): array
    {
        $content = static::getClassObjectFileContent($clazz);
        $allAnnots = static::getTextPregmatch($content, static::ANNOTATION_PATTERN);

        $tmpArray = array();
        foreach ($allAnnots as $annot)
        {
            $tmpArray[static::getAnnotationsName($annot)] = true;
        }

        $out = array();
        foreach (array_keys($tmpArray) as $key)
        {
            if (count($pick) > 0 && ! in_array($key, $pick)) continue;

            $out[] = $key;
        }

        return $out;
    }

    public static function getAnnotationsName(String $doc): String
    {
        $rightDelim = (Strings::contain($doc, '(')) ? '(' : ' ';

        $aname = trim(Strings::getBetween($doc, '@', $rightDelim, false));
        $aname = (Strings::isEmpty($aname)) ? Strings::replace('@', '', trim($doc)) : $aname;

        return ucfirst($aname);
    }

    public static function getTextPregmatch(String $content, String $pattern): array
    {
        $out = array();
        $result = array();
        preg_match_all($pattern, $content, $result, PREG_SET_ORDER, 0);

        foreach ($result as $array)
        {
            $out[] = $array[0];
        }

        return $out;
    }

    public static function getClassObjectFileContent($clazz): String
    {
        return file_get_contents(static::getClassObjectFilePath($clazz));
    }

    public static function getClassObjectFilePath($clazz): String
    {
        $clz = (is_object($clazz)) ? get_class($clazz) : $clazz;

        $reflector = new ReflectionClass($clz);
        return $reflector->getFileName();
    }

    /**
     * Return an array of valid found annotations
     * How to use:
     * $autowired = new Any();
     * $autowired->name = 'Autowired';
     * $autowired->start = '@Autowired(';
     * $autowired->end = ')';
     * $aconfigs = array($autowired);
     * $annots = ClassReflection::getDocAnnotations($doc, $aconfigs);
     *
     * @param String $doc
     * @param array $aconfigs
     * @return array
     */
    public static function getDocAnnotations(String $doc, array $aconfigs, bool $keepDelim = true): array
    {
        $doc = static::cleanDoc($doc);
        $annots = static::getTextPregmatch($doc, static::ANNOTATION_PATTERN);
        $outAnnouts = array();

        foreach ($annots as $annot)
        {
            foreach ($aconfigs as $key => $aconfig)
            {
                if (Strings::startsWith(strtolower($annot), strtolower($aconfig->start)))
                {
                    $params = Strings::getBetweenInv($annot, $aconfig->start, $aconfig->end, $keepDelim);
                    
                    $agrsProcessor = new AnnotationArgumentProcessor($params);
                    $aconfig->passedParams = (! $keepDelim && $aconfig->paramType == 0) ? $agrsProcessor->decodeJson() : $params;
                    $outAnnouts[$key] = $aconfig;
                }
            }
        }

        return $outAnnouts;
    }

    /**
     * Clean and organize doc to make annotations extraction easy
     *
     * @param String $doc
     * @return String
     */
    public static function cleanDoc(String $doc): String
    {
        $doc = Strings::replace('/**', '', $doc);
        $doc = Strings::replace('*/', '', $doc);

        $cleanDoc = '';
        foreach (preg_split("/((\r?\n)|(\r\n?))/", $doc) as $line)
        {
            $line = trim($line);
            while (Strings::startsWith($line, '*'))
            {
                $line = substr($line, 1, strlen($line) - 1);
                $line = trim($line) . ' ';
            }

            $cleanDoc .= $line;
        }

        return Strings::replace('@', PHP_EOL . '@', $cleanDoc);
    }

    public static function getClassAnnotations(String $clazz): array
    {
        $annots = array();
        $class = new ReflectionClass($clazz);
        $doc = $class->getDocComment();
        $fannots = ClassReflection::getDocAnnotations($doc, Annotations::getClassAnnotations(), false);

        // @ComponentScan({"scanDirs": ["a/b/c", "f/d/c"], "excludeDirs": ["a/b/c", "f/d/c"]})
        if (Arrays::keyExists('ComponentScan', $fannots))
        {
            $annots['ComponentScan'] = $fannots['ComponentScan'];
        }

        // @PropertySource("${application.name}")
        if (Arrays::keyExists('PropertySource', $fannots))
        {
            $annots['PropertySource'] = $fannots['PropertySource'];
        }

        return $annots;
    }

    public static function getAnnotatedFileds(String $clazz): array
    {
        $vars = array();
        $class = new ExtendedReflectionClass($clazz);
        $ns = $class->getNamespaceName();
        $annots = array();

        foreach ($class->getProperties() as $property)
        {
            $doc = $property->getDocComment();
            $annots = array();

            $fannots = ClassReflection::getDocAnnotations($doc, Annotations::getFieldAnnotations(), false);

            foreach ($fannots as $key => $fannot)
            {
                $annots[$key] = $fannot->passedParams;
            }

            // @Var
            if (Arrays::keyExists('Var', $annots))
            {
                $var = trim(Strings::getStringPart($fannots['Var']->passedParams, ' ', 0));
                if ($var !== false && ! ObjectType::isValid($var, true))
                {
                    // find class in use statements
                    $usClazz = $class->getUseStatement($var);
                    if (! Strings::isEmpty($usClazz))
                    {
                        $diClazz = $usClazz;
                        $annots['Var'] = $diClazz;
                    }
                    else
                    {
                        // find it in beans repository
                        $diClazz = ApplicationContext::getBeansRepoClass($var);

                        if ($diClazz == null)
                        {
                            // set local namespace
                            $diClazz = $ns . '\\' . $var;
                        }

                        // set beans repo class
                        $annots['Var'] = $diClazz;
                    }
                }
            }

            // add annotations to field
            $vars[$property->getName()] = $annots;
        }

        return $vars;
    }

    /**
     * Return true if property exists on given object and its modifier is private
     *
     * @param
     *            $object
     * @param String $property
     * @return bool
     */
    public static function isPropertyPrivate($object, String $property): bool
    {
        if (! Objects::hasProperty($object, $property)) return false;

        return ((new ReflectionProperty($object, $property))->isPrivate());
    }

    /**
     * Return true if property exists on given object and its modifier is public
     *
     * @param
     *            $object
     * @param String $property
     * @return bool
     */
    public static function isPropertyPublic($object, String $property): bool
    {
        if (! Objects::hasProperty($object, $property)) return false;

        $refProp = new ReflectionProperty($object, $property);

        return $refProp->isPublic();
    }

    /**
     * Check if a class implements an interface
     *
     * @param
     *            $clazz
     * @param
     *            $interface
     * @return bool
     */
    public static function implements(String $clazz, String $interface): bool
    {
        $interfaces = class_implements($clazz);

        return (isset($interfaces[$interface]));
    }

    public static function extends(String $subClazz, String $parentClazz): bool
    {
        return ($subClazz == $parentClazz || is_subclass_of($subClazz, $parentClazz));
    }

    /**
     * Parse the given class or a given class object
     *
     * @param $clazz mixed
     *            class to parse or an object of it
     * @return ClassInfo|NULL
     */
    public static function parseClass($clazz): ?ClassInfo
    {
        return ClassReflection::parse(ClassReflection::getClassFile($clazz));
    }

    /**
     * Parse the given php class file
     *
     * @param
     *            $clazz
     * @return ClassInfo|NULL
     */
    public static function parse(String $phpFilePath): ?ClassInfo
    {
        if (! file_exists($phpFilePath)) throw new Exception('Can not find file ' . $phpFilePath);

        $classInfo = new ClassInfo();

        $namespace = "";
        $comments = "";
        $varType = "";
        $accessor = "";
        $openBracesCount = 0;

        $source = file_get_contents($phpFilePath);
        $tokens = token_get_all($source);

        $FLAG_NEXT_STRING_IS = "";
        $FLAG_IN_CLASS = false;
        $FLAG_COMA_FOUND = false;
        $FLAG_IN_FUNCTION = false;

        foreach ($tokens as $token)
        {
            switch ($token[0])
            {
                case T_NAMESPACE:
                    $FLAG_NEXT_STRING_IS = 0;
                    $FLAG_COMA_FOUND = false;
                    break;

                case static::T_BRACE_OPEN:
                    $openBracesCount ++;
                    $FLAG_IN_FUNCTION = false;
                    break;

                case static::T_BRACE_CLOSE:
                    $openBracesCount --;
                    $comments = "";
                    break;

                case T_CLASS:
                    $FLAG_IN_CLASS = true;
                    $FLAG_NEXT_STRING_IS = 1;
                    break;

                case T_COMMENT:
                case T_DOC_COMMENT:
                    $FLAG_NEXT_STRING_IS = 2;
                    $comments .= $token[1];
                    break;

                case T_PRIVATE:
                case T_PUBLIC:
                case T_PROTECTED:
                    $FLAG_NEXT_STRING_IS = 3;
                    $accessor = $token[1];
                    break;

                case T_FUNCTION:
                    if ($FLAG_IN_CLASS) $FLAG_NEXT_STRING_IS = 4;
                    break;

                case T_VARIABLE:
                    if ($FLAG_IN_FUNCTION)
                    {
                        end($classInfo->methods)->parameters[] = new MethodParameter($token[1], $varType);
                    }
                    break;

                case static::T_COMMA:
                    $FLAG_COMA_FOUND = true;
                    break;

                case T_STRING:
                    $value = $token[1];
                    $varType = $value;
                    switch ($FLAG_NEXT_STRING_IS)
                    {
                        case 0: // namespace
                            if ($FLAG_COMA_FOUND)
                            {
                                $FLAG_NEXT_STRING_IS = - 1;
                                break;
                            }
                            $namespace = trim($namespace) == '' ? $value : "{$namespace }\\{$value}";

                            break;

                        case 1: // class
                            $classInfo->name = $token[1];
                            $classInfo->qualifiedName = $namespace . "\\" . $value;
                            $classInfo->comments = $comments;
                            $comments = '';
                            break;

                        case 4: // function
                            $classInfo->methods[] = new ClassMethod($token[1], $accessor, $comments);
                            $comments = "";
                            $accessor = "public";
                            $FLAG_IN_FUNCTION = true;
                            $varType = '';
                            break;
                    }

                    if ($FLAG_NEXT_STRING_IS > 0) $FLAG_NEXT_STRING_IS = - 1;

                    break;
            }

            if ($openBracesCount > 1) $comments = '';
        }

        return $classInfo;
    }

    /**
     * Return given class full file path
     *
     * @param
     *            $clazz
     * @return String
     */
    public static function getClassFile($clazz): String
    {
        return FileUtils::standarizePath((new ReflectionClass($clazz))->getFileName());
    }

    /**
     * Return a given method specific parameter type
     *
     * @param
     *            $clazz
     * @param String $method
     * @param int $parameterIndex
     * @throws InvalidArgumentException
     * @return String
     */
    public static function getMethodParameterType($clazz, String $method, int $parameterIndex): String
    {
        $class = new ReflectionClass($clazz);
        $method = $class->getMethod($method);
        $params = $method->getParameters();

        if (count($params) <= $parameterIndex)
        {
            throw new InvalidArgumentException('Index out of bound');
        }

        return $params[$parameterIndex]->getType();
    }

    public static function getMethodAnnotation($clazz, String $method, $annotationClazz): ?Annotation
    {
        $classInfo = self::parseClass($clazz);
        $classMethod = ClassInfo::castToClassMethod(Arrays::search($classInfo->getMethods(), $method, 'name'));

        return self::getAnnotation($classMethod, $annotationClazz);
    }

    public static function getAnnotation(ClassMethod $classMethod, $annotationClazz): ?Annotation
    {
        if ($classMethod == null) return null;

        foreach ($classMethod->getAnnotations() as $annotation)
        {
            if ($annotation instanceof $annotationClazz) return $annotation;
        }

        return null;
    }

    /**
     * Return a given class constants
     *
     * @param
     *            $clazz
     * @return array
     */
    public static function getConstants($clazz): array
    {
        return (new ReflectionClass($clazz))->getConstants();
    }

    public static function extractMethodParameterType($parameter): String
    {
        $type = trim(Strings::replace('$' . $parameter->name, '', Strings::getBetween($parameter, '>', ']', false)));
        $type = trim(Strings::getUpTo($type, '='));

        return explode(' ', $type)[0];
    }

    public static function getBeanClassNameFromFile(String $fpath): ?String
    {
        $clazz = static::getClassFromFile($fpath);
        if ($clazz == null) return null;

        return static::implements($clazz, IBean::class) ? $clazz : null;
    }

    public static function getClassFromFile(String $fpath): ?String
    {
        $src = file_get_contents($fpath);

        $namespace = Strings::getBetween($src, 'namespace ', ';', false);
        $clazzRow = Strings::getBetween($src, 'class ', '{', false);

        if (Strings::isEmpty($clazzRow)) return null;

        $clazzTokens = explode(' ', $clazzRow);
        $clazzName = $clazzTokens[0];

        return $namespace . '\\' . $clazzName;
    }
}