<?php

/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Manager;

use Openspring\SpringphpFramework\Type\BaseError;

/**
 * This class manage errors globally over the working project
 *
 * @author Khalid ELABBADI
 * @email khalid.elabbadii@gmail.com
 *
 */
class ErrorManager
{
	private static $errors = array();
	
	public static function addError(BaseError $error): void
	{
		static::$errors[] = $error;
	}
	
	public static function addSimpleError(String $message, int $code = 0): void
	{
		static::$errors[] = new BaseError($message, $code);
	}

	public static function setErrors(Array $errors): void
	{
		static::$errors = $errors;
	}
	
	public static function getError(int $index): ?BaseError
	{
		if (!isset(static::$errors[$index])) return null;
		
		return static::$errors[$index];
	}
	
	public static function getErrors(): array
	{
		return static::$errors;
	}

	public static function getLastError(): ?BaseError
	{
		$count = count(static::getErrors());
		
		if ($count == 0) return null;
				  
		return static::getError($count - 1);
	}
	
	public static function getMessages(): String
	{
		$messages = array();
		foreach(static::getErrors() as $error)
		{
			$messages[] = $error->getMessage();
		}

		return implode(PHP_EOL, $messages);
	}

	public static function getLastMessage(): String
	{
		$lastError = static::getLastError();
		return ($lastError === null) ? '' : $lastError->getMessage();
	}
	
	public static function hasErrors(): bool
	{
		return (count(static::getErrors()) > 0);
	}
	
	public static function clean(): void
	{
		static::$errors = array();
	}
}