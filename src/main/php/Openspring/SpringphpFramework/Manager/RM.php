<?php

/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Manager;

use Openspring\SpringphpFramework\Context\ApplicationContext;
use Openspring\SpringphpFramework\Core\Initializable;
use Openspring\SpringphpFramework\Core\Bean\ResourceBundleMessageSource;

class RM implements Initializable
{
    private static $resourceMessagesBean;
    
    public static function initialize(): void
    {
        self::$resourceMessagesBean = ApplicationContext::getBeanOf(ResourceBundleMessageSource::class);
    }
    
    public static function translate(?String $messageCode)
    {
        if (static::getResourceBundleMessageSource() == NULL) return $messageCode;
        
        return self::getResourceBundleMessageSource()->getMessage($messageCode);
    }
        
    private static function getResourceBundleMessageSource(): ?ResourceBundleMessageSource
    {
        return self::$resourceMessagesBean;
    }
}