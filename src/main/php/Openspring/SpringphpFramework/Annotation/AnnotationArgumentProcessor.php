<?php

namespace Openspring\SpringphpFramework\Annotation;

use Openspring\SpringphpFramework\Utils\EncryptionUtils;
use Openspring\SpringphpFramework\Utils\Strings;
use Openspring\SpringphpFramework\Type\Any;

class AnnotationArgumentProcessor
{
    protected $dquotesId;
    protected $argument;

    public function __construct(String $argument)
    {
        $this->dquotesId = EncryptionUtils::newGuid();
        $this->argument = Strings::replace('\"', $this->dquotesId, $argument);
    }
    
    public function decodeJson(bool $assoc=null)
    {
        if (Strings::isEmpty($this->argument)) return "";
        if (is_bool($this->argument)) return $this->argument;
        if (is_numeric($this->argument)) return $this->argument;
        
        // try to decode
        $jobject = json_decode($this->argument, $assoc);
        if ($jobject !== NULL) return $jobject;
        
        $jobject = new Any();
        $stringMatches = $this->extractStringArguments();
        $arrayMatches = $this->extractArrayArguments();
        
        foreach ($stringMatches as $name => $value)
        {
            $jobject->$name = Strings::replace($this->dquotesId, '"', $value);
        }
        
        foreach ($arrayMatches as $name => $value)
        {
            $jobject->$name = Strings::replace($this->dquotesId, '"', $value);
        }
        
        return ($assoc === true) ? (array) $jobject : $jobject;
    }
    
    public function encodeJson()
    {
        $jobject = $this->decodeJson();
        return json_encode($jobject);
    }

    protected function extractStringArguments()
    {
        $args = [];
        $matches = [];
        $pattern = '/([a-zA-Z]+)[\s]*=[\s]*"([^"]*)"/';
        preg_match_all($pattern, $this->argument, $matches, PREG_SET_ORDER, 0);
        
        foreach ($matches as $match)
        {
            $args[$match[1]] = $match[2];
        }

        return $args;
    }

    protected function extractArrayArguments()
    {
        $args = [];
        $matches = [];
        $pattern = '/([a-zA-Z]+)[\s]*=[\s]*(\[[^\[]*\])/';
        preg_match_all($pattern, $this->argument, $matches, PREG_SET_ORDER, 0);
        
        foreach ($matches as $match)
        {
            $args[$match[1]] = $match[2];
        }
        
        return $args;
    }
}