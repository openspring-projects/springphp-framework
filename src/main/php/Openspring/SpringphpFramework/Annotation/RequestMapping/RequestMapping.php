<?php
/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Annotation\RequestMapping;

use Openspring\SpringphpFramework\Annotation\Rest\Mapping;
use Openspring\SpringphpFramework\Annotation\Rest\MappingAnnotation;

class RequestMapping extends Mapping implements MappingAnnotation
{
    public function __construct(?String $doc = null, String $modifier = 'public')
    {
        parent::__construct($doc, $modifier, 'RequestMapping');
    }

    public function setMethod(Array $method): void
    {
        $this->method = $method;
    }

    public function getMethod(): array
    {
        return $this->method;
    }

    public function castToRequestMapping(): RequestMapping
    {
        return $this;
    }
}