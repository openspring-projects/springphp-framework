<?php

/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Core\Stereotype;

use Openspring\SpringphpFramework\Annotation\AnnotationDefiner;
use Openspring\SpringphpFramework\Annotation\ValidatorAnnotationDefiner;
use Openspring\SpringphpFramework\Enumeration\ObjectType;
use Openspring\SpringphpFramework\Utils\Arrays;
use Openspring\SpringphpFramework\Validation\Validator\Complex\MaxConstraint;
use Openspring\SpringphpFramework\Validation\Validator\Complex\MinConstraint;
use Openspring\SpringphpFramework\Validation\Validator\Complex\RegexConstraint;
use Openspring\SpringphpFramework\Validation\Validator\Complex\SizeConstraint;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Bool\AssertBoolConstraint;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Bool\AssertFalseConstraint;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Bool\AssertTrueConstraint;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Date\DateConstraint;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Date\FutureConstraint;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Date\FutureOrPresentConstraint;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Date\PastConstraint;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Date\PastOrPresentConstraint;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Mixed\EmailConstraint;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Mixed\NotBlankConstraint;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Mixed\NotEmptyConstraint;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Mixed\NotNullConstraint;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Mixed\URLConstraint;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Number\NegativeConstraint;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Number\NegativeOrZeroConstraint;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Number\PositiveConstraint;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Number\PositiveOrZeroConstraint;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Number\AssertNumberConstraint;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Mixed\InArrayConstraint;

/**
 * Supported Annotations
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
class Annotations
{
    private static $valAnnots = null;

    public static function getAllAnnotations(): array
    {
        return array_merge(static::getClassAnnotations(), static::getValidationAnnotations(), static::getFieldAnnotations(), static::getMethodAnnotations());
    }

    public static function getFieldAnnotations(): array
    {
        $targetString = array(
            ObjectType::STRING);

        $annots['Autowired'] = new AnnotationDefiner('@Autowired(', ')', array(), array(), 1);
        $annots['Value'] = new AnnotationDefiner('@Value(', ')', $targetString, array());
        $annots['Var'] = new AnnotationDefiner('@var ', PHP_EOL, $targetString, array(), 1);

        return $annots;
    }

    public static function getValidationAnnotation(String $key): ValidatorAnnotationDefiner
    {
        $valAnnots = static::getValAnnots();

        return $valAnnots[$key];
    }

    public static function getValidationAnnotations(array $pick = array()): array
    {
        $valAnnots = static::getValAnnots();

        $out = $valAnnots;
        if (count($pick) > 0)
        {
            $out = array();
            foreach ($valAnnots as $key => $annot)
            {
                if (! Arrays::valueExists($key, $pick)) continue;
                $vclazz = $annot->validator;
                $annot->validator = new $vclazz();
                $out[$key] = $annot;
            }
        }
        else
        {
            foreach ($out as $annot)
            {
                $vclazz = $annot->validator;
                $annot->validator = new $vclazz();
            }
        }

        return $out;
    }

    public static function getClassAnnotations(): array
    {
        $annots = array();
        $classTarget = array(
            ObjectType::CLAZZ);
        $csConfig = array(
            'baseDirs' => ObjectType::ARRAY);

        $annots['ComponentScan'] = new AnnotationDefiner('@ComponentScan(', ')', $classTarget, $csConfig);
        $annots['PropertySource'] = new AnnotationDefiner('@PropertySource(', ')', $classTarget, array(), 1);

        return $annots;
    }

    public static function getMethodAnnotations(): array
    {
        return array();
    }

    protected static function getValAnnots(): array
    {
        if (static::$valAnnots == null)
        {
            static::$valAnnots = array();
            
            // configs
            $simpleConfig = array('message' => ObjectType::STRING);
            $dateConfig = array('format' => ObjectType::STRING, 'message' => ObjectType::STRING);
            $minMaxConfig = array('value' => ObjectType::INTEGER, 'message' => ObjectType::STRING);
            $sizeConfig = array('min' => ObjectType::INTEGER,'max' => ObjectType::INTEGER,'message' => ObjectType::STRING);
            $regexConfig = array('pattern' => ObjectType::STRING,'message' => ObjectType::STRING);
            $inArrayConfig = array('values' => ObjectType::STRING,'message' => ObjectType::STRING);

            // targets
            $anyTarget = ObjectType::toArray();
            $stringTarget = array(ObjectType::STRING);
            $stringNbrBoolTarget = array(ObjectType::BOOLEAN, ObjectType::DOUBLE, ObjectType::INTEGER, ObjectType::FLOAT, ObjectType::STRING);
            $intTarget = array(ObjectType::INTEGER);
            $boolTarget = array(ObjectType::BOOLEAN);
            $arrayTarget = array(ObjectType::ARRAY);
            $stringArrayTarget = array(ObjectType::STRING, ObjectType::ARRAY);

            static::$valAnnots['InArray'] = new ValidatorAnnotationDefiner('@InArray(', ')', InArrayConstraint::class, $stringNbrBoolTarget, $inArrayConfig);
            static::$valAnnots['Regex'] = new ValidatorAnnotationDefiner('@Regex(', ')', RegexConstraint::class, $stringTarget, $regexConfig);
            static::$valAnnots['Size'] = new ValidatorAnnotationDefiner('@Size(', ')', SizeConstraint::class, $stringArrayTarget, $sizeConfig);
            static::$valAnnots['Min'] = new ValidatorAnnotationDefiner('@Min(', ')', MinConstraint::class, $intTarget, $minMaxConfig);
            static::$valAnnots['Max'] = new ValidatorAnnotationDefiner('@Max(', ')', MaxConstraint::class, $intTarget, $minMaxConfig);

            static::$valAnnots['NotNull'] = new ValidatorAnnotationDefiner('@NotNull(', ')', NotNullConstraint::class, $anyTarget, $simpleConfig);
            static::$valAnnots['NotEmpty'] = new ValidatorAnnotationDefiner('@NotEmpty(', ')', NotEmptyConstraint::class, $stringArrayTarget, $simpleConfig);
            static::$valAnnots['NotBlank'] = new ValidatorAnnotationDefiner('@NotBlank(', ')', NotBlankConstraint::class, $stringTarget, $simpleConfig);
            static::$valAnnots['Email'] = new ValidatorAnnotationDefiner('@Email(', ')', EmailConstraint::class, $stringTarget, $simpleConfig);
            static::$valAnnots['URL'] = new ValidatorAnnotationDefiner('@URL(', ')', URLConstraint::class, $stringTarget, $simpleConfig);
            static::$valAnnots['AssertBool'] = new ValidatorAnnotationDefiner('@AssertBool(', ')', AssertBoolConstraint::class, $boolTarget, $simpleConfig);
            static::$valAnnots['AssertArray'] = new ValidatorAnnotationDefiner('@AssertArray(', ')', AssertBoolConstraint::class, $arrayTarget, $simpleConfig);
            static::$valAnnots['AssertTrue'] = new ValidatorAnnotationDefiner('@AssertTrue(', ')', AssertTrueConstraint::class, $boolTarget, $simpleConfig);
            static::$valAnnots['AssertFalse'] = new ValidatorAnnotationDefiner('@AssertFalse(', ')', AssertFalseConstraint::class, $boolTarget, $simpleConfig);
            static::$valAnnots['AssertNumber'] = new ValidatorAnnotationDefiner('@AssertNumber(', ')', AssertNumberConstraint::class, $intTarget, $simpleConfig);
            static::$valAnnots['Positive'] = new ValidatorAnnotationDefiner('@Positive(', ')', PositiveConstraint::class, $intTarget, $simpleConfig);
            static::$valAnnots['PositiveOrZero'] = new ValidatorAnnotationDefiner('@PositiveOrZero(', ')', PositiveOrZeroConstraint::class, $intTarget, $simpleConfig);
            static::$valAnnots['Negative'] = new ValidatorAnnotationDefiner('@Negative(', ')', NegativeConstraint::class, $intTarget, $simpleConfig);
            static::$valAnnots['NegativeOrZero'] = new ValidatorAnnotationDefiner('@NegativeOrZero(', ')', NegativeOrZeroConstraint::class, $intTarget, $simpleConfig);
            static::$valAnnots['Date'] = new ValidatorAnnotationDefiner('@Date(', ')', DateConstraint::class, $stringTarget, $dateConfig);
            static::$valAnnots['Past'] = new ValidatorAnnotationDefiner('@Past(', ')', PastConstraint::class, $stringTarget, $dateConfig);
            static::$valAnnots['PastOrPresent'] = new ValidatorAnnotationDefiner('@PastOrPresent(', ')', PastOrPresentConstraint::class, $stringTarget, $dateConfig);
            static::$valAnnots['Future'] = new ValidatorAnnotationDefiner('@Future(', ')', FutureConstraint::class, $stringTarget, $dateConfig);
            static::$valAnnots['FutureOrPresent'] = new ValidatorAnnotationDefiner('@FutureOrPresent(', ')', FutureOrPresentConstraint::class, $stringTarget, $dateConfig);
        }

        return static::$valAnnots;
    }
}