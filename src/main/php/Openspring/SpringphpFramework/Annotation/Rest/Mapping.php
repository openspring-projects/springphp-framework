<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Annotation\Rest;

use Openspring\SpringphpFramework\Annotation\Annotation;
use Openspring\SpringphpFramework\Annotation\AnnotationAttribute;
use Openspring\SpringphpFramework\Annotation\ElementType;
use Openspring\SpringphpFramework\Enumeration\MediaType;
use Openspring\SpringphpFramework\Enumeration\ModifierType;
use Openspring\SpringphpFramework\Enumeration\ObjectType;
use Openspring\SpringphpFramework\Exception\Http\ForbiddenException;
use Openspring\SpringphpFramework\Http\HttpMethod;
use Openspring\SpringphpFramework\Http\Request;
use Openspring\SpringphpFramework\Type\Any;
use Openspring\SpringphpFramework\Type\ArrayList;
use Openspring\SpringphpFramework\Utils\AnnotationUtils;
use Openspring\SpringphpFramework\Utils\Cast;
use Openspring\SpringphpFramework\Utils\Strings;

abstract class Mapping implements Annotation
{
    private $annotationElement;

    /**
     * annotation attributes
     */
    private $value = NULL;
    private $method = array();
    private $produces = NULL;
    private $consumes = NULL;
    private $authorizedGroups = array();
    private $authorizedRoles = array();

    /**
     * class vars
     */
    private $name = '';
    private $modifier = NULL;
    private $doc = NULL;
    private $valid = true;
    private $values = array();

    public function __construct(?String $annotDoc = null, String $modifier, String $name = 'RequestMapping')
    {
        $this->name = $name;
        $this->doc = $annotDoc;
        $this->modifier = $modifier;

        if (! Strings::isEmpty($annotDoc))
        {
            $this->annotationElement = AnnotationUtils::getElement($annotDoc);
            $this->process();
        }
    }

    /**
     * Example of use:
     *
     * @RequestMapping(value = "/clients/{$client_id}", method = ["GET", "POST"], consumes = "application/json", "produces": "application/json", roleIn = "*", "groupIn": "*"})
     */
    public function process(): bool
    {
        $this->annotationElement->validate($this->getAttributes());

        // process rm options values
        $attribs = $this->annotationElement->getAttributes();
        foreach ($attribs as $attribute)
        {
            $annotAttrib = Cast::asAnnotationAttribute($attribute);
            $name = strtolower($annotAttrib->getName());

            switch ($name)
            {
                case 'value':
                    $this->setValue($annotAttrib->getValue());
                    break;

                case 'method':
                    $this->setMethod($annotAttrib->getValue());
                    break;

                case 'consumes':
                    $this->setConsumes($annotAttrib->getValue());
                    break;

                case 'produces':
                    $this->setProduces($annotAttrib->getValue());
                    break;

                case 'groupin':
                    $this->setAuthorizedGroups($annotAttrib->getValue());
                    break;

                case 'rolesin':
                    $this->setAuthorizedRoles($annotAttrib->getValue());
                    break;
            }

            if (! $this->isValid())
            {
                return false;
            }
        }

        return true;
    }

    protected function getRightDoc(String $doc): String
    {
        $nsDoc = strtolower(Strings::replace(' ', '', $doc));
        if (! Strings::contain($nsDoc, 'value=')) return $doc;

        $doc = Strings::ireplaceAll($doc, array(
                                                'value' => '"value"',
                                                'consumes' => '"consumes"',
                                                'produces' => '"produces"',
                                                'groupin' => '"groupin"',
                                                'rolesin' => '"rolesin"',
                                                '=' => ':'));
        $docArgs = Strings::getBetween($doc, '(', ')', false);

        return Strings::replace($docArgs, '{' . $docArgs . '}', $doc);
    }

    public function getName(bool $atted = true): String
    {
        return ($atted) ? '@' . $this->name : $this->name;
    }

    public function setModifier(?String $modifier): void
    {
        $this->modifier = $modifier;
    }

    public function getModifier(): ?String
    {
        return $this->modifier;
    }

    public function setDoc(?String $doc): void
    {
        $this->doc = $doc;
    }

    public function getDoc(): String
    {
        return $this->doc;
    }

    public function isGranted()
    {
        if (! $this->isValid())
        {
            return false;
        }

        if ($this->modifier !== ModifierType::PUBLIC_MOD)
        {
            throw new ForbiddenException('Action is not visible');
        }

        $valid = in_array(Request::getMethod(), $this->getMethod());

        return $valid;
    }

    public function setValue($pvalue)
    {
        $this->value = $pvalue;
        $values = explode('/', $this->value);
        $i = 1;

        if (count($values) > 0) unset($values[0]);

        // convert
        foreach ($values as $value)
        {
            if (Strings::bornedWith(trim($value), '{', '}'))
            {
                $param = Strings::removeAll(trim($value), array(
                                                                '{',
                                                                '}'));
                $this->values[] = array(
                                        'isParam' => true,
                                        'param' => Strings::replace('$', '', $param),
                                        //'value' => '([^\/]+)',
                                        'value' => '$' . $param,
                                        'var' => '$' . $i);

                $i ++;
            }
            else
            {
                $this->values[] = array(
                                        'isParam' => false,
                                        'param' => trim($value),
                                        'value' => trim($value));
            }
        }
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getValues()
    {
        return $this->values;
    }

    public function getMappingItem(Array $methods, Array $authGroups, Array $authRoles, String $clazz, String $wcomp, String $action, String $contextRoot = null, $margs = array()): MappingItem
    {
        $fromURL = '';
        foreach ($this->values as $value)
        {
            $fromURL .= $value['value'] . '/';
        }

        $fromURL = Strings::removeLastChars($fromURL);
        $fromURL = Strings::replaceAll($fromURL, array('^/' => '^'));

        return new MappingItem($methods, $authGroups, $authRoles, $clazz, $fromURL, $wcomp, $action, $this->values, $margs);
    }

    public function setConsumes($consumes)
    {
        $this->consumes = strtolower($consumes);
    }

    public function getConsumes()
    {
        return $this->consumes;
    }

    public function setProduces($produces)
    {
        $this->produces = strtolower($produces);
    }

    public function getProduces()
    {
        return $this->produces;
    }

    public function addAuthorizedGroup(?String $groupInValue)
    {
        array_push($this->authorizedGroups, $groupInValue);
    }

    public function setAuthorizedGroups(?String $groupInValue)
    {
        $this->authorizedGroups = explode(',', $groupInValue);
    }

    public function getAuthorizedGroups(): array
    {
        return $this->authorizedGroups;
    }

    public function addAuthorizedRole(?String $authorizedRole)
    {
        array_push($this->authorizedRoles, $authorizedRole);
    }

    public function setAuthorizedRoles(?String $authorizedRoles)
    {
        $this->authorizedRoles = explode(',', $authorizedRoles);
    }

    public function getAuthorizedRoles(): array
    {
        return $this->authorizedRoles;
    }

    public function isValid(): bool
    {
        return $this->valid;
    }

    public function getElementType(): String
    {
        return ElementType::METHOD;
    }

    public function getAttributes(): ArrayList
    {
        $attribs = new ArrayList();

        $attribs->add(new AnnotationAttribute('value', null, ObjectType::STRING));
        $attribs->add(new AnnotationAttribute('method', HttpMethod::toArray(), ObjectType::ARRAY));
        $attribs->add(new AnnotationAttribute('consumes', MediaType::toArray(), ObjectType::STRING));
        $attribs->add(new AnnotationAttribute('produces', MediaType::toArray(), ObjectType::STRING));
        $attribs->add(new AnnotationAttribute('groupin', null, ObjectType::STRING, false));
        $attribs->add(new AnnotationAttribute('rolesin', null, ObjectType::STRING, false));

        return $attribs;
    }

    public function toSimpleObject()
    {
        $out = new Any();

        $out->name = $this->getName(false);
        $out->value = $this->value;
        $out->values = $this->values;
        $out->method = $this->getMethod();
        $out->produces = $this->produces;
        $out->consumes = $this->consumes;
        $out->authorizedGroups = $this->authorizedGroups;
        $out->authorizedRoles = $this->authorizedRoles;

        return $out;
    }
}