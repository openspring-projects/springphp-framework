<?php

/*
 * Copyright 2002-2019 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace Openspring\SpringphpFramework\Annotation\Rest;

use Openspring\SpringphpFramework\Web\PathVariable;
use JsonSerializable;
use phpDocumentor\Reflection\Types\String_;
use Openspring\SpringphpFramework\Utils\Strings;

class MappingItem implements JsonSerializable
{
    public $clazz = '';
    public $path = '';
    public $pathVariables = array();
    public $wcomp = '';
    public $action = '';
    public $methods = array();
    public $authGroups = array();
    public $authRoles = array();

    public function __construct(Array $methods, Array $authGroups, Array $authRoles, String $clazz, String $path, String $wcomp, String $action, Array $pathVariables, $margs = array())
    {
        $this->clazz = $clazz;
        $this->path = $path;
        $this->wcomp = $wcomp;
        $this->action = $action;
        $this->methods = $methods;
        $this->authGroups = $authGroups;
        $this->authRoles = $authRoles;

        $i = 1;
        foreach ($pathVariables as $pathVariable)
        {
            if ($pathVariable['isParam'])
            {
                $this->pathVariables[] = new PathVariable($pathVariable['param'], $this->getParamType($margs, $pathVariable['param']), $i);
            }

            $i ++;
        }
    }
    
    protected function getParamType(Array $margs, String $param): String
    {
        $defaultType = 'String';
        
        foreach($margs as $marg)
        {
            if ($marg->getName() == '$' . $param)
            {
                return (Strings::isEmpty($marg->getType())) ? $defaultType : $marg->getType();
            }
        }
        
        return $defaultType;
    }

    public static function New(): MappingItem
    {
        return new MappingItem(array(), array(), array(), '', '', '', '', array(), array());
    }

    public function getClazz(): String
    {
        return $this->clazz;
    }

    public function setClazz(String $clazz): void
    {
        $this->clazz = $clazz;
    }

    public function getPath(): String
    {
        return $this->path;
    }

    public function getWcomp(): String
    {
        return $this->wcomp;
    }

    public function getAction(): String
    {
        return $this->action;
    }

    public function setPath(String $path): void
    {
        $this->path = $path;
    }

    public function setWcomp(String $wcomp): void
    {
        $this->wcomp = $wcomp;
    }

    public function setAction(String $action): void
    {
        $this->action = $action;
    }

    public function getPathVariables(): array
    {
        return $this->pathVariables;
    }

    public function setPathVariables($pathVariables): void
    {
        $this->pathVariables = $pathVariables;
    }
    
    public function getMethods(): array
    {
        return $this->methods;
    }
    
    public function getAuthGroups(): array
    {
        return $this->authGroups;
    }
    
    public function getAuthRoles(): array
    {
        return $this->authRoles;
    }

    public function jsonSerialize()
    {
        return [
            $this->methods,
            $this->authGroups,
            $this->authRoles,
            $this->clazz,
            $this->path,
            $this->pathVariables,
            $this->wcomp,
            $this->action];
    }
}