<?php
/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Annotation;

use Openspring\SpringphpFramework\Exception\Annotation\InvalidAnnotationAttributeException;
use Openspring\SpringphpFramework\Exception\Annotation\InvalidAnnotationAttributeValueException;
use Openspring\SpringphpFramework\Exception\Annotation\RequiredAnnotationAttributeException;
use Openspring\SpringphpFramework\Exception\Annotation\UnknownAnnotationAttributeTypeException;
use Openspring\SpringphpFramework\Type\ArrayList;
use Openspring\SpringphpFramework\Utils\Arrays;
use Openspring\SpringphpFramework\Utils\Cast;

/**
 * Annotation element
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
class AnnotationElement
{
    private $name;
    private $attributes;

    public function __construct(String $name = null)
    {
        $this->setName($name);
        $this->attributes = new ArrayList();
    }

    public function getName(): String
    {
        return $this->name;
    }

    public function getAttributes(): ArrayList
    {
        return $this->attributes;
    }

    public function setName(String $name): void
    {
        $this->name = $name;
    }

    public function setAttributes(ArrayList $attributes): void
    {
        $this->attributes = $attributes;
    }

    public function addAttribute($attribute): void
    {
        $this->attributes->add($attribute);
    }

    public function validate(ArrayList $supportedAttribs): void
    {
        // check required attributes
        foreach ($supportedAttribs as $attribute)
        {
            if (! $attribute->isRequired()) continue;

            $found = Arrays::search($this->getAttributes()->toArray(), strtolower($attribute->getName()), 'name');
            if ($found == null)
            {
                throw new RequiredAnnotationAttributeException('Unknown attribute ' . $attribute->getName() . ' for annotation ' . $this->getName());
            }
        }

        // check data
        foreach ($this->getAttributes() as $attribute)
        {
            $found = Arrays::search($supportedAttribs->toArray(), strtolower($attribute->getName()), 'name');
            $supportedAttrib = Cast::asAnnotationAttribute($found);
            if ($supportedAttrib == null)
            {
                throw new InvalidAnnotationAttributeException('Unknown attribute ' . $attribute->getName() . ' for annotation ' . $this->getName());
            }

            // validate value
            switch ($supportedAttrib->getType())
            {
                case 'String':
                    if (! is_string($attribute->getValue()))
                    {
                        throw new InvalidAnnotationAttributeValueException('Invalid annotation attribute value: attribute ' . $attribute->getName() . ' expected String');
                    }
                    break;

                case 'Int':
                    if (! is_numeric($attribute->getValue()))
                    {
                        throw new InvalidAnnotationAttributeValueException('Invalid annotation attribute value: attribute ' . $attribute->getName() . ' expected Int');
                    }
                    break;

                case 'Array':
                    if (! is_array($attribute->getValue()))
                    {
                        throw new InvalidAnnotationAttributeValueException('Invalid annotation attribute value: attribute ' . $attribute->getName() . ' expected Array');
                    }
                    break;

                case 'Bool':
                    if (! is_array($attribute->getValue()))
                    {
                        throw new InvalidAnnotationAttributeValueException('Invalid annotation attribute value: attribute ' . $attribute->getName() . ' expected Bool');
                    }
                    break;

                default:
                    throw new UnknownAnnotationAttributeTypeException('Invalid annotation attribute value: attribute ' . $attribute->getName() . ' expected Bool');
            }

            // validate data
            $this->validateData($supportedAttrib, $attribute->getValue());
        }
    }

    private function validateData(AnnotationAttribute $annotationAttribute, $targetValue): void
    {
        $name = $annotationAttribute->getName();
        $value = $annotationAttribute->getValue();
        $type = $annotationAttribute->getValueType();

        if ($value == null) return;

        switch ($type)
        {
            case 'String':
                if ($targetValue != $value)
                {
                    throw new InvalidAnnotationAttributeValueException('Invalid annotation attribute value: attribute ' . $name . ' expected ' . $this->toString($value));
                }
                break;

            case 'Int':
                if ($targetValue != $value)
                {
                    throw new InvalidAnnotationAttributeValueException('Invalid annotation attribute value: attribute ' . $name . ' expected ' . $this->toString($value));
                }
                break;

            case 'Array':
                if (! Arrays::contains($this->getAsArray($targetValue), $value))
                {
                    throw new InvalidAnnotationAttributeValueException('Invalid annotation attribute value: attribute ' . $name . ' expected ' . $this->toString($value));
                }
                break;

            case 'Bool':
                if (! is_array($value == $targetValue))
                {
                    throw new InvalidAnnotationAttributeValueException('Invalid annotation attribute value: attribute ' . $name . ' expected ' . $this->toString($value));
                }
                break;
        }
    }

    private function getAsArray($value): array
    {
        if (is_array($value)) return $value;
        return array($value);
    }

    private function toString($value): String
    {
        return json_encode($value);
    }
}