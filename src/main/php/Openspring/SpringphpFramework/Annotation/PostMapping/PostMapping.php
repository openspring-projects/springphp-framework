<?php
/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Annotation\RequestMapping;

use Openspring\SpringphpFramework\Annotation\Annotation;
use Openspring\SpringphpFramework\Annotation\AnnotationAttribute;
use Openspring\SpringphpFramework\Annotation\Rest\Mapping;
use Openspring\SpringphpFramework\Annotation\Rest\MappingAnnotation;
use Openspring\SpringphpFramework\Enumeration\ObjectType;
use Openspring\SpringphpFramework\Enumeration\MediaType;
use Openspring\SpringphpFramework\Http\HttpMethod;
use Openspring\SpringphpFramework\Type\ArrayList;
use Openspring\SpringphpFramework\Utils\Strings;

class PostMapping extends Mapping implements Annotation, MappingAnnotation
{
    private $method = array();

    public function __construct(?String $doc = null, String $modifier)
    {
        parent::__construct($doc, $modifier, 'PostMapping');
        $this->setMethod(array(HttpMethod::POST));
    }

    public function setMethod(Array $method): void
    {
        $this->method = $method;
    }

    public function getMethod(): array
    {
        return $this->method;
    }

    public function getAttributes(): ArrayList
    {
        $attribs = new ArrayList();

        $attribs->add(new AnnotationAttribute('value', null, ObjectType::STRING));
        $attribs->add(new AnnotationAttribute('consumes', MediaType::toArray(), ObjectType::STRING));
        $attribs->add(new AnnotationAttribute('produces', MediaType::toArray(), ObjectType::STRING));
        $attribs->add(new AnnotationAttribute('groupin', null, ObjectType::STRING, false));
        $attribs->add(new AnnotationAttribute('rolesin', null, ObjectType::STRING, false));

        return $attribs;
    }

    public function castToRequestMapping(): RequestMapping
    {
        $doc = Strings::replace($this->getName(false), 'RequestMapping', $this->getDoc());
        $doc = $this->getRightDoc($doc);
        $doc = Strings::replace('"consumes"', '"method": ["' . HttpMethod::POST . '"], "consumes"', $doc);

        $requestMapping = new RequestMapping($doc, $this->getModifier());

        return $requestMapping;
    }
}