<?php
/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace  Openspring\SpringphpFramework\Annotation;

use Openspring\SpringphpFramework\Enumeration\ObjectType;

/**
 * Annotation attribute
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
class AnnotationAttribute
{
    private $name;
    private $value;
    private $type;
    private $isRequired = true;

    public function __construct(String $name, $value = '', $type = ObjectType::STRING, bool $isRequired = true)
    {
        $this->setName($name);
        $this->setValue($value);
        $this->setType($type);
        $this->setIsRequired($isRequired);
    }

    public function getName()
    {
        return $this->name;
    }

    public function hasValue(): bool
    {
        return ($this->value !== null);
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getValueType(): String
    {
        if ($this->value == null)
            return 'NULL';
        if (is_string($this->value))
            return ObjectType::STRING;
        if (is_array($this->value))
            return ObjectType::ARRAY;
        if (is_numeric($this->value))
            return ObjectType::INT;
        if (is_bool($this->value))
            return ObjectType::BOOL;

        return 'NULL';
    }

    public function getValueAsString(): String
    {
        return $this->value;
    }

    public function getValueAsArray(): array
    {
        return $this->value;
    }

    public function getValueAsInt(): int
    {
        return $this->value;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

    public function setType(String $type)
    {
        ObjectType::validate($type);

        return $this->type = $type;
    }

    public function getType(): String
    {
        return $this->type;
    }
    
    public function isRequired(): bool
    {
        return $this->isRequired;
    }
    
    public function setIsRequired(bool $isRequired): void
    {
        $this->isRequired = $isRequired;
    }

    public static function cast($annotationAttribute): AnnotationAttribute
    {
        return $annotationAttribute;
    }
}