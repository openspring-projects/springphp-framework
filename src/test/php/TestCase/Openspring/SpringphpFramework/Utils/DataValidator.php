<?php
namespace TestCase\Openspring\SpringphpFramework\Utils;

use Openspring\SpringphpFramework\Utils\DataValidator;
use PHPUnit\Framework\TestCase;

/**
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
class DataValidatorTest extends TestCase
{
    public function testIsPhoneNumber()
    {
        $this->assertTrue(DataValidator::isPhoneNumber('212662048370'));
        $this->assertTrue(DataValidator::isPhoneNumber('+212662048370'));
        $this->assertTrue(DataValidator::isPhoneNumber('0662048370'));
        $this->assertTrue(DataValidator::isPhoneNumber('06-620-483-70'));
        
        $this->assertFalse(DataValidator::isPhoneNumber('+21266204837000000000000000'));
        $this->assertFalse(DataValidator::isPhoneNumber('+21266'));
        $this->assertFalse(DataValidator::isPhoneNumber('+21266-c-204 8370'));
        $this->assertFalse(DataValidator::isPhoneNumber('+21266 204ab 8370'));
        $this->assertFalse(DataValidator::isPhoneNumber('abc'));
        $this->assertFalse(DataValidator::isPhoneNumber(''));
    }

    public function testIsEmail()
    {
        $this->assertTrue(DataValidator::isEmail('kha.elabbadi@gmail.com'));
        $this->assertTrue(DataValidator::isEmail('_kha_elabbadi@g.com'));
        $this->assertTrue(DataValidator::isEmail('kha.1255@gmail.com'));
        
        $this->assertFalse(DataValidator::isEmail('kha.elabbadi@gmail'));
        $this->assertFalse(DataValidator::isEmail('abc_212662048370000000000000_khalud_elabbadi_000000000000000012458454545@gmail.com'));
        $this->assertFalse(DataValidator::isEmail('@gmail.com'));
        $this->assertFalse(DataValidator::isEmail(' @gmail.com'));
        $this->assertFalse(DataValidator::isEmail('kha elabbadi@gmail.com'));
    }
}