<?php
namespace TestCase\Openspring\SpringphpFramework\Utils;

use Openspring\SpringphpFramework\Exception\IllegalArgumentException;
use Openspring\SpringphpFramework\Enumeration\ProfileType;
use Openspring\SpringphpFramework\Utils\Strings;
use PHPUnit\Framework\TestCase;

class StringsTest extends TestCase
{
    public function testGetUpTo()
    {
        $input = 'int = 0';
        
        $this->assertSame('int ', Strings::getUpTo($input, '='));
        $this->assertSame($input, Strings::getUpTo($input, '$'));
        $this->assertSame('', Strings::getUpTo($input, '$', false));
    }
    
    public function testGetBetween()
    {
        // case 1
        $input = 'Hi {name}++, how are you doing {day}';
        $this->assertSame('{name}', Strings::getBetween($input, '{', '}'));
        $this->assertSame('name', Strings::getBetween($input, '{', '}', false));
        
        // case 2
        $input = '@Var ClientDto++';
        $this->assertSame('ClientDto', Strings::getBetween($input, '@Var', '++', false));
        
        // case 3
        $input = '+ ClientDto+';
        $this->assertSame('ClientDto', Strings::getBetween($input, '+', '+', false));
        $this->assertSame(' ClientDto', Strings::getBetween($input, '+', '+', false, false));
        
        // case 4
        $input = '+ ClientDto+';
        $this->assertSame('ClientDto', Strings::getBetween($input, '+', '+', false));
        
        // case 5
        $input = 'abc + ClientDto+';
        $this->assertSame('ClientDto', Strings::getBetween($input, '+', '+', false));
        
        // case 6
        $input = '+ ClientDto+ cv';
        $this->assertSame('ClientDto', Strings::getBetween($input, '+', '+', false));
        
        // case 7
        $input = 'abc +*. ClientDto+ 3 fcd';
        $this->assertSame('ClientDto', Strings::getBetween($input, '+*.', '+ 3', false));
    }
    
    public function testContainMultipe()
    {
        $input = '{name},';
        
        $this->assertTrue(Strings::containMultiple($input, '{', '}', ','));
        $this->assertTrue(Strings::containMultiple($input, '{', '}', ','));
        $this->assertFalse(Strings::containMultiple($input, '{', '}', 'Name'));
        
        $this->expectException(IllegalArgumentException::class);
        $this->assertTrue(Strings::containMultiple($input));
    }
    
    public function testContainMultipeIgnoreCase()
    {
        $input = '{name},';
        
        $this->assertTrue(Strings::containMultipleIgnoreCase($input, '{', '}', ','));
        $this->assertTrue(Strings::containMultipleIgnoreCase($input, '{', '}', 'name'));
        $this->assertFalse(Strings::containMultipleIgnoreCase($input, '{', '}', ':'));
        $this->assertTrue(Strings::containMultipleIgnoreCase($input, '{', '}', 'Name'));
        
        $this->expectException(IllegalArgumentException::class);
        $this->assertTrue(Strings::containMultipleIgnoreCase($input));
    }
    
    public function testStrip()
    {
        $input = ' The quick brown fox jumps over the lazy dog ';
        $expect = 'Thequickbrownfoxjumpsoverthelazydog';
        $this->assertEquals($expect, Strings::stripSpace($input));
    }
    
    public function testClean()
    {
        $input = ' <b>ASDF</b> !@#$%^&*()_+"\';:>< ';
        $this->assertSame('ASDF !@#$%^&*()_+"\';:><', Strings::clean($input));
        $this->assertSame('asdf !@#$%^&*()_+\\"\\\';:><', Strings::clean($input, true, true));
    }
    
    public function testParseLines()
    {
        $this->assertSame(['asd'], Strings::parseLines('asd', false));
        $this->assertSame(['asd' => 'asd'], Strings::parseLines('asd', true));
        $this->assertSame(['asd' => 'asd'], Strings::parseLines('asd'));
        $lines = ['', false, 123, 456, ' 123   ', '      ', 'ASD', '0'];
        $this->assertSame([
            '123' => '123',
            '456' => '456',
            'ASD' => 'ASD',
            '0'   => '0',
        ], Strings::parseLines(implode("\r", $lines), true));
        $this->assertSame([
            '123' => '123',
            '456' => '456',
            'ASD' => 'ASD',
            '0'   => '0',
        ], Strings::parseLines(implode("\n", $lines), true));
        $this->assertSame([
            '123',
            '456',
            '123',
            'ASD',
            '0',
        ], Strings::parseLines(implode("\r\n", $lines), false));
    }
    
    public function testHtmlentities()
    {
        $this->assertEquals('One &amp; Two &lt;&gt; &amp;mdash;', Strings::htmlEnt('One & Two <> &mdash;'));
        $this->assertEquals('One &amp; Two &lt;&gt; &mdash;', Strings::htmlEnt('One &amp; Two <> &mdash;', true));
    }
    
    public function testUnique()
    {
        $this->assertEquals(15, strlen(Strings::unique()));
        $this->assertEquals(8, strlen(Strings::unique(null)));
        $this->assertEquals(10, strlen(Strings::unique('t-')));
        $this->assertNotSame(Strings::unique(), Strings::unique());
    }
    
    public function testRandom()
    {
        $this->assertEquals(10, strlen(Strings::random()));
        $this->assertEquals(10, strlen(Strings::random(10)));
        $this->assertEquals(10, strlen(Strings::random(10, true)));
        $this->assertEquals(10, strlen(Strings::random(10, false)));
        $this->assertNotSame(Strings::random(), Strings::random());
        $this->assertNotSame(Strings::random(), Strings::random());
        $this->assertNotSame(Strings::random(), Strings::random());
    }
    
    public function testZeroPad()
    {
        $this->assertEquals('0341', Strings::zeroPad('0341', 1));
        $this->assertEquals('341', Strings::zeroPad(341, 3));
        $this->assertEquals('0341', Strings::zeroPad(341, 4));
        $this->assertEquals('000341', Strings::zeroPad(341, 6));
    }
    
    public function testTruncateSafe()
    {
        $this->assertEquals('The quick brown fox...', Strings::truncateSafe('The quick brown fox jumps over the lazy dog', 24));
        $this->assertEquals('The quick brown fox jumps over the lazy dog',
            Strings::truncateSafe('The quick brown fox jumps over the lazy dog', 55));
        $this->assertEquals('Th...', Strings::truncateSafe('The quick brown fox jumps over the lazy dog', 2));
        $this->assertEquals('The...', Strings::truncateSafe('The quick brown fox jumps over the lazy dog', 3));
        $this->assertEquals('The...', Strings::truncateSafe('The quick brown fox jumps over the lazy dog', 7));
    }
    
    public function testLimitChars()
    {
        $this->assertEquals('The quick brown fox jump...', Strings::limitChars('The quick brown fox jumps over the lazy dog', 24));
        $this->assertEquals('The quick brown fox jumps over the lazy dog',
            Strings::limitChars('The quick brown fox jumps over the lazy dog', 55));
        $this->assertEquals('Th...', Strings::limitChars('The quick brown fox jumps over the lazy dog', 2));
        $this->assertEquals('The...', Strings::limitChars('The quick brown fox jumps over the lazy dog', 3));
        $this->assertEquals('The qui...', Strings::limitChars('The quick brown fox jumps over the lazy dog', 7));
        $this->assertEquals('The quick brown fox jumps over the lazy dog',
            Strings::limitChars('The quick brown fox jumps over the lazy dog', 150));
    }
    
    public function testLimitWords()
    {
        $this->assertEquals('The quick brown...', Strings::limitWords('The quick brown fox jumps over the lazy dog', 3));
        $this->assertEquals('The quick brown fox jumps...', Strings::limitWords('The quick brown fox jumps over the lazy dog', 5));
        $this->assertEquals('The...', Strings::limitWords('The quick brown fox jumps over the lazy dog', 1));
        $this->assertEquals('The quick brown fox jumps over the lazy dog',
            Strings::limitWords('The quick brown fox jumps over the lazy dog', 90));
        $this->assertEquals('The quick brown fox jumps over the...', Strings::limitWords('The quick brown fox jumps over the lazy dog', 7));
    }
    
    public function testLike()
    {
        $this->assertTrue(Strings::like('a', 'a'));
        $this->assertTrue(Strings::like('test/*', 'test/first/second'));
        $this->assertTrue(Strings::like('*/test', 'first/second/test'));
        $this->assertTrue(Strings::like('test', ProfileType::TEST, false));
        $this->assertFalse(Strings::like('a', ' a'));
        $this->assertFalse(Strings::like('first/', 'first/second/test'));
        $this->assertFalse(Strings::like('test', ProfileType::TEST));
        $this->assertFalse(Strings::like('/', '/something'));
    }
    
    public function testSlug()
    {
        $this->assertEquals('a-simple-title', Strings::toSafeURLString(' A simple     title '));
        $this->assertEquals('this-post-it-has-a-dash', Strings::toSafeURLString('This post -- it has a dash'));
        $this->assertEquals('123-1251251', Strings::toSafeURLString('123----1251251'));
        $this->assertEquals('one23-1251251', Strings::toSafeURLString('123----1251251', '-', true));
        $this->assertEquals('a-simple-title', Strings::toSafeURLString('A simple title', '-'));
        $this->assertEquals('this-post-it-has-a-dash', Strings::toSafeURLString('This post -- it has a dash', '-'));
        $this->assertEquals('123-1251251', Strings::toSafeURLString('123----1251251', '-'));
        $this->assertEquals('one23-1251251', Strings::toSafeURLString('123----1251251', '-', true));
        $this->assertEquals('a_simple_title', Strings::toSafeURLString('A simple title', '_'));
        $this->assertEquals('this_post_it_has_a_dash', Strings::toSafeURLString('This post -- it has a dash', '_'));
        $this->assertEquals('123_1251251', Strings::toSafeURLString('123----1251251', '_'));
        $this->assertEquals('one23_1251251', Strings::toSafeURLString('123----1251251', '_', true));
        // Blank seperator tests
        $this->assertEquals('asimpletitle', Strings::toSafeURLString('A simple title', ''));
        $this->assertEquals('thispostithasadash', Strings::toSafeURLString('This post -- it has a dash', ''));
        $this->assertEquals('1231251251', Strings::toSafeURLString('123----1251251', ''));
        $this->assertEquals('one231251251', Strings::toSafeURLString('123----1251251', '', true));
    }
    
    public function testEsc()
    {
        $this->assertSame(
            '&lt;a href="/test"&gt;Test !@#$%^&amp;*()_+\/&lt;/a&gt;',
            Strings::esc('<a href="/test">Test !@#$%^&*()_+\\/</a>')
            );
    }
    
    public function testEscXML()
    {
        $this->assertSame(
            '&lt;a href=&quot;/test&quot;&gt;Test!@#$%^&amp;*()_+\/&lt;/a&gt;',
            Strings::escXml('<a href="/test">Test!@#$%^&*()_+\\/</a>')
            );
    }
    
    public function testSplitCamelCase()
    {
        $this->assertSame('_', Strings::splitCamelCase('_'));
        $this->assertSame('word', Strings::splitCamelCase('word'));
        $this->assertSame('word_and_word', Strings::splitCamelCase('wordAndWord'));
        $this->assertSame('word_123_number', Strings::splitCamelCase('word123Number'));
        $this->assertSame('word number', Strings::splitCamelCase('wordNumber', ' '));
        $this->assertSame('word Number', Strings::splitCamelCase('wordNumber', ' ', false));
        $this->assertSame('word_Number', Strings::splitCamelCase('wordNumber', '_', false));
        $this->assertSame('Word_Number', Strings::splitCamelCase('WordNumber', '_', false));
    }
    
    public function testTestName2Human()
    {
        $this->assertSame('test', Strings::testName2Human('test'));
        $this->assertSame('testTest', Strings::testName2Human('testTest'));
        $this->assertSame('test_Test', Strings::testName2Human('test_Test'));
        $this->assertSame('test_test', Strings::testName2Human('test_test'));
        $this->assertSame('test test', Strings::testName2Human('test test'));
        $this->assertSame('test Test', Strings::testName2Human('test Test'));
        $this->assertSame('Function', Strings::testName2Human('testFunctionTest'));
        $this->assertSame('Function', Strings::testName2Human('testFunction_Test'));
        $this->assertSame('Function', Strings::testName2Human('Function_Test'));
        $this->assertSame('Function Trim', Strings::testName2Human('FunctionTrim_Test'));
        $this->assertSame('Function Trim', Strings::testName2Human('Function_Trim_Test'));
        $this->assertSame('Function Trim', Strings::testName2Human('Function_ Trim _Test'));
        $this->assertSame('Function Trim', Strings::testName2Human('Function _ Trim_ Test'));
        $this->assertSame('Function Trim', Strings::testName2Human('Function _ trim_ Test'));
        $this->assertSame('Function Trim', Strings::testName2Human('Function _Trim_ Test'));
        $this->assertSame('Function Trim', Strings::testName2Human('Function_trim_Test'));
        $this->assertSame('Function Trim', Strings::testName2Human('Function _trim_ Test'));
        $this->assertSame('Function Trim', Strings::testName2Human('Function_ trim _Test'));
        $this->assertSame('Function Trim', Strings::testName2Human('Function _ trim _ Test'));
        $this->assertSame('Function Trim', Strings::testName2Human('testFunction _ trim _ Test'));
        $this->assertSame('Function Trim', Strings::testName2Human('testfunction _ trim _ Test'));
        $this->assertSame('Function Trim', Strings::testName2Human('TestFunction _ trim _ Test'));
        $this->assertSame('Function Trim', Strings::testName2Human('Test_Function _ trim _ Test'));
        $this->assertSame('Function Trim', Strings::testName2Human('Test_ Function _ trim _ Test'));
        $this->assertSame('Function Trim', Strings::testName2Human('Test _ Function _ trim _ Test'));
        $this->assertSame('Function Trim', Strings::testName2Human('Test_ Function _ trim _ Test'));
        $this->assertSame('Function Test', Strings::testName2Human('Test_Function_test_Test'));
        $this->assertSame('Function Test', Strings::testName2Human('Test_Function_Test_Test'));
        $this->assertSame('Function JQuery', Strings::testName2Human('Test_FunctionJQuery_Test'));
        $this->assertSame('Function IE', Strings::testName2Human('Test_FunctionIE_Test'));
        $this->assertSame('Function IE Test', Strings::testName2Human('Test_FunctionIE_TestTest'));
        $this->assertSame('Test Function IE Test', Strings::testName2Human('Test_testFunctionIE_TestTest'));
        $this->assertSame('Function IE', Strings::testName2Human('\\JBZoo\\Test_FunctionIE_Test'));
        $this->assertSame('Function IE', Strings::testName2Human('\\JBZoo\\PHPHunit\\Test_FunctionIE_Test'));
        $this->assertSame('Function IE', Strings::testName2Human('\\JBZoo\\PHPHunit\\Some\\Test_FunctionIE_Test'));
        $this->assertSame('Function IE', Strings::testName2Human('\\JBZoo\\PHPHunit\\Some\\Some\\Test_FunctionIE_Test'));
    }
    
    public function testGenerateUUID()
    {
        $this->assertNotSame(Strings::uuid(), Strings::uuid());
        $this->assertNotSame(Strings::uuid(), Strings::uuid());
        $this->assertNotSame(Strings::uuid(), Strings::uuid());
    }
    
    public function testGetClassName()
    {
        $this->assertSame('JBZoo', Strings::getClassName('JBZoo'));
        $this->assertSame('JBZoo', Strings::getClassName('\JBZoo'));
        $this->assertSame('CCK', Strings::getClassName('\JBZoo\CCK'));
        $this->assertSame('Element', Strings::getClassName('\JBZoo\CCK\Element'));
        $this->assertSame('Repeatable', Strings::getClassName('\JBZoo\CCK\Element\Repeatable'));
        $this->assertSame('StringsTest', Strings::getClassName($this));
        $this->assertSame('StringsTest', Strings::getClassName($this, false));
        $this->assertSame('StringsTest', Strings::getClassName($this, false));
        $this->assertSame('stringstest', Strings::getClassName($this, true));
    }
    
    public function testInc()
    {
        $this->assertSame('title (2)', Strings::inc('title', null, 0));
        $this->assertSame('title(3)', Strings::inc('title(2)', null, 0));
        $this->assertSame('title-2', Strings::inc('title', 'dash', 0));
        $this->assertSame('title-3', Strings::inc('title-2', 'dash', 0));
        $this->assertSame('title (4)', Strings::inc('title', null, 4));
        $this->assertSame('title (2)', Strings::inc('title', 'foo', 0));
    }
    
    public function test()
    {
        $queries = Strings::splitSql('SELECT * FROM #__foo;SELECT * FROM #__bar;');
        $this->assertSame([
            'SELECT * FROM #__foo;',
            'SELECT * FROM #__bar;',
        ], $queries);
        $queries = Strings::splitSql('
            ALTER TABLE `#__redirect_links` DROP INDEX `idx_link_old`;
            -- Some comment
            ALTER TABLE `#__redirect_links` MODIFY `old_url` VARCHAR(2048) NOT NULL;
            -- Some comment
            -- Some comment --
            ALTER TABLE `#__redirect_links` MODIFY `new_url` VARCHAR(2048) NOT NULL;
            -- Some comment
            ALTER TABLE `#__redirect_links` MODIFY `referer` VARCHAR(2048) NOT NULL;
            
            ALTER TABLE `#__redirect_links` ADD INDEX `idx_old_url` (`old_url`(100));
        ');
        $this->assertSame([
            'ALTER TABLE `#__redirect_links` DROP INDEX `idx_link_old`;',
            'ALTER TABLE `#__redirect_links` MODIFY `old_url` VARCHAR(2048) NOT NULL;',
            'ALTER TABLE `#__redirect_links` MODIFY `new_url` VARCHAR(2048) NOT NULL;',
            'ALTER TABLE `#__redirect_links` MODIFY `referer` VARCHAR(2048) NOT NULL;',
            'ALTER TABLE `#__redirect_links` ADD INDEX `idx_old_url` (`old_url`(100));',
        ], $queries);
    }
}