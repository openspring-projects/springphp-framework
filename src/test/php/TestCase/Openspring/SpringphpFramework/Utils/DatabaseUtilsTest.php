<?php
namespace TestCase\Openspring\SpringphpFramework\Utils;

use Openspring\SpringphpFramework\Orm\Type\DataType;
use Openspring\SpringphpFramework\Utils\DatabaseUtils;
use PHPUnit\Framework\TestCase;


class DatabaseUtilsTest extends TestCase
{
    public function testCleanString()
    {
        $this->assertSame("spring\'php", DatabaseUtils::cleanString("spring'php"));
        $this->assertSame("&lt;b&gt;springphp&lt;/b&gt;", DatabaseUtils::cleanString("<b>springphp</b>", DataType::HTML));
        $this->assertSame("&lt;name&gt;springphp&lt;/name&gt;", DatabaseUtils::cleanString("<name>springphp</name>", DataType::XML));
    }

    public function testArrayToInOperator()
    {
        $this->assertSame("(1,2,3)", DatabaseUtils::arrayToInOperator(array(1,2,3)));
        $this->assertSame("('1','2','3')", DatabaseUtils::arrayToInOperator(array(1,2,3), true));
    }

    public function testGetEntityAlias()
    {
        $this->assertSame("_tbl864", DatabaseUtils::getEntityAlias('tbl_user'));
    }
}