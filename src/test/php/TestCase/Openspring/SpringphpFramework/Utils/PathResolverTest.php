<?php

namespace TestCase\Openspring\SpringphpFramework\Utils;

use Openspring\SpringphpFramework\Utils\PathResolver;
use PHPUnit\Framework\TestCase;

/**
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
class PathResolverTest extends TestCase
{
    public function testResolveRelativePath()
    {
        $expected = 'src/resources/application.properties';
        $path = 'file:src/resources/application.properties';
        $fpath = PathResolver::resolve($path);
        
        $this->assertEquals($expected, $fpath);
    }
    
    public function testResolveUserDirPath()
    {
        $expected = 'src/resources/application.properties';
        $path = 'file:${user.dir}/src/resources/application.properties';
        $fpath = PathResolver::resolve($path);
        
        $this->assertStringEndsWith($expected, $fpath);
    }
}