<?php
namespace TestCase\Openspring\SpringphpFramework\Utils;

use PHPUnit\Framework\TestCase;
use Openspring\SpringphpFramework\Utils\ClassUtils;
use TestCase\Openspring\SpringphpFramework\Helper\SunnyBean;
use TestCase\Openspring\SpringphpFramework\Helper\HunnyBean;

class ClassUtilsTest extends TestCase
{
    public function testGetConstructor()
    {
        $params = ClassUtils::getConstructorParameters(SunnyBean::class);
        $this->assertCount(0, $params);
        
        $params = ClassUtils::getConstructorParameters(HunnyBean::class);
        $this->assertCount(2, $params);
    }
    
    public function testGetGetterAndGetSetter()
    {
        $this->assertEquals('getName', ClassUtils::getGetter('name'));
        $this->assertEquals('setName', ClassUtils::getSetter('name'));
    }
}