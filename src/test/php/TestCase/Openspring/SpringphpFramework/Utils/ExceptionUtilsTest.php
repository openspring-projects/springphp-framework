<?php

namespace TestCase\Openspring\SpringphpFramework\Utils;

use PHPUnit\Framework\TestCase;
use Exception;
use Openspring\SpringphpFramework\Utils\ExceptionUtils;

class ExceptionUtilsTest extends TestCase
{
    public function testGetTraces()
    {
        $e = new Exception('oups');
        $traces = ExceptionUtils::getTraces($e);
        
        $expected = 'ExceptionUtilsTest.php:13 oups';
        
        $this->assertStringContainsString($expected, $traces);
    }
    
    public function testGetArgs()
    {
        $e = new Exception('oups');
        $trace = $e->getTrace()[3];
        $args = ExceptionUtils::getArgs($trace['args']);
        
        $expected = ' with 1 args (@object)';
        
        $this->assertEquals($expected, $args);
    }
}