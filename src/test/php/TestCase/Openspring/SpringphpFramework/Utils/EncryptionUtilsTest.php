<?php
namespace TestCase\Openspring\SpringphpFramework\Utils;

use Openspring\SpringphpFramework\Utils\EncryptionUtils;
use PHPUnit\Framework\TestCase;

class EncryptionUtilsTest extends TestCase
{
    public function testEncryptDecrypt()
    {
        $string = "Hello World";
        $encryptedString =  EncryptionUtils::encrypt($string);
        $decryptedString =  EncryptionUtils::decrypt($encryptedString);
        
        $this->assertNotSame($string, $encryptedString);
        $this->assertSame($string, $decryptedString);
    }

    public function testHashPassword()
    {
        $password = "Azerty.$.#.2019";
        $hashedPassword1 = EncryptionUtils::hashPassword($password);
        $hashedPassword2 = EncryptionUtils::hashPassword($password);
        
        $this->assertSame($hashedPassword1, $hashedPassword2);
    }
}