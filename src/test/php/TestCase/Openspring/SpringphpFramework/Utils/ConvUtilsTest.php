<?php
namespace TestCase\Openspring\SpringphpFramework\Utils;

use Openspring\SpringphpFramework\Utils\ConvUtils;
use PHPUnit\Framework\TestCase;

/**
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
class ConvUtilsTest extends TestCase
{
    public function testHtmlToUTF8()
    {
        $html = "<strong>khalid</strong>";
        $utf8 = ConvUtils::htmlToUTF8($html);

        $this->assertSame("&lt;strong&gt;khalid&lt;/strong&gt;", $utf8);
    }

    public function testToBool()
    {
        $this->assertTrue(ConvUtils::toBool("1"));
        $this->assertTrue(ConvUtils::toBool("yes"));
        $this->assertTrue(ConvUtils::toBool("true"));
        $this->assertTrue(ConvUtils::toBool("Yes"));
        $this->assertTrue(ConvUtils::toBool("True"));
        $this->assertTrue(ConvUtils::toBool("YES"));
        $this->assertTrue(ConvUtils::toBool("TRUE"));
        
        $this->assertFalse(ConvUtils::toBool("0"));
        $this->assertFalse(ConvUtils::toBool("no"));
        $this->assertFalse(ConvUtils::toBool("false"));
        $this->assertFalse(ConvUtils::toBool("No"));
        $this->assertFalse(ConvUtils::toBool("False"));
        $this->assertFalse(ConvUtils::toBool("NO"));
        $this->assertFalse(ConvUtils::toBool("FALSE"));
    }

    public function testBoolToInt()
    {
        $this->assertSame(1, ConvUtils::boolToInt(true));
        $this->assertSame(0, ConvUtils::boolToInt(false));
    }

    public function testStringToHexAndHexToString()
    {
        $istring = "springphp";
        $hex = ConvUtils::stringToHex($istring);
        $string = ConvUtils::hexToString($hex);
        
        $this->assertSame($istring, $string);
    }
}