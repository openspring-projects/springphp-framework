<?php

namespace TestCase\Openspring\SpringphpFramework\Utils;

use Openspring\SpringphpFramework\Type\Any;
use Openspring\SpringphpFramework\Utils\Arrays;
use PHPUnit\Framework\TestCase;

class ArraysTest extends TestCase
{
    public function testToObject()
    {
        $array = array("name" => "khalid", "age" => 36);
        $object = Arrays::toObject($array);
        
        $this->assertEquals('khalid', $object->name);
        $this->assertEquals(36, $object->age);
    }
    
    public function testGet()
    {
        $array = array("a" => "aa", "b" => "bb");
        $a = Arrays::get($array, "a", "a");
        $c = Arrays::get($array, "c", "c");
        
        $this->assertEquals('aa', $a);
        $this->assertEquals('c', $c);
    }
    
    public function testTrim()
    {
        $array1 = array("a", false, 0, null, "b", "");
        $result = Arrays::trim($array1);
        
        $this->assertCount(2, $result);
    }

	public function testIsEmpty()
	{
	    $array1 = array("a", false, 0, null, "b", "");
	    $array2 = array();
	    
	    $this->assertFalse(Arrays::isEmpty($array1));
	    $this->assertTrue(Arrays::isEmpty($array2));
	}

	public function testGetColumn()
	{
	    $item1 = new Any();
	    $item1->name = "khalid";
	    $item1->age = 36;
	    
	    $item2 = new Any();
	    $item2->name = "rachid";
	    $item2->age = 45;
	    
	    $array1 = array($item1, $item2);
	    
	    $nameColumnArray = Arrays::getColumn($array1, 'name');
	    
	    $this->assertCount(2, $nameColumnArray);
	    $this->assertSame("khalid", $nameColumnArray[0]);
	    $this->assertSame("rachid", $nameColumnArray[1]);
	}

	public function testGetIntersectionValues()
	{
	    $array1 = array("a", "b", "c", "d");
	    $array2 = array("c", "a", "d", "e");
	    
	    $result = Arrays::getIntersectionValues($array1, $array2);
	    
	    $this->assertCount(3, $result);
	    $this->assertSame("a", $result[0]);
	    $this->assertSame("c", $result[2]);
	    $this->assertSame("d", $result[3]);
	}

	public function testGetDiffValues($array1 = array(), $array2 = array())
	{
	    $array1 = array("a", "b", "c", "d");
	    $array2 = array("c", "a", "d", "e");
	    
	    $result = Arrays::getDiffValues($array1, $array2);
	    
	    $this->assertCount(1, $result);
	    $this->assertSame("b", $result[1]);
	}
	
	public function testUnique()
	{
	    $array = [10, 100, 1231, 10, 600, 20, 40, 1231, 20, 6, 1];
	    $this->assertSame([10, 100, 1231, 600, 20, 40, 6, 1], Arrays::unique($array));
	    $array = ['hello', 'world', 'this', 'is', 'a', 'test', 'hello', 'is', 'a', 'word'];
	    /** @noinspection ArgumentEqualsDefaultValueInspection */
	    $this->assertSame(['hello', 'world', 'this', 'is', 'a', 'test', 'word'], Arrays::unique($array, false));
	    $array = [
	        'asd_1' => 'asd',
	        'asd_2' => 'asd',
	    ];
	    $this->assertSame(['asd_1' => 'asd'], Arrays::unique($array, true));
	}
	
	public function testGetItem()
	{
	    $array = [];
	    $array['abc'] = 'def';
	    $array['nested'] = ['key1' => 'val1', 'key2' => 'val2', 'key3' => 'val3'];
	    // Looks for $array['abc']
	    /** @noinspection PhpParamsInspection */
	    $this->assertEquals('def', Arrays::getItem($array['abc']));
	    // Looks for $array['nested']['key2']
	    $this->assertEquals('val2', Arrays::getItem($array['nested']['key2']));
	    // Looks for $array['not-exist']
	    $this->assertEquals('default', Arrays::getItem($array['not-exist'], 'default'));
	}
	
	public function testFirst()
	{
	    $test = ['a' => ['a', 'b', 'c']];
	    $this->assertEquals('a', Arrays::firstItem(Arrays::getItem($test['a'])));
	}
	
	public function testFirstKey()
	{
	    $test = ['a' => ['a' => 'b', 'c' => 'd']];
	    $this->assertEquals('a', Arrays::firstKey(Arrays::getItem($test['a'])));
	}
	
	public function testLast()
	{
	    $test = ['a' => ['a', 'b', 'c']];
	    $this->assertEquals('c', Arrays::lastItem(Arrays::getItem($test['a'])));
	}
	
	public function testLastKey()
	{
	    $test = ['a' => ['a' => 'b', 'c' => 'd']];
	    $this->assertEquals('c', Arrays::lastKey(Arrays::getItem($test['a'])));
	}
	
	public function testFlatten()
	{
	    $input = [
	        'a',
	        'b',
	        'c',
	        'd',
	        [
	            'first'  => 'e',
	            'f',
	            'second' => 'g',
	            [
	                'h',
	                'third' => 'i',
	                [[[['j', 'k', 'l']]]],
	            ],
	        ],
	    ];
	    $expectNoKeys = range('a', 'l');
	    $expectWithKeys = [
	        'a',
	        'b',
	        'c',
	        'd',
	        'first'  => 'e',
	        'f',
	        'second' => 'g',
	        'h',
	        'third'  => 'i',
	        'j',
	        'k',
	        'l',
	    ];
	    $this->assertEquals($expectWithKeys, Arrays::flat($input));
	    $this->assertEquals($expectNoKeys, Arrays::flat($input, false));
	    /** @noinspection ArgumentEqualsDefaultValueInspection */
	    $this->assertEquals($expectWithKeys, Arrays::flat($input, true));
	}
	
	public function testSearch()
	{
	    $users = [
	        1 => (object)['username' => 'brandon', 'age' => 20],
	        2 => (object)['username' => 'matt', 'age' => 27],
	        3 => (object)['username' => 'jane', 'age' => 53],
	        4 => (object)['username' => 'john', 'age' => 41],
	        5 => (object)['username' => 'steve', 'age' => 11],
	        6 => (object)['username' => 'fred', 'age' => 42],
	        7 => (object)['username' => 'rasmus', 'age' => 21],
	        8 => (object)['username' => 'don', 'age' => 15],
	        9 => ['username' => 'darcy', 'age' => 33]
	    ];
	    $test = [
	        1 => 'brandon',
	        2 => 'devon',
	        3 => ['troy'],
	        4 => 'annie'
	    ];
	    
	    $this->assertNull(Arrays::search($test, 'bob'));
	    $this->assertEquals($test[3], Arrays::search($test, 'troy'));
	    $this->assertEquals($test[4], Arrays::search($test, 'annie'));
	    $this->assertEquals($test[2], Arrays::search($test, 'devon', 'devon'));
	    
	    $this->assertEquals($users[7], Arrays::search($users, 'rasmus', 'username'));
	    $this->assertEquals($users[9], Arrays::search($users, 'darcy', 'username'));
	    $this->assertEquals($users[1], Arrays::search($users, 'brandon'));
	}
	
	public function testMapDeep()
	{
	    $input = [
	        '<',
	        'abc',
	        '>',
	        'def',
	        ['&', 'test', '123'],
	        (object)['hey', '<>'],
	    ];
	    $expect = [
	        '&lt;',
	        'abc',
	        '&gt;',
	        'def',
	        ['&amp;', 'test', '123'],
	        (object)['hey', '<>'],
	    ];
	    $this->assertEquals($expect, Arrays::mapDeep($input, 'htmlentities'));
	}

	public function testIsAssoc()
	{
	    $this->assertFalse(Arrays::isAssoc(['a', 'b', 'c']));
	    $this->assertFalse(Arrays::isAssoc(['0' => 'a', '1' => 'b', '2' => 'c']));
	    $this->assertTrue(Arrays::isAssoc(['1' => 'a', '0' => 'b', '2' => 'c']));
	    $this->assertTrue(Arrays::isAssoc(['a' => 'a', 'b' => 'b', 'c' => 'c']));
	}
	
	public function testUnshiftAssoc()
	{
	    $array = ['a' => 1, 'b' => 2, 'c' => 3];
	    Arrays::unshiftAssoc($array, 'new', 0);
	    $this->assertSame($array, ['new' => 0, 'a' => 1, 'b' => 2, 'c' => 3]);
	    $array = ['a' => 1, 'b' => 2, 'c' => 3];
	    $newArray = Arrays::unshiftAssoc($array, 'new', 42);
	    $this->assertSame($newArray, ['new' => 42, 'a' => 1, 'b' => 2, 'c' => 3]);
	}
	
	public function testGetField()
	{
	    $array = [
	        ['name' => 'Bob', 'age' => 37],
	        ['name' => 'Fred', 'age' => 37],
	        ['name' => 'Jane', 'age' => 29],
	        ['name' => 'Brandon', 'age' => 20],
	        ['age' => 41],
	    ];
	    $this->assertSame([37, 37, 29, 20, 41], Arrays::getField($array, 'age'));
	    $array = [
	        (object)['name' => 'Bob', 'age' => 37],
	        (object)['name' => 'Fred', 'age' => 37],
	        (object)['name' => 'Jane', 'age' => 29],
	        (object)['name' => 'Brandon', 'age' => 20],
	        (object)['age' => 41],
	    ];
	    $this->assertSame(['Bob', 'Fred', 'Jane', 'Brandon'], Arrays::getField($array, 'name'));
	}
	
	public function testGroupByKey()
	{
	    $array = [
	        ['name' => 'Bob', 'age' => 37],
	        ['name' => 'Bob', 'age' => 66],
	        ['name' => 'Fred', 'age' => 20],
	        ['age' => 41],
	    ];
	    $this->assertSame([
	        'Bob'  => [
	            ['name' => 'Bob', 'age' => 37],
	            ['name' => 'Bob', 'age' => 66],
	        ],
	        'Fred' => [
	            ['name' => 'Fred', 'age' => 20],
	        ],
	    ], Arrays::groupByKey($array, 'name'));
	    $array = [
	        (object)['name' => 'Bob', 'age' => 37],
	        (object)['name' => 'Bob', 'age' => 66],
	        (object)['name' => 'Fred', 'age' => 20],
	        (object)['age' => 41],
	    ];
	    $this->assertEquals([
	        'Bob'  => [
	            (object)['name' => 'Bob', 'age' => 37],
	            (object)['name' => 'Bob', 'age' => 66],
	        ],
	        'Fred' => [
	            (object)['name' => 'Fred', 'age' => 20],
	        ],
	    ], Arrays::groupByKey($array, 'name'));
	}
	
	public function testMapRecursive()
	{
	    $array = [1, 2, 3, 4, 5];
	    $result = Arrays::map(function ($number) {
	        return ($number * $number);
	    }, $array);
	        $this->assertEquals([1, 4, 9, 16, 25], $result);
	        $array = [1, 2, 3, 4, 5, [6, 7, [8, [[[9]]]]]];
	        $result = Arrays::map(function ($number) {
	            return ($number * $number);
	        }, $array);
	            $this->assertEquals([1, 4, 9, 16, 25, [36, 49, [64, [[[81]]]]]], $result);
	}
	
	public function testSortByArray()
	{
	    $array = [
	        'address'   => '1',
	        'name'      => '2',
	        'dob'       => '3',
	        'no_sort_1' => '4',
	        'no_sort_2' => '5',
	    ];
	    $this->assertEquals([
	        'dob'       => '3',
	        'name'      => '2',
	        'address'   => '1',
	        'no_sort_1' => '4',
	        'no_sort_2' => '5',
	    ], Arrays::sortByArray($array, ['dob', 'name', 'address']));
	}
	
	public function testAddEachKey()
	{
	    $array = [1, 2, 3, 4, 5];
	    $this->assertSame([
	        'prefix_0' => 1,
	        'prefix_1' => 2,
	        'prefix_2' => 3,
	        'prefix_3' => 4,
	        'prefix_4' => 5,
	    ], Arrays::addEachKey($array, 'prefix_'));
	    $array = ['a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5];
	    $this->assertSame([
	        'prefix_a' => 1,
	        'prefix_b' => 2,
	        'prefix_c' => 3,
	        'prefix_d' => 4,
	        'prefix_e' => 5,
	    ], Arrays::addEachKey($array, 'prefix_'));
	}
	
	public function testToComment()
	{
	    $array = [
	        'Name' => 'Denis  ',
	        'Date' => 2015,
	    ];
	    $this->assertEquals('Name: Denis  ;' . PHP_EOL . 'Date: 2015;', Arrays::toComment($array));
	}

	public function testIsAttr()
	{
	    $array = [
	        'key'   => 'asd',
	        'null'  => null,
	        'false' => false,
	    ];
	    $this->assertTrue(Arrays::keyExists('key', $array));
	    $this->assertTrue(Arrays::keyExists('null', $array));
	    $this->assertTrue(Arrays::keyExists('false', $array));
	    $this->assertSame('asd', Arrays::keyExists('key', $array, true));
	    $this->assertSame(null, Arrays::keyExists('undefined', $array, true));
	    /** @noinspection ArgumentEqualsDefaultValueInspection */
	    $this->assertSame(true, Arrays::keyExists('key', $array, false));
	    /** @noinspection ArgumentEqualsDefaultValueInspection */
	    $this->assertSame(false, Arrays::keyExists('undefined', $array, false));
	    $this->assertFalse(Arrays::keyExists('undefined', $array));
	    $this->assertFalse(Arrays::keyExists('', $array));
	    $this->assertFalse(Arrays::keyExists(null, $array));
	    $this->assertFalse(Arrays::keyExists(false, $array));
	}
	
	public function testIn()
	{
	    $array = [
	        'key'         => 'asd',
	        'null'        => null,
	        'some-bool'   => false,
	        'some-string' => '1234567890098765432111111',
	        'some-int'    => 1111112345678900987654321,
	    ];
	    $this->assertFalse(Arrays::valueExists(0, $array));
	    $this->assertTrue(Arrays::valueExists(false, $array));
	    /** @noinspection ArgumentEqualsDefaultValueInspection */
	    $this->assertFalse(Arrays::valueExists(0, $array, false));
	    /** @noinspection ArgumentEqualsDefaultValueInspection */
	    $this->assertTrue(Arrays::valueExists(false, $array, false));
	    $this->assertSame('some-string', Arrays::valueExists('1234567890098765432111111', $array, true));
	    //$this->assertTrue('some-int', Arrays::in(1111112345678900987654321, $array, true));
	    //$this->assertTrue('some-bool', Arrays::in(false, $array, true));
	}
	
	public function testWrap()
	{
	    $this->assertEquals([], Arrays::wrap(null));
	    $this->assertEquals([1, 2, 3], Arrays::wrap([1, 2, 3]));
	    $this->assertEquals([0], Arrays::wrap(0));
	    $this->assertEquals([['key' => 'value']], Arrays::wrap(['key' => 'value']));
	}
	
	public function testImplodeNested()
	{
	    $this->assertSame('1,2,3', Arrays::implode(',', [1, 2, 3]));
	    $this->assertSame('123', Arrays::implode('', [1, 2, 3]));
	    $this->assertSame('1,2,3,4,5,6', Arrays::implode(',', [1, 2, 3, [4, 5, 6]]));
	    $this->assertSame('123456', Arrays::implode('', [1, 2, 3, [4, 5, 6]]));
	    $this->assertSame(
	        '1|||||||2|||||||3|||||||4|||||||5|||||||6|||||||7|||||||8|||||||9',
	        Arrays::implode('|||||||', [1, 2, 3, [4, 5, 6, [7, 8, 9]]])
	        );
	    $this->assertSame('1,2,3', Arrays::implode(',', ['key1' => 1, 'key2' => 2, 'key3' => 3]));
	}
	
	public function testExplode()
	{
	    $string = 'a:b:c';
	    $array = Arrays::explode(':', $string);
	    
	    $this->assertCount(3, $array);
	    $this->assertEquals('a', $array[0]);
	    $this->assertEquals('b', $array[1]);
	    $this->assertEquals('c', $array[2]);
	    
	    $array = Arrays::explode(':', $string, true);
	    
	    $this->assertCount(3, $array);
	    $this->assertEquals('a', $array[0]);
	    $this->assertEquals(':b', $array[1]);
	    $this->assertEquals(':c', $array[2]);
	}
	
	public function testContain()
	{
	    $trueFind = ['a', 'b', 'c'];
	    $falsefind = ['a', 'b', 'x'];
	    $array = ['a', 'b', 'c', 'd', 'e'];
	    
	    $this->assertTrue(Arrays::contains($trueFind, $array));
	    $this->assertFalse(Arrays::contains($falsefind, $array));
	}
}