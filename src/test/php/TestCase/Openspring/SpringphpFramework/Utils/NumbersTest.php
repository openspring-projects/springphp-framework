<?php

namespace TestCase\Openspring\SpringphpFramework\Utils;

use PHPUnit\Framework\TestCase;
use Openspring\SpringphpFramework\Utils\Numbers;

class NumbersTest extends TestCase
{
    public function testFormatDecimalGivenDecNbrThen2DecReturned()
    {
        $d = Numbers::formatDecimal("33.33333333", 2, ',');
        
        $this->assertEquals("33,33", $d);
    }
}