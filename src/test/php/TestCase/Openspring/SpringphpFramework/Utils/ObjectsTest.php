<?php
namespace TestCase\Openspring\SpringphpFramework\Utils;

use Openspring\SpringphpFramework\Type\Any;
use Openspring\SpringphpFramework\Utils\Objects;
use TestCase\Openspring\SpringphpFramework\Helper\UserDto;
use TestCase\Openspring\SpringphpFramework\Helper\UserEntity;
use PHPUnit\Framework\TestCase;

/**
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 *
 */
class ObjectsTest extends TestCase
{

    public function testIsNull()
    {
        $object = NULL;
        $this->assertTrue(Objects::isNull($object));

        $object = new Any();
        $this->assertFalse(Objects::isNull($object));
    }

    public function testIsNotNull()
    {
        $object = NULL;
        $this->assertFalse(Objects::isNotNull($object));

        $object = new Any();
        $this->assertTrue(Objects::isNotNull($object));
    }

    public function testObjectToArray()
    {
        $target = new Any();
        $target->name = 'khalid';
        $target->age = 36;
        $target->eyes = 'black';

        $array = Objects::objectToArray($target);

        $this->assertEquals('khalid', $array['name']);
        $this->assertEquals(36, $array['age']);
        $this->assertEquals('black', $array['eyes']);
    }

    public function testHasProperty()
    {
        $target = new Any();
        $target->name = 'khalid';
        $target->age = 36;
        $target->eyes = 'black';

        $this->assertTrue(Objects::hasProperty($target, 'name'));
        $this->assertFalse(Objects::hasProperty($target, 'fname'));
    }

    public function testHasAllProperties()
    {
        $target = new Any();
        $target->name = 'khalid';
        $target->age = 36;
        $target->eyes = 'black';

        $this->assertTrue(Objects::hasAllProperties($target, array('name','age','eyes')));
        $this->assertFalse(Objects::hasAllProperties($target, array('fname','age','eyes')));
    }

    public function testObjectAutoMap()
    {
        // source object
        $sourceItem = new Any();
        $sourceItem->name = 'khalid';
        $sourceItem->age = 36;
        $sourceItem->children = array('Assil');
        $sourceItem->wives = array('FZ');
        $sourceItem->father = new Any();
        $sourceItem->father->name = 'Mouhamed';
        $sourceItem->father->age = 85;
        $sourceItem->father->job = 'Ingeener';
        $sourceItem->userId = '100';

        // destination object
        $destItem = new Any();
        $destItem->name = '';
        $destItem->age = 0;
        $destItem->children = array();
        $destItem->father = new Any();
        $destItem->father->name = '';
        $destItem->father->age = 0;
        $destItem->userId = '0';

        Objects::autoMapObject($sourceItem, $destItem, array('userId' => 'userId'));

        $this->assertEquals($sourceItem->name, $destItem->name);
        $this->assertEquals($sourceItem->age, $destItem->age);

        $this->assertEquals($sourceItem->father->name, $destItem->father->name);
        $this->assertEquals($sourceItem->father->age, $destItem->father->age);

        $this->assertEquals($sourceItem->children, $destItem->children);
        
        $this->assertObjectNotHasAttribute('wives', $destItem);
        $this->assertObjectHasAttribute('job', $destItem->father);
        
        $this->assertEquals(100, $destItem->userId);
    }
    
    public function testArrayAutoMap()
    {
        $job1 = new Any();
        $job1->name= 'Ing';
        
        $job2 = new Any();
        $job2->name= 'Teacher';
        
        // source object
        $sourceItem = new Any();
        $sourceItem->name = 'khalid';
        $sourceItem->jobs = [$job1, $job2];
        
        // destination object
        $destItem = new Any();
        $destItem->name = '';
        $destItem->jobs = [];
        
        Objects::autoMapObject($sourceItem, $destItem);
        
        $this->assertEquals($sourceItem->name, $destItem->name);
        $this->assertCount(2, $destItem->jobs);
    }

    public function testAutomapWithEntity()
    {
        $userEntity = new UserEntity();

        $userEntity->setUserId('100');
        $userEntity->setGroupId(1);
        $userEntity->setLogin('khalid');

        $userDto = new UserDto();

        Objects::autoMapObject($userEntity->toSimpleObject(), $userDto);
        
        $this->assertEquals($userEntity->getUserId(), $userEntity->getUserId());
    }

    public function testGetProperty()
    {
        $objectItem = new Any();
        $objectItem->name = 'khalid';

        $this->assertEquals('khalid', Objects::getProperty($objectItem, 'name'));
    }

    public function testGetArrayProperty()
    {
        $array = array('name' => 'khalid');

        $this->assertEquals('khalid', Objects::getArrayProperty($array, 'name'));
    }

    public function testGetArrayItem()
    {
        $array = array('name','age');

        $this->assertEquals('name', Objects::getArrayItem($array, 0));
        $this->assertEquals('age', Objects::getArrayItem($array, 1));
        $this->assertEquals('default', Objects::getArrayItem($array, 2, 'default'));
    }

    public function testFindArrayItem()
    {
        $item1 = new Any();
        $item1->name = 'khalid';
        $item1->age = 36;

        $item2 = new Any();
        $item2->name = 'hamid';
        $item2->age = 40;

        $item3 = new Any();
        $item3->name = 'rachid';
        $item3->age = 55;

        $array = array($item1,$item2,$item3);

        $this->assertEquals($item1, Objects::findArrayItem($array, 'name', 'khalid'));
    }
}