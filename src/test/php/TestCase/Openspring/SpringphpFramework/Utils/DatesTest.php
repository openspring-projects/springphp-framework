<?php
namespace TestCase\Openspring\SpringphpFramework\Utils;

use Openspring\SpringphpFramework\Utils\Dates;
use PHPUnit\Framework\TestCase;
use DateTime;
use DateTimeZone;

class DatesTest extends TestCase
{
    protected function setUp(): void
    {
        date_default_timezone_set('UTC');
    }

    public function testToStamp()
    {
        //$this->assertSame('1446203259', Dates::toStamp(new DateTime('2015-10-30 11:07:39')));
        $this->assertSame(0, Dates::toStamp('undefined date', false));
        $this->assertTrue(is_numeric(Dates::toStamp()));
        $time = time();
        $this->assertSame($time, Dates::toStamp());
        $this->assertSame($time, Dates::toStamp($time));
        $this->assertTrue(is_numeric(Dates::toStamp('+1 week')));
        $this->assertTrue(is_numeric(Dates::toStamp(new DateTime())));
    }

    /**
     * Test that factory() returns a DateTime object.
     */
    public function testFactory()
    {
        $this->assertInstanceOf(DateTime::class, Dates::factory());
        $this->assertInstanceOf(DateTime::class, Dates::factory('1988-02-26 12:23:12'));
        $this->assertInstanceOf(DateTime::class, Dates::factory(time()));
        $datetime = new DateTime();
        $this->assertSame($datetime, Dates::factory($datetime));
    }

    public function testTimezone()
    {
        $this->assertInstanceOf(DateTimeZone::class, Dates::timezone());
        $dtz = new DateTimeZone('America/Los_Angeles');
        $this->assertSame($dtz, Dates::timezone($dtz));
    }

    public function testSql()
    {
        $format = Dates::SQL_FORMAT;
        $this->assertSame(date($format), Dates::sql());
        $this->assertSame(date($format), Dates::sql(''));
        $this->assertSame(date($format), Dates::sql(0));
        $this->assertSame(date($format), Dates::sql());
        $this->assertSame(date($format), Dates::sql(false));
        //TODO: fix it
        // on gitlab cdi print error:
        // --- Expected
        //   +++ Actual
        //   @@ @@
        //   -'2020-02-09 17:10:23'
        //   +'2020-02-09 17:10:24'
        $this->assertSame(date($format), Dates::sql('string'));
        $this->assertSame('2015-10-30 11:07:39', Dates::sql('1446203259'));
        $this->assertSame('2015-10-30 11:07:39', Dates::sql(1446203259));
        $this->assertSame('2015-10-30 11:07:39', Dates::sql('2015-10-30 11:07:39'));
        $this->assertSame('2015-10-30 00:00:00', Dates::sql('2015-10-30'));
    }

    public function testIsDate()
    {
        $this->assertFalse(Dates::is(''));
        $this->assertFalse(Dates::is(null));
        $this->assertFalse(Dates::is(false));
        // $this->assertFalse(Dates::is('-0100')); // WAT????
        $this->assertFalse(Dates::is('string'));
        $this->assertFalse(Dates::is('1446203259'));
        $this->assertFalse(Dates::is(1446203259));
        $this->assertTrue(Dates::is('now'));
        $this->assertTrue(Dates::is('2015-10-30'));
        $this->assertTrue(Dates::is('2015-10-30 11:07:39'));
    }

    public function testHuman()
    {
        $this->assertSame('30 Oct 2015 00:00', Dates::human('2015-10-30'));
        $this->assertSame('30 October 2015', Dates::human('2015-10-30', 'd F Y'));
        $this->assertSame('30 Oct 2015', Dates::human('2015-10-30', 'd M Y'));
    }

    public function testIsThisWeek()
    {
        $this->assertTrue(Dates::isThisWeek('+0 week'));
        $this->assertFalse(Dates::isThisWeek('+2 week'));
        $this->assertFalse(Dates::isThisWeek('-2 week'));
    }

    public function testIsThisMonth()
    {
        $this->assertTrue(Dates::isThisMonth('+0 month'));
        $this->assertFalse(Dates::isThisMonth('+2 month'));
        $this->assertFalse(Dates::isThisMonth('-2 month'));
    }

    public function testIsThisYear()
    {
        $this->assertTrue(Dates::isThisYear('+0 year'));
        $this->assertFalse(Dates::isThisYear('+2 year'));
        $this->assertFalse(Dates::isThisYear('-2 year'));
    }

    public function testIsTomorrow()
    {
        $this->assertTrue(Dates::isTomorrow('+1 day'));
        $this->assertFalse(Dates::isTomorrow('+0 day'));
        $this->assertFalse(Dates::isTomorrow('-1 day'));
    }

    public function testIsToday()
    {
        $this->assertTrue(Dates::isToday('+0 day'));
        $this->assertFalse(Dates::isToday('+2 day'));
        $this->assertFalse(Dates::isToday('-2 day'));
    }

    public function testIsYesterday()
    {
        $this->assertTrue(Dates::isYesterday('-1 day'));
        $this->assertFalse(Dates::isYesterday('+0 day'));
        $this->assertFalse(Dates::isYesterday('+1 day'));
    }

    public function testConst()
    {
        $this->assertSame('Y-m-d H:i:s', Dates::SQL_FORMAT);
        $this->assertSame('0000-00-00 00:00:00', Dates::SQL_NULL);
    }
	
	public function testDiff()
    {
        $now = DateTime::createFromFormat('Y-m-d', Dates::getCurrentDate());
        $tomorrow = DateTime::createFromFormat('Y-m-d', Dates::getCurrentDate());
        $tomorrow->modify('+1 day');
        
        $seconds = Dates::getBetweenDatesInSeconds($now, $tomorrow);
		
        $this->assertEquals(86400, $seconds);
    }
}