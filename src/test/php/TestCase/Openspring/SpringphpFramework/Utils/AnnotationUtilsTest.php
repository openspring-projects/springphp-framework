<?php

namespace TestCase\Openspring\SpringphpFramework\Utils;

use Openspring\SpringphpFramework\Annotation\AnnotationAttribute;
use Openspring\SpringphpFramework\Enumeration\MediaType;
use Openspring\SpringphpFramework\Enumeration\ObjectType;
use Openspring\SpringphpFramework\Exception\InvalidJsonStringException;
use Openspring\SpringphpFramework\Exception\Annotation\InvalidAnnotationAttributeValueException;
use Openspring\SpringphpFramework\Http\HttpMethod;
use Openspring\SpringphpFramework\Type\ArrayList;
use Openspring\SpringphpFramework\Utils\AnnotationUtils;
use PHPUnit\Framework\TestCase;

class AnnotationUtilsTest extends TestCase
{
    public function testGivenBadJsonGetElementThenException()
	{
	    $badAnnotDoc =  '/**' . PHP_EOL .
                	    '"*' . PHP_EOL .
                	    '* @RequestMapping({&-*+++++: "})' . PHP_EOL .
                	    '"*/';
	    $this->expectException(InvalidJsonStringException::class);
	    
	    AnnotationUtils::getElement($badAnnotDoc);
	}
	
	public function testGivenGoodJsonGetElementThenOK()
	{
	    $annotDoc = '/**' . PHP_EOL .
            	    '"*' . PHP_EOL .
            	    '* @RequestMapping({"value": "/groups/enable", "method":"POST", "consumes":"application/json", "produces":"application/json", "groupIn":"*"})' . PHP_EOL .
            	    '"*/';
	    $annotationElement = AnnotationUtils::getElement($annotDoc);
	    
	    $this->assertEquals('RequestMapping', $annotationElement->getName());
	    $this->assertCount(5, $annotationElement->getAttributes());
	}
	
	public function testGivenGoodJsonGetElementsThenOK()
	{
	    $annotDoc = '/**' . PHP_EOL .
            	    '"* Return the api status' . PHP_EOL .
            	    '* @RequestMapping({"value": "/groups/enable", "method":"POST", "consumes":"application/json", "produces":"application/json", "groupIn":"*"})' . PHP_EOL .
            	    '* @GetMapping({"value": "/groups/enable", "consumes":"application/json", "produces":"application/json", "groupIn":"*"}) abc' . PHP_EOL .
            	    '* @PostMapping({"value": "/groups/enable", "consumes":"application/json", "produces":"application/json", "groupIn":"*"})' . PHP_EOL .
            	    '* @PutMapping({"value": "/groups/enable", "consumes":"application/json", "produces":"application/json", "groupIn":"*"})' . PHP_EOL .
            	    '"* Return the api status' . PHP_EOL .
            	    '"*/';
	    $annotationElements = AnnotationUtils::getElements($annotDoc);
	    
	    $this->assertCount(4, $annotationElements);
	}
	
	public function testValidate()
	{
	    $supportedAttribs = new ArrayList();
	    
	    $supportedAttribs->add(new AnnotationAttribute('value', null, ObjectType::STRING));
	    $supportedAttribs->add(new AnnotationAttribute('method', HttpMethod::toArray(), ObjectType::ARRAY));
	    $supportedAttribs->add(new AnnotationAttribute('consumes', MediaType::toArray(), ObjectType::STRING));
	    $supportedAttribs->add(new AnnotationAttribute('produces', MediaType::toArray(), ObjectType::STRING));
	    
	    $annotDoc = '/**' . PHP_EOL .
            	    '"*' . PHP_EOL .
            	    '* @RequestMapping({"value": "/groups/enable", "method":"PO-ST", "consumes":"application/json", "produces":"application/json"})' . PHP_EOL .
            	    '"*/';
	    $annotationElement = AnnotationUtils::getElement($annotDoc);
	    
	    $this->expectException(InvalidAnnotationAttributeValueException::class);
	    
	    $annotationElement->validate($supportedAttribs);
	}
}