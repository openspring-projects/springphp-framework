<?php

namespace TestCase\Openspring\SpringphpFramework\Validation;

use Openspring\SpringphpFramework\Validation\Validator;
use PHPUnit\Framework\TestCase;
use TestCase\Openspring\SpringphpFramework\Validation\Helper\ClientDto;

class ValidatorTest extends TestCase
{
    private $clientDto;

    public function setUp(): void
    {
        $clientDto = new ClientDto();

        $clientDto->userId = '123456';
        $clientDto->groupId = 2;
        $clientDto->surname = 'Thor-Asgardian';
        $clientDto->login = 'driss.elrajji';
        $clientDto->firstName = 'Driss';
        $clientDto->lastName = 'EL-RAJJI';
        $clientDto->email = 'driss.elrajji@gmail.com';
        $clientDto->birthDate = '25/05/1975';
        $clientDto->childBirthDate = '04/04/2000';
        $clientDto->phoneNumber = '212056235245';
        $clientDto->married = true;
        $clientDto->deathDate = null;
        $clientDto->expirationDate = '28/03/2035';
        $clientDto->roles = array('admin');

        $this->clientDto = $clientDto;
    }

    public function testValidateOK()
    {
        $validator = new Validator(true);
        $violations = $validator->validate($this->clientDto);

        $this->assertCount(0, $violations);
    }

    public function testValidateKO()
    {
        $this->clientDto->login = null;
        $this->clientDto->email = 'toto.com';
        $this->clientDto->married = 'YES';
        $this->clientDto->groupId = 10;
        $this->clientDto->userId = 'a';

        $validator = new Validator(true);
        $violations = $validator->validate($this->clientDto);

        $this->assertCount(5, $violations);

        $expectedMessages = [
            'Invalid user id',
            'GroupId max value is 3',
            'Login can not be blank',
            'Invalid email address',
            'Invalid Data type passed in field married. Expected ["Boolean"] for annotation @AssertTrue'];
        $messages = $validator->getMessages();

        $this->assertEquals($expectedMessages, $messages);
    }

    public function testIsValidOK()
    {
        $validator = new Validator(true);
        $this->assertTrue($validator->isValid($this->clientDto));
    }

    public function testIsValidKO()
    {
        $this->clientDto->login = null;
        $this->clientDto->email = 'toto.com';
        $this->clientDto->married = 'YES';
        $this->clientDto->groupId = 10;
        $this->clientDto->userId = 'a';

        $validator = new Validator(true);

        $this->assertFalse($validator->isValid($this->clientDto));

        $expectedMessages = [
            'Invalid user id',
            'GroupId max value is 3',
            'Login can not be blank',
            'Invalid email address',
            'Invalid Data type passed in field married. Expected ["Boolean"] for annotation @AssertTrue'];
        $messages = $validator->getMessages();

        $this->assertEquals($expectedMessages, $messages);
    }
}