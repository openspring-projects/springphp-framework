<?php

namespace TestCase\Openspring\SpringphpFramework\Validation;

use Openspring\SpringphpFramework\Exception\Data\DataValidationException;
use Openspring\SpringphpFramework\Validation\ValidatorFactory;
use PHPUnit\Framework\TestCase;
use TestCase\Openspring\SpringphpFramework\Validation\Helper\ClientDto;

class ValidatorFactoryTest extends TestCase
{
    private $clientDto;

    public function setUp(): void
    {
        $clientDto = new ClientDto();

        $clientDto->userId = '123456';
        $clientDto->groupId = 2;
        $clientDto->surname = 'Thor-Asgardian';
        $clientDto->login = 'driss.elrajji';
        $clientDto->firstName = 'Driss';
        $clientDto->lastName = 'EL-RAJJI';
        $clientDto->email = 'driss.elrajji@gmail.com';
        $clientDto->birthDate = '25/05/1975';
        $clientDto->childBirthDate = '04/04/2000';
        $clientDto->phoneNumber = '212056235245';
        $clientDto->married = true;
        $clientDto->deathDate = null;
        $clientDto->expirationDate = '28/03/2035';
        $clientDto->roles = array(
            'admin');

        $this->clientDto = $clientDto;
    }

    public function testGetValidator()
    {
        $validator = ValidatorFactory::getValidator();
        $violations = $validator->validate($this->clientDto);

        $this->assertCount(0, $violations);
    }

    public function testGetStrictValidator()
    {
        $this->clientDto->userId = null;

        $validator = ValidatorFactory::getStrictValidator();

        $this->expectException(DataValidationException::class);
        $validator->validate($this->clientDto);
    }
}