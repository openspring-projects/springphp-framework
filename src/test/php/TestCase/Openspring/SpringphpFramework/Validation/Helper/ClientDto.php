<?php

namespace TestCase\Openspring\SpringphpFramework\Validation\Helper;

use Openspring\SpringphpFramework\Validation\Validable;

class ClientDto implements Validable
{
    /**
     * @NotBlank({"message": "UserId can not be null"})
     * @Size({"min": 5, "max":128, "message": "Invalid user id"})
     */
    var $userId;

    /**
     * @Min({"value": 0, "message": "GroupId min value is 0"})
     * @Max({"value": 3, "message": "GroupId max value is 3"})
     */
    var $groupId;

    /**
     * @NotNull({"message": "Surname can not be null"})
     */
    var $surname;

    /**
     * @NotBlank({"message": "Login can not be blank"})
     */
    var $login;

    /**
     * @NotBlank({"message": "First name can not be blank"})
     * @Size({"min": 5, "max": 128, "message": "First name size must be in [5, 128] interval"})
     */
    var $firstName;

    /**
     * @NotBlank({"message": "Last name can not be blank"})
     * @Size({"min": 5, "max": 128, "message": "Last name size must be in [5, 128] interval"})
     */
    var $lastName;

    /**
     * @NotBlank({"message": "Email can not be blank"})
     * @Email({"message": "Invalid email address"})
     */
    var $email;

    /**
     * @Past({"format": "d/m/Y", "message": "Invalid past date"})
     */
    var $birthDate;

    /**
     * @PastOrPresent({"format": "d/m/Y", "message": "Invalid past or present date"})
     */
    var $childBirthDate;

    /**
     * @NotBlank({"message": "Phone number can not be blank"})
     * @Size({"min": 9, "max": 25, "message": "Phone number size must be in [9, 25] interval"})
     */
    var $phoneNumber;

    /**
     * @AssertTrue({"message": "A valid client must be married"})
     */
    var $married;

    /**
     * @Future({"format": "d/m/Y", "message": "Invalid future date"})
     */
    var $deathDate;

    /**
     * @FutureOrPresent({"format": "d/m/Y", "message": "Invalid future or present date"})
     */
    var $expirationDate;

    /**
     * @NotEmpty({"message": "Roles can not be empty"})
     * @var array
     */
    var $roles;
}