<?php

namespace TestCase\Openspring\SpringphpFramework\Validation\Validator\Complex;

use Openspring\SpringphpFramework\Core\Stereotype\Annotations;
use Openspring\SpringphpFramework\Type\Any;
use Openspring\SpringphpFramework\Validation\Validator\Complex\MaxConstraint;
use PHPUnit\Framework\TestCase;

class MaxConstraintTest extends TestCase
{
    public function testIsValid()
    {
        $validator = new MaxConstraint();
        $config = Annotations::getValidationAnnotation('Max');
        $config->passedParams = new Any();
        $config->passedParams->value = 100;
        $config->passedParams->message = 'Invalid data';

        $this->assertTrue($validator->isValid('property1', 0, $config));
        $this->assertTrue($validator->isValid('property1', 99, $config));
        $this->assertTrue($validator->isValid('property1', 100, $config));
        $this->assertTrue($validator->isValid('property1', null, $config));

        $this->assertFalse($validator->isValid('property1', 'abc', $config));
        $this->assertFalse($validator->isValid('property1', '9a9', $config));
        $this->assertFalse($validator->isValid('property1', '9 9', $config));
        $this->assertFalse($validator->isValid('property1', ' ', $config));
        $this->assertFalse($validator->isValid('property1', true, $config));
        $this->assertFalse($validator->isValid('property1', false, $config));
        $this->assertFalse($validator->isValid('property1', 101, $config));
        $this->assertFalse($validator->isValid('property1', 500, $config));
    }
}