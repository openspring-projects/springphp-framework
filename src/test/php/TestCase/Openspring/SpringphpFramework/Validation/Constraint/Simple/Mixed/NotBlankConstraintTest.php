<?php

namespace TestCase\Openspring\SpringphpFramework\Validation\Validator\Simple\Mixed;

use Openspring\SpringphpFramework\Core\Stereotype\Annotations;
use Openspring\SpringphpFramework\Type\Any;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Mixed\NotBlankConstraint;
use PHPUnit\Framework\TestCase;

class NotBlankConstraintTest extends TestCase
{
    public function testIsValid()
    {
        $validator = new NotBlankConstraint();
        $config = Annotations::getValidationAnnotation('NotBlank');
        $config->passedParams = new Any();
        $config->passedParams->message = 'Invalid data';

        $this->assertFalse($validator->isValid('property1', '', $config));
        $this->assertFalse($validator->isValid('property1', ' ', $config));
        $this->assertFalse($validator->isValid('property1', '    ', $config));
        $this->assertFalse($validator->isValid('property1', null, $config));
        $this->assertFalse($validator->isValid('property1', array(), $config));
        $this->assertFalse($validator->isValid('property1', new Any(), $config));

        $this->assertTrue($validator->isValid('property1', 'abc', $config));
    }
}