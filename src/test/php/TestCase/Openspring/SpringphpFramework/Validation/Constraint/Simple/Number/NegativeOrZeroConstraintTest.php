<?php

namespace TestCase\Openspring\SpringphpFramework\Validation\Validator\Simple\Number;

use Openspring\SpringphpFramework\Core\Stereotype\Annotations;
use Openspring\SpringphpFramework\Type\Any;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Number\NegativeOrZeroConstraint;
use PHPUnit\Framework\TestCase;

class NegativeOrZeroConstraintTest extends TestCase
{
    public function testIsValid()
    {
        $validator = new NegativeOrZeroConstraint();
        $config = Annotations::getValidationAnnotation('NegativeOrZero');
        $config->passedParams = new Any();
        $config->passedParams->message = 'Invalid data';

        
        $this->assertFalse($validator->isValid('property1', 'abc', $config));
        $this->assertFalse($validator->isValid('property1', ' ', $config));
        $this->assertFalse($validator->isValid('property1', new Any(), $config));
        $this->assertFalse($validator->isValid('property1', 1, $config));
        $this->assertFalse($validator->isValid('property1', 10, $config));
        $this->assertFalse($validator->isValid('property1', 'a-15b', $config));
        $this->assertFalse($validator->isValid('property1', array(), $config));
        
        $this->assertTrue($validator->isValid('property1', 0, $config));
        $this->assertTrue($validator->isValid('property1', null, $config));
        $this->assertTrue($validator->isValid('property1', '', $config));

        $this->assertTrue($validator->isValid('property1', - 1, $config));
        $this->assertTrue($validator->isValid('property1', - 500, $config));
        $this->assertTrue($validator->isValid('property1', - 900.50, $config));
    }
}