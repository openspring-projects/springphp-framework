<?php

namespace TestCase\Openspring\SpringphpFramework\Validation\Validator\Simple\Mixed;

use Openspring\SpringphpFramework\Core\Stereotype\Annotations;
use Openspring\SpringphpFramework\Type\Any;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Mixed\URLConstraint;
use PHPUnit\Framework\TestCase;

class URLConstraintTest extends TestCase
{
    public function testIsValid()
    {
        $validator = new URLConstraint();
        $config = Annotations::getValidationAnnotation('URL');
        $config->passedParams = new Any();
        $config->passedParams->message = 'Invalid data';
        
        $this->assertTrue($validator->isValid('property1', '', $config));
        $this->assertTrue($validator->isValid('property1', null, $config));

        $this->assertTrue($validator->isValid('property1', 'http://www.google.com', $config));
        $this->assertTrue($validator->isValid('property1', 'https://www.google.com', $config));

        $this->assertTrue($validator->isValid('property1', 'http://www.google.com/gmail', $config));
        $this->assertTrue($validator->isValid('property1', 'https://www.google.com/gmail', $config));

        $this->assertTrue($validator->isValid('property1', 'http://www.google.com/gmail/inbox', $config));
        $this->assertTrue($validator->isValid('property1', 'https://www.google.com/gmail/inbox', $config));

        $this->assertTrue($validator->isValid('property1', 'http://www.google.com/gmail?a=b&c=100', $config));
        $this->assertTrue($validator->isValid('property1', 'https://www.google.com/gmail?a=b&c=100', $config));

        $this->assertFalse($validator->isValid('property1', 'http:/www.google.com/gmail', $config));
        $this->assertFalse($validator->isValid('property1', 'https:/www.google.com/gmail', $config));

        $this->assertFalse($validator->isValid('property1', '://www.google.com/gmail?a=b&c=100', $config));
        $this->assertFalse($validator->isValid('property1', 'https:www.google.com/gmail?a=b&c=100', $config));

        $this->assertFalse($validator->isValid('property1', 'toto', $config));
    }
}