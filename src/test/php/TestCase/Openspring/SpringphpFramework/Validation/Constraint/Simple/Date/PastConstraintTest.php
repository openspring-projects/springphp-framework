<?php

namespace TestCase\Openspring\SpringphpFramework\Validation\Validator\Simple\Date;

use Openspring\SpringphpFramework\Core\Stereotype\Annotations;
use Openspring\SpringphpFramework\Type\Any;
use Openspring\SpringphpFramework\Utils\Dates;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Date\PastConstraint;
use PHPUnit\Framework\TestCase;
use DateInterval;
use DateTime;

class PastConstraintTest extends TestCase
{
    public function testIsValid()
    {
        $datetime = DateTime::createFromFormat('Y-m-d', Dates::getCurrentDate());
        $today = DateTime::createFromFormat('Y-m-d', Dates::getCurrentDate())->format('Y-m-d');
        $datetime->modify('+1 year');
        $date = $datetime->format('Y-m-d');
        $datetime = $datetime->format('Y-m-d');

        $validator = new PastConstraint();
        $config = Annotations::getValidationAnnotation('FutureOrPresent');
        $config->passedParams = new Any();
        $config->passedParams->format = 'Y-m-d';
        $config->passedParams->message = 'Invalid data';
        
        $this->assertTrue($validator->isValid('property1', '', $config));
        $this->assertTrue($validator->isValid('property1', null, $config));

        $this->assertFalse($validator->isValid('property1', $today, $config));
        $this->assertFalse($validator->isValid('property1', $date, $config));
        $this->assertFalse($validator->isValid('property1', $datetime, $config));

        $datetime = DateTime::createFromFormat('Y-m-d', Dates::getCurrentDate());
        $datetime->sub(new DateInterval('P1D'));
        $date1 = $datetime->format('Y-m-d');
        $datetime->sub(new DateInterval('P1M'));
        $date2 = $datetime->format('Y-m-d');

        $this->assertTrue($validator->isValid('property1', $date1, $config));
        $this->assertTrue($validator->isValid('property1', $date2, $config));
    }
}