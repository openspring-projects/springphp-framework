<?php

namespace TestCase\Openspring\SpringphpFramework\Validation\Validator\Simple\Date;

use Openspring\SpringphpFramework\Core\Stereotype\Annotations;
use Openspring\SpringphpFramework\Type\Any;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Date\DateConstraint;
use PHPUnit\Framework\TestCase;

class DateConstraintTest extends TestCase
{
    public function testIsValid()
    {
        $validator = new DateConstraint();
        $config = Annotations::getValidationAnnotation('Date');
        $config->passedParams = new Any();
        $config->passedParams->message = 'Invalid data';

        $config->passedParams->format = 'Y-m-d';
        $this->assertTrue($validator->isValid('property1', '', $config));
        $this->assertTrue($validator->isValid('property1', null, $config));
        
        $config->passedParams->format = 'Y-m-d';
        $this->assertTrue($validator->isValid('property1', '2018-10-10', $config));

        $config->passedParams->format = 'Y-m-d';
        $this->assertTrue($validator->isValid('property1', '2018-10-10', $config));

        $config->passedParams->format = 'F j, Y, g:i a';
        $this->assertTrue($validator->isValid('property1', 'March 10, 2001, 5:16 pm', $config));

        $config->passedParams->format = 'm.d.y';
        $this->assertTrue($validator->isValid('property1', '03.10.01', $config));

        $config->passedParams->format = 'Y-m-d';
        $this->assertFalse($validator->isValid('property1', '2018-50-10', $config));
    }
}