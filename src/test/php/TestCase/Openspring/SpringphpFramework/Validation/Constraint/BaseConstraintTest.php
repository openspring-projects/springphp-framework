<?php

namespace TestCase\Openspring\SpringphpFramework\Validation;

use Openspring\SpringphpFramework\Enumeration\ObjectType;
use Openspring\SpringphpFramework\Type\Any;
use Openspring\SpringphpFramework\Type\DynamicObject;
use Openspring\SpringphpFramework\Validation\BaseConstraint;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Mixed\NotNullConstraint;
use PHPUnit\Framework\TestCase;

class BaseConstraintTest extends TestCase
{
    public function testValidateRegex()
    {
        $pattern = '/[a-z]+/';
        $baseConstraint = new BaseConstraint();

        $this->assertTrue($baseConstraint->validateRegex('property1', 'azerty', $pattern));
        $this->assertFalse($baseConstraint->validateRegex('property1', 'AZERTY', $pattern));
        $this->assertFalse($baseConstraint->validateRegex('property1', '0123', $pattern));
    }

    public function testIsMetaValidOK()
    {
        $baseConstraint = new BaseConstraint();

        $config = new Any();
        $sizeConfig = array(
            'min' => ObjectType::INTEGER,
            'max' => ObjectType::INTEGER,
            'message' => ObjectType::STRING);
        $sizeTarget = array(
            ObjectType::STRING,
            ObjectType::INTEGER,
            ObjectType::BOOLEAN);

        $passedParams = new DynamicObject(array(
            'min' => 10,
            'max' => 20,
            'message' => 'Incorrect length !'));

        $config->start = '';
        $config->end = '';
        $config->target = $sizeTarget;
        $config->expectedParams = $sizeConfig;
        $config->validator = NotNullConstraint::class;
        $config->passedParams = $passedParams;

        $this->assertTrue($baseConstraint->isMetaValid('property1', 15, $config));
        $this->assertCount(0, $baseConstraint->getMessages());
    }

    public function testIsMetaValidWithInvalidDataKO()
    {
        $baseConstraint = new BaseConstraint();

        $config = new Any();
        $sizeConfig = array(
            'min' => ObjectType::INTEGER,
            'max' => ObjectType::INTEGER,
            'message' => ObjectType::STRING);
        $sizeTarget = array(
            ObjectType::INTEGER);

        $passedParams = new DynamicObject(array(
            'min' => 10,
            'max' => 20,
            'message' => 'Incorrect length !'));

        $config->start = '';
        $config->end = '';
        $config->target = $sizeTarget;
        $config->expectedParams = $sizeConfig;
        $config->validator = NotNullConstraint::class;
        $config->passedParams = $passedParams;

        $this->assertFalse($baseConstraint->isMetaValid('property1', "koko", $config));
        $this->assertCount(1, $baseConstraint->getMessages());
    }

    public function testIsMetaValidWithInvalidParamsKO()
    {
        $baseConstraint = new BaseConstraint();

        $config = new Any();
        $sizeConfig = array(
            'min' => ObjectType::INTEGER,
            'max' => ObjectType::INTEGER,
            'message' => ObjectType::STRING);
        $sizeTarget = array(
            ObjectType::INTEGER);

        $passedParams = new DynamicObject(array(
            'min' => 'a',
            'max' => 'b',
            'message' => 'Incorrect length !'));

        $config->start = '';
        $config->end = '';
        $config->target = $sizeTarget;
        $config->expectedParams = $sizeConfig;
        $config->validator = NotNullConstraint::class;
        $config->passedParams = $passedParams;

        $this->assertFalse($baseConstraint->isMetaValid('property1', 15, $config));
        $this->assertCount(1, $baseConstraint->getMessages());
    }
}