<?php

namespace TestCase\Openspring\SpringphpFramework\Validation\Validator\Complex;

use Openspring\SpringphpFramework\Core\Stereotype\Annotations;
use Openspring\SpringphpFramework\Type\Any;
use Openspring\SpringphpFramework\Validation\Validator\Complex\SizeConstraint;
use PHPUnit\Framework\TestCase;

class SizeConstraintTest extends TestCase
{
    public function testIsValid()
    {
        $validator = new SizeConstraint();
        $config = Annotations::getValidationAnnotation('Size');
        $config->passedParams = new Any();
        $config->passedParams->min = 2;
        $config->passedParams->max = 5;
        $config->passedParams->message = 'Invalid data';

        // strings
        $this->assertTrue($validator->isValid('property1', 'ab', $config));
        $this->assertTrue($validator->isValid('property1', 'abc', $config));
        $this->assertTrue($validator->isValid('property1', 'abcde', $config));

        $this->assertFalse($validator->isValid('property1', 0, $config));
        $this->assertFalse($validator->isValid('property1', array(), $config));
        $this->assertTrue($validator->isValid('property1', null, $config));
        $this->assertTrue($validator->isValid('property1', '', $config));
        
        $this->assertFalse($validator->isValid('property1', ' ', $config));
        $this->assertFalse($validator->isValid('property1', 'a', $config));
        $this->assertFalse($validator->isValid('property1', 'abcdef', $config));
        $this->assertFalse($validator->isValid('property1', 'abcdefghkl', $config));
        
        $this->assertFalse($validator->isValid('property1', true, $config));
        $this->assertFalse($validator->isValid('property1', false, $config));
        $this->assertFalse($validator->isValid('property1', 99, $config));
        
        // arrays
        $this->assertTrue($validator->isValid('property1', array(1,2), $config));
        $this->assertTrue($validator->isValid('property1', array(1,2,3,4,5), $config));
        
        $this->assertFalse($validator->isValid('property1', array(1,2,3,4,5,6), $config));
    }
}