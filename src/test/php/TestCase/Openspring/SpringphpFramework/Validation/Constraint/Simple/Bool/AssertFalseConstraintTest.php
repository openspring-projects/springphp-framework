<?php

namespace TestCase\Openspring\SpringphpFramework\Validation\Validator\Simple\Bool;

use Openspring\SpringphpFramework\Core\Stereotype\Annotations;
use Openspring\SpringphpFramework\Type\Any;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Bool\AssertFalseConstraint;
use PHPUnit\Framework\TestCase;

class AssertFalseConstraintTest extends TestCase
{
    public function testIsValid()
    {
        $validator = new AssertFalseConstraint();
        $config = Annotations::getValidationAnnotation('AssertFalse');
        $config->passedParams = new Any();
        $config->passedParams->message = 'Invalid data';

        $this->assertTrue($validator->isValid('property1', false, $config));
        
        $this->assertFalse($validator->isValid('property1', true, $config));
        $this->assertFalse($validator->isValid('property1', '00', $config));
        $this->assertFalse($validator->isValid('property1', 'abc', $config));
        $this->assertFalse($validator->isValid('property1', 1, $config));
        $this->assertFalse($validator->isValid('property1', 0, $config));
        $this->assertTrue($validator->isValid('property1', null, $config));
        $this->assertTrue($validator->isValid('property1', '', $config));
        $this->assertFalse($validator->isValid('property1', ' ', $config));
    }
}