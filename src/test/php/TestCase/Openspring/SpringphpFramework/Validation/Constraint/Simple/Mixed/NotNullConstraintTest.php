<?php

namespace TestCase\Openspring\SpringphpFramework\Validation\Validator\Simple\Mixed;

use Openspring\SpringphpFramework\Core\Stereotype\Annotations;
use Openspring\SpringphpFramework\Type\Any;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Mixed\NotNullConstraint;
use PHPUnit\Framework\TestCase;

class NotNullConstraintTest extends TestCase
{
    public function testIsValid()
    {
        $validator = new NotNullConstraint();
        $config = Annotations::getValidationAnnotation('NotNull');
        $config->passedParams = new Any();
        $config->passedParams->message = 'Invalid data';

        $this->assertTrue($validator->isValid('property1', '', $config));
        $this->assertTrue($validator->isValid('property1', ' ', $config));
        $this->assertTrue($validator->isValid('property1', '    ', $config));
        $this->assertTrue($validator->isValid('property1', array(), $config));
        $this->assertTrue($validator->isValid('property1', new Any(), $config));
        $this->assertTrue($validator->isValid('property1', 'abc', $config));

        $this->assertFalse($validator->isValid('property1', null, $config));
    }
}