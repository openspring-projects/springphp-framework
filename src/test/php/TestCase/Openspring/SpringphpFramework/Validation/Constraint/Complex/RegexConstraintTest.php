<?php

namespace TestCase\Openspring\SpringphpFramework\Validation\Validator\Complex;

use Openspring\SpringphpFramework\Core\Stereotype\Annotations;
use Openspring\SpringphpFramework\Type\Any;
use Openspring\SpringphpFramework\Validation\Validator\Complex\RegexConstraint;
use PHPUnit\Framework\TestCase;

class RegexConstraintTest extends TestCase
{
    public function testIsValid()
    {
        $validator = new RegexConstraint();
        $config = Annotations::getValidationAnnotation('Regex');
        $config->passedParams = new Any();
        $config->passedParams->pattern = '/^def/';
        $config->passedParams->message = 'Invalid data';

        $this->assertTrue($validator->isValid('property1', 'def', $config));
        $this->assertTrue($validator->isValid('property1', '', $config));
        $this->assertFalse($validator->isValid('property1', 'abcdef', $config));
        $this->assertFalse($validator->isValid('property1', 'abcdef', $config));
    }
}