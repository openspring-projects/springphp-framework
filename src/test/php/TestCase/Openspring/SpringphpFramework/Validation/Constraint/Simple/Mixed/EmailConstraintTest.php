<?php

namespace TestCase\Openspring\SpringphpFramework\Validation\Validator\Simple\Mixed;

use Openspring\SpringphpFramework\Core\Stereotype\Annotations;
use Openspring\SpringphpFramework\Type\Any;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Mixed\EmailConstraint;
use PHPUnit\Framework\TestCase;

class EmailConstraintTest extends TestCase
{
    public function testIsValid()
    {
        $validator = new EmailConstraint();
        $config = Annotations::getValidationAnnotation('Email');
        $config->passedParams = new Any();
        $config->passedParams->message = 'Invalid data';
        
        $this->assertTrue($validator->isValid('property1', '', $config));
        $this->assertTrue($validator->isValid('property1', null, $config));

        $this->assertTrue($validator->isValid('property1', 'kha.elabbadi@gmail.com', $config));
        $this->assertTrue($validator->isValid('property1', 'a@d.com', $config));
        $this->assertTrue($validator->isValid('property1', 'kha123@gmail.com', $config));
        $this->assertTrue($validator->isValid('property1', '123_elabbadi@gmail.com', $config));
        $this->assertTrue($validator->isValid('property1', '__.elabbadi@gmail.com', $config));

        $this->assertFalse($validator->isValid('property1', '@gmail.com', $config));
        $this->assertFalse($validator->isValid('property1', ' @gmail.com', $config));
        $this->assertFalse($validator->isValid('property1', 'abc@ .com', $config));
        $this->assertFalse($validator->isValid('property1', 'abc@gmail.', $config));
        $this->assertFalse($validator->isValid('property1', 'abc@gmail. ', $config));
        $this->assertFalse($validator->isValid('property1', 'abc@gmail', $config));
        $this->assertFalse($validator->isValid('property1', 'a bc@gmail.com', $config));
        $this->assertFalse($validator->isValid('property1', 'abc@gmail%com', $config));
    }
}