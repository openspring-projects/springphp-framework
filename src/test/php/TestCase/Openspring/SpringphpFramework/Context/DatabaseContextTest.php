<?php
namespace TestCase\Openspring\SpringphpFramework\Context;

use Openspring\SpringphpFramework\Context\ApplicationContext;
use Openspring\SpringphpFramework\Http\HttpMethod;
use Openspring\SpringphpFramework\Orm\DatabaseContext;
use TestCase\Openspring\SpringphpFramework\Helper\SpringphpTestRunner;
use PHPUnit\Framework\TestCase;

final class DatabaseContextTest extends TestCase
{
    public function setUp(): void
    {
        SpringphpTestRunner::setSpringphpContext();
        ApplicationContext::initialize();
        SpringphpTestRunner::runHttpMethod(HttpMethod::GET, 'esapi/groups/status', true);
        SpringphpTestRunner::runApplication();
    }
	
	public function testInstantiation()
    {
        $this->assertNotNull(DatabaseContext::getDataSourcesManager());
    }
}