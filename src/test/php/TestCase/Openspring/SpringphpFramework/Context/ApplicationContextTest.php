<?php

namespace TestCase\Openspring\SpringphpFramework\Context;

use Openspring\SpringphpFramework\Context\ApplicationContext;
use Openspring\SpringphpFramework\Utils\Cast;
use PHPUnit\Framework\TestCase;
use TestCase\Openspring\SpringphpFramework\Helper\DummyBean;
use TestCase\Openspring\SpringphpFramework\Helper\DunnyBean;
use TestCase\Openspring\SpringphpFramework\Helper\DunnyChildBean;
use TestCase\Openspring\SpringphpFramework\Helper\HunnyBean;
use TestCase\Openspring\SpringphpFramework\Helper\IPerson;
use TestCase\Openspring\SpringphpFramework\Helper\LucyBean;
use TestCase\Openspring\SpringphpFramework\Helper\SpringphpTestRunner;
use TestCase\Openspring\SpringphpFramework\Helper\TotoBean;
use TestCase\Openspring\SpringphpFramework\Helper\Mock\Application\Resource\UserController;

final class ApplicationContextTest extends TestCase
{

    public function setUp(): void
    {
        SpringphpTestRunner::setSpringphpContext();
    }

    public function testBeanSingletonAddGetRemove()
    {
        $dummy = new DummyBean();
        $toto = new TotoBean();
        $lucy = new LucyBean();

        ApplicationContext::addBeanSingleton($dummy);
        ApplicationContext::addBeanSingleton($toto);

        $this->assertCount(4, ApplicationContext::getBeans());

        $this->assertSame($dummy, ApplicationContext::getBeanOf(DummyBean::class));
        $this->assertSame($toto, ApplicationContext::getBeanOf(TotoBean::class));

        ApplicationContext::removeBean(DummyBean::class);

        $this->assertCount(3, ApplicationContext::getBeans());

        $this->assertNull(ApplicationContext::getBeanOf(DummyBean::class));
        $this->assertSame($toto, ApplicationContext::getBeanOf(TotoBean::class));

        ApplicationContext::addBeanSingleton($dummy);
        ApplicationContext::addBeanSingleton($lucy);

        $persons = ApplicationContext::getBeansOf(IPerson::class);

        $this->assertCount(2, $persons);
        $this->assertSame($lucy, ApplicationContext::getBeanOf(LucyBean::class));
        $this->assertSame($toto, ApplicationContext::getBeanOf(TotoBean::class));
    }

    public function testBeanPrototypeAddGetRemove()
    {
        ApplicationContext::addBeanPrototype(DunnyBean::class);
        ApplicationContext::addBeanPrototype(DunnyChildBean::class);

        // case 1: direct class
        $dunny1 = ApplicationContext::getBeanOf(DunnyBean::class);
        $dunny1->setName('khalid');

        $dunny2 = ApplicationContext::getBeanOf(DunnyBean::class);
        $dunny2->setName('hamid');

        $this->assertNotEquals($dunny1->getName(), $dunny2->getName());

        // case 2: child class
        $dunnyChild = ApplicationContext::getBeanOf(DunnyChildBean::class);
        $this->assertInstanceOf(DunnyChildBean::class, $dunnyChild);

        // case 3: select by interface
        $persons = ApplicationContext::getBeansOf(IPerson::class);
        $this->assertCount(4, $persons);
    }

    public function testBeanSingletonLazyAddGet()
    {
        ApplicationContext::addBeanSingleton(HunnyBean::class);

        $hunny1 = ApplicationContext::getBeanOf(HunnyBean::class);
        $this->assertNotNull($hunny1);
        $hunny1->setName('hunny');

        $hunny2 = ApplicationContext::getBeanOf(HunnyBean::class);

        $this->assertEquals($hunny2->getName(), $hunny1->getName());
    }

    public function testBeanPrototypeAddGetRemoveWithNotEmptyConstructor()
    {
        ApplicationContext::addBeanPrototype(HunnyBean::class);

        // case 1: direct class
        $hunny = ApplicationContext::getBeanOf(HunnyBean::class);
        $this->assertNotNull($hunny);

        $this->assertEquals('driss', $hunny->getName());
        $this->assertEquals(42, $hunny->getAge());
    }

    public function testBeanOverriding()
    {
        $this->assertTrue(ApplicationContext::isBeanOverridingAllowed());

        ApplicationContext::allowBeanOverriding(false);
        $this->assertFalse(ApplicationContext::isBeanOverridingAllowed());

        ApplicationContext::allowBeanOverriding(true);
        $this->assertTrue(ApplicationContext::isBeanOverridingAllowed());
    }

    public function testApplicationName()
    {
        $this->assertEquals('esapi', ApplicationContext::getApplicationName());
    }

    public function testStartupDate()
    {
        $this->assertNotEmpty(ApplicationContext::getStartupDate());
    }

    public function testSetMapping()
    {
        $this->assertCount(6, ApplicationContext::getMapping());
    }

    public function testGetMapping()
    {
        $mappings = ApplicationContext::getMapping();
        $this->assertCount(6, $mappings);

        $mappingItem1 = Cast::asMappingItem($mappings[0]);

        $this->assertEquals(UserController::class, $mappingItem1->getClazz());
        $this->assertEquals('EditMyProfile', $mappingItem1->getAction());
        $this->assertEquals('account/edit/(.*)', $mappingItem1->getPath());
        $this->assertCount(1, $mappingItem1->getPathVariables());
    }
}