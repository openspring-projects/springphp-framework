<?php

namespace TestCase\Openspring\SpringphpFramework\Core\Bean;

use TestCase\Openspring\SpringphpFramework\Helper\DummyBean;
use PHPUnit\Framework\TestCase;

final class BeanTest extends TestCase
{
    public function testInstanceOf()
    {
        $dummyBean = new DummyBean();
        
        $this->assertTrue($dummyBean->instanceOf(DummyBean::class));
    }
}