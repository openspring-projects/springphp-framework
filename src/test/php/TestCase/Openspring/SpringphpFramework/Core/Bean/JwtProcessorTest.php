<?php

namespace TestCase\Openspring\SpringphpFramework\Security\Jwt;

use Openspring\SpringphpFramework\Context\ApplicationContext;
use Openspring\SpringphpFramework\Core\Bean\JwtProcessor;
use Openspring\SpringphpFramework\Exception\Http\BadRequestException;
use Openspring\SpringphpFramework\Exception\Http\ForbiddenException;
use Openspring\SpringphpFramework\Security\Jwt\Core\JwtParameters;
use Openspring\SpringphpFramework\Security\Jwt\Enumeration\EncryptionAlgorithm;
use Openspring\SpringphpFramework\Security\Jwt\Enumeration\TokenElement;
use Openspring\SpringphpFramework\Security\Jwt\Enumeration\TokenPayload;
use Openspring\SpringphpFramework\Security\Jwt\Enumeration\TokenType;
use PHPUnit\Framework\TestCase;
use TestCase\Openspring\SpringphpFramework\Helper\JwtData;
use TestCase\Openspring\SpringphpFramework\Helper\StaticJwtParameterProvider;
use TestCase\Openspring\SpringphpFramework\Helper\Mock\Application\MockUserDetailsProvider;

final class JwtProcessorTest extends TestCase
{
    private $jwtProcessor;
    private $clientId;

    public static function setUpBeforeClass(): void
    {
        ApplicationContext::addBeanSingleton(new StaticJwtParameterProvider());
        ApplicationContext::addBeanSingleton(new MockUserDetailsProvider());
        ApplicationContext::$currentMappingItem = null;
    }

    public function setUp(): void
    {
        $this->clientId = 'store-front';
        $jwtParameters = new JwtParameters(JwtData::PUBLIC_KEY_1024, JwtData::PRIVATE_KEY_1024);
        $jwtParameters->setAlgorithm(EncryptionAlgorithm::RS512);

        $this->setJwtProcessor(new JwtProcessor($this->clientId));
        
        
    }

    public function setJwtProcessor(JwtProcessor $jwtProcessor): void
    {
        $this->jwtProcessor = $jwtProcessor;
    }

    public function testCreateAccessTokenDecodeToken()
    {
        $this->jwtProcessor->addRefreshToken(false);

        $userId = 'cffd1f6545930cfd2e54fc6b161dcbbe91ff2f59';
        $tokenArray = $this->jwtProcessor->createAccessToken($userId);

        $this->assertArrayHasKey(TokenElement::ACCESS_TOKEN, $tokenArray);
        $this->assertArrayHasKey(TokenElement::EXPIRES_IN, $tokenArray);

        $this->jwtProcessor->process($tokenArray[TokenElement::ACCESS_TOKEN], JwtData::PUBLIC_KEY_1024);
        $payload = $this->jwtProcessor->getPayload();

        $this->assertEquals($this->clientId, $payload['sub']);
        $this->assertEquals($userId, $payload['uid']);
        $this->assertEquals(TokenType::BEARER, $payload[TokenElement::TOKEN_TYPE]);

        $this->assertArrayHasKey(TokenPayload::AUD, $payload);
        $this->assertArrayHasKey(TokenPayload::SUB, $payload);
        $this->assertArrayHasKey(TokenPayload::ISS, $payload);
        $this->assertArrayHasKey(TokenPayload::IAT, $payload);
        $this->assertArrayHasKey(TokenPayload::ID, $payload);
    }

    public function testDecodeInvalidToken_BadRequest()
    {
        $token = "invalid.token.given";

        $this->expectException(ForbiddenException::class);
        $this->jwtProcessor->process($token, JwtData::PUBLIC_KEY_1024);
    }

    public function testDecodeInvalidToken_Unauthorized()
    {
        $this->expectException(BadRequestException::class);
        $this->jwtProcessor->process(JwtData::EXPIRED_ACCESS_TOKEN, JwtData::PUBLIC_KEY_1024);
    }

    public function testCreateAccessRefreshTokenDecodeToken()
    {
        $this->jwtProcessor->addRefreshToken(true);

        $userId = 'cffd1f6545930cfd2e54fc6b161dcbbe91ff2f59';
        $tokenArray = $this->jwtProcessor->createAccessToken($userId);

        $this->assertArrayHasKey(TokenElement::ACCESS_TOKEN, $tokenArray);
        $this->assertArrayHasKey(TokenElement::EXPIRES_IN, $tokenArray);
        $this->assertArrayHasKey(TokenElement::REFRESH_TOKEN, $tokenArray);
        $this->assertArrayHasKey(TokenElement::REFRESH_EXPIRES_IN, $tokenArray);

        $this->jwtProcessor->process($tokenArray[TokenElement::ACCESS_TOKEN], JwtData::PUBLIC_KEY_1024);
        $payload = $this->jwtProcessor->getPayload();

        $this->assertEquals($this->clientId, $payload['sub']);
        $this->assertEquals($userId, $payload['uid']);
        $this->assertEquals(TokenType::BEARER, $payload[TokenElement::TOKEN_TYPE]);

        $this->assertArrayHasKey(TokenPayload::AUD, $payload);
        $this->assertArrayHasKey(TokenPayload::SUB, $payload);
        $this->assertArrayHasKey(TokenPayload::ISS, $payload);
        $this->assertArrayHasKey(TokenPayload::IAT, $payload);
        $this->assertArrayHasKey(TokenPayload::ID, $payload);
    }
}