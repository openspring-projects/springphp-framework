<?php

namespace TestCase\Openspring\SpringphpFramework\Core\Bean;

use Openspring\SpringphpFramework\Core\Bean\ResourceBundleMessageSource;
use TestCase\Openspring\SpringphpFramework\Helper\SpringphpTestRunner;
use PHPUnit\Framework\TestCase;

final class ResourceBundleMessageSourceTest extends TestCase
{
    var $resourcesMessages = null;
    
    public function setUp(): void
    {
        SpringphpTestRunner::setSpringphpContext();
        
        $this->resourcesMessages = new ResourceBundleMessageSource();
        $this->resourcesMessages->addBaseName("messages");
    }

    public function testAddBaseName()
    {
        $this->resourcesMessages->addBaseName("bundles");
        
        $this->assertSame('native-messages', $this->resourcesMessages->getBaseNames()[0]);
        $this->assertSame('messages', $this->resourcesMessages->getBaseNames()[1]);
        $this->assertSame('bundles', $this->resourcesMessages->getBaseNames()[2]);
    }
    
    public function testLoad()
    {
        $this->resourcesMessages->setDefaultLocale('en');
        $this->resourcesMessages->load();
        
        $this->assertSame('User', $this->resourcesMessages->getMessage('M000'));
    }
    
    public function testTranslationArgs()
    {
        $this->resourcesMessages->setDefaultLocale('en');
        $this->resourcesMessages->load();
        
        $this->assertSame('Hi {name}, you are {age}', $this->resourcesMessages->getMessage('M002'));
        $this->assertSame('Hi khalid, you are {age}', $this->resourcesMessages->getMessage('M002', 'khalid'));
        $this->assertSame('Hi khalid, you are 36', $this->resourcesMessages->getMessage('M002', 'khalid', 36));
    }
    
    public function testTranslation()
    {
        $this->resourcesMessages->setDefaultLocale('en');
        $this->resourcesMessages->load();
        
        $this->assertSame('User', $this->resourcesMessages->getMessage('M000'));
        
        $this->resourcesMessages->setDefaultLocale('fr');
        $this->resourcesMessages->load();
        
        $this->assertSame('Utilisateur', $this->resourcesMessages->getMessage('M000'));
    }
}