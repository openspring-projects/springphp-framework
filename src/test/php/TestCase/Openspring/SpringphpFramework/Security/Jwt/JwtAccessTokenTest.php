<?php

namespace TestCase\Openspring\SpringphpFramework\Security\Jwt;

use Openspring\SpringphpFramework\Context\ApplicationContext;
use Openspring\SpringphpFramework\Exception\Http\UnauthorizedException;
use Openspring\SpringphpFramework\Security\Jwt\JwtAccessToken;
use Openspring\SpringphpFramework\Security\Jwt\Core\Jwt;
use Openspring\SpringphpFramework\Security\Jwt\Enumeration\TokenElement;
use Openspring\SpringphpFramework\Security\Jwt\Enumeration\TokenPayload;
use Openspring\SpringphpFramework\Security\Jwt\Enumeration\TokenType;
use PHPUnit\Framework\TestCase;
use TestCase\Openspring\SpringphpFramework\Helper\JwtData;
use TestCase\Openspring\SpringphpFramework\Helper\StaticJwtParameterProvider;

final class JwtAccessTokenTest extends TestCase
{
    private $jwtAccessToken;
    private $jwt;
    private $clientId;

    public static function setUpBeforeClass(): void
    {
        ApplicationContext::addBeanSingleton(new StaticJwtParameterProvider());
    }

    public function setUp(): void
    {
        $this->clientId = 'store-front';
        $userClaims = array(
            'nonce' => '00000',
            'state' => '1111',
            'session_state' => '1111');

        $this->setJwtAccessToken(new JwtAccessToken($userClaims));
        $this->setJwt(new Jwt());
    }

    public function setJwt(Jwt $jwt): void
    {
        $this->jwt = $jwt;
    }

    public function setJwtAccessToken(JwtAccessToken $jwtAccessToken): void
    {
        $this->jwtAccessToken = $jwtAccessToken;
    }

    public function testCreateAccessToken()
    {
        $clientId = 'store-front';
        $userId = 'cffd1f6545930cfd2e54fc6b161dcbbe91ff2f59';

        $tokenArray = $this->jwtAccessToken->createAccessToken($clientId, $userId);

        $this->assertArrayHasKey(TokenElement::ACCESS_TOKEN, $tokenArray);
        $this->assertArrayHasKey(TokenElement::EXPIRES_IN, $tokenArray);
        $this->assertArrayHasKey(TokenElement::REFRESH_TOKEN, $tokenArray);
        $this->assertArrayHasKey(TokenElement::REFRESH_EXPIRES_IN, $tokenArray);

        $payload = $this->jwt->decode($tokenArray[TokenElement::ACCESS_TOKEN], JwtData::PUBLIC_KEY_1024);

        $this->assertEquals($this->clientId, $payload['sub']);
        $this->assertEquals($userId, $payload['uid']);
        $this->assertEquals(TokenType::BEARER, $payload[TokenElement::TOKEN_TYPE]);

        $this->assertArrayHasKey(TokenPayload::AUD, $payload);
        $this->assertArrayHasKey(TokenPayload::SUB, $payload);
        $this->assertArrayHasKey(TokenPayload::ISS, $payload);
        $this->assertArrayHasKey(TokenPayload::IAT, $payload);
        $this->assertArrayHasKey(TokenPayload::ID, $payload);
    }

    public function testVerityTokenPayloadIntegrity_OK()
    {
        $clientId = 'store-front';
        $userId = 'cffd1f6545930cfd2e54fc6b161dcbbe91ff2f59';

        // config accesstoken
        $userClaims = array(
            'nonce' => '00000',
            'state' => '1111',
            'session_state' => '1111');

        $this->setJwtAccessToken(new JwtAccessToken($userClaims));

        $tokenArray = $this->jwtAccessToken->createAccessToken($clientId, $userId);

        $this->assertArrayHasKey(TokenElement::ACCESS_TOKEN, $tokenArray);
        $this->assertArrayHasKey(TokenElement::EXPIRES_IN, $tokenArray);
        $this->assertArrayHasKey(TokenElement::REFRESH_TOKEN, $tokenArray);
        $this->assertArrayHasKey(TokenElement::REFRESH_EXPIRES_IN, $tokenArray);

        $payload = $this->jwt->decode($tokenArray[TokenElement::ACCESS_TOKEN], JwtData::PUBLIC_KEY_1024);

        $this->assertEquals($this->clientId, $payload['sub']);
        $this->assertEquals($userId, $payload['uid']);
        $this->assertEquals(TokenType::BEARER, $payload[TokenElement::TOKEN_TYPE]);

        $this->assertArrayHasKey(TokenPayload::AUD, $payload);
        $this->assertArrayHasKey(TokenPayload::SUB, $payload);
        $this->assertArrayHasKey(TokenPayload::ISS, $payload);
        $this->assertArrayHasKey(TokenPayload::IAT, $payload);
        $this->assertArrayHasKey(TokenPayload::ID, $payload);

        $result = $this->jwtAccessToken->verifyTokenPayload($payload);
        $this->assertTrue($result);
    }

    public function testVerityTokenPayloadIntegrity_KO()
    {
        $clientId = 'store-client-2';
        $userId = 'cffd1f6545930cfd2e54fc6b161dcbbe91ff2f59';

        // config accesstoken
        $userClaims = array(
            'nonce' => '00000',
            'state' => '1111',
            'session_state' => '1111');

        $this->setJwtAccessToken(new JwtAccessToken($userClaims));

        $tokenArray = $this->jwtAccessToken->createAccessToken($clientId, $userId);

        $this->assertArrayHasKey(TokenElement::ACCESS_TOKEN, $tokenArray);
        $this->assertArrayHasKey(TokenElement::EXPIRES_IN, $tokenArray);
        $this->assertArrayHasKey(TokenElement::REFRESH_TOKEN, $tokenArray);
        $this->assertArrayHasKey(TokenElement::REFRESH_EXPIRES_IN, $tokenArray);

        $payload = $this->jwt->decode($tokenArray[TokenElement::ACCESS_TOKEN], JwtData::PUBLIC_KEY_1024);

        $this->assertEquals($clientId, $payload['sub']);
        $this->assertEquals($userId, $payload['uid']);
        $this->assertEquals(TokenType::BEARER, $payload[TokenElement::TOKEN_TYPE]);

        $this->assertArrayHasKey(TokenPayload::AUD, $payload);
        $this->assertArrayHasKey(TokenPayload::SUB, $payload);
        $this->assertArrayHasKey(TokenPayload::ISS, $payload);
        $this->assertArrayHasKey(TokenPayload::IAT, $payload);
        $this->assertArrayHasKey(TokenPayload::ID, $payload);

        $this->expectException(UnauthorizedException::class);
        $this->jwtAccessToken->verifyTokenPayload($payload);
    }
}