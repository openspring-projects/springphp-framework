<?php

namespace TestCase\Openspring\SpringphpFramework\Security\Jwt;

use Openspring\SpringphpFramework\Context\ApplicationContext;
use Openspring\SpringphpFramework\Security\Jwt\JwtIdToken;
use Openspring\SpringphpFramework\Security\Jwt\Contract\IdToken;
use Openspring\SpringphpFramework\Security\Jwt\Core\Jwt;
use Openspring\SpringphpFramework\Security\Jwt\Enumeration\TokenElement;
use Openspring\SpringphpFramework\Security\Jwt\Enumeration\TokenPayload;
use Openspring\SpringphpFramework\Security\Jwt\Enumeration\TokenType;
use PHPUnit\Framework\TestCase;
use TestCase\Openspring\SpringphpFramework\Helper\JwtData;
use TestCase\Openspring\SpringphpFramework\Helper\StaticJwtParameterProvider;

final class JwtIdTokenTest extends TestCase
{
    private $idToken;
    private $jwt;

    public static function setUpBeforeClass(): void
    {
        ApplicationContext::addBeanSingleton(new StaticJwtParameterProvider());
    }
    
    public function setUp(): void
    {
        $userClaims = array(
            'nonce' => '00000',
            'state' => '1111',
            'session_state' => '1111');

        $this->setIdToken(new JwtIdToken($userClaims));
        $this->setJwt(new Jwt());
    }

    public function setJwt(Jwt $jwt): void
    {
        $this->jwt = $jwt;
    }

    public function setIdToken(IdToken $idToken): void
    {
        $this->idToken = $idToken;
    }

    public function testCreateIdToken()
    {
        $clientId = 'store-front';
        $userId = 'cffd1f6545930cfd2e54fc6b161dcbbe91ff2f59';

        $idToken = $this->idToken->createIdToken($clientId, $userId);
        $this->assertNotFalse($idToken);

        $playload = $this->jwt->decode($idToken, JwtData::PUBLIC_KEY_1024);

        $this->assertEquals($userId, $playload['sub']);
        $this->assertEquals(TokenType::ID, $playload[TokenElement::TOKEN_TYPE]);

        $this->assertArrayHasKey(TokenPayload::AUD, $playload);
        $this->assertArrayHasKey(TokenPayload::SUB, $playload);
        $this->assertArrayHasKey(TokenPayload::ISS, $playload);
        $this->assertArrayHasKey(TokenPayload::IAT, $playload);
        $this->assertArrayHasKey(TokenPayload::ID, $playload);

        $this->assertArrayHasKey('nonce', $playload);
        $this->assertArrayHasKey('state', $playload);
        $this->assertArrayHasKey('session_state', $playload);
    }
}