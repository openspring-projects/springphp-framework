<?php
namespace TestCase\Openspring\SpringphpFramework\Security\Jwt;

use Openspring\SpringphpFramework\Security\Jwt\Core\Jwt;
use Openspring\SpringphpFramework\Security\Jwt\Enumeration\EncryptionAlgorithm;
use PHPUnit\Framework\TestCase;
use TestCase\Openspring\SpringphpFramework\Helper\JwtData;

final class JwtTest extends TestCase
{
    private $jwt;

    public function setUp(): void
    {
        $this->setJwt(new Jwt());
    }

    public function setJwt(Jwt $jwt): void
    {
        $this->jwt = $jwt;
    }

    public function testEncodeDecodeAlgoHSXXX()
    {
        $payload = array('name' => 'khalid','age' => 36);
        
        // HS256
        $encoded = $this->jwt->encode($payload, JwtData::SECRET_KEY, EncryptionAlgorithm::HS256);
        $decoded = $this->jwt->decode($encoded, JwtData::SECRET_KEY);
        $this->assertEquals($payload, $decoded);
        
        // HS384
        $encoded = $this->jwt->encode($payload, JwtData::SECRET_KEY, EncryptionAlgorithm::HS384);
        $decoded = $this->jwt->decode($encoded, JwtData::SECRET_KEY);
        $this->assertEquals($payload, $decoded);
        
        // HS512
        $encoded = $this->jwt->encode($payload, JwtData::SECRET_KEY, EncryptionAlgorithm::HS512);
        $decoded = $this->jwt->decode($encoded, JwtData::SECRET_KEY);
        $this->assertEquals($payload, $decoded);
    }
    
    public function testEncodeDecodeAlgoRSXXX()
    {
        $payload = array('name' => 'khalid','age' => 36);
        
        // RS256
        $encoded = $this->jwt->encode($payload, JwtData::PRIVATE_KEY_512, EncryptionAlgorithm::RS256);
        $decoded = $this->jwt->decode($encoded, JwtData::PUBLIC_KEY_512);
        $this->assertEquals($payload, $decoded);
        
        // RS384
        $encoded = $this->jwt->encode($payload, JwtData::PRIVATE_KEY_1024, EncryptionAlgorithm::RS384);
        $decoded = $this->jwt->decode($encoded, JwtData::PUBLIC_KEY_1024);
        $this->assertEquals($payload, $decoded);
        
        // RS512
        $encoded = $this->jwt->encode($payload, JwtData::PRIVATE_KEY_1024, EncryptionAlgorithm::RS512);
        $decoded = $this->jwt->decode($encoded, JwtData::PUBLIC_KEY_1024);
        $this->assertEquals($payload, $decoded);
    }

    public function testEncodeDecodeUrlSafeBase64()
    {
        $url = 'https://stackoverflow.com/clients/khalid elabbadi/être à root +-* $';
        $encodeUrl = $this->jwt->encodeUrlSafeBase64($url);
        $decodedUrl = $this->jwt->decodeUrlSafeBase64($encodeUrl);

        $this->assertEquals($url, $decodedUrl);
    }
}