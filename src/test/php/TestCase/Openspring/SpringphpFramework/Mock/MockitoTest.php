<?php
namespace TestCase\Openspring\SpringphpFramework\Mock;

use Openspring\SpringphpFramework\Context\ApplicationContext;
use Openspring\SpringphpFramework\Mock\Mockito;
use Openspring\SpringphpFramework\Mock\MockitoStore;
use TestCase\Openspring\SpringphpFramework\Helper\SpringphpTestRunner;
use PHPUnit\Framework\TestCase;

class MockitoTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        SpringphpTestRunner::setSpringphpContext(true);
    }

    public function testBeanExistence()
    {
        $this->assertNotNull(ApplicationContext::getBeanOf(MockitoStore::class));
    }

    public function testWhenReturnValue()
    {
        $clients = array('khalid','rachid');
        Mockito::when('kh0')->thenReturn($clients);

        $this->assertSame($clients, Mockito::get('kh0'));
        $this->assertNull(Mockito::get('kh1'));
    }
}