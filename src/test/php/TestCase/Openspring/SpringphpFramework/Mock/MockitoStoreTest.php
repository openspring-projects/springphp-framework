<?php
namespace TestCase\Openspring\SpringphpFramework\Mock;

use Openspring\SpringphpFramework\Enumeration\MockitoCategoryType;
use Openspring\SpringphpFramework\Enumeration\ObjectType;
use Openspring\SpringphpFramework\Mock\MockitoStore;
use Openspring\SpringphpFramework\Type\Dictionary;
use PHPUnit\Framework\TestCase;

class MockitoStoreTest extends TestCase
{
    private $mockito = null;

    public function setUp(): void
    {
        if ($this->mockito == null)
        {
            $this->mockito = new MockitoStore();
        }
    }

    public function testInstantiation()
    {
        $this->assertNotNull($this->mockito);
    }

    public function testGetStore()
    {
        $this->assertInstanceOf(Dictionary::class, $this->mockito->getStore());
    }

    public function testWhenReturn()
    {
        $clients = array('khalid','rachid');
        $this->mockito
             ->forKey('select * from client')
             ->returnValue($clients, MockitoCategoryType::VALUE);

        $melement = $this->mockito->findByKey('select * from client');

        $this->assertEquals(ObjectType::ARRAY, $melement->getType());
        $this->assertSame($clients, $melement->getValue());
    }
}