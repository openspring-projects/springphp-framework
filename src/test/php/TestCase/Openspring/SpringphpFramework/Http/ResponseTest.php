<?php

namespace TestCase\Openspring\SpringphpFramework\Http;

use Openspring\SpringphpFramework\Enumeration\MediaType;
use Openspring\SpringphpFramework\Exception\Http\MediaTypeNotAcceptableException;
use Openspring\SpringphpFramework\Http\HttpMethod;
use Openspring\SpringphpFramework\Http\HttpStatus;
use Openspring\SpringphpFramework\Http\Request;
use Openspring\SpringphpFramework\Http\Response;
use Openspring\SpringphpFramework\Http\ResponseEntity;
use PHPUnit\Framework\TestCase;
use TestCase\Openspring\SpringphpFramework\Helper\SpringphpTestRunner;

/**
 * Test class of Response class
 *
 * @author Khalid ELABBADI
 * @email kha.elabbadi@gmail.com
 * @community openspring
 */
class ResponseTest extends TestCase
{

    public function setUp(): void
    {
        SpringphpTestRunner::setSpringphpContext();
        SpringphpTestRunner::runHttpMethod(HttpMethod::POST, 'esapi/account/update');
    }

    public function testGetHttpStatus()
    {
        $this->assertEquals(HttpStatus::OK, Response::getHttpStatus());
    }

    public function testSetHttpStatus()
    {
        Response::setHttpStatus(HttpStatus::NOT_FOUND);
        $this->assertEquals(HttpStatus::NOT_FOUND, Response::getHttpStatus());
    }

    public function testSetMediaType()
    {
        $this->assertEquals(MediaType::APPLICATION_JSON_VALUE, Response::getMediaType());

        Response::setMediaType(MediaType::TEXT_HTML_VALUE);
        $this->assertSame(MediaType::TEXT_HTML_VALUE, Response::getMediaType());
    }

    public function testIsMediaTypeNative()
    {
        Response::setMediaType(MediaType::TEXT_HTML_VALUE);
        $this->assertTrue(Response::isMediaTypeTextHTML());
    }

    public function testGetMediaType()
    {
        Response::setMediaType(MediaType::TEXT_HTML_VALUE);
        $this->assertSame(MediaType::TEXT_HTML_VALUE, Response::getMediaType());
    }

    public function testSetObject()
    {
        Response::setBody('khalid');
        $this->assertSame('khalid', Response::getBody());
    }

    public function testGetObject()
    {
        Response::setBody('khalid');
        $this->assertSame('khalid', Response::getBody());
    }

    public function testSetResponseEntity()
    {
        Response::setResponseEntity(new ResponseEntity('khalid', HttpStatus::CREATED));
        $this->assertEquals(HttpStatus::CREATED, Response::getHttpStatus());
        $this->assertSame('khalid', Response::getBody());
    }

    public function testCreated()
    {
        Response::created('khalid');
        $this->assertEquals(HttpStatus::CREATED, Response::getHttpStatus());
        $this->assertSame('khalid', Response::getBody());
    }

    public function testOk()
    {
        Response::ok('khalid');
        $this->assertEquals(HttpStatus::OK, Response::getHttpStatus());
        $this->assertSame('khalid', Response::getBody());
    }

    public function testBadRequest()
    {
        Response::badRequest('khalid');
        $this->assertEquals(HttpStatus::BAD_REQUEST, Response::getHttpStatus());
        $this->assertSame('khalid', Response::getBody());
    }

    public function testUnauthorized()
    {
        Response::unauthorized('khalid');
        $this->assertEquals(HttpStatus::UNAUTHORIZED, Response::getHttpStatus());
        $this->assertSame('khalid', Response::getBody());
    }

    public function testForbidden()
    {
        Response::forbidden('khalid');
        $this->assertEquals(HttpStatus::FORBIDDEN, Response::getHttpStatus());
        $this->assertSame('khalid', Response::getBody());
    }

    public function testNotFound()
    {
        Response::notFound('khalid');
        $this->assertEquals(HttpStatus::NOT_FOUND, Response::getHttpStatus());
        $this->assertSame('khalid', Response::getBody());
    }

    public function testSetResponse()
    {
        Response::setResponse('khalid', HttpStatus::OK);

        $this->assertEquals(HttpStatus::OK, Response::getHttpStatus());
        $this->assertEquals('khalid', Response::getBody());
    }

    public function testRender_UnsupportedMediaTypeException()
    {
        Request::setAccept(MediaType::APPLICATION_JSON_VALUE);
        Response::setMediaType(MediaType::APPLICATION_ATOM_XML_VALUE);
        Response::setResponseEntity(new ResponseEntity('Hello'));

        $this->expectException(MediaTypeNotAcceptableException::class);
        Response::render();
    }

    public function testRender_MediaTypeNotAcceptableException()
    {
        Request::setContentType(MediaType::APPLICATION_PDF_VALUE);
        Response::setMediaType(MediaType::APPLICATION_PDF_VALUE);
        Response::setResponseEntity(new ResponseEntity('Hello'));

        $this->expectException(MediaTypeNotAcceptableException::class);
        Response::render();
    }

    public function testRender_Json_OK()
    {
        Request::setContentType(MediaType::APPLICATION_JSON_VALUE);
        Response::setMediaType(MediaType::APPLICATION_JSON_VALUE);
        Response::setResponseEntity(new ResponseEntity('<b>Hello</b>'));

        $this->expectOutputString('"<b>Hello<\/b>"');
        Response::render();
    }

    public function testRender_HTML_OK()
    {
        Request::setContentType(MediaType::TEXT_HTML_VALUE);
        Response::setMediaType(MediaType::TEXT_HTML_VALUE);
        Response::setResponseEntity(new ResponseEntity('<b>Hello</b>'));

        $this->expectOutputString('<b>Hello</b>');
        Response::render();
    }
}