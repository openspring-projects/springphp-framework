<?php
namespace TestCase\Openspring\SpringphpFramework\Http;

use Openspring\SpringphpFramework\Http\HttpStatusCategory;
use Openspring\SpringphpFramework\Http\HttpStatus;
use Openspring\SpringphpFramework\Http\ResponseEntity;
use Openspring\SpringphpFramework\Type\Any;
use PHPUnit\Framework\TestCase;

class ResponseEntityTest extends TestCase
{
    private $responseEntity;
    
    public function setUp(): void
    {
        $body = new Any();
        $body->name = 'khalid';
        
        $this->responseEntity = new ResponseEntity($body, HttpStatus::NOT_FOUND);
    }

    public function testGetStatusCategory()
    {
        $this->assertSame(HttpStatusCategory::ERROR, HttpStatusCategory::getTypeOf($this->responseEntity->getStatusCategory()));
    }

    public function testIsInformational()
    {
        $this->assertFalse($this->responseEntity->isInformational());
        $this->responseEntity->setHttpStatusCode(HttpStatus::SWITCHING_PROTOCOLS);
        $this->assertTrue($this->responseEntity->isInformational());
    }

    public function testIsSuccess()
    {
        $this->assertFalse($this->responseEntity->isSuccess());
        $this->responseEntity->setHttpStatusCode(HttpStatus::OK);
        $this->assertTrue($this->responseEntity->isSuccess());
    }

    public function testIsRedirection()
    {
        $this->assertFalse($this->responseEntity->isRedirection());
        $this->responseEntity->setHttpStatusCode(HttpStatus::PERMANENT_REDIRECT);
        $this->assertTrue($this->responseEntity->isRedirection());
    }

    public function testIsClientError()
    {
        $this->assertTrue($this->responseEntity->isClientError());
        $this->responseEntity->setHttpStatusCode(HttpStatus::OK);
        $this->assertFalse($this->responseEntity->isClientError());
    }

    public function testIsServerError()
    {
        $this->assertFalse($this->responseEntity->isServerError());
        $this->responseEntity->setHttpStatusCode(HttpStatus::INTERNAL_SERVER_ERROR);
        $this->assertTrue($this->responseEntity->isServerError());
    }

    public function testGetHttpStatusCode()
    {
        $this->assertSame(HttpStatus::NOT_FOUND, $this->responseEntity->getHttpStatusCode());
        $this->responseEntity->setHttpStatusCode(HttpStatus::BAD_REQUEST);
        $this->assertSame(HttpStatus::BAD_REQUEST, $this->responseEntity->getHttpStatusCode());
        $this->responseEntity->setHttpStatusCode(HttpStatus::OK);
        $this->assertSame(HttpStatus::OK, $this->responseEntity->getHttpStatusCode());
        $this->responseEntity->setHttpStatusCode(HttpStatus::CREATED);
        $this->assertSame(HttpStatus::CREATED, $this->responseEntity->getHttpStatusCode());
    }

    public function testGetBody()
    {
        $body = new Any();
        $body->name = 'khalid';
        
        $this->assertEquals($body, $this->responseEntity->getBody());
    }

    public function testSetBody()
    {
        $body = new Any();
        $body->name = 'khalid';
        
        $this->assertEquals($body, $this->responseEntity->getBody());
        
        $body->name = 'rachid';
        $this->responseEntity->setBody($body);
        
        $this->assertEquals($body, $this->responseEntity->getBody());        
    }

    public function testCast()
    {
        $this->assertInstanceOf(ResponseEntity::class, ResponseEntity::cast($this->responseEntity));
    }
}