<?php
namespace TestCase\Openspring\SpringphpFramework\Http;

use Openspring\SpringphpFramework\Exception\InvalidValueException;
use Openspring\SpringphpFramework\Http\HttpMethod;
use Openspring\SpringphpFramework\Http\Request;
use TestCase\Openspring\SpringphpFramework\Helper\SpringphpTestRunner;
use Openspring\SpringphpFramework\Enumeration\MediaType;
use PHPUnit\Framework\TestCase;

class RequestTest extends TestCase
{
    public function setUp(): void
    {
        SpringphpTestRunner::setSpringphpContext();
        SpringphpTestRunner::runHttpMethod(HttpMethod::PUT, 'esapi/account/update', true);
    }

    public function testGetMethod()
    {
        $this->assertSame(HttpMethod::PUT, Request::getMethod());
    }

    public function testSetMediaType()
    {
        Request::setContentType(MediaType::APPLICATION_JSON_VALUE);
        $this->assertSame(MediaType::APPLICATION_JSON_VALUE, Request::getContentType());
        
        $this->expectException(InvalidValueException::class);
        Request::setContentType('PNG');
    }

    public function testGetParameters()
    {
        $this->assertEquals($_REQUEST, Request::getParameters());
    }

    public function testGetPostParameters()
    {
        $this->assertEquals($_POST, Request::getPostParameters());
    }

    public function testGetGetParameters()
    {
        $this->assertEquals($_GET, Request::getGetParameters());
    }

    public function testGetCookieParameters()
    {
        $this->assertEquals($_COOKIE, Request::getCookieParameters());
    }

    public function testSetCookieParameter()
    {
        //TODO: to be tested
        
        /*Request::setCookieParameter('login', 'khalid');
        $_COOKIE['login'] = 'khalid';
        
        $this->assertSame('khalid', Request::getCookieParameter('login'));*/
		
		$this->assertTrue(true);
    }

    public function testGetAnyParameter()
    {
        $_GET['login'] = 'khalid';
        
        $this->assertSame('khalid', Request::getAnyParameter('login'));
    }

    public function testGetParameter()
    {
        $_REQUEST['login'] = 'khalid';
        
        $this->assertSame('khalid', Request::getParameter('login'));
    }

    public function testGetPostGetParameter()
    {
        $_GET['login'] = 'khalid';
        $_POST['login'] = 'rachid';
        
        $this->assertSame('rachid', Request::getPostGetParameter('login'));
    }

    public function testGetGetPostParameter()
    {
        $_GET['login'] = 'khalid';
        $_POST['login'] = 'rachid';
        
        $this->assertSame('khalid', Request::getGetPostParameter('login'));
    }

    public function testGetPostParameter()
    {
        $_POST['login'] = 'khalid';
        
        $this->assertSame('khalid', Request::getPostParameter('login'));
        $this->assertSame('toto', Request::getPostParameter('name', 'toto'));
    }

    public function testGetGetParameter()
    {
        $_GET['login'] = 'khalid';
        
        $this->assertSame('khalid', Request::getGetParameter('login'));
        $this->assertSame('toto', Request::getGetParameter('name', 'toto'));
    }

    public function testGetCookieParameter()
    {
        $_COOKIE['login'] = 'khalid';
        
        $this->assertSame('khalid', Request::getCookieParameter('login'));
        $this->assertSame('toto', Request::getCookieParameter('name', 'toto'));
    }

    public function testGetRequestBody()
    {
        //TODO: to test
		$this->assertTrue(true);
    }

    public function testGetRequestBodyParameter()
    {
        //TODO: to test
		$this->assertTrue(true);
    }

    public function testGetHeaderParameter()
    {
        //TODO: to test
        
        /*$_SERVER['Authorization'] = 'Bearer w6p0cmUgc3ByaW5ncGhw';
        
        $this->assertSame('Bearer w6p0cmUgc3ByaW5ncGhw', Request::getHeaderParameter('Authorization'));
        $this->assertSame(null, Request::getHeaderParameter('Authorization', null));*/
		
		$this->assertTrue(true);
    }
}