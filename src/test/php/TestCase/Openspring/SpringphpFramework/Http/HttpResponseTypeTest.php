<?php
namespace TestCase\Openspring\SpringphpFramework\Http;

use Openspring\SpringphpFramework\Http\HttpStatus;
use Openspring\SpringphpFramework\Http\HttpStatusCategory;
use PHPUnit\Framework\TestCase;

class HttpResponseTypeTest extends TestCase
{
    public function testGetTypeOf()
    {
        $this->assertSame(HttpStatusCategory::INFO, HttpStatusCategory::getTypeOf(1));
        $this->assertSame(HttpStatusCategory::SUCCESS, HttpStatusCategory::getTypeOf(2));
        $this->assertSame(HttpStatusCategory::REDIRECT, HttpStatusCategory::getTypeOf(3));
        $this->assertSame(HttpStatusCategory::ERROR, HttpStatusCategory::getTypeOf(4));
        $this->assertSame(HttpStatusCategory::ERROR, HttpStatusCategory::getTypeOf(5));
    }

    public function testGetStatusCategory()
    {
        $this->assertSame(5, HttpStatusCategory::getStatusCategory(HttpStatus::INTERNAL_SERVER_ERROR));
        $this->assertSame(4, HttpStatusCategory::getStatusCategory(HttpStatus::BAD_REQUEST));
        $this->assertSame(3, HttpStatusCategory::getStatusCategory(HttpStatus::MULTIPLE_CHOICES));
        $this->assertSame(2, HttpStatusCategory::getStatusCategory(HttpStatus::CREATED));
        $this->assertSame(1, HttpStatusCategory::getStatusCategory(HttpStatus::SWITCHING_PROTOCOLS));
    }
}