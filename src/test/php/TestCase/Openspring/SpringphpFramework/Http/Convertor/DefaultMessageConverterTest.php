<?php

namespace TestCase\Openspring\SpringphpFramework\Http;

use Openspring\SpringphpFramework\Http\Converter\TextHTMLMessageConverter;
use TestCase\Openspring\SpringphpFramework\Helper\SpringphpTestRunner;
use Openspring\SpringphpFramework\Enumeration\MediaType;
use PHPUnit\Framework\TestCase;

class DefaultMessageConverterTest extends TestCase
{
	public function setUp(): void
    {
        SpringphpTestRunner::setSpringphpContext();
    }
	
    public function testReadWrite()
    {
		$html = '<strong>toto</strong>';
		$dmConv = new TextHTMLMessageConverter();
		
        $this->assertEquals($html, $dmConv->read($html));
		$this->assertEquals($html, $dmConv->write($html));
    }
	
	public function testGetMediaType()
    {
		$dmConv = new TextHTMLMessageConverter();
		$this->assertEquals(MediaType::TEXT_HTML_VALUE, $dmConv->getMediaType());
    }
}