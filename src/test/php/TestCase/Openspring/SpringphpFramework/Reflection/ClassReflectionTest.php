<?php

namespace TestCase\Openspring\SpringphpFramework\Log;

use Openspring\SpringphpFramework\Annotation\RequestMapping\RequestMapping;
use Openspring\SpringphpFramework\Context\Environment;
use Openspring\SpringphpFramework\Core\Stereotype\Annotations;
use Openspring\SpringphpFramework\Enumeration\FieldAnnotation;
use Openspring\SpringphpFramework\Enumeration\MediaType;
use Openspring\SpringphpFramework\Exception\InvalidArgumentException;
use Openspring\SpringphpFramework\Http\HttpMethod;
use Openspring\SpringphpFramework\Reflection\ClassReflection;
use Openspring\SpringphpFramework\Type\ClassInfo;
use Openspring\SpringphpFramework\Type\ClassMethod;
use PHPUnit\Framework\TestCase;
use TestCase\Openspring\SpringphpFramework\Helper\HunnyBean;
use TestCase\Openspring\SpringphpFramework\Helper\IPerson;
use TestCase\Openspring\SpringphpFramework\Helper\SpringphpTestRunner;
use TestCase\Openspring\SpringphpFramework\Helper\TotoBean;
use TestCase\Openspring\SpringphpFramework\Helper\DI\BuyController;
use TestCase\Openspring\SpringphpFramework\Helper\DI\TestController;
use TestCase\Openspring\SpringphpFramework\Helper\DI\Service\ClientService;
use TestCase\Openspring\SpringphpFramework\Helper\Mock\Application\Resource\UserController;
use TestCase\Openspring\SpringphpFramework\Reflection\Helper\DummyClass;
use TestCase\Openspring\SpringphpFramework\Reflection\Helper\Parameter;
use TestCase\Openspring\SpringphpFramework\Reflection\Helper\Person;

class ClassReflectionTest extends TestCase
{
    private $classFile = '';

    public function setUp(): void
    {
        SpringphpTestRunner::setSpringphpContext();
        $this->classFile = ClassReflection::getClassFile(UserController::class);
    }

    public function testGetAllAnnotationsKeys()
    {
        $allAnnots = ClassReflection::getAllAnnotationsKeys(TestController::class, FieldAnnotation::toArray());
        $this->assertEquals(FieldAnnotation::toArray(), $allAnnots);
    }

    public function testGetAnnotationName()
    {
        $this->assertEquals('Var', ClassReflection::getAnnotationsName('@var ClientService'));
        $this->assertEquals('Var', ClassReflection::getAnnotationsName('@var '));
        $this->assertEquals('Var', ClassReflection::getAnnotationsName('@var'));

        $this->assertEquals('Autowired', ClassReflection::getAnnotationsName('@Autowired()'));
        $this->assertEquals('Autowired', ClassReflection::getAnnotationsName('@Autowired(true)'));
        $this->assertEquals('Autowired', ClassReflection::getAnnotationsName('@Autowired(false)'));

        $this->assertEquals('Autowired', ClassReflection::getAnnotationsName('@Autowired(false) '));
        $this->assertEquals('Autowired', ClassReflection::getAnnotationsName('@Autowired(false) abc'));
        $this->assertEquals('Autowired', ClassReflection::getAnnotationsName('@Autowired(false) drdr'));
    }

    public function testGetAnnotatedFileds()
    {
        $clientService0VarAutowired0 = array(
            'Var' => ClientService::class,
            'Autowired' => 'true');
        $clientService2Autowired = array(
            'Autowired' => 'true');
        $clientService1Var = array(
            'Var' => ClientService::class);
        $appName0ValueVar = array(
            'Var' => 'String',
            'Value' => '${app.name}');

        $controller = new TestController();
        $vars = ClassReflection::getAnnotatedFileds(get_class($controller));

        $this->assertEquals($clientService0VarAutowired0, $vars['clientService0VarAutowired0']);
        $this->assertEquals($clientService1Var, $vars['clientService1Var']);
        $this->assertEquals($clientService2Autowired, $vars['clientService2Autowired']);
        $this->assertEquals($clientService0VarAutowired0, $vars['clientService3VarAutowired']);

        $this->assertEquals([], $vars['clientService4None']);
        $this->assertEquals([], $vars['clientService5None']);
        $this->assertSame([], $vars['clientService6None']);

        $this->assertEquals($appName0ValueVar, $vars['appName0ValueVar']);
    }

    public function testDI()
    {
        $controller = new BuyController();

        $this->assertNotNull($controller->clientService);
        $this->assertNotNull($controller->commandService);
        $this->assertNotNull($controller->providerService);
        $this->assertNotNull($controller->sellService);

        $this->assertEquals(Environment::getApplicationName(), $controller->applicationName);
        $this->assertEquals(Environment::getApplicationName(), $controller->sellService->applicationName);

        $this->assertEquals('khalid', $controller->getName());
        $this->assertEquals('MS', $controller->getLabel());
        $this->assertEquals(15000, $controller->getTotal());

        $this->assertEquals('khalid', $controller->sellService->getName());
        $this->assertEquals('MS', $controller->sellService->getLabel());
        $this->assertEquals(15000, $controller->sellService->getTotal());
    }

    public function testCleanDoc()
    {
        $expected = 'cococo from top * ' . PHP_EOL . '@ComponentScan({"baseDirs": ["a/b", "c/d"], "excludeDirs"= ["a/b", "c/d"] }) ' . PHP_EOL . '@var string ' . PHP_EOL . '@Autowired(true) Hello from here ';
        $doc = '/** cococo from top ' . '* @ComponentScan({' . PHP_EOL . '*                     "baseDirs": ["a/b", "c/d"],' . PHP_EOL . '*                     "excludeDirs"= ["a/b", "c/d"]' . PHP_EOL . '*                 })' . PHP_EOL . '* @var string' . PHP_EOL . '* @Autowired(true)' . PHP_EOL . '* Hello from here' . PHP_EOL . '*/';
        $cleanDoc = ClassReflection::cleanDoc($doc);

        $this->assertEquals($expected, $cleanDoc);
    }

    public function testGetDocAnnotationsDocInMiddle()
    {
        $aconfis = Annotations::getAllAnnotations();

        $doc1 = '/** This method send emails in masse ' . '* @PropertySource("file:a/b/c")' . PHP_EOL . '* @ComponentScan({' . PHP_EOL . '*                     "baseDirs": ["a/b", "c/d"],' . PHP_EOL . '*                     "excludeDirs": ["a/b", "c/d"]' . PHP_EOL . '*                 })' . PHP_EOL . '* @Autowired()' . PHP_EOL . '* @var string' . PHP_EOL . '* @Value("${app.name}")' . PHP_EOL . '* NB: never pass invalid emails to this function' . PHP_EOL . '*/';

        $annots = ClassReflection::getDocAnnotations($doc1, $aconfis);

        $this->assertEquals('@Value("${app.name}")', $annots['Value']->passedParams);
        $this->assertEquals('@Autowired()', $annots['Autowired']->passedParams);
        $this->assertEquals('@PropertySource("file:a/b/c")', $annots['PropertySource']->passedParams);
        $this->assertEquals('@ComponentScan({ "baseDirs": ["a/b", "c/d"], "excludeDirs": ["a/b", "c/d"] })', $annots['ComponentScan']->passedParams);
        $this->assertEquals('@var string', $annots['Var']->passedParams);
    }

    public function testGetDocAnnotationsDocAtEnd()
    {
        $aconfis = Annotations::getAllAnnotations();

        $doc2 = '/**' . PHP_EOL . '* Inject service client' . PHP_EOL . '* @var ClientService' . PHP_EOL . '*/';

        $annots = ClassReflection::getDocAnnotations($doc2, $aconfis);

        $this->assertEquals('@var ClientService', $annots['Var']->passedParams);
    }

    public function testGetDocAnnotationsDocAtFirst()
    {
        $aconfis = Annotations::getAllAnnotations();

        $doc2 = '/**' . PHP_EOL . '* @var ClientService' . PHP_EOL . '* Inject service client' . PHP_EOL . '*/';

        $annots = ClassReflection::getDocAnnotations($doc2, $aconfis);

        $this->assertEquals('@var ClientService Inject service client', $annots['Var']->passedParams);
    }

    public function testIsPropertyPrivatePublic()
    {
        $toto = new TotoBean();

        $this->assertTrue(ClassReflection::isPropertyPrivate($toto, 'name'));
        $this->assertTrue(ClassReflection::isPropertyPublic($toto, 'age'));
    }

    public function testImplements()
    {
        $this->assertTrue(ClassReflection::implements(TotoBean::class, IPerson::class));
    }

    public function testParseClass()
    {
        $classInfo = ClassReflection::parseClass(UserController::class);

        $this->assertEquals('UserController', $classInfo->getName());
        $this->assertEquals(UserController::class, $classInfo->getQualifiedName());
        $this->assertEquals('/** User controller */', $classInfo->getComments());
        $this->assertCount(4, $classInfo->getMethods());

        $classMethod_1 = ClassInfo::castToClassMethod($classInfo->getMethods()[1]);

        $this->assertEquals('editMyProfile', $classMethod_1->getName());
        $this->assertCount(1, $classMethod_1->getAnnotations());
        $this->assertCount(1, $classMethod_1->getParameters());

        $methodParameter_0 = ClassMethod::castToMethodParameter($classMethod_1->getParameters()[0]);
        $requestMapping_0 = ClassMethod::castToRequestMapping($classMethod_1->getAnnotations()[0]);

        $this->assertEquals('$user_id', $methodParameter_0->getName());
        $this->assertEquals(MediaType::APPLICATION_JSON_VALUE, $requestMapping_0->getConsumes());
        $this->assertEquals(MediaType::APPLICATION_JSON_VALUE, $requestMapping_0->getProduces());
        $this->assertEquals('/account/edit/{$user_id}', $requestMapping_0->getValue());
        $this->assertCount(2, $requestMapping_0->getMethod());
        $this->assertCount(3, $requestMapping_0->getValues());
        $this->assertTrue($requestMapping_0->getValues()[2]['isParam']);
        $this->assertEquals('user_id', $requestMapping_0->getValues()[2]['param']);
    }

    public function testParse()
    {
        $classInfo = ClassReflection::parse(ClassReflection::getClassFile(UserController::class));
        $this->assertNotNull($classInfo);
    }

    public function testParseClassObject()
    {
        SpringphpTestRunner::setSpringphpContext();
        SpringphpTestRunner::runHttpMethod(HttpMethod::PUT, 'esapi/account/update', true);
        SpringphpTestRunner::runApplication(true);

        $classInfo = ClassReflection::parseClass(new UserController());
        $this->assertNotNull($classInfo);
    }

    public function testGetMethodAnnotation()
    {
        $annotation = ClassReflection::getMethodAnnotation(UserController::class, 'updateMyProfile', RequestMapping::class);
        $this->assertInstanceOf(RequestMapping::class, $annotation);
    }

    public function testGetMethodAnnotationOnObject()
    {
        SpringphpTestRunner::setSpringphpContext();
        SpringphpTestRunner::runHttpMethod(HttpMethod::PUT, 'esapi/account/update', true);
        SpringphpTestRunner::runApplication(true);

        $annotation = ClassReflection::getMethodAnnotation(new UserController(), 'updateMyProfile', RequestMapping::class);
        $this->assertInstanceOf(RequestMapping::class, $annotation);
    }

    public function testGetClassFile()
    {
        $expectedPath = SpringphpTestRunner::getRootPath('src/test/php/TestCase/Openspring/SpringphpFramework/Helper/Mock/Application/Resource/UserController.php');
        $this->assertEquals($expectedPath, $this->classFile);
    }

    //TODO: to fix on gitlab ci (not working)
    public function __testGetMethodParameterType()
    {
        $type = ClassReflection::getMethodParameterType(new DummyClass(), 'invalidArgumentExceptionHandler', 0);
        $exptectedType = InvalidArgumentException::class;

        $this->assertSame($exptectedType, $type);
    }

    public function testGetConstants()
    {
        $constants = ClassReflection::getConstants(Person::class);

        $this->assertCount(3, $constants);
        $this->assertEquals('BLACK', $constants['HEAR_COLOR']);
        $this->assertEquals('GREEN', $constants['EYES_COLOR']);
        $this->assertEquals('WHITE', $constants['COLOR']);
    }

    public function testExtractMethodParameterType()
    {
        $parameter = new Parameter('page', '[<required> int $page = 0]');

        $type = ClassReflection::extractMethodParameterType($parameter);

        $this->assertSame('int', $type);
    }

    public function testGetClassFromFile()
    {
        $fname = Environment::getRootPath('/src/test/php/TestCase/Openspring/SpringphpFramework/Helper/HunnyBean.php');
        $cn = ClassReflection::getClassFromFile($fname);

        $this->assertEquals(HunnyBean::class, $cn);
    }

    public function testGetBeanClassNameFromFileOK()
    {
        $fname = Environment::getRootPath('/src/test/php/TestCase/Openspring/SpringphpFramework/Helper/HunnyBean.php');
        $cn = ClassReflection::getBeanClassNameFromFile($fname);

        $this->assertEquals(HunnyBean::class, $cn);
    }

    public function testGetBeanClassNameFromFileKO()
    {
        $fname = Environment::getRootPath('/src/test/php/TestCase/Openspring/SpringphpFramework/Helper/JwtData.php');
        $cn = ClassReflection::getBeanClassNameFromFile($fname);

        $this->assertNull($cn);
    }
}