<?php

namespace TestCase\Openspring\SpringphpFramework\Log;

use Openspring\SpringphpFramework\Reflection\ExtendedReflectionClass;
use PHPUnit\Framework\TestCase;
use TestCase\Openspring\SpringphpFramework\Reflection\Helper\UseClass;

class ExtendedReflectionClassTest extends TestCase
{
    public function testGetUseStatements()
    {
        $expected = [
            "HttpStatus" => "Openspring\SpringphpFramework\Http\HttpStatus",
            "MediaType" => "Openspring\SpringphpFramework\Enumeration\MediaType",
            "ObjectType" => "Openspring\SpringphpFramework\Enumeration\ObjectType",
            "FA" => "Openspring\SpringphpFramework\Enumeration\FieldAnnotation",
            "MT" => "Openspring\SpringphpFramework\Enumeration\MemeType",
            "JavaEntityResponse" => "Openspring\SpringphpFramework\Http\ResponseEntity",
            "Exception" => "Exception",
            "MyException" => "Exception",
            "MyException1" => "Exception"];
        $us = new ExtendedReflectionClass(UseClass::class);
        $uses = $us->getUseStatements();
        
        $this->assertEquals($expected, $uses);
    }
    
    public function testGetUseStatement()
    {
        $expected = [
            "HttpStatus" => "Openspring\SpringphpFramework\Http\HttpStatus",
            "MediaType" => "Openspring\SpringphpFramework\Enumeration\MediaType",
            "ObjectType" => "Openspring\SpringphpFramework\Enumeration\ObjectType",
            "FA" => "Openspring\SpringphpFramework\Enumeration\FieldAnnotation",
            "MT" => "Openspring\SpringphpFramework\Enumeration\MemeType",
            "JavaEntityResponse" => "Openspring\SpringphpFramework\Http\ResponseEntity",
            "Exception" => "Exception",
            "MyException" => "Exception",
            "MyException1" => "Exception"];
        $us = new ExtendedReflectionClass(UseClass::class);
        $uses = $us->getUseStatements();
        
        $this->assertEquals($expected, $uses);
    }
}