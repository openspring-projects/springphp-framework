<?php

namespace TestCase\Openspring\SpringphpFramework\Reflection\Helper;

class Person
{
    const HEAR_COLOR = 'BLACK';
	const EYES_COLOR = 'GREEN';
	const COLOR = 'WHITE';
}