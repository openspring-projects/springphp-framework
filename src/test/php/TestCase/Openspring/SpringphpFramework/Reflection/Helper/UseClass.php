<?php

namespace TestCase\Openspring\SpringphpFramework\Reflection\Helper;

use Openspring\SpringphpFramework\Enumeration\FieldAnnotation as FA;
use Openspring\SpringphpFramework\Enumeration\MediaType;
use Openspring\SpringphpFramework\Enumeration\MemeType as MT;
use Openspring\SpringphpFramework\Enumeration\ObjectType;
use Openspring\SpringphpFramework\Http\HttpStatus;
use Openspring\SpringphpFramework\Http\ResponseEntity as JavaEntityResponse;
use Exception;
use Exception as MyException;
use Exception as MyException1;

class UseClass
{

    public function toto()
    {
        $a = new JavaEntityResponse();
        $b = HttpStatus::ALREADY_REPORTED . MediaType::APPLICATION_CBOR_VALUE . ObjectType::BOOLEAN;
        $c = FA::AUTOWIRED . MT::AAB;

        if ($a && $b)
        {
            throw new Exception('' . $c);
        }
        else
        {
            throw new MyException('');
        }

        throw new MyException1('');
    }
}