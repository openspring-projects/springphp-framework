<?php

namespace TestCase\Openspring\SpringphpFramework\Reflection\Helper;

use Openspring\SpringphpFramework\Core\Bean\ControllerAdvice;
use Openspring\SpringphpFramework\Exception\InvalidArgumentException;
use Openspring\SpringphpFramework\Http\HttpStatus;
use Openspring\SpringphpFramework\Http\ResponseEntity;
use Exception;

class DummyClass implements ControllerAdvice
{
    public function exceptionHandler(Exception $e): ResponseEntity
    {
        return new ResponseEntity('error from controllerAdvice.exception()', HttpStatus::NOT_FOUND);
    }

    public function invalidArgumentExceptionHandler(InvalidArgumentException $e): ResponseEntity
    {
        return new ResponseEntity('error from controllerAdvice.invalidArgumentException()', HttpStatus::BAD_REQUEST);
    }
}