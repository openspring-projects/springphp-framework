<?php

namespace TestCase\Openspring\SpringphpFramework\Reflection\Helper;

class Parameter
{
    public $name;
    public $string;

    public function __construct($name, $string)
    {
        $this->name = $name;
        $this->string = $string;
    }

    public function __toString()
    {
        return $this->string;
    }
}