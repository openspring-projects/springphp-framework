<?php

namespace TestCase\Openspring\SpringphpFramework\Web;

use Openspring\SpringphpFramework\Exception\Http\NotFoundException;
use Openspring\SpringphpFramework\Http\HttpMethod;
use PHPUnit\Framework\TestCase;
use TestCase\Openspring\SpringphpFramework\Helper\SpringphpTestRunner;
use TestCase\Openspring\SpringphpFramework\Helper\Mock\Application\Resource\UserController;

final class RouterTest extends TestCase
{
    public function setUp(): void
    {
        SpringphpTestRunner::setSpringphpContext();
    }

    public function testGiven_valid_uri_then_OK(): void
    {
        SpringphpTestRunner::runHttpMethod(HttpMethod::PUT, 'esapi/account/update', true);

        $this->assertSame($_REQUEST['wcomp'], UserController::class);
        $this->assertSame($_REQUEST['action'], 'UpdateMyProfile');
    }

    public function testGiven_invalid_uri_then_KO(): void
    {
        $this->expectException(NotFoundException::class);
        SpringphpTestRunner::runHttpMethod(HttpMethod::GET, 'esapi/account/empty', true);
    }
}