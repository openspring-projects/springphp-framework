<?php
namespace TestCase\Openspring\SpringphpFramework\Web;

use Openspring\SpringphpFramework\Exception\Http\NotFoundException;
use Openspring\SpringphpFramework\Exception\Http\UnauthorizedException;
use Openspring\SpringphpFramework\Http\HttpMethod;
use Openspring\SpringphpFramework\Http\Request;
use PHPUnit\Framework\TestCase;
use TestCase\Openspring\SpringphpFramework\Helper\SpringphpTestRunner;
use TestCase\Openspring\SpringphpFramework\Helper\Mock\Application\Resource\GroupController;

final class RestControllerTest extends TestCase
{
    public function setUp(): void
    {
        SpringphpTestRunner::setSpringphpContext();
    }
    
    public function testCallingOpenEndpointThenOK(): void
    {
        $this->expectOutputRegex('/^"up"/');
        
        SpringphpTestRunner::runHttpMethod(HttpMethod::GET, 'esapi/groups/status', true);
        SpringphpTestRunner::runApplication();
        
        $this->assertSame(Request::getParameter('wcomp'), GroupController::class);
        $this->assertSame(Request::getParameter('action'), 'Status');
    }
    
    public function testCallingSecuredEndpointCreateWithUserAuthentificatedThenOK(): void
    {
        $this->expectOutputRegex('/^true/');
        
        SpringphpTestRunner::runHttpMethod(HttpMethod::POST, 'esapi/groups/create', true);
        SpringphpTestRunner::runApplication(true);
        
        $this->assertSame(Request::getParameter('wcomp'), GroupController::class);
        $this->assertSame(Request::getParameter('action'), 'Create');
    }
    
    public function testCallingSecuredEndpointUpdateThenUnauthorized(): void
    {
        $this->expectException(UnauthorizedException::class);
        
        SpringphpTestRunner::runHttpMethod(HttpMethod::PUT, 'esapi/groups/update', true);
        SpringphpTestRunner::runApplication();
        
        $this->assertSame(Request::getParameter('wcomp'), GroupController::class);
        $this->assertSame(Request::getParameter('action'), 'Update');
    }
    
    public function testCallingNotFoundEndpointDeleteThenNotFound(): void
    {
        $this->expectException(NotFoundException::class);
        
        SpringphpTestRunner::runHttpMethod(HttpMethod::DELETE, 'esapi/groups/delete', true);
        SpringphpTestRunner::runApplication();
        
        $this->assertSame(Request::getParameter('wcomp'), GroupController::class);
        $this->assertSame(Request::getParameter('action'), 'Delete');
    }
    
    public function testCallingInvalidEndpointDeleteThenNotFoundExceptionException(): void
    {
        $this->expectException(NotFoundException::class);
        
        SpringphpTestRunner::runHttpMethod(HttpMethod::POST, 'esapi/groups/enable', true);
        SpringphpTestRunner::runApplication();
        
        $this->assertSame(Request::getParameter('wcomp'), GroupController::class);
        $this->assertSame(Request::getParameter('action'), 'Enable');
    }
}