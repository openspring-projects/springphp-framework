<?php
namespace TestCase\Openspring\SpringphpFramework\Web;

use Openspring\SpringphpFramework\Exception\Http\HttpClientException;
use Openspring\SpringphpFramework\Http\HttpHeaders;
use Openspring\SpringphpFramework\Http\Converter\TextHTMLMessageConverter;
use Openspring\SpringphpFramework\Utils\RestTemplate;
use PHPUnit\Framework\TestCase;

final class RestTemplateTest extends TestCase
{
    public function testGetOK(): void
    {
        $restTemplate = new RestTemplate();
        $restTemplate->setSslVerifyHost(false);
        $restTemplate->setSslVerifyPeer(false);
        
        $restTemplate->addMessageConvertor(new TextHTMLMessageConverter());
        
        $data = $restTemplate->get("http://connectein.com/esapi/", new HttpHeaders(), null);
        
        $this->assertSame('{"console":[],"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.ZmFsc2U.S-P-NRXOqywULFbjTPDah7QxG2GROdKdGVHg82nGcCk","executionTime":"1,00"}', $data);
    }
    
    public function testGetKoHttpClientException(): void
    {
        $restTemplate = new RestTemplate();
        $restTemplate->setSslVerifyHost(false);
        $restTemplate->setSslVerifyPeer(false);
        
        $restTemplate->addMessageConvertor(new TextHTMLMessageConverter());
        
        $this->expectException(HttpClientException::class);
        
        $restTemplate->get("http://127.0.0.1:8080/404", new HttpHeaders(), null);
    }
}