<?php

namespace TestCase\Openspring\SpringphpFramework\Log;

use Openspring\SpringphpFramework\Context\Environment;
use Openspring\SpringphpFramework\Core\SpringphpApplication;
use Openspring\SpringphpFramework\Core\Bean\EnvironmentPostProcessor;
use Openspring\SpringphpFramework\Core\Stereotype\Configuration;
use Openspring\SpringphpFramework\Log\LogLevel;

/**
 * Global logging class
 *
 * @author Khalid ELABBADI
 * @email khalid.elabbadii@gmail.com
 */
class LogPropertiesOverwriter extends Configuration implements EnvironmentPostProcessor
{
    public function postProcessEnvironment(SpringphpApplication $application): void
    {
        Environment::enableLog(true);
        Environment::setLogLevel(LogLevel::DEBUG);
        Environment::setLogFormat('[%d]-[%l]%m');
        Environment::setLogFile('logs/server_#{date}.log');
    }
}