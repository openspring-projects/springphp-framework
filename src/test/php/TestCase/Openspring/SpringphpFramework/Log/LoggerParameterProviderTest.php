<?php

namespace TestCase\Openspring\SpringphpFramework\Log;

use Openspring\SpringphpFramework\Context\Environment;
use Openspring\SpringphpFramework\Log\LogLevel;
use Openspring\SpringphpFramework\Log\LogUtils;
use Openspring\SpringphpFramework\Log\LoggerParameterProvider;
use PHPUnit\Framework\TestCase;
use TestCase\Openspring\SpringphpFramework\Helper\SpringphpTestRunner;

class LoggerParameterProviderTest extends TestCase
{
    public function setUp(): void
    {
        SpringphpTestRunner::setSpringphpContext();
    }

    public function testSettersGetters()
    {
        $clazzName = 'LoggerParameterProviderTest';
        $paramProvider = new LoggerParameterProvider();
        
        $paramProvider->setClazzName($clazzName);
        $paramProvider->setNamespace(LoggerParameterProviderTest::class);
        $paramProvider->setEnabled(Environment::isLogEnabled());
        $paramProvider->setLevel(LogUtils::getLevelByName(Environment::getLogLevel()));
        $paramProvider->setFilePath(Environment::getLogFile());
        $paramProvider->setPattern(Environment::getLogFormat());
        
        $this->assertEquals($clazzName, $paramProvider->getClazzName());
        $this->assertEquals(LoggerParameterProviderTest::class, $paramProvider->getNamespace());
        $this->assertEquals(true, $paramProvider->getEnabled());
        $this->assertEquals(LogLevel::ERROR, $paramProvider->getLevel());
        $this->assertEquals('logs/server_%d.log', $paramProvider->getFilePath());
        $this->assertEquals('[%d][%ns][%c][%l] %m', $paramProvider->getPattern());
    }
}