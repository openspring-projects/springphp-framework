<?php

namespace TestCase\Openspring\SpringphpFramework\Log;

use PHPUnit\Framework\TestCase;
use TestCase\Openspring\SpringphpFramework\Helper\SpringphpTestRunner;
use Openspring\SpringphpFramework\Log\Logger;
use Openspring\SpringphpFramework\Log\LogFactory;
use Openspring\SpringphpFramework\Log\LogLevel;
use TestCase\Openspring\SpringphpFramework\Helper\UserDto;

class LogFactoryTest extends TestCase
{
    /**
     * @var Logger
     */
    private $logger;

    public function setUp(): void
    {
        SpringphpTestRunner::setSpringphpContext();
        $this->logger = LogFactory::getLogger(self::class);
    }
    
    public function testNewLogger()
    {
        $this->logger = LogFactory::getLogger(self::class);
        
        $this->assertNotNull($this->logger);
        $this->assertEquals(LogLevel::ERROR, $this->logger->getLevel());
        
        $this->logger->setLevel(LogLevel::DEBUG);
    }
    
    public function testNewLoggerFromContext()
    {
        $this->logger = LogFactory::getLogger(self::class);
        
        $this->assertNotNull($this->logger);
        $this->assertEquals(LogLevel::DEBUG, $this->logger->getLevel());
    }
    
    public function testNewLoggerFromContextButClassNameIsDifferent()
    {
        $this->logger = LogFactory::getLogger(UserDto::class);
        
        $this->assertNotNull($this->logger);
        $this->assertEquals(LogLevel::ERROR, $this->logger->getLevel());
    }
}