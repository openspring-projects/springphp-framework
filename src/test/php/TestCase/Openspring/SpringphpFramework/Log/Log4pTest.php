<?php

namespace TestCase\Openspring\SpringphpFramework\Log;

use Openspring\SpringphpFramework\Context\Environment;
use Openspring\SpringphpFramework\Log\LogUtils;
use Openspring\SpringphpFramework\Log\LoggerParameterProvider;
use Openspring\SpringphpFramework\Log\Implementation\Log4p;
use PHPUnit\Framework\TestCase;
use TestCase\Openspring\SpringphpFramework\Helper\SpringphpTestRunner;
use Openspring\SpringphpFramework\Log\Logger;
use Openspring\SpringphpFramework\Log\LogLevel;
use Openspring\SpringphpFramework\Type\Any;
use TestCase\Openspring\SpringphpFramework\Helper\UserDto;

class Log4pTest extends TestCase
{
    /**
     * @var Logger
     */
    private $logger;

    public function setUp(): void
    {
        SpringphpTestRunner::setSpringphpContext();
        
        $paramProvider = new LoggerParameterProvider();
        
        $paramProvider->setClazzName('Log4pTest');
        $paramProvider->setNamespace(Log4pTest::class);
        $paramProvider->setEnabled(Environment::isLogEnabled());
        $paramProvider->setLevel(LogUtils::getLevelByName(Environment::getLogLevel()));
        $paramProvider->setFilePath(Environment::getLogFile());
        $paramProvider->setPattern(Environment::getLogFormat());
        
        $this->logger = new Log4p();
        $this->logger->setLoggerParameterProvider($paramProvider);
    }

    public function testLoggingWithLevelSetToError()
    {
        $this->logger->debug('test debug');
        $this->logger->error('test error');
        $this->logger->warn('test warn');
        $this->logger->info('test info');
        
        $this->assertFileExists($this->logger->getFilePath());
    }
    
    public function testLoggingWithLevelSetToDebug()
    {
        $this->logger->setLevel(LogLevel::DEBUG);
        
        $this->logger->debug('test debug');
        $this->logger->error('test error');
        $this->logger->warn('test warn');
        $this->logger->info('test info');
        
        $this->assertFileExists($this->logger->getFilePath());
    }
    
    public function testLoggingOfArray()
    {
        $this->logger->error(array(1,2,3,4,5));
        
        $this->assertFileExists($this->logger->getFilePath());
    }
    
    public function testLoggingOfObjectToStringNotDefined()
    {
        $object = new Any();
        $object->name = 'khalid';
        $object->age = 36;
        
        $this->logger->error($object);
        
        $this->assertFileExists($this->logger->getFilePath());
    }
    
    public function testLoggingOfObjectToStringDefined()
    {
        $object = new UserDto();
        $object->setUserId('100');
        $object->setGroupId(2);
        $object->setLogin('khalid');
        
        $this->logger->error($object);
        
        $this->assertFileExists($this->logger->getFilePath());
    }
    
    public function testLoggingOfBoolean()
    {
        $this->logger->error(true);
        
        $this->assertFileExists($this->logger->getFilePath());
    }
}