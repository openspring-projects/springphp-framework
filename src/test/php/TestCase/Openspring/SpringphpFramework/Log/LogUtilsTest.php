<?php

namespace TestCase\Openspring\SpringphpFramework\Log;

use PHPUnit\Framework\TestCase;
use Openspring\SpringphpFramework\Log\LogUtils;
use Openspring\SpringphpFramework\Log\LogLevelName;

class LogUtilsTest extends TestCase
{
    public function testGetLevelName()
    {
        $this->assertEquals(LogLevelName::DEBUG, LogUtils::getLevelName(0));
        $this->assertEquals(LogLevelName::INFO, LogUtils::getLevelName(1));
        $this->assertEquals(LogLevelName::WARN, LogUtils::getLevelName(2));
        $this->assertEquals(LogLevelName::ERROR, LogUtils::getLevelName(3));
    }
    
    public function testGetLevelByName()
    {
        $this->assertEquals(0, LogUtils::getLevelByName(LogLevelName::DEBUG));
        $this->assertEquals(1, LogUtils::getLevelByName(LogLevelName::INFO));
        $this->assertEquals(2, LogUtils::getLevelByName(LogLevelName::WARN));
        $this->assertEquals(3, LogUtils::getLevelByName(LogLevelName::ERROR));
    }
    
    public function testGetDottedNamespace()
    {
        $this->assertEquals('O.S.L.L', LogUtils::getDottedNamespace(LogLevelName::class));
        $this->assertEquals('O', LogUtils::getDottedNamespace('Openspring'));
        $this->assertEquals('O', LogUtils::getDottedNamespace('\Openspring'));
    }
}