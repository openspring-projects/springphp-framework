<?php

namespace TestCase\Openspring\SpringphpFramework\Helper\DI\Service;

use Openspring\SpringphpFramework\Core\Stereotype\Bean;

class CommandService extends Bean
{
    private $name;
    
    public function getTotal()
    {
        return 15000;
    }

}