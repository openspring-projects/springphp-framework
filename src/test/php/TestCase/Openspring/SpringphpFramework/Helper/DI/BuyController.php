<?php

namespace TestCase\Openspring\SpringphpFramework\Helper\DI;

use Openspring\SpringphpFramework\Core\Stereotype\Bean;
use TestCase\Openspring\SpringphpFramework\Helper\DI\Service\ClientService;
use TestCase\Openspring\SpringphpFramework\Helper\DI\Service\CommandService;
use TestCase\Openspring\SpringphpFramework\Helper\DI\Service\ProviderService;

class BuyController extends Bean
{
    /**
     * @var SellService
     * @Autowired()
     */
    public $sellService;
    
    /**
     * @Value("${springphp.application.name}")
     */
    public $applicationName;
    
    /**
     * @Autowired()
     * @var ClientService
     */
    public $clientService;

    /**
     * @var CommandService
     * @Autowired(true)
     */
    public $commandService;

    /**
     * @var ProviderService
     * @Autowired(false)
     */
    public $providerService;
    
    public function __construct()
    {
        $this->enableAnnotations();
    }

    public function getName()
    {
        return $this->clientService->getName();
    }

    public function getTotal()
    {
        return $this->commandService->getTotal();
    }

    public function getLabel()
    {
        return $this->providerService->getLabel();
    }
}