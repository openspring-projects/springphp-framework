<?php

namespace TestCase\Openspring\SpringphpFramework\Helper\DI\Service;

use Openspring\SpringphpFramework\Core\Stereotype\Bean;

class ProviderService extends Bean
{
    public function getLabel()
    {
        return "MS";
    }
}