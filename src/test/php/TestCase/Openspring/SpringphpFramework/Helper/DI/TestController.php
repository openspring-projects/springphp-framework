<?php

namespace TestCase\Openspring\SpringphpFramework\Helper\DI;

use Openspring\SpringphpFramework\Core\Stereotype\Bean;
use TestCase\Openspring\SpringphpFramework\Helper\DI\Service\ClientService;

class TestController extends Bean
{
    /**
     * feld goes here
     * @var ClientService
     * hhhh
     * @Autowired(true)
     * yeapppp
     */
    private $clientService0VarAutowired0;
    
    /**
     * fdfdf
     * @var ClientService
     */
    private $clientService1Var;
    
    /**
     *
     * @Autowired(true)
     */
    private $clientService2Autowired;
    
    /**
     *
     * @var ClientService
     * @Autowired(true)
     */
    private $clientService3VarAutowired;
    
    /**
     *
     * @toto()
     */
    private $clientService4None;

    /**
     * hello there
     */
    private $clientService5None;

    private $clientService6None;
    
    /**
     * @Value("${app.name}")
     * @var String
     */
    private $appName0ValueVar;

    public function getClientName()
    {
        return $this->clientService->getName();
    }
}