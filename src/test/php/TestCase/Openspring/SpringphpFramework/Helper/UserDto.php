<?php

namespace TestCase\Openspring\SpringphpFramework\Helper;

class UserDto
{
    public $userId = NULL;
    public $groupId = NULL;
    public $login = NULL;

    public function setUserId($value)
    {
        $this->userId = $value;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function setGroupId($value)
    {
        $this->groupId = $value;
    }

    public function getGroupId()
    {
        return $this->groupId;
    }

    public function setLogin($value)
    {
        $this->login = $value;
    }

    public function getLogin()
    {
        return $this->login;
    }
    
    public function __toString()
    {
        return $this->getLogin() . ' ' . $this->getGroupId() . ' ' . $this->getUserId();
    }
}