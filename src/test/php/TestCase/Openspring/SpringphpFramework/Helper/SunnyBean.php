<?php

namespace TestCase\Openspring\SpringphpFramework\Helper;

use Openspring\SpringphpFramework\Core\Stereotype\Bean;
use Openspring\SpringphpFramework\Core\Stereotype\BeanInstanciator;
use Openspring\SpringphpFramework\Core\Stereotype\IBean;

class SunnyBean extends Bean implements BeanInstanciator
{
    private $name;
    
    public function __construct()
    {
        
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public static function getInstance(): IBean
    {
        
    }
}