<?php

namespace TestCase\Openspring\SpringphpFramework\Helper;

use Openspring\SpringphpFramework\Core\Stereotype\Bean;
use Openspring\SpringphpFramework\Core\Stereotype\BeanInstanciator;
use Openspring\SpringphpFramework\Core\Stereotype\IBean;

class HunnyBean extends Bean implements BeanInstanciator
{
    private $name;
    private $age;

    public function __construct(String $name, int $age)
    {
        $this->setName($name);
        $this->setAge($age);
    }

    public function getName(): String
    {
        return $this->name;
    }

    public function setName(String $name)
    {
        $this->name = $name;
    }

    public function getAge(): int
    {
        return $this->age;
    }

    public function setAge(int $age)
    {
        $this->age = $age;
    }

    public static function getInstance(): IBean
    {
        return new HunnyBean('driss', 42);
    }
}