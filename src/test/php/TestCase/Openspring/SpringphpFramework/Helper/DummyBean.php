<?php
namespace TestCase\Openspring\SpringphpFramework\Helper;

use Openspring\SpringphpFramework\Core\Stereotype\Bean;

class DummyBean extends Bean
{
    private $name;
    
    public function getName()
    {
        return $this->name;
    }
    
    public function setName($name)
    {
        $this->name = $name;
    }
}