<?php
namespace TestCase\Openspring\SpringphpFramework\Helper;

use Openspring\SpringphpFramework\Orm\Column;
use Openspring\SpringphpFramework\Orm\DaoSupport;
use Openspring\SpringphpFramework\Orm\Type\DataType;
use Openspring\SpringphpFramework\Orm\Type\GenerationType;
use Openspring\SpringphpFramework\Orm\Type\PrimaryKey;

class UserEntity extends DaoSupport
{
    public $userId = NULL;
    public $groupId = NULL;
    public $login = NULL;

    function __construct($params = NULL)
    {
        parent::__construct('#__user');

        $this->userId = new Column($this, 'user_id', 32, DataType::VARCHAR, new PrimaryKey(GenerationType::MANUAL));
        $this->groupId = new Column($this, 'group_id', 1, DataType::INT, null);
        $this->login = new Column($this, 'login', 128, DataType::VARCHAR, null);

        $this->initialize($params);
    }

    public function setUserId($value)
    {
        $this->userId->set($value);
    }

    public function getUserId()
    {
        return $this->userId->get();
    }

    public function setGroupId($value)
    {
        $this->groupId->set($value);
    }

    public function getGroupId()
    {
        return $this->groupId->get();
    }

    public function setLogin($value)
    {
        $this->login->set($value);
    }

    public function getLogin()
    {
        return $this->login->get();
    }
}