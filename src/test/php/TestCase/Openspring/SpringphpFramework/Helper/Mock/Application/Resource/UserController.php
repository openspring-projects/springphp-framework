<?php

namespace TestCase\Openspring\SpringphpFramework\Helper\Mock\Application\Resource;

use Openspring\SpringphpFramework\Web\RestController;
use Openspring\SpringphpFramework\Type\RequestBody;

/** User controller */
class UserController extends RestController
{
    public function initialize()
    {
        
    }

    /**
     *
     * @RequestMapping({"value": "/account/edit/{$user_id}", "method": ["GET", "POST"], "consumes": "application/json", "produces": "application/json", "groupIn": "*"})
     */
    public function editMyProfile(String $user_id): array
    {
        return array(
            'user_id' => 'U0000000000001',
            'name' => 'Khalid ELABBADI'
        );
    }

    /**
     *
     * @RequestMapping({"value": "/admin/user/edit/{$user_id}", "method": ["GET", "POST"], "consumes": "application/json", "produces": "application/json", "groupIn": "2"})
     */
    public function editUser(String $user_id): array
    {
        return array(
            'user_id' => 'U0000000000002',
            'name' => 'Rachid RADI'
        );
    }

    /**
     *
     * @RequestMapping({"value": "/account/update", "method": ["PUT"], "consumes": "application/json", "produces": "application/json", "groupIn": "*"})
     */
    public function updateMyProfile(RequestBody $body)
    {
        return true;
    }
}