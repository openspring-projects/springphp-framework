<?php

namespace TestCase\Openspring\SpringphpFramework\Helper\Mock\Application;

use Openspring\SpringphpFramework\Core\Stereotype\Configuration;
use Openspring\SpringphpFramework\Security\IUser;
use Openspring\SpringphpFramework\Security\UserDetailsProvider;
use Openspring\SpringphpFramework\Test\TestRunner;

class MockUserDetailsProvider extends Configuration implements UserDetailsProvider
{
    public function getUserById(String $userId): IUser
    {
        $mockUserDto = TestRunner::buildMockedIUser(new MockUserDto());
        $mockUserDto->setUserId($userId);

        return $mockUserDto;
    }
}