<?php
namespace TestCase\Openspring\SpringphpFramework\Helper\Mock\Application;

use Openspring\SpringphpFramework\Security\IUser;

class MockUserDto implements IUser
{
    var $userId = null;
    var $groupId = null;
    var $login = null;
    var $email = null;
    var $firstName = null;
    var $lastName = null;
    var $roles = array();

    public function getUserId(): String
    {
        return $this->userId;
    }

    public function getGroupId(): int
    {
        return $this->groupId;
    }

    public function getLogin(): String
    {
        return $this->login;
    }

    public function getEmail(): String
    {
        return $this->email;
    }

    public function getFirstName(): String
    {
        return $this->firstName;
    }

    public function getLastName(): String
    {
        return $this->lastName;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function setUserId(String $userId): void
    {
        $this->userId = $userId;
    }

    public function setGroupId(int $groupId): void
    {
        $this->groupId = $groupId;
    }

    public function setLogin(String $login): void
    {
        $this->login = $login;
    }

    public function setEmail(String $email): void
    {
        $this->email = $email;
    }

    public function setFirstName(String $firstName): void
    {
        $this->firstName = $firstName;
    }

    public function setLastName(String $lastName): void
    {
        $this->lastName = $lastName;
    }

    public function setRoles(array $roles = array()): void
    {
        $this->roles = $roles;
    }
}