<?php

namespace TestCase\Openspring\SpringphpFramework\Helper\Mock\Application\Resource;

use Openspring\SpringphpFramework\Web\RestController;
use Openspring\SpringphpFramework\Type\RequestBody;

/** Group controller */
class GroupController extends RestController
{
    public function initialize()
    {
        
    }
    
    /**
     *
     * @GetMapping({"value": "/groups/status", "consumes": "application/json", "produces": "application/json"})
     */
    public function status(): String
    {
        return 'up';
    }

    /**
     *
     * @RequestMapping({"value": "/groups/create", "method": ["POST"], "consumes": "application/json", "produces": "application/json", "groupIn": "*"})
     */
    public function create(RequestBody $body)
    {
        return true;
    }
    
    /**
     *
     * @RequestMapping({"value": "/groups/update", "method": ["PUT"], "consumes": "application/json", "produces": "application/json", "groupIn": "2"})
     */
    public function update(RequestBody $body)
    {
        return true;
    }
    
    /**
     *
     * @RequestMapping({"value": "/groups/delete", "method": ["DELETE"], "consumes": "application/json", "produces": "application/json", "groupIn": "*"})
     */
    public function delete(RequestBody $body)
    {
        return true;
    }
    
    /**
     *
     * @RequestMapping({"value": "/groups/enable", "method": ["POST"], "consumes": "application/json", "produces": "application/json", "groupIn": "*"})
     */
    public function enable(RequestBody $body)
    {
        return true;
    }
}