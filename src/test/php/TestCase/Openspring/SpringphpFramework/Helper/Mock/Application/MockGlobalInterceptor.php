<?php

namespace TestCase\Openspring\SpringphpFramework\Helper\Mock\Application;

use Openspring\SpringphpFramework\Core\Bean\HttpInterceptor;
use Openspring\SpringphpFramework\Core\Stereotype\Configuration;

class MockGlobalInterceptor extends Configuration implements HttpInterceptor
{
    public function preHandle(): bool
    {
        return true;
    }

    public function postHandle(): bool
    {
        return true;
    }	
}