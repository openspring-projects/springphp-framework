<?php

namespace TestCase\Openspring\SpringphpFramework\Helper\Mock\Application;

use Openspring\SpringphpFramework\Core\Configurable;
use Openspring\SpringphpFramework\Core\SpringphpApplication;
use Openspring\SpringphpFramework\Core\Bean\JwtProcessor;
use Openspring\SpringphpFramework\Core\Bean\ResourceBundleMessageSource;
use TestCase\Openspring\SpringphpFramework\Helper\StaticJwtParameterProvider;

/**
 * @ComponentScan({ "baseDirs": ["src/test/php/TestCase/Openspring/SpringphpFramework/Helper"],
 *                  "excludeDirs": [""]
 *                  })
 * @PropertySource("file:src/test/resources/application.properties")
 *
 */
class MockApplication extends SpringphpApplication implements Configurable
{
    public function __construct(String $basePath, String $runConfigPath = 'src/test/resources/', String $testConfigPath = 'src/test/resources/')
    {
        parent::__construct($basePath, $runConfigPath, $testConfigPath);
    }

    public function configure(): void
    {
        $resourceMessages = new ResourceBundleMessageSource();
        $resourceMessages->addBaseName("messages");

        $this->addConfiguration(new MockGlobalInterceptor())
            ->addConfiguration($resourceMessages)
            ->addConfiguration(new StaticJwtParameterProvider())
            ->addConfiguration(new MockUserDetailsProvider())
            ->addConfiguration(new JwtProcessor('store-realm'))
            ->execute();
    }
}