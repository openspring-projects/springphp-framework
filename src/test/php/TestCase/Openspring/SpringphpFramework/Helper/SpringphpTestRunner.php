<?php

namespace TestCase\Openspring\SpringphpFramework\Helper;

use Openspring\SpringphpFramework\Context\ApplicationContext;
use Openspring\SpringphpFramework\Security\IUser;
use Openspring\SpringphpFramework\Security\Jwt\Core\JwtParameterProvider;
use Openspring\SpringphpFramework\Test\TestRunner;
use TestCase\Openspring\SpringphpFramework\Helper\Mock\Application\MockApplication;
use TestCase\Openspring\SpringphpFramework\Helper\Mock\Application\MockUserDetailsProvider;
use TestCase\Openspring\SpringphpFramework\Helper\Mock\Application\MockUserDto;

class SpringphpTestRunner extends TestRunner
{
    public static function setSpringphpContext(bool $injectMockito = false, bool $mockJwtParameterProvider = true, bool $mockUserRepo = true): void
    {
        parent::setSpringphpContext($injectMockito);

        if ($mockJwtParameterProvider)
        {
            ApplicationContext::addBeanSingleton(new StaticJwtParameterProvider());
            static::setJwtParameterProvider(ApplicationContext::getBeanOf(JwtParameterProvider::class, true));
        }

        if ($mockUserRepo)
        {
            ApplicationContext::addBeanSingleton(new MockUserDetailsProvider());
        }
    }

    public static function runApplication(bool $withJWT = false): void
    {
        parent::startApplication(new MockApplication(self::getRootPath()), $withJWT);
    }

    public static function bootApplication(bool $withJWT = false, String $httpMethod = null, String $uri = null, bool $injectMockito = false): void
    {
        parent::bootstrapApplication(new MockApplication(self::getRootPath()), $withJWT, $httpMethod, $uri, $injectMockito);
    }

    public static function getMockedIUser(): IUser
    {
        return parent::buildMockedIUser(new MockUserDto());
    }
}