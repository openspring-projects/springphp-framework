<?php

namespace TestCase\Openspring\SpringphpFramework\Helper;

use Openspring\SpringphpFramework\Core\Stereotype\Configuration;
use Openspring\SpringphpFramework\Security\Jwt\Core\JwtParameterProvider;
use Openspring\SpringphpFramework\Security\Jwt\Core\JwtParameters;
use Openspring\SpringphpFramework\Security\Jwt\Enumeration\EncryptionAlgorithm;

class StaticJwtParameterProvider extends Configuration implements JwtParameterProvider
{
    public function getJwtParameters(String $clientId): JwtParameters
    {
        $jwtParameters = new JwtParameters(JwtData::PUBLIC_KEY_1024, JwtData::PRIVATE_KEY_1024);
        $jwtParameters->setAlgorithm(EncryptionAlgorithm::RS512);
        $jwtParameters->setIssuer('http://localhost:4200/home');
        
        switch ($clientId)
        {
            case 'store-client-2':
                $jwtParameters->setExpiresIn(-300);
                break;
        }

        return $jwtParameters;
    }
}