<?php
namespace TestCase\Openspring\SpringphpFramework\Helper;

use Openspring\SpringphpFramework\Orm\Column;
use Openspring\SpringphpFramework\Orm\DaoSupport;
use Openspring\SpringphpFramework\Orm\Type\DataType;
use Openspring\SpringphpFramework\Orm\Type\GenerationType;
use Openspring\SpringphpFramework\Orm\Type\PrimaryKey;

class PersonEntity extends DaoSupport
{
    public $person_id;
    public $name;
    public $age;
    public $salary;
    public $birth_date;
    public $login_time;
    public $married;
    public $password;
    public $html;
    public $xml;
    public $json;

    function __construct($params = NULL)
    {
        parent::__construct('#__person');

        $this->person_id = new Column($this, 'person_id', 32, DataType::GUID, new PrimaryKey(GenerationType::MANUAL));
        $this->name = new Column($this, 'name', 128, DataType::VARCHAR, null);
        $this->age = new Column($this, 'age', 3, DataType::INT, null);
        $this->salary = new Column($this, 'salary', 8, DataType::FLOAT, null);
        $this->birth_date = new Column($this, 'birth_date', 0, DataType::DATE, null);
        $this->login_time = new Column($this, 'login_time', 0, DataType::DATETIME, null);
        $this->married = new Column($this, 'married', 1, DataType::BOOLEAN, null);
        $this->password = new Column($this, 'password', 32, DataType::HASH, null);
        $this->html = new Column($this, 'html', 128, DataType::HTML, null);
        $this->xml = new Column($this, 'xml', 128, DataType::XML, null);
        $this->json = new Column($this, 'json', 128, DataType::JSON, null);

        $this->initialize($params);
    }

    public function setPerson_id($person_id)
    {
        $this->person_id->set($person_id);
    }

    public function getPerson_id()
    {
        return $this->person_id->get();
    }

    public function setName($name)
    {
        $this->name->set($name);
    }

    public function getName()
    {
        return $this->name->get();
    }

    public function setAge($age)
    {
        $this->age->set($age);
    }

    public function getAge()
    {
        return $this->age->get();
    }

    public function setSalary($salary)
    {
        $this->salary->set($salary);
    }

    public function getSalary()
    {
        return $this->salary->get();
    }

    public function setBirth_date($birth_date)
    {
        $this->birth_date->set($birth_date);
    }

    public function getBirth_date()
    {
        return $this->birth_date->get();
    }

    public function setLogin_time($login_time)
    {
        $this->login_time->set($login_time);
    }

    public function getLogin_time()
    {
        return $this->login_time->get();
    }

    public function setMarried($married)
    {
        $this->married->set($married);
    }

    public function getMarried()
    {
        return $this->married->get();
    }

    public function setPassword($password)
    {
        $this->password->set($password);
    }

    public function getPassword()
    {
        return $this->password->get();
    }

    public function setHtml($html)
    {
        $this->html->set($html);
    }

    public function getHtml()
    {
        return $this->html->get();
    }

    public function setXml($xml)
    {
        $this->xml->set($xml);
    }

    public function getXml()
    {
        return $this->xml->get();
    }

    public function setJson($json)
    {
        $this->json->set($json);
    }

    public function getJson()
    {
        return $this->json->get();
    }
}