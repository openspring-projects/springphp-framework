<?php

namespace TestCase\Openspring\SpringphpFramework\Helper;

class DunnyChildBean extends DunnyBean
{
    private $childName;

    public function getChildName()
    {
        return $this->childName;
    }

    public function setChildName($childName)
    {
        $this->childName = $childName;
    }
}