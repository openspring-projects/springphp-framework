<?php
namespace TestCase\Openspring\SpringphpFramework\Helper;

use Openspring\SpringphpFramework\Core\Stereotype\Bean;

class TotoBean extends Bean implements IPerson
{
    private $name = 'khalid';
    public $age = 36;
}