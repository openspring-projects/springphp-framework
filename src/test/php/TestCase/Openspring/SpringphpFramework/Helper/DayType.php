<?php
namespace TestCase\Openspring\SpringphpFramework\Helper;

use Openspring\SpringphpFramework\Enumeration\EnumerationType;

class DayType extends EnumerationType
{
	const MONDAY = "MONDAY";
	const TUESDAY = "TUESDAY";
	const WEDNESDAY = "WEDNESDAY";
	const THURSDAY = "THURSDAY";
	const FRIDAY = "FRIDAY";
	const SATURDAY = "SATURDAY";
	const SUNDAY = "SUNDAY";
}