<?php
namespace TestCase\Openspring\SpringphpFramework\Orm\Query;

use Openspring\SpringphpFramework\Orm\Query\WhereBuilder;
use PHPUnit\Framework\TestCase;

class WhereBuilderTest extends TestCase
{
    public function testQueryGeneration()
    {
        $wb = new WhereBuilder();
        $where1 = $wb->andEqual('name', 'khalid')->getWhere();

        $this->assertEquals("AND (name='khalid')", $where1);

        $wb->init();
        $expectedSQL = "AND (name='khalid') AND ((name='khalid') OR (age=36)) AND ((job='ING') OR (tag LIKE '%php%') AND (tag LIKE '%c#%') OR  (city IN ('Rabat','Casa')))";
        $where2 = $wb->openAndBracket()
            ->andEqual('name', 'khalid')
            ->orEqual('age', 36)
            ->closeBracket()
            ->openAndBracket()
            ->andEqual('job', 'ING')
            ->orLike('tag', 'php')
            ->andLike('tag', 'c#')
            ->orEqual('city', array('Rabat','Casa'))
            ->closeBracket()
            ->getWhere();

        $this->assertEquals($expectedSQL, $where2);
    }
}