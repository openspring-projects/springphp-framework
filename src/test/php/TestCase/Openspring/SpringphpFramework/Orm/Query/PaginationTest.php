<?php

namespace TestCase\Openspring\SpringphpFramework\Orm\Query;

use Openspring\SpringphpFramework\Orm\Query\Pagination;
use Openspring\SpringphpFramework\Type\Any;
use PHPUnit\Framework\TestCase;

class PaginationTest extends TestCase
{
    public function testGetPage1()
    {
        $page = new Any();
        $page->startRow = 0;
        $page->endRow = 25;
        $page->totalPages = 5;
        
        $pager = new Pagination();
        
        $this->assertEquals($page, $pager->getPager(1, 25, 124));
    }
    
    public function testGetPage2()
    {
        $page = new Any();
        $page->startRow = 25;
        $page->endRow = 50;
        $page->totalPages = 5;
        
        $pager = new Pagination();
        
        $this->assertEquals($page, $pager->getPager(2, 25, 124));
    }
    
    public function testGetTheOnlyOnePageThatExists()
    {
        $page = new Any();
        $page->startRow = 0;
        $page->endRow = 25;
        $page->totalPages = 1;
        
        $pager = new Pagination();
        
        $this->assertEquals($page, $pager->getPager(1, 25, 20));
    }
    
    public function testGetInexistantPage()
    {
        $page = new Any();
        $page->startRow = 0;
        $page->endRow = 25;
        $page->totalPages = 1;
        
        $pager = new Pagination();
        
        $this->assertEquals($page, $pager->getPager(5, 25, 20));
    }
}