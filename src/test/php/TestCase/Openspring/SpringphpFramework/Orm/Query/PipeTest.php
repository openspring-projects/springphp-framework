<?php

namespace TestCase\Openspring\SpringphpFramework\Orm\Query;;

use Openspring\SpringphpFramework\Orm\Query\Pipe;
use Openspring\SpringphpFramework\Type\DynamicObject;
use PHPUnit\Framework\TestCase;

class PipeTest extends TestCase
{
    public function testJson()
    {
        $target = '{"name": "toto"}';
		$expected = new DynamicObject(['name' => 'toto']);
        $object = Pipe::json($target);
		
        $this->assertEquals($expected->name, $object->name);
    }
	
	public function testHtmlNewLine()
    {
        $target = 'header' . PHP_EOL . 'text';
		$expected = 'header<br />text';
		
		$actual = Pipe::htmlNewLine($target);
		
		$this->assertEquals($expected, $actual);
    }
	
	public function testBoolean()
    {
        $this->assertTrue(Pipe::boolean(1));
		$this->assertFalse(Pipe::boolean(0));
    }
	
	public function testDateTime()
    {
        $this->assertEquals('2019-09-05 12:18:55', Pipe::dateTime('2019-09-05 12:18:55'));
		$this->assertEquals('2019-09-05 12:18:55', Pipe::dateTime('2019-09-05 12:18:55.254545584710'));
    }
	
	public function testNull_Nvl()
    {
        $this->assertEquals('', Pipe::notNull(null));
		$this->assertEquals('toto', Pipe::notNull('toto'));
		
		$this->assertEquals('', Pipe::nvl(null));
		$this->assertEquals('toto', Pipe::nvl(null, 'toto'));
		$this->assertEquals('toto', Pipe::nvl('toto', 'fofo'));
    }
}