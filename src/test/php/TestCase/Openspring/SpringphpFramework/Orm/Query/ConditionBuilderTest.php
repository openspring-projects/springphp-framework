<?php

namespace TestCase\Openspring\SpringphpFramework\Orm\Query;

use Openspring\SpringphpFramework\Orm\Query\Condition;
use Openspring\SpringphpFramework\Orm\Query\ConditionBuilder;
use PHPUnit\Framework\TestCase;

class ConditionBuilderTest extends TestCase
{
    public function testOr()
    {
        $expected = "(login = 'khalid') OR (birthDate BETWEEN '2019-01-01' AND '2019-12-12')";
        $cb = new ConditionBuilder();

        $cb->or((new Condition('login'))->equals('khalid'))
            ->or((new Condition('name'))->like(''))
            ->or((new Condition('sal'))->greaterOrEqualThan(''))
            ->or((new Condition('birthDate'))->between('2019-01-01', '2019-12-12'));

        $actual = $cb;

        $this->assertEquals($expected, $actual);
    }

    public function testAnd()
    {
        $expected = "(login = 'khalid') AND (birthDate BETWEEN '2019-01-01' AND '2019-12-12')";
        $cb = new ConditionBuilder();

        $cb->and((new Condition('login'))->equals('khalid'))
            ->and((new Condition('name'))->like(''))
            ->and((new Condition('sal'))->greaterOrEqualThan(''))
            ->and((new Condition('birthDate'))->between('2019-01-01', '2019-12-12'));

        $actual = $cb;

        $this->assertEquals($expected, $actual);
    }

    public function testOrAnd()
    {
        $expected = "(login = 'khalid') OR (sal >= 25000) AND (birthDate BETWEEN '2019-01-01' AND '2019-12-12')";
        $cb = new ConditionBuilder();

        $cb->and((new Condition('login'))->equals('khalid'))
            ->and((new Condition('name'))->like(''))
            ->or((new Condition('sal'))->greaterOrEqualThan(25000))
            ->and((new Condition('birthDate'))->between('2019-01-01', '2019-12-12'));

        $actual = $cb;

        $this->assertEquals($expected, $actual);
    }

    public function testGroupingOrAnd()
    {
        $expected = "((login LIKE '%java%') OR (email LIKE '%java%') OR (preview LIKE '%java%') OR (desc LIKE '%java%')) AND ((login IS NULL) OR (email IS NULL))";
        $q = 'java';

        // group 1
        $cb1 = new ConditionBuilder();
        $cb1->or((new Condition('login'))->like($q))
            ->or((new Condition('email'))->like($q))
            ->or((new Condition('preview'))->like($q))
            ->or((new Condition('desc'))->like($q));

        // group 2
        $cb2 = new ConditionBuilder();
        $cb2->or((new Condition('login'))->isNull())
            ->or((new Condition('email'))->isNull());

        // group 3
        $cb3 = new ConditionBuilder();
        $cb3->and($cb1)->and($cb2);

        // go for testing
        $actual = $cb3;

        $this->assertEquals($expected, $actual);
    }
}