<?php

namespace TestCase\Openspring\SpringphpFramework\Orm\Query;

use Openspring\SpringphpFramework\Orm\Query\Select;
use PHPUnit\Framework\TestCase;

class SelectTest extends TestCase
{
    const FNAME = 'login';

    public function testJson()
    {
        $this->assertEquals(self::FNAME . Select::PIPE_KWORD . Select::JSON, Select::asJson(self::FNAME));
    }

    public function testHtmlNewLine()
    {
        $this->assertEquals(self::FNAME . Select::PIPE_KWORD . Select::HTML_NEW_LINE, Select::asHtmlNewLined(self::FNAME));
    }

    public function testBoolean()
    {
        $this->assertEquals(self::FNAME . Select::PIPE_KWORD . Select::BOOLEAN, Select::asBool(self::FNAME));
    }

    public function testDateTime()
    {
        $this->assertEquals(self::FNAME . Select::PIPE_KWORD . Select::DATE_TIME, Select::asDateTime(self::FNAME));
    }

    public function testNull_Nvl()
    {
        $this->assertEquals(self::FNAME . Select::PIPE_KWORD . Select::NVL, Select::asNvl(self::FNAME));
    }

    public function testasCamlCased()
    {
        $this->assertEquals('user_id userId', Select::asCamlCased('user_id'));
        $this->assertEquals('u.user_id userId', Select::asCamlCased('u.user_id'));
        $this->assertEquals('userId', Select::asCamlCased('userId'));
        $this->assertEquals('u.userId', Select::asCamlCased('u.userId'));
        $this->assertEquals('u.user_id userId', Select::asCamlCased('u.user_id userId'));
    }
}