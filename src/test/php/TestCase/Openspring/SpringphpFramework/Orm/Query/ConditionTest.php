<?php

namespace TestCase\Openspring\SpringphpFramework\Orm\Query;

use Openspring\SpringphpFramework\Orm\Query\Condition;
use PHPUnit\Framework\TestCase;

class ConditionTest extends TestCase
{

    public function testEquals()
    {
        $this->assertEquals("login = 'khalid'", (new Condition('login'))->equals('khalid'));
        $this->assertEquals("", (new Condition('login'))->equals(''));
        $this->assertEquals("", (new Condition('login'))->equals(' '));
        $this->assertEquals("", (new Condition('login'))->equals(null));
    }

    public function testLikeStrict()
    {
        $this->assertEquals("login LIKE 'khalid'", (new Condition('login'))->likeStrict('khalid'));
        $this->assertEquals("", (new Condition('login'))->likeStrict(''));
        $this->assertEquals("", (new Condition('login'))->likeStrict(' '));
        $this->assertEquals("", (new Condition('login'))->likeStrict(null));
    }

    public function testLike()
    {
        $this->assertEquals("login LIKE '%khalid%'", (new Condition('login'))->like('khalid'));
        $this->assertEquals("", (new Condition('login'))->like(''));
        $this->assertEquals("", (new Condition('login'))->like(' '));
        $this->assertEquals("", (new Condition('login'))->like(null));
    }

    public function testGreaterThan()
    {
        $this->assertEquals("sal > 5000", (new Condition('sal'))->greaterThan(5000));
        $this->assertEquals("", (new Condition('sal'))->greaterThan(''));
        $this->assertEquals("", (new Condition('sal'))->greaterThan(' '));
        $this->assertEquals("", (new Condition('sal'))->greaterThan(null));
    }

    public function testGreaterOrEqualThan()
    {
        $this->assertEquals("sal >= 5000", (new Condition('sal'))->greaterOrEqualThan(5000));
        $this->assertEquals("", (new Condition('sal'))->greaterOrEqualThan(''));
        $this->assertEquals("", (new Condition('sal'))->greaterOrEqualThan(' '));
        $this->assertEquals("", (new Condition('sal'))->greaterOrEqualThan(null));
    }

    public function testLessThan()
    {
        $this->assertEquals("sal < 5000", (new Condition('sal'))->lessThan(5000));
        $this->assertEquals("", (new Condition('sal'))->lessThan(''));
        $this->assertEquals("", (new Condition('sal'))->lessThan(' '));
        $this->assertEquals("", (new Condition('sal'))->lessThan(null));
    }

    public function testLessOrEqualThan()
    {
        $this->assertEquals("sal <= 5000", (new Condition('sal'))->lessOrEqualThan(5000));
        $this->assertEquals("", (new Condition('sal'))->lessOrEqualThan(''));
        $this->assertEquals("", (new Condition('sal'))->lessOrEqualThan(' '));
        $this->assertEquals("", (new Condition('sal'))->lessOrEqualThan(null));
    }

    public function testBetween()
    {
        $this->assertEquals("addDate BETWEEN '2019-02-02' AND '2019-09-09'", (new Condition('addDate'))->between('2019-02-02', '2019-09-09'));
        $this->assertEquals("", (new Condition('addDate'))->between('', '2019-09-09'));
        $this->assertEquals("", (new Condition('addDate'))->between('2019-09-09', ''));
        $this->assertEquals("", (new Condition('addDate'))->between('', ''));
        $this->assertEquals("", (new Condition('addDate'))->between(' ', ' '));
        $this->assertEquals("", (new Condition('addDate'))->between(null, null));
        $this->assertEquals("", (new Condition('addDate'))->between(' ', ''));
        $this->assertEquals("", (new Condition('addDate'))->between(null, ' '));
    }
    
    public function testIsNull()
    {
        $this->assertEquals("login IS NULL", (new Condition('login'))->isNull());
    }

    // test not operators
    public function testNotEquals()
    {
        $this->assertEquals("login <> 'khalid'", (new Condition('login'))->notEquals('khalid'));
        $this->assertEquals("", (new Condition('login'))->notEquals(''));
        $this->assertEquals("", (new Condition('login'))->notEquals(' '));
        $this->assertEquals("", (new Condition('login'))->notEquals(null));
    }

    public function testNotLikeStrict()
    {
        $this->assertEquals("login NOT LIKE 'khalid'", (new Condition('login'))->notLikeStrict('khalid'));
        $this->assertEquals("", (new Condition('login'))->notLikeStrict(''));
        $this->assertEquals("", (new Condition('login'))->notLikeStrict(' '));
        $this->assertEquals("", (new Condition('login'))->notLikeStrict(null));
    }

    public function testNotLike()
    {
        $this->assertEquals("login NOT LIKE '%khalid%'", (new Condition('login'))->notLike('khalid'));
        $this->assertEquals("", (new Condition('login'))->notLike(''));
        $this->assertEquals("", (new Condition('login'))->notLike(' '));
        $this->assertEquals("", (new Condition('login'))->notLike(null));
    }

    public function testNotBetween()
    {
        $this->assertEquals("addDate NOT BETWEEN '2019-02-02' AND '2019-09-09'", (new Condition('addDate'))->notBetween('2019-02-02', '2019-09-09'));
        $this->assertEquals("", (new Condition('addDate'))->notBetween('', '2019-09-09'));
        $this->assertEquals("", (new Condition('addDate'))->notBetween('2019-09-09', ''));
        $this->assertEquals("", (new Condition('addDate'))->notBetween('', ''));
        $this->assertEquals("", (new Condition('addDate'))->notBetween(' ', ' '));
        $this->assertEquals("", (new Condition('addDate'))->notBetween(null, null));
        $this->assertEquals("", (new Condition('addDate'))->notBetween(' ', ''));
        $this->assertEquals("", (new Condition('addDate'))->notBetween(null, ' '));
    }
    
    public function testIsNotNull()
    {
        $this->assertEquals("login IS NOT NULL", (new Condition('login'))->isNotNull());
    }
}