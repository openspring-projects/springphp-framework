<?php

namespace TestCase\Openspring\SpringphpFramework\Orm\Query;

use Openspring\SpringphpFramework\Http\HttpMethod;
use Openspring\SpringphpFramework\Mock\DataMockito;
use Openspring\SpringphpFramework\Orm\DatabaseContext;
use Openspring\SpringphpFramework\Orm\Query\QueryBuilder;
use Openspring\SpringphpFramework\Orm\Type\DataType;
use Openspring\SpringphpFramework\Orm\Type\QueryParameter;
use Openspring\SpringphpFramework\Type\DynamicObject;
use PHPUnit\Framework\TestCase;
use TestCase\Openspring\SpringphpFramework\Helper\SpringphpTestRunner;

class QueryBuilderTest extends TestCase
{

    public static function setUpBeforeClass(): void
    {
        SpringphpTestRunner::setSpringphpContext(true);
        SpringphpTestRunner::runHttpMethod(HttpMethod::PUT, 'esapi/account/update', true);
        SpringphpTestRunner::runApplication(true);
    }

    public function testInstantiation()
    {
        $qb = new QueryBuilder(DatabaseContext::getConnection());
        $this->assertNotNull($qb);
    }

    public function testClear()
    {
        $qb = new QueryBuilder(DatabaseContext::getConnection());

        $qb->select('name', 'age')->from('client');

        $qb->clear();

        $query = $qb->getQuery();

        $this->assertEquals('', $query);
    }

    public function testEmptySelect()
    {
        $qb = new QueryBuilder(DatabaseContext::getConnection());

        $query = $qb->select()
            ->from('client')
            ->getQuery();

        $expected = "SELECT * FROM client";

        $this->assertEquals($expected, $query);
    }

    public function testCamlcaseEmptySelect()
    {
        $qb = new QueryBuilder(DatabaseContext::getConnection());

        $query = $qb->camelcase()
            ->select()
            ->from('client')
            ->getQuery();

        $expected = "SELECT * FROM client";

        $this->assertEquals($expected, $query);
    }

    public function testAliasWithEmptySelect()
    {
        $qb = new QueryBuilder(DatabaseContext::getConnection());

        $query = $qb->for('c')
            ->select()
            ->from('client')
            ->getQuery();

        $expected = "SELECT c.* FROM client";

        $this->assertEquals($expected, $query);
    }

    public function testCamlcaseSelect()
    {
        $qb = new QueryBuilder(DatabaseContext::getConnection());

        $query = $qb->camelcase()
            ->select('age', 'first_name', 'totalStudents')
            ->from('client')
            ->getQuery();

        $expected = "SELECT age, first_name firstName, totalStudents FROM client";

        $this->assertEquals($expected, $query);
    }

    public function testCamlcaseWithTableAliasSelect()
    {
        $qb = new QueryBuilder(DatabaseContext::getConnection());

        $query = $qb->camelcase()
            ->for('c')
            ->select('age', 'first_name', 'totalStudents')
            ->from('client')
            ->getQuery();

        $expected = "SELECT c.age, c.first_name firstName, c.totalStudents FROM client";

        $this->assertEquals($expected, $query);
    }

    public function testCamlcaseWithTableAliasSelectManyTabs()
    {
        $qb = new QueryBuilder(DatabaseContext::getConnection());

        $query = $qb->camelcase()
            ->for('c')
            ->select('age', 'first_name', 'totalStudents')
            ->for('u')
            ->select('login', 'password', 'add_date')
            ->from('client c')
            ->from('user u')
            ->getQuery();

        $expected = "SELECT c.age, c.first_name firstName, c.totalStudents, u.login, u.password, u.add_date addDate FROM client c, user u";

        $this->assertEquals($expected, $query);
    }

    public function testSelectFromFirst()
    {
        $qb = new QueryBuilder(DatabaseContext::getConnection());

        $query = $qb->camelcase()
            ->from('client c')
            ->from('user u')
            ->for('c')
            ->select('age', 'first_name', 'totalStudents')
            ->for('u')
            ->select('login', 'password', 'add_date')
            ->getQuery();

        $expected = "SELECT c.age, c.first_name firstName, c.totalStudents, u.login, u.password, u.add_date addDate FROM client c, user u";

        $this->assertEquals($expected, $query);
    }

    public function testSelect()
    {
        $qb = new QueryBuilder(DatabaseContext::getConnection());

        $query = $qb->select('name', 'age')
            ->from('client')
            ->getQuery();

        $expected = "SELECT name, age FROM client";

        $this->assertEquals($expected, $query);
    }

    public function testWhere()
    {
        $qb = new QueryBuilder(DatabaseContext::getConnection());

        $query = $qb->select('name', 'age')
            ->from('client')
            ->where("name like '%kha%'")
            ->getQuery();

        $expected = "SELECT name, age FROM client WHERE name like '%kha%'";

        $this->assertEquals($expected, $query);
    }

    public function testGroupBy()
    {
        $qb = new QueryBuilder(DatabaseContext::getConnection());

        $query = $qb->select('name', 'age', 'city')
            ->from('client')
            ->where("name like '%kha%'")
            ->groupBy('city')
            ->getQuery();

        $expected = "SELECT name, age, city FROM client WHERE name like '%kha%' GROUP BY city";

        $this->assertEquals($expected, $query);
    }

    public function testOrderBy()
    {
        $qb = new QueryBuilder(DatabaseContext::getConnection());

        $query = $qb->select('name', 'age', 'city')
            ->from('client')
            ->where("name like '%kha%'")
            ->groupBy('city')
            ->orderBy('name')
            ->getQuery();

        $expected = "SELECT name, age, city FROM client WHERE name like '%kha%' GROUP BY city ORDER BY name";

        $this->assertEquals($expected, $query);
    }

    public function testHaving()
    {
        $qb = new QueryBuilder(DatabaseContext::getConnection());

        $query = $qb->select('name', 'age', 'city')
            ->from('client')
            ->where("name like '%kha%'")
            ->groupBy('city')
            ->having('count > 0')
            ->getQuery();

        $expected = "SELECT name, age, city FROM client WHERE name like '%kha%' GROUP BY city HAVING count > 0";

        $this->assertEquals($expected, $query);
    }

    public function testInnerJoin()
    {
        $qb = new QueryBuilder(DatabaseContext::getConnection());

        $query = $qb->select('name', 'age', 'city')
            ->from('client')
            ->innerJoin('order', 'client.client_id=order.client_id')
            ->getQuery();

        $expected = "SELECT name, age, city FROM client INNER JOIN order ON client.client_id=order.client_id";

        $this->assertEquals($expected, $query);
    }

    public function testLeftjoin()
    {
        $qb = new QueryBuilder(DatabaseContext::getConnection());

        $query = $qb->select('name', 'age', 'city')
            ->from('client')
            ->leftJoin('order', 'client.client_id=order.client_id')
            ->getQuery();

        $expected = "SELECT name, age, city FROM client LEFT JOIN order ON client.client_id=order.client_id";

        $this->assertEquals($expected, $query);
    }

    public function testSetParameter()
    {
        $qb = new QueryBuilder(DatabaseContext::getConnection());

        $query = $qb->select('name', 'age', 'city')
            ->from('client')
            ->where("client_id=:idClient")
            ->setParameter('idClient', 100, DataType::INT)
            ->getQuery();

        $expected = "SELECT name, age, city FROM client WHERE client_id=100";

        $this->assertEquals($expected, $query);
    }

    public function testSetQueryParameter()
    {
        $qb = new QueryBuilder(DatabaseContext::getConnection());

        $query = $qb->select('name', 'age', 'city')
            ->from('client')
            ->where("client_id=:idClient")
            ->setQueryParameter(new QueryParameter('idClient', 100, DataType::INT))
            ->getQuery();

        $expected = "SELECT name, age, city FROM client WHERE client_id=100";

        $this->assertEquals($expected, $query);
    }

    public function testSetQueryParameters()
    {
        $qb = new QueryBuilder(DatabaseContext::getConnection());

        $query = $qb->select('name', 'age', 'city')
            ->from('client')
            ->where("client_id=:idClient")
            ->setQueryParameters(array(
            new QueryParameter('idClient', 100, DataType::INT)))
            ->getQuery();

        $expected = "SELECT name, age, city FROM client WHERE client_id=100";

        $this->assertEquals($expected, $query);
    }

    public function testSetQuery()
    {
        $qb = new QueryBuilder(DatabaseContext::getConnection());

        $query = $qb->select('name', 'age', 'city')
            ->from('client')
            ->getQuery();
        $expected = "SELECT name, age, city FROM client";

        $this->assertEquals($expected, $query);

        // set query
        $query = $qb->setQuery('SELECT * FROM client')->getQuery();
        $expected = "SELECT * FROM client";

        $this->assertEquals($expected, $query);
    }

    public function testReset()
    {
        $qb = new QueryBuilder(DatabaseContext::getConnection());

        $query = $qb->select('name', 'age', 'city')
            ->from('client')
            ->getQuery();
        $expected = "SELECT name, age, city FROM client";

        $this->assertEquals($expected, $query);

        // reset
        $query = $qb->reset()->getQuery();
        $expected = '';

        $this->assertEquals($expected, $query);
    }

    public function testGetWhere()
    {
        $qb = new QueryBuilder(DatabaseContext::getConnection());

        $where = $qb->select('name', 'age', 'city')
            ->from('client')
            ->where("client_id=:idClient")
            ->setQueryParameters(array(
            new QueryParameter('idClient', 100, DataType::INT)))
            ->getQueryWhere();
        $expected = "client_id=100";

        $this->assertEquals($expected, $where);
    }

    public function testExecute()
    {
        $clients = array(
            new DynamicObject(array(
                'name' => 'toto')),
            new DynamicObject(array(
                'name' => 'fofo')),
            new DynamicObject(array(
                'name' => 'hoho')),
            new DynamicObject(array(
                'name' => 'roro')),
            new DynamicObject(array(
                'name' => 'coco')));

        $qb = new QueryBuilder(DatabaseContext::getConnection());

        // add mocks
        DataMockito::when('SELECT name FROM client')->thenReturn($clients);

        // run query
        $resultList = $qb->select('name')
            ->from('client')
            ->execute();

        $this->assertEquals($clients, $resultList->toArray());
    }

    public function testExecuteFirst()
    {
        $toto = new DynamicObject(array(
            'name' => 'toto'));
        $fofo = new DynamicObject(array(
            'name' => 'fofo'));
        $hoho = new DynamicObject(array(
            'name' => 'hoho'));
        $roro = new DynamicObject(array(
            'name' => 'roro'));
        $coco = new DynamicObject(array(
            'name' => 'coco'));

        $clients = array(
            $toto,
            $fofo,
            $hoho,
            $roro,
            $coco);

        $qb = new QueryBuilder(DatabaseContext::getConnection());

        // add mocks
        DataMockito::when('SELECT name FROM client')->thenReturn($clients);

        // run query
        $resultList = $qb->select('name')
            ->from('client')
            ->execute();

        $first = $resultList->first();
        $last = $resultList->last();

        $this->assertEquals($toto, $first);
        $this->assertEquals($coco, $last);
    }

    /*public function testQbWithConditionBuilder()
    {
        $expected = "SELECT u.first_name firstName, u.last_name lastName FROM #__user u WHERE (u.name = 'khalid')";
        $qb = new QueryBuilder(DatabaseContext::getConnection());

        $u = new Alias('#__user', 'u');
        $cb = new ConditionBuilder();

        $cb->and((new Condition($u->FN('name')))->equals('khalid'));

        $query = $qb->select($u->CC('first_name'))
            ->select($u->CC('last_name'))
            ->from($u)
            ->where($cb)
            ->getQuery();

        $this->assertEquals($expected, $query);
    }*/
}