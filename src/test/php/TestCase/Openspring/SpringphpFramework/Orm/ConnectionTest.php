<?php
namespace TestCase\Openspring\SpringphpFramework\Orm;

use Openspring\SpringphpFramework\Http\HttpMethod;
use Openspring\SpringphpFramework\Mock\DataMockito;
use Openspring\SpringphpFramework\Orm\DatabaseContext;
use TestCase\Openspring\SpringphpFramework\Helper\SpringphpTestRunner;
use PHPUnit\Framework\TestCase;
use Openspring\SpringphpFramework\Orm\Connection;
use Openspring\SpringphpFramework\Orm\Driver\Mysql\MysqlDriverMock;

class ConnectionTest extends TestCase
{
    private static $connection;

    public static function setUpBeforeClass(): void
    {
        SpringphpTestRunner::bootApplication(true, HttpMethod::PUT, 'esapi/account/update', true);
        static::$connection = DatabaseContext::getConnection();
        DataMockito::enableDebug();
    }

    public function connection(): Connection
    {
        return static::$connection;
    }

    public function testInstantiation()
    {
        $this->assertNotNull($this->connection());
    }

    public function testGetDataSourceConfig()
    {
        $config = $this->connection()->getDataSourceConfig();

        $this->assertEquals('mockdb', $config->getDatabase());
        $this->assertEquals(MysqlDriverMock::class, $config->getDialect());
        $this->assertEquals('localhost', $config->getHost());
        $this->assertEquals('root', $config->getUser());
        $this->assertEquals('', $config->getPassword());
        $this->assertEquals('3306', $config->getPort());
        $this->assertEquals('esapi_', $config->getTablePrefix());
    }

    public function testGetDriver()
    {
        $this->assertNotNull($this->connection()->getDriver());
    }
}