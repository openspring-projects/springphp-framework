<?php
namespace TestCase\Openspring\SpringphpFramework\Orm\Type;

use Openspring\SpringphpFramework\Orm\Type\DataType;
use PHPUnit\Framework\TestCase;

class DataTypeTest extends TestCase
{
    public function testGetTypeName()
    {
        $this->assertEquals('GUID', DataType::getTypeName(0));
        $this->assertEquals('VARCHAR', DataType::getTypeName(1));
        $this->assertEquals('INT', DataType::getTypeName(2));
        $this->assertEquals('FLOAT', DataType::getTypeName(3));
        $this->assertEquals('DATE', DataType::getTypeName(4));
        $this->assertEquals('DATETIME', DataType::getTypeName(5));
        $this->assertEquals('BOOLEAN', DataType::getTypeName(6));
        $this->assertEquals('HASH', DataType::getTypeName(7));
        $this->assertEquals('HTML', DataType::getTypeName(8));
        $this->assertEquals('XML', DataType::getTypeName(9));
        $this->assertEquals('JSON', DataType::getTypeName(10));
    }
}