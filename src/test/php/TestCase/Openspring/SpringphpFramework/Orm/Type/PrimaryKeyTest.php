<?php
namespace TestCase\Openspring\SpringphpFramework\Orm\Type;

use Openspring\SpringphpFramework\Orm\Type\GenerationType;
use Openspring\SpringphpFramework\Orm\Type\PrimaryKey;
use PHPUnit\Framework\TestCase;

class PrimaryKeyTest extends TestCase
{
    public function testAutoAndManual()
    {
        $pkAuto = new PrimaryKey(GenerationType::AUTO);

        $this->assertEquals(GenerationType::AUTO, $pkAuto->getStrategy());
        $this->assertTrue($pkAuto->isAUTO());
        $this->assertFalse($pkAuto->isMANUAL());

        $pkAuto->setStrategy(GenerationType::MANUAL);

        $this->assertEquals(GenerationType::MANUAL, $pkAuto->getStrategy());
        $this->assertTrue($pkAuto->isMANUAL());
        $this->assertFalse($pkAuto->isAUTO());
    }
}