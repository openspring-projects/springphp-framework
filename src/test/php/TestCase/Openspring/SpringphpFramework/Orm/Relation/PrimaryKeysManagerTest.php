<?php
namespace TestCase\Openspring\SpringphpFramework\Orm\Relation;

use Openspring\SpringphpFramework\Orm\Column;
use Openspring\SpringphpFramework\Orm\Relation\PrimaryKeysManager;
use Openspring\SpringphpFramework\Orm\Type\DataType;
use Openspring\SpringphpFramework\Orm\Type\GenerationType;
use Openspring\SpringphpFramework\Orm\Type\PrimaryKey;
use PHPUnit\Framework\TestCase;

class PrimaryKeysManagerTest extends TestCase
{

    public function testGetPrimaryKeysAuto()
    {
        $userId = new Column(null, 'user_id', 32, DataType::INT, new PrimaryKey(GenerationType::AUTO));
        $userId->set(10);

        $pkeys = array($userId);
        $pkeysMngr = new PrimaryKeysManager($pkeys);

        $this->assertEquals($pkeys, $pkeysMngr->getPrimaryKeys());
        $this->assertEquals('(user_id=10)', $pkeysMngr->getWhereCondition());
        $this->assertTrue($pkeysMngr->isAUTO());
        $this->assertFalse($pkeysMngr->isEditMode());

        $pkeysMngr->setAutoInsertId(20);
        $this->assertEquals('(user_id=20)', $pkeysMngr->getWhereCondition());

        $pkeys = array(new Column(null, 'user_id', 5, DataType::INT, new PrimaryKey(GenerationType::MANUAL), false, false, 10),new Column(null, 'group_id', 5, DataType::INT, new PrimaryKey(GenerationType::MANUAL), false, false, 11));
        $pkeysMngr->setPrimaryKeys($pkeys);
        $this->assertFalse($pkeysMngr->isAUTO());
    }

    public function testGetPrimaryKeysManual()
    {
        $userId1 = new Column(null, 'user_id', 5, DataType::INT, new PrimaryKey(GenerationType::MANUAL));
        $userId1->set(10);

        $userId2 = new Column(null, 'group_id', 5, DataType::INT, new PrimaryKey(GenerationType::MANUAL));
        $userId2->set(11);

        $pkeys = array($userId1,$userId2);
        
        $pkeysMngr = new PrimaryKeysManager($pkeys);

        $this->assertEquals($pkeys, $pkeysMngr->getPrimaryKeys());
        $this->assertEquals('(user_id=10) AND (group_id=11)', $pkeysMngr->getWhereCondition());
        $this->assertFalse($pkeysMngr->isAUTO());
        $this->assertFalse($pkeysMngr->isEditMode());
    }
}