<?php
namespace TestCase\Openspring\SpringphpFramework\Orm;

use Openspring\SpringphpFramework\Exception\Data\InvalidDatabaseOperationException;
use Openspring\SpringphpFramework\Http\HttpMethod;
use Openspring\SpringphpFramework\Mock\DataMockito;
use Openspring\SpringphpFramework\Orm\DatabaseContext;
use Openspring\SpringphpFramework\Orm\Query\QueryBuilder;
use Openspring\SpringphpFramework\Type\DynamicObject;
use PHPUnit\Framework\TestCase;
use TestCase\Openspring\SpringphpFramework\Helper\SpringphpTestRunner;
use TestCase\Openspring\SpringphpFramework\Helper\UserEntity;

class DaoSupportTest extends TestCase
{
	private $qb;
	
    public static function setUpBeforeClass(): void
    {
        SpringphpTestRunner::bootApplication(true, HttpMethod::PUT, 'esapi/account/update', true);
    }
	
	public function setUp(): void
	{
		$this->qb = new QueryBuilder(DatabaseContext::getConnection());
		DataMockito::enableDebug();
	}
	
	public function testInstantiation()
    {
		$userBo = new UserEntity();
		
		$this->assertNotNull($userBo);
    }
    
	public function testLoadById()
    {
		$userToto = new DynamicObject(array(
			'user_id' => 100, 
			'group_id' => 1, 
			'login' => 'khalid')
		);
		
		// add mocks
		$expected = "SELECT * FROM esapi_user WHERE user_id='100'";
		DataMockito::when($expected)->thenReturn(array($userToto));
        
		// call and assert
		$userBo = new UserEntity();		
		$result = $userBo->loadById('user_id', 100);
        
        $this->assertTrue($result);
		
		$this->assertEquals(100, $userBo->getUserId());
		$this->assertEquals(1, $userBo->getGroupId());
		$this->assertEquals('khalid', $userBo->getLogin());
    }
	
    public function testLoadByIds()
    {
        $userToto = new DynamicObject(array(
            'user_id' => 100,
            'group_id' => 1,
            'login' => 'khalid')
            );
        
        // add mocks
        $expected = "SELECT * FROM esapi_user WHERE user_id='100' AND group_id=1";
        DataMockito::when($expected)->thenReturn(array($userToto));
        
        // call and assert
        $userBo = new UserEntity();
        $result = $userBo->loadByIds(array('user_id' => 100, 'group_id' => 1));
        
        $this->assertTrue($result);
        
        $this->assertEquals(100, $userBo->getUserId());
        $this->assertEquals(1, $userBo->getGroupId());
        $this->assertEquals('khalid', $userBo->getLogin());
    }
    
    public function testLoadByMax()
    {
        $userToto = new DynamicObject(array(
            'user_id' => 100,
            'group_id' => 1,
            'login' => 'khalid')
            );
        
        // add mocks
        $expected = "SELECT * FROM esapi_user WHERE group_id=(SELECT MAX(group_id) FROM esapi_user)";
        DataMockito::when($expected)->thenReturn(array($userToto));
        
        // call and assert
        $userBo = new UserEntity();
        $result = $userBo->loadByMax('group_id');
        
        $this->assertTrue($result);
        
        $this->assertEquals(100, $userBo->getUserId());
        $this->assertEquals(1, $userBo->getGroupId());
        $this->assertEquals('khalid', $userBo->getLogin());
    }
	
	public function testGetNextMaxValue()
    {
		$dto = new DynamicObject(array('MAX_VALUE' => 5));
		
		// add mocks
		$expected = "SELECT MAX(group_id) MAX_VALUE FROM esapi_user";
		DataMockito::when($expected)->thenReturn(array($dto));
        
		// call and assert
		$userBo = new UserEntity();		
		$max = $userBo->getNextMaxValue('group_id');
        
        $this->assertEquals(6, $max);
    }
	
	public function testGetMaxValue()
    {
		$dto = new DynamicObject(array('MAX_VALUE' => 5));
		
		// add mocks
		$expected = "SELECT MAX(group_id) MAX_VALUE FROM esapi_user";
		DataMockito::when($expected)->thenReturn(array($dto));
        
		// call and assert
		$userBo = new UserEntity();		
		$max = $userBo->getMaxValue('group_id');
        
        $this->assertEquals(5, $max);
    }
	
    public function testExistById()
    {
        $dto = new DynamicObject(array('TOTAL' => 1));
        
        // add mocks
        $expected = "SELECT COUNT(1) TOTAL FROM esapi_user WHERE group_id=1";
        DataMockito::when($expected)->thenReturn(array($dto));
        
        $userBo = new UserEntity();
        $result = $userBo->existById('group_id', 1);
        
        $this->assertTrue($result);
    }
    
    public function testExistByIds()
    {
        $dto = new DynamicObject(array('TOTAL' => 1));
        
        // add mocks
        $expected = "SELECT COUNT(1) TOTAL FROM esapi_user WHERE user_id='1000' AND group_id=1";
        DataMockito::when($expected)->thenReturn(array($dto));
        
        $userBo = new UserEntity();
        $result = $userBo->existByIds(array('user_id' => '1000', 'group_id' => 1));
        
        $this->assertTrue($result);
    }
	
	public function testLoadByCond()
    {
		$item1 = new DynamicObject(array('user_id' => 100, 'group_id' => 1, 'login' => 'toto'));
		$item2 = new DynamicObject(array('user_id' => 200, 'group_id' => 1, 'login' => 'fofo'));
		$item3 = new DynamicObject(array('user_id' => 300, 'group_id' => 2, 'login' => 'koko'));
		
		// add mocks
		$expected = "SELECT user_id userId,group_id groupId,login login FROM esapi_user WHERE userId IN(100, 200, 300)";
		DataMockito::when($expected)->thenReturn(array($item1, $item2, $item3));
        
		// call and assert
		$userBo = new UserEntity();		
		$resultList = $userBo->where('userId IN(100, 200, 300)');
        
        $this->assertEquals($resultList->totalRows, 3);
    }
    
    public function testSetGet()
    {
        $userBo = new UserEntity();
        $userBo->setUserId('100000000');
        
        $this->assertEquals('100000000', $userBo->get('userId'));
        
        $userBo->set('userId', '20000000');
        $this->assertEquals('20000000', $userBo->get('userId'));
    }
    
    public function testIEditMode()
    {
        $item1 = new DynamicObject(array('user_id' => 100, 'group_id' => 1, 'login' => 'toto'));
        
        // add mocks
        $expected = "SELECT * FROM esapi_user WHERE user_id='100'";
        DataMockito::when($expected)->thenReturn(array($item1));
        
        // call and assert
        $userBo = new UserEntity();
        $this->assertFalse($userBo->isEditMode());
        
        $userBo->loadById('user_id', '100');
        $this->assertTrue($userBo->isEditMode());
    }
    
    public function testSaveInsert()
    {
        // add mocks
        $expected = "INSERT INTO esapi_user(user_id,group_id,login) VALUES('100',1,'khalid')";
        DataMockito::when($expected)->thenReturn(true);
        
        // call and assert
        $userBo = new UserEntity();
        
        $userBo->setUserId('100');
        $userBo->setGroupId(1);
        $userBo->setLogin('khalid');
        
        $this->assertTrue($userBo->save());
    }
    
    public function testSaveUpdateByManualEdit()
    {
        // add mocks
        $expected = "UPDATE esapi_user SET group_id=1,login='khalid' WHERE ((user_id='100'))";
        DataMockito::when($expected)->thenReturn(true);
        
        // call and assert
        $userBo = new UserEntity();
        
        $userBo->edit();
        
        $userBo->setUserId('100');
        $userBo->setGroupId(1);
        $userBo->setLogin('khalid');
        
        $this->assertTrue($userBo->save());
    }
    
    public function testSaveUpdateByAutoEdit()
    {
        $userToto = new DynamicObject(array(
            'user_id' => 100,
            'group_id' => 1,
            'login' => 'khalid')
            );
        
        // add mocks
        $loadExpected = "SELECT * FROM esapi_user WHERE user_id='100'";
        $saveExpected = "UPDATE esapi_user SET login='rachid' WHERE ((user_id='100'))";
        DataMockito::when($loadExpected)->thenReturn(array($userToto));
        DataMockito::when($saveExpected)->thenReturn(true);
        
        // call and assert
        $userBo = new UserEntity();
        $result = $userBo->loadById('user_id', 100);
        
        $this->assertTrue($result);
        
        $this->assertEquals(100, $userBo->getUserId());
        $this->assertEquals(1, $userBo->getGroupId());
        $this->assertEquals('khalid', $userBo->getLogin());
        
        // update and save
        $userBo->setLogin('rachid');
        $saved = $userBo->save();
        
        $this->assertTrue($saved);
        
        $this->assertEquals(100, $userBo->getUserId());
        $this->assertEquals(1, $userBo->getGroupId());
        $this->assertEquals('rachid', $userBo->getLogin());
    }
    
    public function testDelete()
    {
        $userToto = new DynamicObject(array(
            'user_id' => 100,
            'group_id' => 1,
            'login' => 'khalid')
            );
        
        // add mocks
        $expected = "SELECT * FROM esapi_user WHERE user_id='100'";
        $deleteExcpected = "DELETE FROM esapi_user WHERE (user_id='100')";
        DataMockito::when($expected)->thenReturn(array($userToto));
        DataMockito::when($deleteExcpected)->thenReturn(true);
        
        // call and assert
        $userBo = new UserEntity();
        $result = $userBo->loadById('user_id', 100);
        
        $this->assertTrue($result);
        
        $this->assertEquals(100, $userBo->getUserId());
        $this->assertEquals(1, $userBo->getGroupId());
        $this->assertEquals('khalid', $userBo->getLogin());
        
        $deleted = $userBo->delete();
        
        $this->assertTrue($deleted);
    }
    
    public function testDeleteException()
    {
        $userBo = new UserEntity();
        
        $this->expectException(InvalidDatabaseOperationException::class);
        $userBo->delete();
    }
    
    public function testDeleteCascade()
    {
        $exptectedDeleteComments = "DELETE FROM esapi_comments WHERE (user_id='100')";
        $exptectedDeletePosts = "DELETE FROM esapi_posts WHERE (user_id='100')";
        $exptectedDeleteUser = "DELETE FROM esapi_user WHERE (user_id='100')";
        
        DataMockito::when($exptectedDeleteComments)->thenReturn(true);
        DataMockito::when($exptectedDeletePosts)->thenReturn(true);
        DataMockito::when($exptectedDeleteUser)->thenReturn(true);
        
        $userBo = new UserEntity();
        $userBo->edit();
        $userBo->setUserId('100');
        $deleted = $userBo->deleteCascade(['comments', 'posts']);
        
        $this->assertTrue($deleted);
    }
    
    public function testDeleteItems()
    {
        $exptected = "DELETE FROM esapi_user WHERE group_id=1";
        DataMockito::when($exptected)->thenReturn(true);
        
        $userBo = new UserEntity();
        $deleted = $userBo->deleteItems('group_id=1');
        
        $this->assertTrue($deleted);
    }
    
    public function testGetPrimaryWhereCondition()
    {
        $userBo = new UserEntity();
        
        $userBo->setUserId('100');
        $pkeysWhere = $userBo->getPrimaryWhereCondition();
        
        $this->assertEquals("(user_id='100')", $pkeysWhere);
    }
    
    public function testToArray()
    {
        $expected = array(
            'userId' => 100,
            'groupId' => 1,
            'login' => 'khalid');
        
        $userBo = new UserEntity();
        
        $userBo->setUserId('100');
        $userBo->setGroupId(1);
        $userBo->setLogin('khalid');
        
        $array = $userBo->toArray();
        
        $this->assertEquals($expected, $array);
    }
    
    public function testToSimpleObject()
    {
        $expected = new DynamicObject(array(
            'userId' => '100',
            'groupId' => '1',
            'login' => 'khalid'));
        
        $userBo = new UserEntity();
        
        $userBo->setUserId('100');
        $userBo->setGroupId(1);
        $userBo->setLogin('khalid');
        
        $simpleObject = $userBo->toSimpleObject();
        
        $this->assertEquals($expected->userId, $simpleObject->userId);
        $this->assertEquals($expected->groupId, $simpleObject->groupId);
        $this->assertEquals($expected->login, $simpleObject->login);
        
        $this->assertCount(3, ((array)$simpleObject));
    }
}