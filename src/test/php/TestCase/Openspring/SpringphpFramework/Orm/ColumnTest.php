<?php
namespace TestCase\Openspring\SpringphpFramework\Orm;

use Openspring\SpringphpFramework\Exception\InvalidEnumerationException;
use Openspring\SpringphpFramework\Orm\DaoSupport;
use Openspring\SpringphpFramework\Orm\Type\DataType;
use Openspring\SpringphpFramework\Orm\Column;
use Openspring\SpringphpFramework\Orm\Type\GenerationType;
use Openspring\SpringphpFramework\Orm\Type\PrimaryKey;
use PHPUnit\Framework\TestCase;
use TestCase\Openspring\SpringphpFramework\Helper\PersonEntity;
use TestCase\Openspring\SpringphpFramework\Helper\UserEntity;
use Openspring\SpringphpFramework\Type\DynamicObject;
use Openspring\SpringphpFramework\Type\Any;

class ColumnTest extends TestCase
{
    public function testInstantiation()
    {
        $userBo = new UserEntity();
        
        $this->assertNotNull($userBo);
    }
    
    public function testGetEntity()
    {
        $userBo = new UserEntity();
        
        $this->assertInstanceOf(DaoSupport::class, $userBo->userId->getEntity());
    }
    
    public function testSetGetName()
    {
        $userBo = new UserEntity();
        
        $this->assertEquals('user_id', $userBo->userId->getName());
        
        $userBo->userId->setName('userId');
        $this->assertEquals('userId', $userBo->userId->getName());
    }
    
    public function testPrimaryKey()
    {
        $userBo = new UserEntity();
        
        // check userId is a manual pk
        $this->assertTrue($userBo->userId->isPrimaryKey());
        $this->assertTrue($userBo->userId->getPrimaryKey()->isMANUAL());
        $this->assertFalse($userBo->userId->getPrimaryKey()->isAUTO());
        
        // check groupId is not pk
        $this->assertFalse($userBo->groupId->isPrimaryKey());
        $this->assertNull($userBo->groupId->getPrimaryKey());
        
        $userBo->userId->setPrimaryKey(new PrimaryKey(GenerationType::AUTO));
        
        // check userId is an auto pk
        $this->assertTrue($userBo->userId->isPrimaryKey());
        $this->assertFalse($userBo->userId->getPrimaryKey()->isMANUAL());
        $this->assertTrue($userBo->userId->getPrimaryKey()->isAUTO());
        
        // test multiple pkeys
        $userBo->userId->setPrimaryKey(new PrimaryKey(GenerationType::MANUAL));
        $userBo->groupId->setPrimaryKey(new PrimaryKey(GenerationType::MANUAL));
        
        $this->assertTrue($userBo->userId->isPrimaryKey());
        $this->assertTrue($userBo->userId->getPrimaryKey()->isMANUAL());
        $this->assertFalse($userBo->userId->getPrimaryKey()->isAUTO());
        
        $this->assertTrue($userBo->groupId->isPrimaryKey());
        $this->assertTrue($userBo->groupId->getPrimaryKey()->isMANUAL());
        $this->assertFalse($userBo->groupId->getPrimaryKey()->isAUTO());
    }
    
    public function testChanged()
    {
        $userBo = new UserEntity();
        
        $this->assertFalse($userBo->userId->isChanged());
        
        $userBo->userId->setChanged(true);
        $this->assertTrue($userBo->userId->isChanged());
    }
    
    public function testColumnLen()
    {
        $userBo = new UserEntity();
        $this->assertEquals(32, $userBo->userId->getColumnLen());
        
        $userBo->userId->setColumnLen(64);
        $this->assertEquals(64, $userBo->userId->getColumnLen());
    }
    
    public function testUnique()
    {
        $userBo = new UserEntity();
        
        $this->assertFalse($userBo->groupId->isUnique());
        
        // check still true because userId is pk
        $userBo->groupId->setUnique(true);
        $this->assertTrue($userBo->groupId->isUnique());
    }
    
    public function testRequired()
    {
        $userBo = new UserEntity();
        
        $this->assertFalse($userBo->userId->isRequired());
        
        // check still true because userId is pk
        $userBo->userId->setRequired(true);
        $this->assertTrue($userBo->userId->isRequired());
        
        $this->assertFalse($userBo->groupId->isUnique());
        
        // check still true because userId is pk
        $userBo->groupId->setRequired(true);
        $this->assertTrue($userBo->groupId->isRequired());
    }
    
    public function testColumnType()
    {
        $userBo = new UserEntity();
        $this->assertEquals(DataType::VARCHAR, $userBo->userId->getColumnType());
        
        $userBo->userId->setColumnType(DataType::INT);
        $this->assertEquals(DataType::INT, $userBo->userId->getColumnType());
    }
    
    public function testColumnTypeException()
    {
        $userBo = new UserEntity();
        
        $this->expectException(InvalidEnumerationException::class);
        $userBo->userId->setColumnType('sometype');
    }
    
    public function testSetValue()
    {
        $userBo = new UserEntity();
        
        $userBo->userId->setValue('200');
        $this->assertEquals(200, $userBo->userId->get());
    }
	
	public function testSetGet()
    {
        $json = new DynamicObject(array("name" => "toto"));
        
        $personBo = new PersonEntity();
		
        $personBo->setName('toto');
        $personBo->setAge(36);
        $personBo->setSalary(15000);
        $personBo->setBirth_date('1983-06-17');
        $personBo->setLogin_time('1983-06-17 18:02:02');
        $personBo->setMarried(true);
        $personBo->setPassword('toto@2019');
        $personBo->setHtml('<b>toto</b>');
        $personBo->setXml('<b>toto</b>');
        $personBo->setJson('{"name": "toto"}');
        
        $this->assertEquals(32, $personBo->person_id->length());
        $this->assertEquals(36, $personBo->getAge());
        $this->assertEquals(15000, $personBo->getSalary());
        $this->assertEquals('1983-06-17', $personBo->getBirth_date());
        $this->assertEquals('1983-06-17 18:02:02', $personBo->getLogin_time());
        $this->assertEquals(1, $personBo->getMarried());
        $this->assertEquals(32, $personBo->password->length());
        
        $this->assertEquals('<b>toto</b>', $personBo->getHtml());
        $this->assertEquals('<b>toto</b>', $personBo->getXml());
        
        $this->assertEquals($json->name, $personBo->getJson()->name);
    }
    
    public function testGetAsBoolean()
    {
        $personBo = new PersonEntity();
        
        $personBo->setMarried(true);
        $this->assertTrue($personBo->married->getAsBoolean());
        
        $personBo->setMarried(false);
        $this->assertFalse($personBo->married->getAsBoolean());
    }
    
    public function testIncrementDecrement()
    {
        $personBo = new PersonEntity();
        
        $personBo->setAge(36);
        $this->assertEquals(36, $personBo->getAge());
        
        $personBo->age->increment();
        $this->assertEquals(37, $personBo->getAge());
        
        $personBo->age->decrement();
        $personBo->age->decrement();
        $this->assertEquals(35, $personBo->getAge());
    }
	
	public function testSetCurrentDateAndCurrentDateTime()
    {
        $personBo = new PersonEntity();

        $this->assertNull($personBo->getBirth_date());
		$this->assertNull($personBo->getLogin_time());
		
		$personBo->birth_date->setCurrentDate();
		$personBo->login_time->setCurrentDateTime();
		
		$this->assertNotNull($personBo->getBirth_date());
		$this->assertNotNull($personBo->getLogin_time());
    }
	
	public function testReplaceAll()
    {
        $personBo = new PersonEntity();

        $personBo->setName('toto');
		$personBo->name->replaceAll('t', 'f');
        $this->assertEquals('fofo', $personBo->getName());
    }
	
	public function testCapitalize()
    {
        $personBo = new PersonEntity();

        $personBo->setName('toto');
		$personBo->name->capitalize();
        $this->assertEquals('Toto', $personBo->getName());
    }
	
	public function testCapitalizeFirstWord()
    {
        $personBo = new PersonEntity();

        $personBo->setName('toto tita');
		$personBo->name->capitalizeFirstWord();
        $this->assertEquals('Toto tita', $personBo->getName());
    }
	
	public function testCapitalizeAllWords()
    {
        $personBo = new PersonEntity();

        $personBo->setName('toto tita');
		$personBo->name->capitalizeAllWords();
        $this->assertEquals('Toto Tita', $personBo->getName());
    }
	
	public function testUppercaseLowercase()
    {
        $personBo = new PersonEntity();
        $personBo->setName('tOTo');
		
		$personBo->name->uppercase();
        $this->assertEquals('TOTO', $personBo->getName());
		
		$personBo->name->lowercase();
        $this->assertEquals('toto', $personBo->getName());
    }
	
	public function testPlusMinus()
    {
        $personBo = new PersonEntity();
        
        $personBo->setAge(36);
        $this->assertEquals(36, $personBo->getAge());
        
        $personBo->age->plus(4);
        $this->assertEquals(40, $personBo->getAge());
        
        $personBo->age->minus(5);
        $this->assertEquals(35, $personBo->getAge());
    }
	
	public function testEqualsAndIs_IgnoreCase()
    {
        $personBo = new PersonEntity();
        
        $personBo->setName('toto');
        $this->assertTrue($personBo->name->equals('toto'));
		$this->assertFalse($personBo->name->equals('Toto'));
		$this->assertTrue($personBo->name->is('toto'));
		
		$this->assertTrue($personBo->name->equalsIgnoreCase('Toto'));
		$this->assertTrue($personBo->name->isIgnoreCase('TOTO'));
    }
	
	public function testIsNumeric()
    {
        $personBo = new PersonEntity();

        $this->assertFalse($personBo->person_id->isNumeric());
        $this->assertTrue($personBo->age->isNumeric());
        $this->assertTrue($personBo->salary->isNumeric());
        $this->assertFalse($personBo->birth_date->isNumeric());
        $this->assertFalse($personBo->login_time->isNumeric());
        $this->assertTrue($personBo->married->isNumeric());
        $this->assertFalse($personBo->password->isNumeric());
    }
	
	public function testIsEmpty()
    {
        $personBo = new PersonEntity();
		
		$personBo->setName('toto');

        $this->assertFalse($personBo->name->isEmpty());
        $this->assertTrue($personBo->age->isEmpty());
    }
	
	public function testInArray()
    {
        $personBo = new PersonEntity();
		
		$personBo->setName('toto');

        $this->assertTrue($personBo->name->inArray(array('toto', 'koko')));
        $this->assertFalse($personBo->name->inArray(array('fofo', 'koko')));
    }
	
	public function testOneOf()
    {
        $personBo = new PersonEntity();
		
		$personBo->setName('toto');

        $this->assertTrue($personBo->name->oneOf('toto', 'koko'));
        $this->assertFalse($personBo->name->oneOf('fofo', 'koko'));
    }
	
	public function testLength()
    {
        $personBo = new PersonEntity();
		
		$personBo->setName('toto');

        $this->assertEquals(4, $personBo->name->length());
    }
	
	public function testToString()
    {
        $personBo = new PersonEntity();
		
		$personBo->setName('toto');
        $this->assertEquals('toto', $personBo->name);
    }
	
	public function testCast()
    {
        $personBo = new PersonEntity();
		
        $this->assertInstanceOf(Column::class, $personBo->name);
		$this->assertNotInstanceOf(Column::class, new Any());
    }
}