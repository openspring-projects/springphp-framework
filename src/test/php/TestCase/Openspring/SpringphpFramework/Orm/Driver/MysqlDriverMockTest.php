<?php
namespace TestCase\Openspring\SpringphpFramework\Orm\Driver;

use Openspring\SpringphpFramework\Mock\DataMockito;
use Openspring\SpringphpFramework\Orm\DataSource\DataSourceConfig;
use Openspring\SpringphpFramework\Orm\Driver\Mysql\MysqlDriverMock;
use TestCase\Openspring\SpringphpFramework\Helper\SpringphpTestRunner;
use Openspring\SpringphpFramework\Type\DynamicObject;
use PHPUnit\Framework\TestCase;

class MysqlDriverMockTest extends TestCase
{
    private static $driver = null;

    public static function setUpBeforeClass(): void
    {
        SpringphpTestRunner::setSpringphpContext(true);
        static::$driver = new MysqlDriverMock(new DataSourceConfig());
        
        static::$driver->connect();
    }

    public function getDriver(): MysqlDriverMock
    {
        return static::$driver;
    }

    public function testInstantiation()
    {
        $this->assertNotNull($this->getDriver());
    }

    public function testGetQueryCount()
    {
        DataMockito::when('select count(*) from client')->thenReturn(10);
        $count = $this->getDriver()->getQueryCount('select count(*) from client');
        
        $this->assertEquals(10, $count);
    }

    public function testExecute_SQL()
    {
        $clients = array(
            new DynamicObject(array('name' => 'toto')),
            new DynamicObject(array('name' => 'fofo')),
            new DynamicObject(array('name' => 'hoho')),
            new DynamicObject(array('name' => 'roro')),
            new DynamicObject(array('name' => 'coco'))
        );

        DataMockito::when('SELECT * FROM client')->thenReturn($clients);

        $result = $this->getDriver()->execute_SQL('SELECT * FROM client')->toArray();

        $this->assertEquals($clients, $result);
    }

    public function testExecute_CRUD_SQL()
    {
        DataMockito::when("INSERT INTO client(name) VALUES('toto')")->thenReturn(true);

        $result = $this->getDriver()->execute_CRUD_SQL("INSERT INTO client(name) VALUES('toto')");

        $this->assertTrue($result);
    }

    public function testGetLastInsertedRowId()
    {
        $this->assertEquals(0, $this->getDriver()->getLastInsertedRowId());
    }

    public function testIsAlive()
    {
        $this->assertTrue($this->getDriver()->isAlive());
    }
}