<?php
namespace TestCase\Openspring\SpringphpFramework\Manager;

use Openspring\SpringphpFramework\Manager\ErrorManager;
use Openspring\SpringphpFramework\Type\BaseError;
use PHPUnit\Framework\TestCase;

class ErrorManagerTest extends TestCase
{
	public function setUp(): void
    {
		ErrorManager::clean();	
	}
	
    public function testAddError()
    {
		$error = new BaseError('toto not found');
        ErrorManager::addError($error);
		
		$this->assertCount(1, ErrorManager::getErrors());
		$this->assertEquals($error, ErrorManager::getError(0));		
    }
	
	public function testAddSimpleError()
    {
		$error = new BaseError('toto not found');
        ErrorManager::addSimpleError('toto not found');
		
		$this->assertCount(1, ErrorManager::getErrors());
		$this->assertEquals($error, ErrorManager::getError(0));		
    }
	
	public function testSetErrors()
    {
		$error0 = new BaseError('toto not found 0');
		$error1 = new BaseError('toto not found 1');
		$error2 = new BaseError('toto not found 2');
		
		$errors = array($error1, $error2);
		
        ErrorManager::addError($error0);
		ErrorManager::setErrors($errors);
		
		$this->assertCount(2, ErrorManager::getErrors());
		$this->assertEquals($error1, ErrorManager::getError(0));		
    }
	
	public function testGetError()
    {
		$error0 = new BaseError('toto not found 0');
		$error1 = new BaseError('toto not found 1');
		$error2 = new BaseError('toto not found 2');
		
		$errors = array($error0, $error1, $error2);
		
		ErrorManager::setErrors($errors);
		
		$this->assertCount(3, ErrorManager::getErrors());
		
		$this->assertEquals($error0, ErrorManager::getError(0));
		$this->assertEquals($error1, ErrorManager::getError(1));
		$this->assertEquals($error2, ErrorManager::getError(2));
    }
	
	public function testGetErrors()
    {
		$error0 = new BaseError('toto not found 0');
		$error1 = new BaseError('toto not found 1');
		$error2 = new BaseError('toto not found 2');
		
		$errors = array($error0, $error1, $error2);
		
		ErrorManager::setErrors($errors);
		
		$this->assertCount(3, ErrorManager::getErrors());
		
		$this->assertEquals($errors, ErrorManager::getErrors());
    }
	
	public function testGetLastError()
    {
		$error0 = new BaseError('toto not found 0');
		$error1 = new BaseError('toto not found 1');
		$error2 = new BaseError('toto not found 2');
		
		$errors = array($error0, $error1, $error2);
		
		ErrorManager::setErrors($errors);
		
		$this->assertCount(3, ErrorManager::getErrors());
		
		$this->assertEquals($error2, ErrorManager::getLastError());
    }
	
	public function testGetMessages()
    {
		$error0 = new BaseError('toto not found 0');
		$error1 = new BaseError('toto not found 1');
		$error2 = new BaseError('toto not found 2');
		
		$errors = array($error0, $error1, $error2);
		
		$messages  = $error0->getMessage() . PHP_EOL .
					 $error1->getMessage() . PHP_EOL .
					 $error2->getMessage(); echo $messages;
		
		ErrorManager::setErrors($errors);
		
		$this->assertCount(3, ErrorManager::getErrors());
		
		$this->assertEquals($messages, ErrorManager::getMessages());
    }
	
	public function testGetMessage()
    {
		$error0 = new BaseError('toto not found 0');
		$error1 = new BaseError('toto not found 1');
		$error2 = new BaseError('toto not found 2');
		
		$errors = array($error0, $error1, $error2);
		
		$message  = $error2->getMessage();
		
		ErrorManager::setErrors($errors);
		
		$this->assertCount(3, ErrorManager::getErrors());
		
		$this->assertEquals($message, ErrorManager::getLastMessage());
    }
	
	public function testHasErrors()
    {
		$error0 = new BaseError('toto not found 0');
		$error1 = new BaseError('toto not found 1');
		$error2 = new BaseError('toto not found 2');
		
		$errors = array($error0, $error1, $error2);
		
		ErrorManager::setErrors($errors);
		
		$this->assertTrue(ErrorManager::hasErrors());
    }
	
	public function testClean()
    {
		$this->assertCount(0, ErrorManager::getErrors());
		
		$error0 = new BaseError('toto not found 0');
		$error1 = new BaseError('toto not found 1');
		$error2 = new BaseError('toto not found 2');
		
		$errors = array($error0, $error1, $error2);
		
		ErrorManager::setErrors($errors);
		
		$this->assertCount(3, ErrorManager::getErrors());
		
		ErrorManager::clean($errors);
		
		$this->assertCount(0, ErrorManager::getErrors());
    }
}