<?php

namespace TestCase\Openspring\SpringphpFramework\Test;

use Openspring\SpringphpFramework\Context\ApplicationContext;
use Openspring\SpringphpFramework\Context\Environment;
use Openspring\SpringphpFramework\Enumeration\ProfileType;
use Openspring\SpringphpFramework\Http\HttpMethod;
use Openspring\SpringphpFramework\IO\DirectoryUtils;
use PHPUnit\Framework\TestCase;
use TestCase\Openspring\SpringphpFramework\Helper\SpringphpTestRunner;
use TestCase\Openspring\SpringphpFramework\Helper\Mock\Application\Resource\UserController;

final class SpringphpTestRunnerTest extends TestCase
{

    public function testGetRootPath()
    {
        $baseDir = SpringphpTestRunner::getRootPath();
        $srcDir = $baseDir . 'src/main/php/Openspring/SpringphpFramework/Test/';

        $this->assertTrue(DirectoryUtils::exists($srcDir));
    }

    public function testSetSpringphpContext()
    {
        SpringphpTestRunner::setSpringphpContext();

        $this->assertSame(ProfileType::TEST, Environment::getActiveProfile());
        $this->assertSame(SpringphpTestRunner::getRootPath(), Environment::getRootPath());
        $this->assertCount(6, ApplicationContext::getMapping());
        $this->assertSame('src/test/resources/', Environment::getTestConfigPath());
    }

    public function testRunHttpMethod()
    {
        SpringphpTestRunner::runHttpMethod(HttpMethod::PUT, 'esapi/account/update', true);

        $this->assertSame($_REQUEST['wcomp'], UserController::class);
        $this->assertSame($_REQUEST['action'], 'UpdateMyProfile');
    }
}