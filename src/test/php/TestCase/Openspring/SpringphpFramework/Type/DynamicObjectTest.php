<?php
namespace TestCase\Openspring\SpringphpFramework\Type;

use Openspring\SpringphpFramework\Type\DynamicObject;
use PHPUnit\Framework\TestCase;

class DynamicObjectTest extends TestCase
{
    public function testDynamicPropertyCreation()
    {
        $qr = new DynamicObject(array('name' => 'khalid', 'age' => 36));
        
        $this->assertEquals('khalid', $qr->name);
        $this->assertEquals(36, $qr->age);
        
        $qr->addProperty('job', 'ing');
        
        $this->assertEquals('ing', $qr->job);
    }
}