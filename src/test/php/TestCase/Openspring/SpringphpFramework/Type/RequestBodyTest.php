<?php
namespace TestCase\Openspring\SpringphpFramework\Type;

use Openspring\SpringphpFramework\Type\DynamicObject;
use Openspring\SpringphpFramework\Type\RequestBody;
use PHPUnit\Framework\TestCase;

class RequestBodyTest extends TestCase
{
    public function testGetters()
    {
        $settings = new DynamicObject(array('logged' => true,'active' => false));
        $posts = array('hello', 'good', 'bad');
        
        $object = new DynamicObject(
            array(
                'userId' => '12555555',
                'age' => 36,
                'birthDate' => '1983-06-17',
                'married' => true,
                'posts' => $posts,
                'settings' => $settings
            ));

        $body = new RequestBody($object);

        $this->assertEquals($object, $body->getBody());

        $this->assertEquals('12555555', $body->getString('userId'));
        $this->assertEquals(36, $body->getInt('age'));
        $this->assertEquals('1983-06-17', $body->getDate('birthDate'));
        $this->assertTrue($body->getBool('married'));
        $this->assertEquals($posts, $body->getArray('posts'));
        $this->assertEquals($settings, $body->getObject('settings'));
    }
}