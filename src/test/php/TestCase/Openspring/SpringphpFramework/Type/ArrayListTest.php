<?php

namespace TestCase\Openspring\SpringphpFramework\Type;

use Openspring\SpringphpFramework\Exception\ArrayIndexOutOfBoundsException;
use Openspring\SpringphpFramework\Type\ArrayList;
use Openspring\SpringphpFramework\Type\DynamicObject;
use PHPUnit\Framework\TestCase;

class ArrayListTest extends TestCase
{
	private $arrayList;
	
	public function setUp(): void
	{
		$this->arrayList = new ArrayList();
		
		$this->arrayList->add('toto');
		$this->arrayList->add('fofo');
		$this->arrayList->add('koko');
	}
	
	public function testHasIndex()
	{
		$this->assertCount(3, $this->arrayList);
		
		$this->assertTrue($this->arrayList->hasIndex(0));
		$this->assertTrue($this->arrayList->hasIndex(1));
		$this->assertTrue($this->arrayList->hasIndex(2));
		
		$this->assertFalse($this->arrayList->hasIndex(3));
	}
	
	public function testAdd()
	{
		$this->arrayList->add('roro');
		
		$this->assertTrue($this->arrayList->hasIndex(3));
		$this->assertCount(4, $this->arrayList);
	}
	
	public function testRemove()
	{
		$this->assertCount(3, $this->arrayList);
		$this->arrayList->remove(2);
		$this->assertCount(2, $this->arrayList);
	}
	
	public function testAddAll()
	{
		$this->assertCount(3, $this->arrayList);
		$this->arrayList->addAll(array('popo'));
		$this->assertCount(1, $this->arrayList);
	}
	
	public function testRemoveAll()
	{
		$this->assertCount(3, $this->arrayList);
		$this->arrayList->removeAll();
		$this->assertCount(0, $this->arrayList);
	}
	
	public function testGet()
	{
		$this->assertEquals('fofo', $this->arrayList->get(1));
	}
	
	public function testSet()
	{
		$this->arrayList->set(1, 'dodo');
		$this->assertEquals('dodo', $this->arrayList->get(1));
	}
	
	public function testSetException()
	{
		$this->expectException(ArrayIndexOutOfBoundsException::class);
		$this->arrayList->set(5, 'dodo');
	}
	
	public function testHasValue()
	{
		$this->assertTrue($this->arrayList->hasValue('toto'));
		$this->assertFalse($this->arrayList->hasValue('tota'));
	}
	
	public function testContainsObject()
	{
		$this->arrayList = new ArrayList();
		
		$this->arrayList->add(new DynamicObject(array('name' => 'toto', 'age' => 20)));
		$this->arrayList->add(new DynamicObject(array('name' => 'fofo', 'age' => 30)));
		$this->arrayList->add(new DynamicObject(array('name' => 'koko', 'age' => 15)));
		
		$this->assertTrue($this->arrayList->containsObject('name', 'fofo'));
		$this->assertFalse($this->arrayList->containsObject('name', 'fofa'));
	}
	
	public function testFind()
	{
		$this->arrayList = new ArrayList();
		
		$this->arrayList->add(new DynamicObject(array('name' => 'toto', 'age' => 20)));
		$this->arrayList->add(new DynamicObject(array('name' => 'fofo', 'age' => 30)));
		$this->arrayList->add(new DynamicObject(array('name' => 'koko', 'age' => 15)));
		
		$fofoItem = $this->arrayList->find('name', 'fofo');
		$fofaItem = $this->arrayList->find('name', 'fofa');
		
		$this->assertNotNull($fofoItem);
		$this->assertNull($fofaItem);
	}
}