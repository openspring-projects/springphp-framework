<?php
namespace TestCase\Openspring\SpringphpFramework\Type;

use Openspring\SpringphpFramework\Exception\ArrayIndexOutOfBoundsException;
use Openspring\SpringphpFramework\Type\Dictionary;
use Openspring\SpringphpFramework\Type\DynamicObject;
use PHPUnit\Framework\TestCase;

class DictionaryTest extends TestCase
{
    private $dict;

    public function setUp(): void
    {
        $this->dict = new Dictionary();

        $this->dict->add('t', 'toto');
        $this->dict->add('f', 'fofo');
        $this->dict->add('k', 'koko');
    }

    public function testHasKey()
    {
        $this->assertCount(3, $this->dict);

        $this->assertTrue($this->dict->hasKey('t'));
        $this->assertTrue($this->dict->hasKey('f'));
        $this->assertTrue($this->dict->hasKey('k'));

        $this->assertFalse($this->dict->hasKey('d'));
    }

    public function testAdd()
    {
        $this->dict->add('r', 'roro');

        $this->assertTrue($this->dict->hasKey('r'));
        $this->assertCount(4, $this->dict);
    }

    public function testRemove()
    {
        $this->assertCount(3, $this->dict);
        $this->dict->remove('k');
        $this->assertCount(2, $this->dict);
    }

    public function testAddAll()
    {
        $this->assertCount(3, $this->dict);
        $this->dict->addAll(array('p' => 'popo'));
        $this->assertCount(1, $this->dict);
    }

    public function testRemoveAll()
    {
        $this->assertCount(3, $this->dict);
        $this->dict->removeAll();
        $this->assertCount(0, $this->dict);
    }

    public function testGet()
    {
        $this->assertEquals('fofo', $this->dict->get('f'));
    }

    public function testSet()
    {
        $this->dict->set('t', 'tota');
        $this->assertEquals('tota', $this->dict->get('t'));
    }

    public function testSetException()
    {
        $this->expectException(ArrayIndexOutOfBoundsException::class);
        $this->dict->set('d', 'dodo');
    }

    public function testHasValue()
    {
        $this->assertTrue($this->dict->hasValue('toto'));
        $this->assertFalse($this->dict->hasValue('tota'));
    }

    public function testContainsObject()
    {
        $this->dict = new Dictionary();

        $this->dict->add('t', new DynamicObject(array('name' => 'toto','age' => 20)));
        $this->dict->add('f', new DynamicObject(array('name' => 'fofo','age' => 30)));
        $this->dict->add('k', new DynamicObject(array('name' => 'koko','age' => 15)));

        $this->assertTrue($this->dict->containsObject('name', 'fofo'));
        $this->assertFalse($this->dict->containsObject('name', 'fofa'));
    }

    public function testFind()
    {
        $this->dict = new Dictionary();

        $this->dict->add('t', new DynamicObject(array('name' => 'toto','age' => 20)));
        $this->dict->add('f', new DynamicObject(array('name' => 'fofo','age' => 30)));
        $this->dict->add('k', new DynamicObject(array('name' => 'koko','age' => 15)));

        $fofoItem = $this->dict->find('name', 'fofo');
        $fofaItem = $this->dict->find('name', 'fofa');

        $this->assertNotNull($fofoItem);
        $this->assertNull($fofaItem);
    }
}