<?php
namespace TestCase\Openspring\SpringphpFramework\Enumeration;

use Openspring\SpringphpFramework\Exception\InvalidEnumerationException;
use PHPUnit\Framework\TestCase;
use TestCase\Openspring\SpringphpFramework\Helper\DayType;

class EnumerationTypeTest extends TestCase
{
    public function testIsValid()
    {
        $this->assertTrue(DayType::isValid(DayType::MONDAY));
        $this->assertTrue(DayType::isValid('Monday', true));
        $this->assertFalse(DayType::isValid('Monday'));
        $this->assertFalse(DayType::isValid('LUNDI'));
        
        $this->assertTrue(DayType::isValid(array(DayType::MONDAY, DayType::SUNDAY)));
        $this->assertFalse(DayType::isValid(array(DayType::MONDAY, 'Sunday')));
        $this->assertTrue(DayType::isValid(array(DayType::MONDAY, 'Sunday'), true));
        $this->assertFalse(DayType::isValid(array(DayType::MONDAY, DayType::SUNDAY, 'LUNDI')));
    }
    
    public function testValidateKO()
    {
        $this->expectException(InvalidEnumerationException::class);

        DayType::validate('LUNDI');
    }
    
    public function testValueOf()
    {
        $this->assertEquals(DayType::MONDAY, DayType::valueOf(DayType::MONDAY));
        $this->assertNotEquals(DayType::MONDAY, DayType::valueOf('Monday'));
        $this->assertEquals(DayType::MONDAY, DayType::valueOf('Monday', true));
    }

    public function testValidateOK()
    {
        DayType::validate(DayType::MONDAY);
		
		$this->assertTrue(true);
    }

    public function testToList()
    {
        $list = DayType::toArray();

        $this->assertIsArray($list);
        $this->assertCount(7, $list);
        $this->assertEquals(DayType::MONDAY, $list[0]);
    }
}