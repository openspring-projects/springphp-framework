<?php
namespace TestCase\Openspring\SpringphpFramework\Enumeration;

use Openspring\SpringphpFramework\Enumeration\ObjectType;
use Openspring\SpringphpFramework\Type\Any;
use PHPUnit\Framework\TestCase;

class ObjectTypeTest extends TestCase
{
    public function testGetType()
    {
        $string = 'khalid';
        $bool = true;
        $array = array(0, 2);
        $int = 100;
        $double = 0.3;
        $class = new Any();

        $this->assertEquals(ObjectType::STRING, ObjectType::getType($string));
        $this->assertEquals(ObjectType::BOOLEAN, ObjectType::getType($bool));
        $this->assertEquals(ObjectType::ARRAY, ObjectType::getType($array));
        $this->assertEquals(ObjectType::INTEGER, ObjectType::getType($int));
        $this->assertEquals(ObjectType::DOUBLE, ObjectType::getType($double));
        $this->assertEquals(ObjectType::OBJECT, ObjectType::getType($class));
    }
}