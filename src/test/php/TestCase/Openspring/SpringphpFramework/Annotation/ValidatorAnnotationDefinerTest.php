<?php

namespace TestCase\Openspring\SpringphpFramework\Annotation;

use Openspring\SpringphpFramework\Annotation\ValidatorAnnotationDefiner;
use Openspring\SpringphpFramework\Enumeration\ObjectType;
use Openspring\SpringphpFramework\Validation\Validator\Simple\Mixed\NotBlankConstraint;
use PHPUnit\Framework\TestCase;

class ValidatorAnnotationDefinerTest extends TestCase
{
    public function testInstantiation()
    {
        $notBlankConfig = array(
            'min' => ObjectType::INTEGER,
            'max' => ObjectType::INTEGER,
            'message' => ObjectType::STRING);
        $notBlankTarget = array(
            ObjectType::STRING);

        $annotDefiner = new ValidatorAnnotationDefiner('(', ')', NotBlankConstraint::class, $notBlankTarget, $notBlankConfig);
        
        $this->assertEquals('(', $annotDefiner->start);
        $this->assertEquals(')', $annotDefiner->end);
        $this->assertEquals($notBlankConfig, $annotDefiner->expectedParams);
        $this->assertEquals($notBlankTarget, $annotDefiner->target);
    }
}