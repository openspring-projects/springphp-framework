<?php

namespace TestCase\Openspring\SpringphpFramework\Annotation;


use Openspring\SpringphpFramework\Core\Stereotype\Annotations;
use PHPUnit\Framework\TestCase;

class AnnotationsTest extends TestCase
{
    public function testGetAllAnnotations()
    {
        $annots = Annotations::getAllAnnotations();
        
        $this->assertCount(27, $annots);
    }

    public function testGetFieldAnnotations()
    {
        $annots = Annotations::getFieldAnnotations();
        
        $this->assertCount(3, $annots);
        
        $this->assertEquals('@Autowired(', $annots['Autowired']->start);
        $this->assertEquals(')', $annots['Autowired']->end);
    }

    public function testGetValidationAnnotations()
    {
        $annots = Annotations::getValidationAnnotations();
        
        $this->assertCount(22, $annots);
        
        $this->assertEquals('@Min(', $annots['Min']->start);
        $this->assertEquals(')', $annots['Min']->end);
    }
    
    public function testGetValidationAnnotation()
    {
        $maxAnnotDefiner = Annotations::getValidationAnnotation('Max');
        
        $this->assertEquals('@Max(', $maxAnnotDefiner->start);
        $this->assertEquals(')', $maxAnnotDefiner->end);
    }

    public function testGetClassAnnotations()
    {
        $annots = Annotations::getClassAnnotations();
        
        $this->assertCount(2, $annots);
        
        $this->assertEquals('@ComponentScan(', $annots['ComponentScan']->start);
        $this->assertEquals(')', $annots['ComponentScan']->end);
    }

    public function testGetMethodAnnotations()
    {
        $this->assertTrue(true);
    }
}