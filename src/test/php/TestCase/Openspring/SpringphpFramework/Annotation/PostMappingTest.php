<?php

namespace TestCase\Openspring\SpringphpFramework\Annotation;

use Openspring\SpringphpFramework\Annotation\ElementType;
use Openspring\SpringphpFramework\Annotation\RequestMapping\PostMapping;
use Openspring\SpringphpFramework\Enumeration\MediaType;
use Openspring\SpringphpFramework\Enumeration\ModifierType;
use Openspring\SpringphpFramework\Exception\Annotation\InvalidAnnotationAttributeException;
use Openspring\SpringphpFramework\Exception\Annotation\InvalidAnnotationAttributeValueException;
use Openspring\SpringphpFramework\Exception\Annotation\RequiredAnnotationAttributeException;
use Openspring\SpringphpFramework\Http\HttpMethod;
use PHPUnit\Framework\TestCase;

final class PostMappingTest extends TestCase
{

    public function testGivenGoodDocThenOK(): void
    {
        $doc = '/**
                 * @PostMapping({"value": "/groups/{$group_id}", "consumes": "application/json", "produces": "application/json", "groupIn": "1", "rolesIn": "admin"})
                 */';
        $rm = new PostMapping($doc, ModifierType::PUBLIC_MOD);

        $this->assertEquals(MediaType::APPLICATION_JSON_VALUE, $rm->getConsumes());
        $this->assertEquals(MediaType::APPLICATION_JSON_VALUE, $rm->getProduces());
        $this->assertEquals(array('1'), $rm->getAuthorizedGroups());
        $this->assertEquals(array('admin'), $rm->getAuthorizedRoles());
        $this->assertEquals(ElementType::METHOD, $rm->getElementType());
        $this->assertEquals(array(
            HttpMethod::POST), $rm->getMethod());
        $this->assertEquals(ModifierType::PUBLIC_MOD, $rm->getModifier());

        $this->assertEquals('@PostMapping', $rm->getName());
        $this->assertEquals('/groups/{$group_id}', $rm->getValue());
        $this->assertEquals(ModifierType::PUBLIC_MOD, $rm->getModifier());

        $this->assertCount(2, $rm->getValues());
        $this->assertCount(5, $rm->getAttributes());

        $this->assertEquals(false, $rm->getValues()[0]['isParam']);
        $this->assertEquals('groups', $rm->getValues()[0]['param']);

        $this->assertEquals(true, $rm->getValues()[1]['isParam']);
        $this->assertEquals('group_id', $rm->getValues()[1]['param']);
        //$this->assertEquals('(.*)', $rm->getValues()[1]['value']);
        //$this->assertEquals('$1', $rm->getValues()[1]['var']);
    }

    public function testGivenBadDocThenInvalidAnnotationAttributeValueException(): void
    {
        $doc = '/**
                 * @PostMapping({"value": "/groups/{$group_id}", "consumes": "PDF", "produces": "application/json", "groupIn": "1", "rolesIn": "admin"})
                 */';

        $this->expectException(InvalidAnnotationAttributeValueException::class);
        new PostMapping($doc, ModifierType::PUBLIC_MOD);
    }

    public function testGivenBadDocThenRequiredAnnotationAttributeException(): void
    {
        $doc = '/**
                 * @PostMapping({"value": "/groups/{$group_id}", "rolesIn": "admin"})
                 */';

        $this->expectException(RequiredAnnotationAttributeException::class);
        new PostMapping($doc, ModifierType::PUBLIC_MOD);
    }

    public function testGivenBadDocThenInvalidAnnotationAttributeException(): void
    {
        $doc = '/**
                 * @PostMapping({"value": "/groups/{$group_id}", "toto": "toto", "consumes": "application/json", "produces": "application/json", "groupIn": "1", "rolesIn": "admin"})
                 */';

        $this->expectException(InvalidAnnotationAttributeException::class);
        new PostMapping($doc, ModifierType::PUBLIC_MOD);
    }
}