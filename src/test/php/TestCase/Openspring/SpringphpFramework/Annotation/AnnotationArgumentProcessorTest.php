<?php

namespace TestCase\Openspring\SpringphpFramework\Annotation;

use PHPUnit\Framework\TestCase;
use Openspring\SpringphpFramework\Annotation\AnnotationArgumentProcessor;

class AnnotationArgumentProcessorTest extends TestCase
{
    public function testToJsonStringArgsOnly()
    {
        $expectedJson = '{"max": "20", "message": "hello there"}';
        $arguments = 'max = "20", message = "hello there"';
        $processor = new AnnotationArgumentProcessor($arguments);
        
        $actualJson = $processor->encodeJson();
        
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
    
    public function testToJsonArrayArgsOnly()
    {
        $expectedJson = '{"scanDirs":"[\"a\/b\/c\", \"x\/d\/f\"]","excludeDirs":"[\"b\/v\/d\", \"d\/ddp\"]"}';
        $arguments = 'scanDirs = ["a/b/c", "x/d/f"], excludeDirs = ["b/v/d", "d/ddp"]';
        $processor = new AnnotationArgumentProcessor($arguments);
        
        $actualJson = $processor->encodeJson();
        
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
    
    public function testToJsonMixedArgs()
    {
        $expectedJson = '{"max":"20","message":"hello there","scanDirs":"[\"a\/b\/c\", \"x\/d\/f\"]","excludeDirs":"[\"b\/v\/d\", \"d\/ddp\"]"}';
        $arguments = 'max = "20", scanDirs = ["a/b/c", "x/d/f"], message = "hello there", excludeDirs = ["b/v/d", "d/ddp"]';
        $processor = new AnnotationArgumentProcessor($arguments);
        
        $actualJson = $processor->encodeJson();
        
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
    
    public function testToJsonMixedArgsAndSpecialChars()
    {
        $expectedJson = '{"max":"20","said":"coco \"me !\"  ","message":"hello there","scanDirs":"[\"a\/b\/c\", \"x\/d\/f\"]","excludeDirs":"[\"b\/v\/d\", \"d\/ddp\"]"}';
        $arguments = 'max = "20", said = "coco \"me !\"  " scanDirs = ["a/b/c", "x/d/f"], message = "hello there", excludeDirs = ["b/v/d", "d/ddp"]';
        $processor = new AnnotationArgumentProcessor($arguments);
        
        $actualJson = $processor->encodeJson();
        
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
    
    public function testToJsonEmptyArgs()
    {
        $expectedJson = '""';
        $arguments = '';
        $processor = new AnnotationArgumentProcessor($arguments);
        
        $actualJson = $processor->encodeJson();
        
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
    
    public function testToJsonBoolArgs()
    {
        $expectedJson = 'true';
        $arguments = 'true';
        $processor = new AnnotationArgumentProcessor($arguments);
        
        $actualJson = $processor->encodeJson();
        
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
    
    public function testToJsonNumericArgs()
    {
        $expectedJson = '"100"';
        $arguments = 100;
        $processor = new AnnotationArgumentProcessor($arguments);
        
        $actualJson = $processor->encodeJson();
        
        $this->assertJsonStringEqualsJsonString($expectedJson, $actualJson);
    }
}