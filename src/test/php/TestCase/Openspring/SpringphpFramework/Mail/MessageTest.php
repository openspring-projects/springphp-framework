<?php
namespace TestCase\Openspring\SpringphpFramework\Mail;

use Openspring\SpringphpFramework\Exception\InvalidArgumentException;
use Openspring\SpringphpFramework\Mail\Message;
use PHPUnit\Framework\TestCase;

class MessageTest extends TestCase
{
    public function testMessageOK()
    {
        $message = new Message();
        
        $this->assertFalse($message->hasTos());
        $this->assertFalse($message->hasCcs());
        $this->assertFalse($message->hasBccs());
        
        $message->setFrom("kha.elabbadi@gmail.com");
        $message->addTo("tot@gmail.com");
        $message->addTo("fofo@gmail.com");
        
        $message->addCc("dodo@gmail.com");
        $message->addCc("roro@gmail.com");
        
        $message->addBcc("bobo@gmail.com");
        $message->addBcc("nono@gmail.com");
        
        $message->setSubject("info");
        $message->setBody("server 001 is on");
        
        $this->assertTrue($message->hasTos());
        $this->assertTrue($message->hasCcs());
        $this->assertTrue($message->hasBccs());
        
        $this->assertCount(2, $message->getTos());
        $this->assertCount(2, $message->getCcs());
        $this->assertCount(2, $message->getBccs());
    }
    
    public function testMessageKO()
    {
        $message = new Message();
        
        $this->expectException(InvalidArgumentException::class);
        $message->addTo(" tot @gmail .com");
        
        $this->expectException(InvalidArgumentException::class);
        $message->addCc(" tot @gmail .com");
        
        $this->expectException(InvalidArgumentException::class);
        $message->addBcc(" tot @gmail .com");
    }
}