<?php
namespace TestCase\Openspring\SpringphpFramework\Mail;

use Openspring\SpringphpFramework\Mail\MailSender;
use Openspring\SpringphpFramework\Mail\Message;
use TestCase\Openspring\SpringphpFramework\Helper\SpringphpTestRunner;
use Openspring\SpringphpFramework\Utils\Arrays;
use PHPUnit\Framework\TestCase;

class MailSenderTest extends TestCase
{
    public function testMailSenderHeaderAndMessage()
    {
        $expectedHeader = array(
            'MIME-Version: 1.0',
            'Content-type: text/html; charset=utf-8',
            'From: kha.elabbadi@gmail.com',
            'Cc: dodo@gmail.com,roro@gmail.com',
            'Bcc: bobo@gmail.com,nono@gmail.com'
        );
        $expectedBody = "<table><tr><td>server 001 is on</td></tr></table>";
        
        $message = new Message();
        
        $message->setFrom("kha.elabbadi@gmail.com");
        $message->addTo("tot@gmail.com");
        $message->addTo("fofo@gmail.com");
        
        $message->addCc("dodo@gmail.com");
        $message->addCc("roro@gmail.com");
        
        $message->addBcc("bobo@gmail.com");
        $message->addBcc("nono@gmail.com");
        
        $message->setSubject("info");
        $message->setBody("server 001 is on");
        
        $mailSender = new MailSender($message);
        
        $expectedHeader = Arrays::implode(PHP_EOL, $expectedHeader) . PHP_EOL;
        
        
        $this->assertSame($expectedHeader, $mailSender->getHeaders());
        $this->assertSame($expectedBody, $mailSender->getComposedMessage());
    }
    
    public function testMailSenderHeaderAndMessageWithAttachement()
    {
        $message = new Message();
        
        $message->setFrom("kha.elabbadi@gmail.com");
        $message->addTo("tot@gmail.com");
        $message->addCc("dodo@gmail.com");
        $message->addBcc("bobo@gmail.com");
        $message->setReplyTo("bobo@gmail.com");        
        $message->setSubject("info");
        $message->setBody("server 001 is on");
        
        $message->addAttachement(SpringphpTestRunner::getRootPath('src/test/resources/data.txt'));
        $message->addAttachement(SpringphpTestRunner::getRootPath('src/test/resources/parse.csv'));
        
        $mailSender = new MailSender($message);
        
        $expectedHeader = array(
            'MIME-Version: 1.0',
            'Content-Type: multipart/mixed; charset=utf-8',
            'boundary=' . $mailSender->getMimeBoundary(),
            'From: kha.elabbadi@gmail.com',
            'Cc: dodo@gmail.com',
            'Bcc: bobo@gmail.com',
            'Reply-To: bobo@gmail.com'
        );
        $expectedHeader = Arrays::implode(PHP_EOL, $expectedHeader) . PHP_EOL;
        
        $expectedBody = array(
            "This is a multi-part message in MIME format.",
            "",
            "--" . $mailSender->getMimeBoundary(),
            "Content-Type: text/html; charset=\"iso-8859-1\"",
            "Content-Transfer-Encoding: 7bit",
            "",
            "<table><tr><td>server 001 is on</td></tr></table>",
            "",
            "--" . $mailSender->getMimeBoundary(),
            "Content-Type: application/octet-stream; name=\"data.txt\"",
            "Content-Disposition: attachment; filename=\"".SpringphpTestRunner::getRootPath()."src/test/resources/data.txt\"",
            "Content-Transfer-Encoding: base64",
            "",
        	"w6p0cmUgc3ByaW5ncGhw",
        	"",
        	"",
        	"--" . $mailSender->getMimeBoundary(),
        	"Content-Type: application/octet-stream; name=\"parse.csv\"",
        	"Content-Disposition: attachment; filename=\"".SpringphpTestRunner::getRootPath()."src/test/resources/parse.csv\"",
        	"Content-Transfer-Encoding: base64",
        	"",
        	"aWQ7bmFtZTsic29tZSBudW0iCjE7cXdlcnR5OzQyCiIyIjsiU29tZSB3b3JkIjs0MjQy",
        	"",
        	"",
            "--" . $mailSender->getMimeBoundary()
        );
        $expectedBody = md5(Arrays::implode(PHP_EOL, $expectedBody) . PHP_EOL);
        
        $this->assertSame($expectedHeader, $mailSender->getHeaders());
        //$this->assertSame($expectedBody, $mailSender->getComposedMessage());
    }
}