<?php
namespace TestCase\Openspring\SpringphpFramework\IO;

use Openspring\SpringphpFramework\IO\DirectoryUtils;
use Openspring\SpringphpFramework\IO\FileUtils;
use TestCase\Openspring\SpringphpFramework\Helper\SpringphpTestRunner;
use PHPUnit\Framework\TestCase;
use Openspring\SpringphpFramework\Utils\Strings;

class DirectoryUtilsTest extends TestCase
{
    public function setUp(): void
    {
        SpringphpTestRunner::setSpringphpContext();
        SpringphpTestRunner::resetMockFileUpload();
    }

    public function testExists()
    {
        $fpath = SpringphpTestRunner::getRootPath('/src/test/resources/io/data.json');
        $parent = FileUtils::getFileParent($fpath);
        
        $this->assertTrue(DirectoryUtils::exists($parent));
        $this->assertFalse(DirectoryUtils::exists($parent . '/dir1'));
    }
    
    public function testCreateIfNotExists()
    {
        $path = SpringphpTestRunner::getRootPath('/src/test/resources/tmp/dir00001');
        
        DirectoryUtils::removeDirectory($path, true);
        DirectoryUtils::createIfNotExists($path);
        
        $this->assertTrue(DirectoryUtils::exists($path));
    }
    
    public function testRemoveEmptyDirectory()
    {
        $path = SpringphpTestRunner::getRootPath('/src/test/resources/tmp/dir00001');
        
        DirectoryUtils::removeDirectory($path, true);
        DirectoryUtils::createIfNotExists($path);
        
        $this->assertTrue(DirectoryUtils::exists($path));
        
        DirectoryUtils::removeDirectory($path, true);
        $this->assertFalse(DirectoryUtils::exists($path));
    }
    
    public function testRemoveNoneEmptyDirectory()
    {
        $path = SpringphpTestRunner::getRootPath('/src/test/resources/tmp');
        
        DirectoryUtils::removeDirectory($path, true);
        DirectoryUtils::createIfNotExists($path);
        DirectoryUtils::createIfNotExists($path . '/dir1/dir2');
        
        $this->assertTrue(DirectoryUtils::exists($path . '/dir1/dir2'));
        
        DirectoryUtils::removeDirectory($path, false);
        $this->assertTrue(DirectoryUtils::exists($path));
        
        DirectoryUtils::removeDirectory($path, true);
        $this->assertFalse(DirectoryUtils::exists($path));
    }
    
    public function testIsEmpty()
    {
        $path = SpringphpTestRunner::getRootPath('/src/test/resources/tmp');
        
        DirectoryUtils::removeDirectory($path, true);
        DirectoryUtils::createIfNotExists($path);
        DirectoryUtils::createIfNotExists($path . '/dir1/dir2');
        
        $this->assertFalse(DirectoryUtils::isEmpty($path));
        $this->assertTrue(DirectoryUtils::isEmpty($path . '/dir1/dir2'));
        
        DirectoryUtils::removeDirectory($path, true);
        $this->assertFalse(DirectoryUtils::exists($path));
    }
    
    public function testEmptyDirectory()
    {
        $path = SpringphpTestRunner::getRootPath('/src/test/resources/tmp');
        
        DirectoryUtils::removeDirectory($path, true);
        DirectoryUtils::createIfNotExists($path);
        DirectoryUtils::createIfNotExists($path . '/dir1/dir2');
        
        $this->assertTrue(DirectoryUtils::exists($path . '/dir1/dir2'));
        $this->assertFalse(DirectoryUtils::isEmpty($path));
        
        DirectoryUtils::emptyDirectory($path);
        
        $this->assertFalse(DirectoryUtils::exists($path . '/dir1/dir2'));
        $this->assertTrue(DirectoryUtils::exists($path));
        $this->assertTrue(DirectoryUtils::isEmpty($path));
        
        DirectoryUtils::removeDirectory($path, true);
        $this->assertFalse(DirectoryUtils::exists($path));
    }
    
	// TODO: fix it
	/*
    public function testCopyDir()
    {
        $path = SpringphpTestRunner::getRootPath('/src/test/resources/tmp');
        
        DirectoryUtils::removeDirectory($path, true);
        DirectoryUtils::createIfNotExists($path);
        DirectoryUtils::createIfNotExists($path . '/dir1/dir2');
        
        $this->assertTrue(DirectoryUtils::exists($path . '/dir1/dir2'));
        $this->assertFalse(DirectoryUtils::isEmpty($path));
        
        DirectoryUtils::copyDir($path, $path . '/dest');
        
        $this->assertTrue(DirectoryUtils::exists($path . '/dir1/dir2'));
        $this->assertTrue(DirectoryUtils::exists($path . '/dest/dir1/dir2'));
        
        DirectoryUtils::removeDirectory($path, true);
        
        $this->assertFalse(DirectoryUtils::exists($path));
    }
	*/
    
    public function testWritable()
    {
        $path = SpringphpTestRunner::getRootPath('/src/test/resources');
        $this->assertTrue(DirectoryUtils::writable($path));
    }
    
    public function testGetFilesRecursively()
    {
        $path = SpringphpTestRunner::getRootPath('/src/test/resources/io/films');
        
        $files = DirectoryUtils::getFilesRecursively($path, '*.json');
        
        $this->assertCount(2, $files);
    }
    
    public function testGetLastUpatedFile()
    {
        $path = SpringphpTestRunner::getRootPath('/src/test/resources/io/films');
        
        $file = DirectoryUtils::getLastUpatedFile($path);
        
        $this->assertFalse(Strings::isEmpty($file));
    }
}