<?php
namespace TestCase\Openspring\SpringphpFramework\IO;

use Openspring\SpringphpFramework\Enumeration\MemeType;
use Openspring\SpringphpFramework\Exception\IO\FileNotFoundException;
use Openspring\SpringphpFramework\Exception\IO\IOException;
use Openspring\SpringphpFramework\IO\FileUtils;
use TestCase\Openspring\SpringphpFramework\Helper\SpringphpTestRunner;
use PHPUnit\Framework\TestCase;
use Openspring\SpringphpFramework\Exception\InvalidArgumentException;

class FileUtilsTest extends TestCase
{

    public function setUp(): void
    {
        SpringphpTestRunner::setSpringphpContext();
        SpringphpTestRunner::resetMockFileUpload();
    }

    public function testGetFileContent()
    {
        $fileName = SpringphpTestRunner::getRootPath('/src/test/resources/io/data.json');
        $data = FileUtils::getFileContent($fileName);

        $this->assertSame('"être springphp"', $data);
    }

    public function testExists()
    {
        $fpath = SpringphpTestRunner::getRootPath('/src/test/resources/io/data.json');
        $this->assertTrue(FileUtils::exists($fpath));
    }

    public function testFileParentExists()
    {
        $fpath = SpringphpTestRunner::getRootPath('/src/test/resources/io/data.json');
        $this->assertTrue(FileUtils::parentExists($fpath, 1));
        $this->assertTrue(FileUtils::parentExists($fpath, 2));
        $this->assertTrue(FileUtils::parentExists($fpath, 3));
    }

    public function testGetLastUpdatedTime()
    {
        $fileName = SpringphpTestRunner::getRootPath('/src/test/resources/io/data.json');
        $lastUpdateTime = FileUtils::getLastUpdatedTime($fileName);

        $this->assertEquals(10, strlen($lastUpdateTime));
    }

    public function testGetImageMemeTypes()
    {
        $imagesMemeTypes = array(MemeType::PNG,MemeType::JPEG,MemeType::JPG,MemeType::GIF,MemeType::BMP);

        $this->assertEquals($imagesMemeTypes, FileUtils::getImageMemeTypes());
    }

    public function testGetExtension()
    {
        $fileName = SpringphpTestRunner::getRootPath('/src/test/resources/io/data.json');
        $ext = FileUtils::getExtension($fileName);
        $this->assertEquals('json', $ext);

        // file name
        $ext = FileUtils::getExtension('file.1.3.doc');
        $this->assertEquals('doc', $ext);

        // no extension
        $ext = FileUtils::getExtension('batch');
        $this->assertEquals('batch', $ext);

        $fileName = SpringphpTestRunner::getRootPath('/src/test/resources/io/data.ro.co.json');
        $ext = FileUtils::getExtension($fileName);
        $this->assertEquals('json', $ext);

        // no extension
        $fileName = SpringphpTestRunner::getRootPath('/src/test/resources/io/data');
        $ext = FileUtils::getExtension($fileName);
        $this->assertEquals('data', $ext);
    }

    public function testGetFileName()
    {
        $fileName = SpringphpTestRunner::getRootPath('/src/test/resources/io/data.json');
        $actual = FileUtils::getFileName($fileName);
        $this->assertEquals('data.json', $actual);

        // no extension
        $fileName = SpringphpTestRunner::getRootPath('/src/test/resources/io/data');
        $actual = FileUtils::getFileName($fileName);
        $this->assertEquals('data', $actual);
    }

    public function testGetFileNameWithoutExtension()
    {
        $fileName = SpringphpTestRunner::getRootPath('/src/test/resources/io/data.json');
        $actual = FileUtils::getFileNameWithoutExtension($fileName);
        $this->assertEquals('data', $actual);

        $fileName = SpringphpTestRunner::getRootPath('/src/test/resources/io/data');
        $actual = FileUtils::getFileNameWithoutExtension($fileName);
        $this->assertEquals('data', $actual);
    }

    public function testGetDirectory()
    {
        $fileName = SpringphpTestRunner::getRootPath('/src/test/resources/io/data.json');
        $expected1 = SpringphpTestRunner::getRootPath('/src/test/resources/io');
        $expected2 = SpringphpTestRunner::getRootPath('/src/test/resources');
        $expected3 = SpringphpTestRunner::getRootPath('/src/test');

        $this->assertEquals($expected1, FileUtils::getFileParent($fileName));
        $this->assertEquals($expected2, FileUtils::getFileParent($fileName, 2));
        $this->assertEquals($expected3, FileUtils::getFileParent($fileName, 3));
    }

    public function testChangeFileExtension()
    {
        $fileName = SpringphpTestRunner::getRootPath('/src/test/resources/io/data.json');
        $expected = SpringphpTestRunner::getRootPath('/src/test/resources/io/data.doc');

        $actual = FileUtils::changeFileExtension($fileName, 'doc');
        $this->assertEquals($expected, $actual);

        $fileName = SpringphpTestRunner::getRootPath('/src/test/resources/io/data');
        $expected = SpringphpTestRunner::getRootPath('/src/test/resources/io/doc');

        $actual = FileUtils::changeFileExtension($fileName, 'doc');
        $this->assertEquals($expected, $actual);
    }

    public function testIsUploadedFileExists()
    {
        $this->assertFalse(FileUtils::isUploadedFileExists('users'));

        SpringphpTestRunner::mockFileUpload();
        $this->assertTrue(FileUtils::isUploadedFileExists('users'));
    }

    public function testRenameFileAndMoveFile()
    {
        $f3path = SpringphpTestRunner::getRootPath('/src/test/resources/io/file3.json');
        $f4path = SpringphpTestRunner::getRootPath('/src/test/resources/io/file4.json');

        $newFileName = 'file4.json';
        $fpath = $f3path;
        $expected = $f4path;

        if (FileUtils::exists($f4path))
        {
            $newFileName = 'file3.json';
            $fpath = $f4path;
            $expected = $f3path;
        }

        $actual = FileUtils::renameFile($fpath, $newFileName);

        $this->assertTrue($actual);
        $this->assertFileExists($expected);
        $this->assertFileNotExists($fpath);
    }

    // TODO: fix this test
    // public function testSaveUploadedFile()
    // {
    // SpringphpTestRunner::mockFileUpload();
    // $this->assertTrue(FileUtils::isUploadedFileExists('users'));

    // $fileName = "users.json";
    // $destFilePath = Path::getResources("io/");
    // FileUtils::removeFile($destFilePath . $fileName);

    // $actual = FileUtils::saveUploadedFile('users', $destFilePath, $fileName);

    // $this->assertTrue($actual);
    // $this->assertFileExists($destFilePath . $fileName);
    // }
    
    public function testMoveFile_GivenInvalidSourceThenException()
    {
        $f1path = SpringphpTestRunner::getRootPath('/src/test/resources/io/file11.json');
        $f2path = SpringphpTestRunner::getRootPath('/src/test/resources/io/file1-backup.json');

        $this->expectException(FileNotFoundException::class);

        FileUtils::moveFile($f1path, $f2path);
    }

    public function testMoveFile_GivenInvalidDestinationThenException()
    {
        $source = SpringphpTestRunner::getRootPath('/src/test/resources/io/data.json');
        $dest = SpringphpTestRunner::getRootPath('/src/test/resources/backup/data.json');

        $this->expectException(IOException::class);

        FileUtils::moveFile($source, $dest);
    }

    public function testCopyFile()
    {
        $f1path = SpringphpTestRunner::getRootPath('/src/test/resources/io/file1.json');
        $f2path = SpringphpTestRunner::getRootPath('/src/test/resources/io/file2.json');

        $source = $f1path;
        $dest = $f2path;

        if (FileUtils::exists($f2path))
        {
            $source = $f2path;
            $dest = $f1path;
        }

        FileUtils::removeFile($dest);
        $actual = FileUtils::copyFile($source, $dest);

        $this->assertTrue($actual);
        $this->assertFileExists($dest);
        $this->assertFileExists($source);
    }

    public function testRemoveFile()
    {
        $f1path = SpringphpTestRunner::getRootPath('/src/test/resources/io/file1.json');
        $f2path = SpringphpTestRunner::getRootPath('/src/test/resources/io/file2.json');

        $source = $f1path;
        $dest = $f2path;

        if (FileUtils::exists($f2path))
        {
            $source = $f2path;
            $dest = $f1path;
        }

        FileUtils::removeFile($dest);
        $actual = FileUtils::copyFile($source, $dest);

        $this->assertTrue($actual);
        $this->assertFileExists($dest);
        $this->assertFileExists($source);

        // test remove
        FileUtils::removeFile($dest);

        $this->assertFileNotExists($dest);
    }

    public function testCreateFileFromTemplate()
    {
        $templateFile = SpringphpTestRunner::getRootPath('/src/test/resources/io/file.template');
        $dest = SpringphpTestRunner::getRootPath('/src/test/resources/io/file001.txt');

        FileUtils::removeFile($dest);
        $actual = FileUtils::createFileFromTemplate($templateFile, $dest, array('{name}' => 'toto','{times}' => '1000'));
        $content = FileUtils::getFileContent($dest);

        $this->assertTrue($actual);
        $this->assertFileExists($dest);
        $this->assertEquals('Hi toto, Thank you 1000 times !', $content);
    }

    public function testCreateFileFromTemplateException()
    {
        $templateFile = SpringphpTestRunner::getRootPath('/src/test/resources/io/file.template');
        $dest = SpringphpTestRunner::getRootPath('/src/test/resources/tmp/file001.txt');

        FileUtils::removeFile($dest);

        $this->expectException(InvalidArgumentException::class);

        FileUtils::createFileFromTemplate($templateFile, $dest, 100);
    }

    public function testParseCsv()
    {
        $fcsv = SpringphpTestRunner::getRootPath('/src/test/resources/parse.csv');
        $result = FileUtils::parseCsv($fcsv, ';', '"', true);
        $this->assertSame([['id' => '1','name' => 'qwerty','some num' => '42'],['id' => '2','name' => 'Some word','some num' => '4242']], $result);

        $result = FileUtils::parseCsv($fcsv, ';', '"', false);
        $this->assertSame([['id','name','some num'],['1','qwerty','42'],['2','Some word','4242']], $result);
    }

    public function testStandarizePath()
    {
        $path = 'c:\dev\toto';

        $this->assertSame('c:/dev/toto', FileUtils::standarizePath($path));
    }
}