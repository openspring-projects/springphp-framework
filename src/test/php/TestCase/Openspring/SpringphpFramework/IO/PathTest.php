<?php

namespace TestCase\Openspring\SpringphpFramework\IO;

use PHPUnit\Framework\TestCase;
use Openspring\SpringphpFramework\IO\Path;
use TestCase\Openspring\SpringphpFramework\Helper\SpringphpTestRunner;
use Openspring\SpringphpFramework\IO\FileUtils;

class PathTest extends TestCase
{
    public function setUp(): void
    {
        SpringphpTestRunner::setSpringphpContext();
    }
	
	public function testGetRoot()
    {
		$expected = FileUtils::standarizePath(dirname(__DIR__, 7) . '/');
		$actual = Path::getRoot();
		
		$this->assertEquals($expected, $actual);
	}

    public function testGetResources()
    {
		$expected = FileUtils::standarizePath(dirname(__DIR__, 5) . '/resources/');
		$actual = Path::getResources();
		
		$this->assertEquals($expected, $actual);
	}
	
	public function testGetController()
    {
		$expected = FileUtils::standarizePath(dirname(__DIR__, 5) . '/resources/');
		$actual = Path::getResources();
		
		$this->assertEquals($expected, $actual);
	}
}